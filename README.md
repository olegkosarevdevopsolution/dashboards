Команда миграции: `php spark migrate Devolegkosarev\Dashboard\Database\Migrations\Menus\CreateMenuTableMigration` Эта команда выполнит миграцию `CreateMenuTableMigration` и создаст необходимую таблицу для модуля `Menus`.

Заполнение структуры меню: `php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Menus\MainMenuSeeder`

Заполнение переводов меню: `php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Menus\MenuItemTranslationsSeeder`

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Permissions\PermissionsSeeder`
`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Permissions\PermissionsTranslationsSeeder`

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Roles\RolesSeeder`
`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Roles\RolesTranslationsSeeder`

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\ReleaseNotesSeeder`

---------------------------------------------------------

Команда миграции: `php spark migrate Devolegkosarev\Dashboard\Database\Migrations\Menus\CreateMenuTableMigration`
Эта команда выполняет миграцию `CreateMenuTableMigration`, что означает создание необходимой таблицы для модуля `Menus`. Миграции позволяют вам управлять структурой базы данных вашего проекта.

Заполнение структуры меню: `php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Menus\MainMenuSeeder`
Эта команда заполняет структуру меню с помощью сидера `MainMenuSeeder`. Сидеры позволяют вам заполнять базу данных начальными данными или тестовыми данными.

Заполнение переводов меню: `php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Menus\MenuItemTranslationsSeeder`
Эта команда заполняет переводы меню с помощью сидера `MenuItemTranslationsSeeder`. Это может быть полезно, если ваше меню имеет многоязычную поддержку и вам нужно заполнить переводы для каждого пункта меню.

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Permissions\PermissionsSeeder`
Эта команда заполняет разрешения (permissions) с помощью сидера `PermissionsSeeder`. Разрешения позволяют управлять доступом пользователей к определенным функциям и ресурсам в вашем приложении.

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Permissions\PermissionsTranslationsSeeder`
Эта команда заполняет переводы разрешений с помощью сидера PermissionsTranslationsSeeder. Это полезно, если в вашем приложении есть многоязычная поддержка и вам нужно заполнить переводы для каждого разрешения.

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Roles\RolesSeeder`
Эта команда заполняет роли (roles) с помощью сидера `RolesSeeder`. Роли позволяют группировать разрешения и назначать их пользователям для управления их правами в приложении.

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\Roles\RolesTranslationsSeeder`
Эта команда заполняет переводы ролей с помощью сидера `RolesTranslationsSeeder`. Если ваше приложение поддерживает несколько языков, вы можете использовать этот сидер для заполнения переводов ролей.

`php spark db:seed Devolegkosarev\Dashboard\Database\Seeds\ReleaseNotesSeeder`
Эта команда заполняет заметки о релизах (release notes) с помощью сидера ReleaseNotesSeeder. Это может быть полезно, если вы хотите создать начальные заметки о релизах для вашего приложения или модуля.
