<?php

namespace Devolegkosarev\Dashboard\Models\Users;

use CodeIgniter\Model;

/**
 * UserModel
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Models\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class UsersModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'firstname',
        'lastname',
        'secondname',
        'email',
        'avatar',
        'password',
        'reset_token',
        'reset_expire',
        'activated',
        'blocked',
        'activate_token',
        'activate_expire',
        'role_id',
        'created_at',
        'updated_at',
    ];

    protected $useTimestamps = true;
    protected $dateFormat = 'datetime';

    protected $returnType = 'array';

    public function getUser($id)
    {
        return $this->find($id);
    }

    public function getUserByEmail($email)
    {
        return $this
            ->where('email', $email)
            ->get()
            ->getRow();
    }

    public function getUsers()
    {
        return $this->findAll();
    }

    public function createUser($data)
    {
        return $this->insert($data);
    }

    public function updateUser($id, $data)
    {
        return $this->update($id, $data);
    }

    public function deleteUser($id)
    {
        return $this->delete($id);
    }
}
