<?php

namespace Devolegkosarev\Dashboard\Models\Roles;

use CodeIgniter\Model;

class RolesTranslationsModel extends Model
{
    protected $table = 'roles_translations';
    protected $primaryKey = 'id';
    protected $allowedFields = ['role_id', 'language', 'name', 'created_at', 'updated_at'];
    // Указываем формат даты и времени
    protected $dateFormat = 'datetime';

    // Указываем поля, которые содержат дату и время создания и обновления
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    public function getTranslationsByRoleId(int $id = 0)
    {
        return $this->where('role_id', $id)->findAll();
    }

    function create(array $data)
    {
        return $this->db->table($this->table)->insert($data);
    }

    function deleteTranslationsByRoleId(int $id)
    {
        return $this->where('role_id', $id)->delete();
    }
    function updateByRoleId($id = null, $data = null): bool
    {
        return  $this->db->table($this->table)->where('role_id', $id)->where('language', $data['language'])->update($data);
    }
}
