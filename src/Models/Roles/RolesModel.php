<?php

namespace Devolegkosarev\Dashboard\Models\Roles;

use CodeIgniter\Model;

class RolesModel extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $allowedFields = ['type', 'status', 'created_at', 'updated_at'];

    // Указываем формат даты и времени
    protected $dateFormat = 'datetime';

    // Указываем поля, которые содержат дату и время создания и обновления
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    /**
     * The cache driver.
     * 
     * @var \CodeIgniter\Cache\Cache  
     */
    protected $cache;

    public function __construct()
    {
        parent::__construct();
        $this->cache = \CodeIgniter\Config\Services::cache();
    }

    function getAllRoles($language): array
    {
        $cacheKey = 'roles_' . $language;
        $builder = $this->db->table($this->table);
        $builder->select('roles.*, roles_translations.name');
        $builder->join('roles_translations', 'roles.id = roles_translations.role_id');
        $builder->where('roles_translations.language', $language);
        $query = $builder->get();
        $rolesItems = $query->getResult();

        return $rolesItems;
    }
    function getRoleById(int $id = 0): ?object
    {
        if ($id <= 0) {
            return new \stdClass();
        }
        $builder = $this->db->table($this->table);
        $builder->select('roles.*');
        $builder->where('roles.id', $id);
        $query = $builder->get();
        $rolesItem = $query->getRow();
        return $rolesItem;
    }

    function create(array $data): int
    {
        $insert = $this->db->table($this->table)->insert($data);

        return $this->db->insertID();
    }

    public function deleteRoleById(int $id): bool
    {
        return $this->where('id', $id)->delete();
    }

    public function getRolePermissions(int $roleId): array
    {
        // Implement the logic to retrieve the role's permissions based on the roleId
        // Return an array of permission names

        // For example, assuming you have a join table `role_permissions` that links roles and permissions
        $builder = $this->db->table('role_permissions');
        $builder->select('permissions.type as permission_name');
        $builder->join('permissions', 'role_permissions.permission_id = permissions.id');
        $builder->where('role_permissions.role_id', $roleId);
        // $builder->where('role_permissions.active', 1);
        $query = $builder->get();

        $permissions = [];
        foreach ($query->getResult() as $row) {
            $permissions[] = $row->permission_name;
        }

        return $permissions;
    }

    public function updateRolePermissions(int $id, array $data)
    {
    }

    public function deleteRolePermissionsById(int $id)
    {
    }

    public function update($id = null, $data = null): bool
    {
        return  $this->db->table($this->table)->where('id', $id)->update($data);
    }

    public function attachPermission(int $roleId, string $permission)
    {
        $this->db->table('role_permissions')->insert([
            'role_id' => $roleId,
            'permission_id' => $permission
        ]);
    }
}
