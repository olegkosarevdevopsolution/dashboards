<?php

namespace Devolegkosarev\Dashboard\Models\Menus;

use CodeIgniter\Model;

/**
 * Class MainmenuModel
 * 
 * Represents the model for the mainmenu table.
 */
class MainmenuModel extends Model
{
    /**
     * The name of the table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mainmenu';

    /**
     * The primary key for the table.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The return type of the model's methods.
     * 
     * @var string
     */
    protected $returnType = 'array';

    /**
     * The fields that are allowed to be mass-assigned.
     * 
     * @var array
     */
    protected $allowedFields = [
        'status',
        'order',
        'options',
        'parent_id',
        'url',
        'type',
        'created_at',
        'updated_at'
    ];

    /**
     * The format of the date and time fields.
     * 
     * @var string
     */
    protected $dateFormat = 'datetime';

    /**
     * The name of the field that contains the creation date and time.
     * 
     * @var string
     */
    protected $createdField = 'created_at';

    /**
     * The name of the field that contains the update date and time.
     * 
     * @var string
     */
    protected $updatedField = 'updated_at';

    /**
     * The cache driver.
     * 
     * @var \CodeIgniter\Cache\Cache  
     */
    protected $cache;

    public function __construct()
    {
        parent::__construct();
        $this->cache = \CodeIgniter\Config\Services::cache();
    }

    /**
     * Retrieves the menu items for a given language.
     * 
     * @param string $language The language for which to retrieve the menu items.
     * @return array The menu items.
     */
    public function getMenuItems($language): array
    {
        $builder = $this->builder();
        $builder->select('mainmenu.*, menu_item_translations.name');
        $builder->join('menu_item_translations', 'mainmenu.id = menu_item_translations.mainmenu_id');
        $builder->where('menu_item_translations.language', $language);
        $query = $builder->get();

        $menuItem = $query->getResult();

        return $menuItem;
    }

    public function getMenuByRole($roleId, $language = 'en'): array
    {
        return $this->join('role_menu_items', 'mainmenu.id = role_menu_items.mainmenu_id')
            ->select(
                "mainmenu.*,
                menu_item_translations.name As name"
            )
            ->join('roles', 'roles.id = role_menu_items.role_id')
            ->join('menu_item_translations', 'mainmenu.id = menu_item_translations.mainmenu_id')
            ->where('roles.id', $roleId)
            ->where('mainmenu.status', 1)
            ->where('menu_item_translations.language', $language)
            ->orderBy("mainmenu.parent_id", "ASC")
            ->orderBy("mainmenu.order", "ASC")
            ->get()
            ->getResult();
    }

    /**
     * Retrieves the menu items for a given language.
     * 
     * @param string $language The language for which to retrieve the menu items.
     * @return object The menu item.
     */
    public function getMenuItem(int $id): object
    {
        $builder = $this->builder();
        $builder->select('mainmenu.*');
        $builder->where('mainmenu.id', $id);
        $query = $builder->get();
        $menuItem = $query->getRow();
        return $menuItem;
    }

    public function deleteMenuItem(int $id): bool
    {
        $builder = $this->builder();
        $builder->where('id', $id);
        return $builder->delete();
    }
}
