<?php

namespace Devolegkosarev\Dashboard\Models\Menus;

use CodeIgniter\Model;

/**
 * MainmenuTranslationsModel
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Models\Menus;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class MainmenuTranslationsModel extends Model
{
    /**
     * The name of the table associated with the model.
     * 
     * @var string
     */
    protected $table = 'menu_item_translations';

    /**
     * The primary key for the table.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The return type of the model's methods.
     * 
     * @var string
     */
    protected $returnType = 'array';

    /**
     * The fields that are allowed to be mass-assigned.
     * 
     * @var array
     */
    protected $allowedFields = ['mainmenu_id', 'language', 'name', 'created_at', 'updated_at'];

    /**
     * The format of the date and time fields.
     * 
     * @var string
     */
    protected $dateFormat = 'datetime';

    /**
     * The name of the field that contains the creation date and time.
     * 
     * @var string
     */
    protected $createdField = 'created_at';

    /**
     * The name of the field that contains the update date and time.
     * 
     * @var string
     */
    protected $updatedField = 'updated_at';

    /**
     * The cache driver.
     * 
     * @var \CodeIgniter\Cache\Cache  
     */
    protected $cache;

    public function __construct()
    {
        parent::__construct();
        $this->cache = \CodeIgniter\Config\Services::cache();
    }

    public function getTranslationsByMenuItemId(int $id = 0): array
    {
        return $this->where('mainmenu_id', $id)->findAll();
    }
}
