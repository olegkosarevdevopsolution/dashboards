<?php

namespace Devolegkosarev\Dashboard\Models\Permissions;

use CodeIgniter\Model;

class PermissionsTranslationsModel extends Model
{
    protected $table = 'permission_translations';
    protected $primaryKey = 'id';
    protected $allowedFields = ['permission_id', 'language', 'name', 'created_at', 'updated_at'];

    // Указываем формат даты и времени
    protected $dateFormat = 'datetime';

    // Указываем поля, которые содержат дату и время создания и обновления
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
}
