<?php

namespace Devolegkosarev\Dashboard\Models\Permissions;

use CodeIgniter\Model;

class PermissionsModel extends Model
{
    protected $table = 'permissions';
    protected $primaryKey = 'id';
    protected $allowedFields = ['type', 'status', 'created_at', 'updated_at'];

    // Указываем формат даты и времени
    protected $dateFormat = 'datetime';

    // Указываем поля, которые содержат дату и время создания и обновления
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    /**
     * The cache driver.
     * 
     * @var \CodeIgniter\Cache\Cache  
     */
    protected $cache;

    public function __construct()
    {
        parent::__construct();
        $this->cache = \CodeIgniter\Config\Services::cache();
    }

    public function getAllPermissions($language): array
    {
        $cacheKey = 'premissions_' . $language;
        $premissionsItems = $this->cache->get($cacheKey);
        if ($premissionsItems === null) {
            $builder = $this->db->table($this->table);
            $builder->select('permissions.*, permission_translations.name');
            $builder->join('permission_translations', 'permissions.id = permission_translations.permission_id');
            $builder->where('permission_translations.language', $language);
            $query = $builder->get();
            $premissionsItems = $query->getResult();
            $this->cache->save($cacheKey, $premissionsItems, 3600);
        }
        return $premissionsItems;
    }
}
