<?php

namespace Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes;

use CodeIgniter\Model;

/**
 * ReleaseNotesModel
 * 
 * This class provides methods to interact with the "release_notes" table.
 * It allows retrieving, creating, updating, and deleting records related to release notes.
 *
 * @package \Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see CodeIgniter\Model
 */
class ReleaseNotesModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'release_notes';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $allowedFields = ['version', 'slug', 'release_date', 'publish_status', 'version_status'];

    /**
     * Get all release notes.
     *
     * @return array
     */
    public function getReleaseNotes(): array
    {
        return $this->get()->getResult();
    }

    /**
     * Get a release note by ID.
     *
     * @param int $id
     *
     * @return array|null
     */
    public function getReleaseNoteById(int $id): ?array
    {
        return $this->find($id);
    }

    /**
     * Create a release note.
     *
     * @param array $data
     *
     * @return bool
     */
    public function createReleaseNote(array $data): bool
    {
        return $this->insert($data);
    }

    /**
     * Update a release note.
     *
     * @param int   $id
     * @param array $data
     *
     * @return bool
     */
    public function updateReleaseNote(int $id, array $data): bool
    {
        return $this->update($id, $data);
    }

    /**
     * Delete a release note.
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteReleaseNote(int $id): bool
    {
        return $this->delete($id);
    }
}
