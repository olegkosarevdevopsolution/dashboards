<?php

namespace Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes;

use CodeIgniter\Model;

/**
 * ReleaseNotesModel
 * 
 * This class provides methods to interact with the "release_notes" table.
 * It allows retrieving, creating, updating, and deleting records related to release notes.
 *
 * @package \Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see CodeIgniter\Model
 */
class ReleaseNotesChangeModel extends Model
{
    protected $table = 'release_notes_change';
    protected $primaryKey = 'id';
    protected $allowedFields = ['release_notes_id', 'language', 'content'];


    /**
     * Method to get all records from the release_notes_change table by release.
     * If language is "*", return all records for the given release.
     *
     * @param int $releaseId
     * @param string $language
     * @return array|object|null
     */
    public function getByRelease(int $releaseId, string $language = "en")
    {
        if ($language == '*') {
            return $this->where('release_notes_id', $releaseId)->findAll();
        }
        return $this
            ->where('release_notes_id', $releaseId)
            ->where('language', $language)
            ->first();
    }


    /**
     * Get all release notes changes.
     *
     * @return array
     */
    public function getReleaseNotesChanges()
    {
        return $this->findAll();
    }

    /**
     * Get a release notes change by ID.
     *
     * @param int $id
     * @return object|null
     */
    public function getReleaseNotesChangeById($id)
    {
        return $this->find($id);
    }

    /**
     * Create a new release notes change.
     *
     * @param array $data
     * @return bool|int
     */
    public function createReleaseNotesChange($data)
    {
        return $this->insert($data);
    }

    /**
     * Update a release notes change by ID.
     *
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function updateReleaseNotesChange($id, $data)
    {
        return $this->update($id, $data);
    }

    /**
     * Delete a release notes change by ID.
     *
     * @param int $id
     * @return bool
     */
    public function deleteReleaseNotesChange($id)
    {
        return $this->delete($id);
    }
}
