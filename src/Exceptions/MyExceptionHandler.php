<?php

namespace Devolegkosarev\Dashboard\Exceptions;

use CodeIgniter\Debug\BaseExceptionHandler;
use CodeIgniter\Debug\ExceptionHandlerInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Throwable;

/**
 * MyExceptionHandler
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Exceptions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class MyExceptionHandler extends BaseExceptionHandler implements ExceptionHandlerInterface
{
    // You can override the view path.
    protected ?string $viewPath = FCPATH . 'vendor/devolegkosarev/dashboard/src/Views/Errors/html/';

    public function handle(
        Throwable $exception,
        RequestInterface $request,
        ResponseInterface $response,
        int $statusCode,
        int $exitCode
    ): void {
        $statusCode = $exception->getCode();
        $this->render(
            $exception,
            $statusCode,
            $this->viewPath . "Error{$statusCode}.php"
        );

        exit($exitCode);
    }
}
