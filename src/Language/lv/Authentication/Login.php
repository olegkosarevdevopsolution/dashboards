<?php

// app/Language/lv/Authentication/Login.php

return [
    'signIn' => 'Ielogoties',
    'verifyIdentity' => 'Jūsu aizsardzībai, lūdzu, apstipriniet savu identitāti.',
    'emailAddress' => 'Epasta adrese',
    'password' => 'Parole',
    'forgotPassword' => 'Aizmirsi paroli?',
    'rememberMe' => 'Atceries mani',
    'signInButton' => 'Ielogoties',
    'noAccount' => "Vai jums vēl nav konta?",
    'signUp' => 'Reģistrējies'
];