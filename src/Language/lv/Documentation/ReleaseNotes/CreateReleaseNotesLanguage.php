<?php
$language = [
    // Page Titles
    'title' => 'Izlaiduma piezīmju izveide',

    // Headings
    'page_title' => 'Izlaiduma piezīmju izveide',
    'page_description' => 'Izlaiduma piezīmju izveides lapas apraksts atrodas šeit...',

    // Breadcrumb
    'breadcrumb' => 'Izlaiduma piezīmju izveide',

    // Form Labels
    'version' => 'Versija',
    'slug' => 'Slug',
    'release_date' => 'Laidojuma datums',
    'publish_status' => 'Publicēšanas statuss',
    'version_status' => 'Versijas statuss',
    'change_log' => 'Izmaiņu žurnāls {languageName}',
    'change_log_contect' => 'Izmaiņu žurnāls',
    'language' => 'Valoda',
    'btn_save' => 'Saglabāt',
    'btn_cancel' => 'Atcelt',

    // Version Status Options
    'pre_alpha' => 'Pirmalfa',
    'alpha' => 'Alfa',
    'beta' => 'Beta',
    'release_candidate' => 'Laidojuma kandidāts',
    'release' => 'Laidojums'
];

return $language;