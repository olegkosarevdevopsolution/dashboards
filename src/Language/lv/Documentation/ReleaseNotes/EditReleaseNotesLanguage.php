<?php
$language = [
    // Page Titles
    'title' => 'Laidojuma piezīmju rediģēšana',

    // Headings
    'page_title' => 'Laidojuma piezīmju rediģēšana',
    'page_description' => 'Šeit varat pievienot īsu laidojuma piezīmju rediģēšanas lapas aprakstu...',

    // Breadcrumb
    'breadcrumb' => 'Rediģēt laidojuma piezīmes',

    // Form Labels
    'version' => 'Versija',
    'slug' => 'Slug',
    'release_date' => 'Laidojuma datums',
    'publish_status' => 'Publicēšanas statuss',
    'version_status' => 'Versijas statuss',
    'change_log' => 'Izmaiņu žurnāls {languageName}',
    'change_log_contect' => 'Izmaiņu žurnāls',
    'language' => 'Valoda',
    'btn_save' => 'Saglabāt',
    'btn_cancel' => 'Atcelt',

    // Version Status Options
    'pre_alpha' => 'Pirmalfa',
    'alpha' => 'Alfa',
    'beta' => 'Beta',
    'release_candidate' => 'Laidojuma kandidāts',
    'release' => 'Laidojums'
];

return $language;