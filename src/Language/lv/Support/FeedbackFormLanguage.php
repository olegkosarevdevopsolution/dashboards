<?php
// Valodas fails atsauksmju veidlapai

return [
    // Page Titles
    'title' => 'Atsauksmes',

    // Headings
    'page_title' => 'Atsauksmes',
    'page_description' => 'lapas galvenes apraksts ir šeit...',

    // Breadcrumb
    'breadcrumb' => 'Atsauksmes',

    'name' => 'Vārds',
    'name_placeholder' => 'Jūsu vārds',
    'email' => 'E-pasts',
    'email_placeholder' => 'Jūsu e-pasts',
    'subject' => 'Tēma',
    'select_subject' => 'Atlasīt tēmu',
    'subject_options' => [
        'Question' => 'Jautājums',
        'Proposal' => 'Priekšlikums',
        'Issue' => 'Problēma',
    ],
    'message' => 'Ziņojums',
    'message_placeholder' => 'Jūsu ziņojums',
    'submit' => 'Iesniegt',
];