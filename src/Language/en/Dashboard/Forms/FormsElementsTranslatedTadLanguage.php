<?php

/**
 * FormsElementsTranslatedTad
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Forms;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'translationTab' => [
        'label' => 'Translation in {language}',
        'fields' => [
            'dataTranslationLanguageCode' => [
                'label' => 'Language',
            ],
            'dataTranslationName' => [
                'label' => 'Translated Name',
                'help' => 'This field is translated in {language}',
                'placeholder' => 'Translated Name',
                'feedback' => [
                    'invalid'  => 'Please enter a valid translated in {language}',
                    'valid' => 'Translated Valid name'
                ]
            ],
            'dataTranslationCreated' => [
                'label' => 'Translated Created',
            ],
            'dataTranslationUpdated' => [
                'label' => 'Translated Updated',
            ]
        ]
    ],
];
