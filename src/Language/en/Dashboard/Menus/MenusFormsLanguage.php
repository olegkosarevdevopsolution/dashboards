<?php

/**
 * RolesFormsLanguage
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Menus;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
// Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.parent.label
// menuInfoTab.fields.parrent.root
return [
    "menuInfoTab" => [
        'label' => 'Menu Information',
        'fields' => [
            'menuType' => [
                'label' => 'Menu Type',
                'help' => 'Menu Type help',
                'options' => [
                    'link' => 'Link',
                    'header' => 'Header',
                    'page' => 'Page',
                    'custom' => 'Custom',
                    'external' => 'External',
                ],
                'texts' => [
                    'trigger' => 'Select menu type',
                    'noResult' => 'Menu type not found',
                    'search' => 'Search menu type',
                ]
            ],
            'menuStatus' => [
                'label' => 'Status',
                'help' => 'Status help',
            ],
            'menuCreated' => [
                'label' => 'Menu Created',
            ],
            'menuUpdated' => [
                'label' => 'Menu Updated',
            ],
            'role' => [
                'label' => 'Role',
                'help' => 'Role help',
                'texts' => [
                    'trigger' => 'Select role',
                    'noResult' => 'Role not found',
                    'search' => 'Search roles',
                ],
                'feedback' => [
                    'invalid'  => 'Please select role',
                    'valid' => 'Valid role',
                ]
            ],
            'menuParrent' => [
                'label' => 'Parrent Menu',
                'help' => 'Parrent Menu help',
                'options' => [
                    'root' => 'Root',
                ],
                'texts' => [
                    'trigger' => 'Select parrent menu',
                    'noResult' => 'Parrent menu not found',
                    'search' => 'Search parrent menu',
                ]
            ],
            'menuLink' => [
                'label' => 'Menu Link',
                'help' => 'Menu Link help',
                'placeholder' => 'support/feedback',
                'feedback' => [
                    'invalid'  => 'Please enter a valid menu link',
                    'valid' => 'Valid menu link'
                ],
            ],
        ]
    ],
    'menuTranslationTab' => [
        'label' => 'Translation in {language}',
        'fields' => [
            'menuTranslationLanguageCode' => [
                'label' => 'Language',
            ],
            'menuTranslationName' => [
                'label' => 'Menu Translated Name',
                'help' => 'This field is menu translated in {language}',
                'placeholder' => 'Add menus',
                'feedback' => [
                    'invalid'  => 'Please enter a valid menu translated in {language}',
                    'valid' => 'Valid name'
                ]
            ],
            'menuTranslationCreated' => [
                'label' => 'Menu Translated Created',
            ],
            'menuTranslationUpdated' => [
                'label' => 'Menu Translated Updated',
            ]
        ]
    ],

];
