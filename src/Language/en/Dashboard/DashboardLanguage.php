<?php

return [
    'breadcrumbs' => [
        'label' => 'Dashboard',
    ],
    'buttons' => [
        'help' => [
            'tooltip-title' => 'Help dashboard',
        ],
    ],
    'meta' => [
        'title' => 'Dashboard',
        'description' => 'Dashboard description goes here...',
        'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN'],
    ],
    'pageHeader' => [
        'title' => 'Dashboard',
        'description' => 'Page header description goes here...',
    ],
];
