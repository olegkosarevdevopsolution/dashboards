<?php

/**
 * UsersLanguage
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Language\en\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'breadcrumbs' => [
        'label' => 'Users',
    ],
    'buttons' => [
        'help' => [
            'tooltip-title' => 'Help Users List',
        ],
        'create' => [
            'tooltip-title' => 'Help Create User',
        ],
        'edit' => [
            'value' => 'Edit',
        ],
        'delete' => [
            'value' => 'Delete',
        ],
        'block' => [
            'value' => 'Block',
        ],
        'unblock' => [
            'value' => 'Unblock',
        ],
        'activate' => [
            'value' => 'Activate',
        ],
        'deactivate' => [
            'value' => 'Deactivate',
        ],
    ],
    'meta' => [
        'title' => 'Users List',
        'description' => 'Users List description goes here...',
        'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN'],
    ],
    'pageHeader' => [
        'title' => 'Users List',
        'description' => 'Page header description goes here...',
    ],
    'roleCreatedSuccessfully' => 'Role created successfully. Redirecting...',
];
