<?php

/**
 * CreateRoleLanguage
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'breadcrumbs' => [
        'label' => 'Create Role',
    ],
    'buttons' => [
        'help' => [
            'tooltip-title' => 'Help create role',
        ],
        'submit' => 'Create Role',
        'cancel' => 'Cancel',
    ],
    'meta' => [
        'title' => 'Create Role',
        'description' => 'Create Role description goes here...',
        'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN'],
    ],
    'pageHeader' => [
        'title' => 'Create Role',
        'description' => 'Page header description goes here...',
    ],
    'roleCreatedSuccessfully' => 'Role created successfully. Redirecting...',
];
