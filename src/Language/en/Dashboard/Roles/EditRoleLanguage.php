<?php

/**
 * EditRoleLanguage
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'breadcrumbs' => [
        'label' => 'Edit Role',
    ],
    'buttons' => [
        'help' => [
            'tooltip-title' => 'Help edit role',
        ],
        'submit' => 'Update Role',
        'cancel' => 'Cancel',
    ],
    'meta' => [
        'title' => 'Edit Role',
        'description' => 'Edit Role description goes here...',
        'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN'],
    ],
    'pageHeader' => [
        'title' => 'Edit Role',
        'description' => 'Page header description goes here...',
    ],
    'roleUpdatedSuccessfully' => 'Role Updated successfully. Redirecting...',
];
