<?php

/**
 * DeleteRoleLanguage
 *
 * This file contains the language strings for the DeleteRole
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'deleteRole' => 'Delete Role',
    'confirmDeleteRole' => 'Are you sure you want to delete this role?',
    'roleDeletedSuccessfully' => 'Role deleted successfully',
];
