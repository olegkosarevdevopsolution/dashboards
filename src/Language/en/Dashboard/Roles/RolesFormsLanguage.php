<?php

/**
 * RolesFormsLanguage
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    "roleInfoTab" => [
        'label' => 'Role Information',
        'fields' => [
            'roleType' => [
                'label' => 'Role Type',
                'placeholder' => 'Role Type',
                'help' => 'Role Type help'
            ],
            'status' => [
                'label' => 'Status',
                'help' => 'Status help',
            ],
            'created' => [
                'label' => 'Role Created',
            ],
            'updated' => [
                'label' => 'Role Updated',
            ]
        ]
    ],
    'roleTranslationTab' => [
        'label' => 'Translation in {language}',
        'fields' => [
            'roleTranslationLanguageCode' => [
                'label' => 'Language',
            ],
            'roleTranslationName' => [
                'label' => 'Role Translated Name',
                'help' => 'This field is role translated in {language}',
                'placeholder' => 'Administrator',
                'feedback' => [
                    'invalid'  => 'Please enter a valid role translated in {language}',
                    'valid' => 'Valid name'
                ]
            ],
            'roleTranslationCreated' => [
                'label' => 'Role Translated Created',
            ],
            'roleTranslationUpdated' => [
                'label' => 'Role Translated Updated',
            ]
        ]
    ],

];
