<?php

/**
 * Error
 *
 * This file contains the language strings for the Error
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'created' => [
        'roleNotCreated' => 'Role not created. Please try again.',
        'roleTranslationNotCreated' => 'Role create but translation {locale} not created. Please try again.',
    ],
    'edit' => [
        'roleNotUpdated' => 'Role not updated. Please try again.',
        'roleTranslationNotUpdated' => 'Role translation {locale} not updated. Please try again.',
    ],
    'deleted' => [
        'roleNotDeleted' => 'Role not deleted. Please try again.',
        'roleTranslationNotDeleted' => 'Role translation {locale} not deleted. Please try again.',
    ]
];
