<?php

/**
 * Error
 *
 * This file contains the language strings for the Error
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'created' => [
        'permissionsNotCreated' => 'Permission not created. Please try again.',
        'permissionsTranslationNotCreated' => 'Permissions create but translation {locale} not created. Please try again.',
    ],
    'edit' => [
        'permissionsNotUpdated' => 'Permission not updated. Please try again.',
        'permissionsTranslationNotUpdated' => 'Permission translation {locale} not updated. Please try again.',
    ],
    'deleted' => [
        'permissionsNotDeleted' => 'Permission not deleted. Please try again.',
        'permissionsTranslationNotDeleted' => 'Permission translation {locale} not deleted. Please try again.',
    ]
];
