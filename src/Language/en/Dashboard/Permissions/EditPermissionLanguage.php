<?php

/**
 * EditRoleLanguage
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'breadcrumbs' => [
        'label' => 'Edit Permission',
    ],
    'buttons' => [
        'help' => [
            'tooltip-title' => 'Help edit permission',
        ],
        'submit' => 'Update Permission',
        'cancel' => 'Cancel',
    ],
    'meta' => [
        'title' => 'Edit Permission',
        'description' => 'Edit Permission description goes here...',
        'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN'],
    ],
    'pageHeader' => [
        'title' => 'Edit Permission',
        'description' => 'Page header description goes here...',
    ],
    'roleUpdatedSuccessfully' => 'Permission Updated successfully. Redirecting...',
];
