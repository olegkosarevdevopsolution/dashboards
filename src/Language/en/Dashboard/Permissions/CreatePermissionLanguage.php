<?php

/**
 * CreateRoleLanguage
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'breadcrumbs' => [
        'label' => 'Create Permission',
    ],
    'buttons' => [
        'help' => [
            'tooltip-title' => 'Help create permission',
        ],
        'submit' => 'Create Permission',
        'cancel' => 'Cancel',
    ],
    'meta' => [
        'title' => 'Create Permission',
        'description' => 'Create Permission description goes here...',
        'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN'],
    ],
    'pageHeader' => [
        'title' => 'Create Permission',
        'description' => 'Page header description goes here...',
    ],
    'roleCreatedSuccessfully' => 'Permission created successfully. Redirecting...',
];
