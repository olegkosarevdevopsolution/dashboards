<?php

/**
 * RolesFormsLanguage
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Dashboard\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    "permissionInfoTab" => [
        'label' => 'Permission Information',
        'fields' => [
            'permissionType' => [
                'label' => 'Permission Type',
                'placeholder' => 'Permission Type',
                'help' => 'Permission Type help'
            ],
            'status' => [
                'label' => 'Status',
                'help' => 'Status help',
            ],
            'created' => [
                'label' => 'Permission Created',
            ],
            'updated' => [
                'label' => 'Permission Updated',
            ],
            'role' => [
                'label' => 'Role',
                'trigger' => "Select role",
                'noResult' => "Role not found",
                'search' => "Search role",
                'help' => 'Role help',
                'feedback' => [
                    'invalid'  => 'Please select role',
                    'valid' => 'Valid role',
                ]
            ]
        ]
    ],
    'permissionTranslationTab' => [
        'label' => 'Translation in {language}',
        'fields' => [
            'permissionTranslationLanguageCode' => [
                'label' => 'Language',
            ],
            'permissionTranslationName' => [
                'label' => 'Permission Translated Name',
                'help' => 'This field is permission translated in {language}',
                'placeholder' => 'Add permissions',
                'feedback' => [
                    'invalid'  => 'Please enter a valid permission translated in {language}',
                    'valid' => 'Valid name'
                ]
            ],
            'created' => [
                'label' => 'Permission Translated Created',
            ],
            'updated' => [
                'label' => 'Permission Translated Updated',
            ]
        ]
    ],

];
