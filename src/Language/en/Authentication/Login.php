<?php

// app/Language/en/Authentication/Login.php

return [
    'signIn' => 'Sign In',
    'verifyIdentity' => 'For your protection, please verify your identity.',
    'emailAddress' => 'Email Address',
    'password' => 'Password',
    'forgotPassword' => 'Forgot password?',
    'rememberMe' => 'Remember me',
    'signInButton' => 'Sign In',
    'noAccount' => "Don't have an account yet?",
    'signUp' => 'Sign up'
];