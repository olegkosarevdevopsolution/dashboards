<?php

/**
 * Logout
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'logout' => 'Logout',
    'logout_success' => 'Logout successful. 🎉',
    'logout_error' => 'Logout error. 😕',
    'logout_error_message' => 'Logout error. Please try again. 😕',
    'logout_error_message2' => 'An error occurred while deleting cookies. The exit failed.',
];
