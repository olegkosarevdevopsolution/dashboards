<?php


$language = [
    // Page Titles
    'title' => 'Release Notes Creation',

    // Headings
    'page_title' => 'Release Notes Creation',
    'page_description' => 'Release Notes Creation page description goes here...',

    // Breadcrumb
    'breadcrumb' => 'Release Notes Creation',

    // Form Labels
    'version' => 'Version',
    'slug' => 'Slug',
    'release_date' => 'Release Date',
    'publish_status' => 'Publish Status',
    'version_status' => 'Version Status',
    'change_log' => 'Change Log {languageName}',
    'change_log_contect' => 'Change Log',
    'language' => 'Language',
    'btn_save' => 'Save',
    'btn_cancel' => 'Cancel',

    // Version Status Options
    'pre_alpha' => 'Pre-alpha',
    'alpha' => 'Alpha',
    'beta' => 'Beta',
    'release_candidate' => 'Release candidate',
    'release' => 'Release'
];

return $language;
?>