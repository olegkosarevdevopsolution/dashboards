<?php

/**
 * Common
 *
 * Common Alert messages
 *
 * @package \Devolegkosarev\Dashboard\Language\en\Alert;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

return [
    'title' => [
        'primary' => 'Alert',
        'secondary' => 'Alert',
        'success' => 'Success',
        'danger' => 'Error',
        'warning' => 'Warning',
        'info' => 'Info',
        'dark' => 'Alert',
        'light' => 'Alert',
    ]
];
