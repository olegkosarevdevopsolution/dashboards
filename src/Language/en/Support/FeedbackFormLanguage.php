<?php
// Языковой файл для формы обратной связи

return [

    // Page Titles
    'title' => 'Feedback',

    // Headings
    'page_title' => 'Feedback',
    'page_description' => 'page header description goes here...',

    // Breadcrumb
    'breadcrumb' => 'Feedback',

    'name' => 'Name',
    'name_placeholder' => 'Your name',
    'email' => 'Email',
    'email_placeholder' => 'Your email',
    'subject' => 'Subject',
    'select_subject' => 'Select a subject',
    'subject_options' => [
        'question' => 'Question',
        'proposal' => 'Proposal',
        'issue' => 'Issue',
    ],
    'message' => 'Message',
    'message_placeholder' => 'Your message',
    'submit' => 'Submit',
];