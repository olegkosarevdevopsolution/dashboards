<?php
$language = [
    // Page Titles
    'title' => 'Редактирование примечаний к выпуску',

    // Headings
    'page_title' => 'Редактирование примечаний к выпуску',
    'page_description' => 'Здесь можно добавить краткое описание страницы редактирования примечаний к выпуску...',

    // Breadcrumb
    'breadcrumb' => 'Редактировать примечания к выпуску',

    // Form Labels
    'version' => 'Версия',
    'slug' => 'Слаг',
    'release_date' => 'Дата выпуска',
    'publish_status' => 'Статус публикации',
    'version_status' => 'Статус версии',
    'change_log' => 'Журнал изменений {languageName}',
    'change_log_contect' => 'Журнал изменений',
    'language' => 'Язык',
    'btn_save' => 'Сохранить',
    'btn_cancel' => 'Отменить',

    // Version Status Options
    'pre_alpha' => 'Предальфа',
    'alpha' => 'Альфа',
    'beta' => 'Бета',
    'release_candidate' => 'Кандидат на релиз',
    'release' => 'Релиз'
];

return $language;