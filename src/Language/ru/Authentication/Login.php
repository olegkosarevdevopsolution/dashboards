<?php

// app/Language/ru/Authentication/Login.php

return [
    'signIn' => 'Войти',
    'verifyIdentity' => 'Для вашей безопасности, пожалуйста, подтвердите свою личность.',
    'emailAddress' => 'Адрес электронной почты',
    'password' => 'Пароль',
    'forgotPassword' => 'Забыли пароль?',
    'rememberMe' => 'Запомнить меня',
    'signInButton' => 'Войти',
    'noAccount' => "У вас еще нет учетной записи?",
    'signUp' => 'Зарегистрироваться'
];