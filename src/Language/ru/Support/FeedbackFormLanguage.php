<?php
// Языковой файл для формы обратной связи

return [

    // Page Titles
    'title' => 'Обратная связь',

    // Headings
    'page_title' => 'Обратная связь',
    'page_description' => 'описание заголовка страницы находится здесь...',

    // Breadcrumb
    'breadcrumb' => 'Обратная связь',

    'name' => 'Имя',
    'name_placeholder' => 'Ваше имя',
    'email' => 'Email',
    'email_placeholder' => 'Ваш email',
    'subject' => 'Тема',
    'select_subject' => 'Выберите тему',
    'subject_options' => [
        'Question' => 'Вопрос',
        'Proposal' => 'Предложение',
        'Issue' => 'Проблема',
    ],
    'message' => 'Сообщение',
    'message_placeholder' => 'Ваше сообщение',
    'submit' => 'Отправить',
];