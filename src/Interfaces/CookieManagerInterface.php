<?php

namespace Devolegkosarev\Dashboard\Interfaces;

/**
 * CookieManagerInterface
 *
 * Interface CookieManagerInterface
 *
 * namespace \Devolegkosarev\Dashboard\Interfaces;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
interface CookieManagerInterface
{
    /**
     * Set the configuration for the cookie.
     *
     * @param array $cookieConfig The configuration for the cookie. Defaults to an empty array.
     * @return void
     */
    public function setCookieConfig(array $cookieConfig = []): void;

    /**
     * Set a cookie.
     *
     * @param string $name The name of the cookie.
     * @param string $value The value of the cookie.
     * @return void
     */
    public function setCookie(string $name, string $value): void;

    /**
     * Delete all cookies.
     *
     * @return void
     */
    public function deleteAllCookies(): void;

    /**
     * Set the user ID cookie.
     *
     * @param string $userId The user ID.
     * @return void
     */
    public function setUserId(string $userId): void;

    /**
     * Set the user name cookie.
     *
     * @param string $userName The user name.
     * @return void
     */
    public function setUserName(string $userName): void;

    /**
     * Set the user last name cookie.
     *
     * @param string $userLastName The user last name.
     * @return void
     */
    public function setUserLastName(string $userLastName): void;

    /**
     * Set the user second name cookie.
     *
     * @param string $userSecondName The user second name.
     * @return void
     */
    public function setUserSecondName(string $userSecondName): void;

    /**
     * Set the user email cookie.
     *
     * @param string $userEmail The user email.
     * @return void
     */
    public function setUserEmail(string $userEmail): void;

    /**
     * Set the user photo cookie.
     *
     * @param string $userPhoto The user photo.
     * @return void
     */
    public function setUserAvatar(string $userPhoto): void;

    /**
     * Set the user roles ID cookie.
     * @param string $userDepartment The user department.
     * @return void
     */
    public function setUserRolesId(string $userDepartment): void;

    /**
     * Set the language.
     * @param string $language The language default is en
     * @return void
     */
    public function setLanguage(string $language): void;

    /**
     * Get the user ID from the cookie.
     *
     * @return int The user ID or 0 if not set.
     */
    public function getUserId(): int;

    /**
     * Get the user name from the cookie.
     *
     * @return string|null The user name or null if not set.
     */
    public function getUserName(): ?string;

    /**
     * Get the user last name from the cookie.
     *
     * @return string|null The user last name or null if not set.
     */
    public function getUserLastName(): ?string;

    /**
     * Get the user second name from the cookie.
     *
     * @return string|null The user second name or null if not set.
     */
    public function getUserSecondName(): ?string;

    /**
     * Get the user email from the cookie.
     *
     * @return string|null The user email or null if not set.
     */
    public function getUserEmail(): ?string;

    /**
     * Get the user photo from the cookie.
     *
     * @return string|null The user photo or null if not set.
     */
    public function getUserAvatar(): ?string;

    /**
     * Get the user department from the cookie.
     * @return int The user department or 1 if not set.
     */
    public function getUserRolesId(): int;

    /**
     * Get the language from the cookie.
     * @return string|null The language or en if not set.
     */
    public function getLanguage(): ?string;
}
