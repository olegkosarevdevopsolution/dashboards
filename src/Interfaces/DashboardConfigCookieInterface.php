<?php

namespace Devolegkosarev\Dashboard\Interfaces;

/**
 * DashboardConfigCookieInterface
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Interfaces;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
interface DashboardConfigCookieInterface
{
    public function getDashboardConfigCookie();
}
