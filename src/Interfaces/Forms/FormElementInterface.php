<?php

namespace Devolegkosarev\Dashboard\Interfaces\Forms;

/**
 * FormElementInterface
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Interfaces\Forms;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

interface FormElementInterface
{
    public function generate(): string;
}
