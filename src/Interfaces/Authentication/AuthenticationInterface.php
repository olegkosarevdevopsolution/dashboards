<?php

namespace Devolegkosarev\Dashboard\Interfaces\Authentication;

/**
 * AuthenticationInterface
 *
 * Interface for authentication.
 *
 * namespace \Devolegkosarev\Dashboard\Interfaces\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
interface AuthenticationInterface
{
    /**
     * Logs in a user.
     *
     * @param string $username The username.
     * @param string $password The password.
     *
     * @return array The user information.
     */
    public function login($username, $password): array;

    /**
     * Logs out the current user.
     *
     * @return bool
     */
    public function logout(): bool;

    /**
     * Gets the current user.
     *
     * @return array The user information.
     */
    public function getUser(int $id): ?array;

    /**
     * Checks if a user is logged in.
     *
     * @return bool True if the user is logged in, false otherwise.
     */
    public function checkAuthorization(): bool;
}
