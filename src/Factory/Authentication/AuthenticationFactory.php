<?php

namespace Devolegkosarev\Dashboard\Factory\Authentication;

use Config\Services;
use InvalidArgumentException;
use Devolegkosarev\Dashboard\Interfaces\Authentication\AuthenticationInterface;

/**
 * AuthenticationFactory
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Factory\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class AuthenticationFactory
{
    /**
     * @var AuthenticationInterface
     */
    private $authenticator;

    /**
     * AuthenticationFactory constructor.
     *
     * @param string $authenticatorName The name of the authenticator to create
     */
    public function __construct(string $authenticatorName)
    {
        $this->authenticator = $this->createAuthenticator($authenticatorName);
    }

    /**
     * Creates an instance of the authenticator based on the given name.
     *
     * @param string $authenticatorName The name of the authenticator to create
     * @return AuthenticationInterface The created authenticator
     * @throws InvalidArgumentException If the authenticator class does not exist
     */
    private static function createAuthenticator(string $authenticatorName): AuthenticationInterface
    {
        $usersService = Services::usersService();

        $class = 'App\Libraries\Authentication\\' . $authenticatorName . 'AuthenticationLibraries';
        if (class_exists($class)) {
            return new $class($usersService);
        }

        $class = 'Devolegkosarev\Dashboard\Libraries\Authentication\\' . $authenticatorName . 'AuthenticationLibraries';
        if (!class_exists($class)) {
            throw new InvalidArgumentException("Invalid authenticator type: {$authenticatorName}", 500);
        }

        return new $class($usersService);
    }

    /**
     * Returns the authenticator instance.
     *
     * @return AuthenticationInterface The authenticator instance
     */
    public function getAuthenticator(): AuthenticationInterface
    {
        return $this->authenticator;
    }
}
