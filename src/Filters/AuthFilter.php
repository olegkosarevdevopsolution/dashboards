<?php

namespace Devolegkosarev\Dashboard\Filters;

use CodeIgniter\Config\Services;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\URI;

/**
 * AuthFilter
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Filters;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class AuthFilter implements FilterInterface
{
    const LOGIN_ROUTE = 'authentication/login';
    const DASHBOARD_ROUTE = 'dashboard';

public function before(RequestInterface $request, $arguments = null)
{
    // Get the authenticator service
    $authenticator = Services::getAuthenticator();

    // Get the encrypted cookie manager service
    $cookieManager = Services::encryptedCookieManagerService();

    // Get the URI instance from the request
    $uri = service('uri');
    $currentRoute = $uri->getPath();

    // Check if user is authorized
    if (!$authenticator->checkAuthorization()) {
        return redirect()->to(self::LOGIN_ROUTE)->withCookies();
    }

    // Check access based on roles and permissions
    $rolesId = $cookieManager->getUserRolesId();
    $controller = isset($arguments[0]) ? $arguments[0] : null;
    $method = isset($arguments[1]) ? $arguments[1] : null;
    if ($controller !== null && $method !== null) {
        $permission = $controller . ":" . $method;
        if (!$authenticator->checkAccess($rolesId, $permission)) {
            throw new \Exception('Access denied', 403);
        }
    }

    // Redirect logic based on current route
    if (strpos($currentRoute, self::LOGIN_ROUTE) === 0) {
        return redirect()->to(self::DASHBOARD_ROUTE)->withCookies();
    }

    if (strpos($currentRoute, self::DASHBOARD_ROUTE) === 0 && !$authenticator->checkAuthorization()) {
        return redirect()->to(self::LOGIN_ROUTE)->withCookies();
    }
}


    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Логика, выполняемая после выполнения действия
        return $response;
    }
}
