<?php

namespace Devolegkosarev\Dashboard\Controllers\Support;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * FeedbackFormController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Support;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class FeedbackFormController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => lang('Support\FeedbackFormLanguage.title'),
            'breadcrumb' => [
                [
                    'label' => 'Support',
                    'link' => '#'
                ],
                [
                    'label' => lang('Support\FeedbackFormLanguage.breadcrumb'),
                    'active' => true
                ]
            ],
            'page' => [
                'title' => lang('Support\FeedbackFormLanguage.page_title'),
                'description' => lang('Support\FeedbackFormLanguage.page_description')
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ]
        ];
        return view(viewOverview('Pages\\Support\\FeedbackForm'), array_merge($data, $this->data));
    }
    public function sendFeedback()
    {
    }
}
