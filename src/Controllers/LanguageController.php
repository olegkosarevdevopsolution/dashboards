<?php

namespace Devolegkosarev\Dashboard\Controllers;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * LanguageController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class LanguageController extends BaseController
{
    /**
     * Display the languages index page.
     *
     * @param string|null $locale The selected locale (optional)
     * @return \CodeIgniter\HTTP\RedirectResponse The redirect response
     */
    public function index(?string $locale = null)
    {
        $CookieManagerServices = Services::encryptedCookieManagerService();

        // Set the cookie configuration
        $CookieManagerServices->setCookieConfig(
            [
                'expires' => strtotime('+1 year'),
                'prefix' => '',
            ]
        );

        if ($locale === null) {
            $locale = config('App')->defaultLocale;
        }

        // Set the selected language in the session
        $CookieManagerServices->setLanguage($locale);

        // Redirect back to the previous page
        return redirect()->back()->withCookies();
    }
}
