<?php

namespace Devolegkosarev\Dashboard\Controllers\Documentation\UserGuide;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * UserGuideController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Documentation\UserGuide;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class UserGuideController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'User Guide',
            'breadcrumb' => [
                [
                    'label' => 'Support',
                    'link' => '#'
                ],
                [
                    'label' => 'User Guide',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'User Guide',
                'description' => 'User Guide page description goes here...'
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ]
        ];
        return view(viewOverview('Pages\\Documentation\\UserGuide\\UserGuide'), array_merge($data, $this->data));
    }
}
