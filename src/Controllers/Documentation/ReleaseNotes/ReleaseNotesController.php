<?php

namespace Devolegkosarev\Dashboard\Controllers\Documentation\ReleaseNotes;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * ReleaseNotesController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Documentation\ReleaseNotes;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class ReleaseNotesController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Release Notes',
            'breadcrumb' => [
                [
                    'label' => 'Support',
                    'link' => '#'
                ],
                [
                    'label' => 'RELEASE NOTES',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Release Notes',
                'description' => 'Release Notes page description goes here...'
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => $this->getReleaseNotes(),
        ];
        return view(viewOverview('Pages\\Documentation\\ReleaseNotes\\ReleaseNotes'), array_merge($data, $this->data));
    }

    public function getReleaseNotes()
    {
        $releaseNote = service('releaseNoteService');
        $releaseNoteChanges = service('releaseNoteChangeService');
        $cookieManager = service("encryptedCookieManagerService");

        $releases = $releaseNote->getAll();
        // Создаем пустой массив для хранения релизов и изменений
        $result = [];
        // Проходим по каждому релизу в цикле
        foreach ($releases as $release) {
            $releaseId = $release["id"];
            // Получаем все изменения для текущего релиза из таблицы release_notes_change в виде массива
            $changes = $releaseNoteChanges->getByRelease($release["id"], $cookieManager->getLanguage());
            // Добавляем текущий релиз и его изменения в массив result
            $result[] = [
                'release' => $release,
                'changes' => $changes["content"]
            ];
        }
        return $result;
    }
}
