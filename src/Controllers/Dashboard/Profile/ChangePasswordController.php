<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Profile;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * ChangePasswordController
 *
 * Change password Profile controller 
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Profile;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class ChangePasswordController extends BaseController
{
}
