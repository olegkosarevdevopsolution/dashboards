<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Profile;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

class EditProfileController extends BaseController
{
    public function edit()
    {
        $usersService = Services::usersService();
        $encryptedCookieManagerService = Services::encryptedCookieManagerService();
        $user = $usersService->getUser($encryptedCookieManagerService->getUserId());
        if (!$user) {
            session()->setFlashdata('error', 'User not found', 'title', 'Fatal Error');
            return redirect()->to('dashboard');
        }
        $data = [
            'title' => 'Edit Profile',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Edit Profile',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Edit Profile',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/profile#edit',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'user' => $user,
        ];
        return view(viewOverview('Pages\\Dashboard\\Profile\\EditProfile'), array_merge($data, $this->data));
    }
}
