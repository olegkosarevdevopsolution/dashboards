<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Menus;

use Devolegkosarev\Dashboard\Controllers\BaseController;
use Config\Services;

/**
 * CreateMenuItemsController
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Menus;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CreateMenusItemsController extends BaseController
{

    /**
     * MenusRendererService
     * 
     * @var \Devolegkosarev\Dashboard\Services\Menus\MenusRendererService
     */
    private $menuRendererService;

    public function __construct()
    {
        $this->menuRendererService = Services::menuRendererService();
    }
    public function new()
    {
        $languages = $this->data["languages"];
        $currentLanguage = $this->data["currentLanguage"];
        $data = [
            'breadcrumbs' => [
                [
                    'label' => lang('Dashboard\DashboardLanguage.breadcrumbs.label'),
                    'link' => '/dashboard',
                ],
                [
                    'label' => lang('Dashboard\Menus\MenusLanguage.breadcrumbs.label'),
                    'link' => '/dashboard/menus',
                ],
                [
                    'label' => lang('Dashboard\Menus\CreateMenuLanguage.breadcrumbs.label'),
                    'active' => true,
                ],
            ],
            'pageHeader' => [
                'title' => lang('Dashboard\Menus\CreateMenuLanguage.pageHeader.title'),
                'description' => lang('Dashboard\Menus\CreateMenuLanguage.pageHeader.description'),
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'route' => 'documtations/menus#create',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="' . lang('Dashboard\Menus\CreateMenuLanguage.buttons.help.tooltip-title') . '"',
                ],
            ],
            'meta' => [
                'title' => lang('Dashboard\Menus\CreateMenuLanguage.meta.title'),
                'description' => lang('Dashboard\Menus\CreateMenuLanguage.meta.description'),
                'keywords' => lang('Dashboard\Menus\CreateMenuLanguage.meta.keywords'),
            ],
            'translatedTab' => $this->menuRendererService->getFormElementsTranslatedTab($languages),
            'content' => $this->menuRendererService->getFormElements($currentLanguage),
        ];
        return view(viewOverview('Pages\\Dashboard\\Menus\\CreateMenuItems'), array_merge($data, $this->data));
    }
}
