<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Menus;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

class EditMenusItemsController extends BaseController
{
    /**
     * MenusRendererService
     * 
     * @var \Devolegkosarev\Dashboard\Services\Menus\MenusRendererService
     */
    private $menuRendererService;

    /**
     * MenusRendererService
     * 
     * @var \Devolegkosarev\Dashboard\Services\Menus\mainmenuService
     */
    private $mainmenuService;

    public function __construct()
    {
        $this->menuRendererService = Services::menuRendererService();
        $this->mainmenuService = Services::mainmenuService();
    }

    public function edit(int $id = 0)
    {

        $getMenuItem = $this->mainmenuService->getMenuItem($id);
        if (!$getMenuItem) {
            throw new \Exception(lang('Dashboard\Menus\EditMenuLanguage.errors.notFound'));
        }
        $languages = $this->data["languages"];
        $currentLanguage = $this->data["currentLanguage"];

        $data = [
            'title' => 'Edit Menu Item',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Menu Items',
                    'link' => '/dashboard/menus'
                ],
                [
                    'label' => 'Edit Menu Item',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Edit Menu Item',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/menus#edit',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'translatedTab' => $this->menuRendererService->getFormElementsTranslatedTab($languages, $getMenuItem->translations),
            'content' => $this->menuRendererService->getFormElements($currentLanguage, $getMenuItem),
        ];
        return view(viewOverview('Pages\\Dashboard\\Menus\\EditMenuItems'), array_merge($data, $this->data));
    }

    public function update(int $id = 0)
    {
    }
}
