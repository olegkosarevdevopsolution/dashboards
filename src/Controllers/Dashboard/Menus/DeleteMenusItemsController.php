<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Menus;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * DeleteMenuItemsController
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Menus;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class DeleteMenusItemsController extends BaseController
{
}
