<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Menus;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

class MenusItemsController extends BaseController
{
    public function index()
    {
        $menuRendererService = Services::menuRendererService();
        $data = [
            'title' => 'Menu Items',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Menu Items',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Menu Items',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Create',
                    'class' => 'btn-theme',
                    'icon' => 'fa-plus-circle',
                    'route' => 'permissions/create',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="Create new menu item"',
                ],
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/permissions',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
            'rows' => $menuRendererService->prepareMenuItems($this->data["currentLanguage"])
        ];
        return view(viewOverview('Pages\\Dashboard\\Menus\\MenuItems'), array_merge($this->data, $data));
    }
}
