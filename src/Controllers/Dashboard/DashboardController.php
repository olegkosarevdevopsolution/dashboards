<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard;

use Devolegkosarev\Dashboard\Controllers\BaseController;

class DashboardController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => lang('dashboard.title'),
            'breadcrumb' => [
                [
                    'label' => lang('dashboard.breadcrumb'),
                    'active' => true
                ]
            ],
            'page' => [
                'title' => lang('dashboard.page_title'),
                'description' => lang('dashboard.page_description')
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ]
        ];
        return view(viewOverview('Pages\\Dashboard\\Dashboard'), array_merge($data, $this->data));
    }
}
