<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Settings;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * EditSettingsController
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Settings;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class EditSettingsController extends BaseController
{
    public function edit()
    {
        $data = [
            'title' => 'Edit Settings',
            // 'breadcrumb' => [
            //     [
            //         'label' => 'Dashboard',
            //         'link' => '#'
            //     ],
            //     [
            //         'label' => 'Edit Settings',
            //         'active' => true
            //     ]
            // ],
            // 'page' => [
            //     'title' => 'Edit Settings',
            //     'description' => 'page header description goes here...'
            // ],
            // 'buttons' => [
            //     [
            //         'label' => 'Help',
            //         'class' => 'btn-theme',
            //         'icon' => 'fa-question-circle',
            //         'route' => 'documtations/settings#edit',
            //         'target' => '_blank',
            //         'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
            //     ]
            // ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
        ];
        return view(viewOverview('Pages\\Dashboard\\Settings\\EditSettings'), array_merge($data, $this->data));
    }
    public function update()
    {
    }
}
