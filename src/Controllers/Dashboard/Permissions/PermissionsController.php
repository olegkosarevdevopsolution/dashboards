<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Permissions;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * PermissionsController
 *
 * Controller for Permissions 
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class PermissionsController extends BaseController
{
    public function index()
    {
        $permissionsService = Services::permissionsService();
        $permissionsRenderer = Services::premissionsRendererService();
        $data = [
            'title' => 'Permissions',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Permissions',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Permissions',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Create',
                    'class' => 'btn-theme',
                    'icon' => 'fa-plus-circle',
                    'route' => 'permissions/create',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="Create new permission"',
                ],
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/permissions',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' =>  $permissionsRenderer->preRenderPremissions($permissionsService->getAllPermissions($this->data["currentLanguage"]))
        ];
        return view(viewOverview('Pages\\Dashboard\\Permissions\\Permissions'), array_merge($data, $this->data));
    }
}
