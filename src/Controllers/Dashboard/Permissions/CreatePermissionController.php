<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Permissions;

use Devolegkosarev\Dashboard\Controllers\BaseController;
use Config\Services;

/**
 * CreatePermissionsController
 *
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Permissions
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CreatePermissionController extends BaseController
{
    private $permissionService;
    private $formService;
    private $permissionsTranslationsService;
    private $roleService;
    public function __construct()
    {
        $this->roleService = Services::rolesService();
        $this->permissionService = Services::permissionsService();
        $this->formService = Services::formService();
        $this->permissionsTranslationsService = Services::permissionsTranslationsService();
    }

    public function new()
    {
        $languages = $this->data["languages"];
        $data = [
            'breadcrumbs' => [
                [
                    'label' => lang('Dashboard\DashboardLanguage.breadcrumbs.label'),
                    'link' => '/dashboard',
                ],
                [
                    'label' => lang('Dashboard\Permissions\PermissionsLanguage.breadcrumbs.label'),
                    'link' => '/dashboard/permissions',
                ],
                [
                    'label' => lang('Dashboard\Permissions\CreatePermissionLanguage.breadcrumbs.label'),
                    'active' => true,
                ],
            ],
            'pageHeader' => [
                'title' => lang('Dashboard\Permissions\CreatePermissionLanguage.pageHeader.title'),
                'description' => lang('Dashboard\Permissions\CreatePermissionLanguage.pageHeader.description'),
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'route' => 'documtations/permissions#create',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="' . lang('Dashboard\Permissions\CreatePermissionLanguage.buttons.help.tooltip-title') . '"',
                ],
            ],
            'meta' => [
                'title' => lang('Dashboard\Permissions\CreatePermissionLanguage.meta.title'),
                'description' => lang('Dashboard\Permissions\CreatePermissionLanguage.meta.description'),
                'keywords' => lang('Dashboard\Permissions\CreatePermissionLanguage.meta.keywords'),
            ],
            'translatedTab' => $this->formService->translatedTab($this->getFormElementsTranslatedTab($languages), $languages),
            'content' => $this->getFormElements(),
        ];
        return view(viewOverview('Pages\\Dashboard\\Permissions\\CreatePermission'), array_merge($data, $this->data));
    }
    public function store()
    {
        $request = request();
        $getPost = $request->getPost();
        $permissionId = $this->permissionService->createPermission([
            'type' => $getPost["permissionType"],
            'status' => (int) $getPost["status"]
        ]);
        foreach ($getPost as $key => $value) {
            if (strstr($key, 'dataTranslatedInput',) == true) {
                $lang = strtolower(str_replace('dataTranslatedInput', '', $key));
                $dataTranslatedUpdatedInput = [
                    'language' => $lang,
                    'name' => $getPost[$key],
                    'permission_id' => $permissionId
                ];
                var_dump($dataTranslatedUpdatedInput);
                var_dump($this->permissionsTranslationsService->insert($dataTranslatedUpdatedInput));
            }
        }
        foreach ($getPost['role'] as $roleKey => $roleId) {
            $this->roleService->attachPermission($roleId, $permissionId);
        }

        session()->setFlashdata('success', lang('Dashboard\Permissions\CreatePermissionLanguage.flash.success'));
        return redirect()->to('/dashboard/permissions');
    }

    /**
     * Retrieves an array of form elements translated for each language.
     *
     * @param array $languages An array of languages.
     * @return array An array of form elements.
     */
    private function getFormElementsTranslatedTab(array $languages = []): array
    {
        foreach ($languages as $language) {
            $languageCode = $language["code"];
            $languageName = $language["name"];
            $formElements[$languageCode] = [
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationLanguageCode.label')
                    ],
                    'attributes' => [
                        'readonly' => true,
                        'value' => $languageCode
                    ],
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationName.label')
                    ],
                    'attributes' => [
                        'placeholder' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationName.placeholder', [], $languageCode),
                        'id' => 'dataTranslatedInput',
                        'name' => 'dataTranslatedInput'
                    ],
                    'help' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationName.help', ['language' => $languageName]),
                    'invalid_tooltip' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationName.invalid_tooltip', ['language' => $languageName]),
                    'valid_tooltip' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationName.valid_tooltip'),
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationCreated.label')
                    ],
                    'attributes' => [
                        'id' => 'dataTranslatedCreatedInput',
                        'name' => 'dataTranslatedCreatedInput',
                        'class' => 'form-control',
                        'value' => date("Y-m-d H:i:s"),
                        'disabled' => true
                    ],
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionTranslationTab.fields.permissionTranslationUpdated.label')
                    ],
                    'attributes' => [
                        'id' => 'dataTranslatedUpdatedInput',
                        'name' => 'dataTranslatedUpdatedInput',
                        'class' => 'form-control',
                        'value' => date("Y-m-d H:i:s"),
                        'disabled' => true
                    ],
                ]
            ];
        }

        return $formElements;
    }

    /**
     * Retrieves the form elements.
     *
     * @return string The HTML representation of the form elements
     */
    private function getFormElements(): string
    {
        $formElements = [
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.permissionType.label')
                ],
                'attributes' => [
                    'id' => 'permissionTypeInput',
                    'name' => 'permissionType',
                    'placeholder' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.permissionType.placeholder'),
                ],
                'help' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.permissionType.help')
            ],
            [
                'type' => 'select',
                'label' => [
                    'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.role.label'),
                ],
                'attributes' => [
                    'id' => 'roleSelect',
                    'name' => 'role[]',
                    'multiple' => true,
                    'required' => true
                ],
                'help' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.role.help'),
                'options' => $this->getAllRolesOptions($this->data["currentLanguage"])
            ],

            [
                'type' => 'switch',
                'class' => 'form-switch',
                'label' => [
                    'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.status.label')
                ],
                'attributes' => [
                    'id' => 'status',
                    'name' => 'status',
                    'class' => '',
                    'checked' => true,
                    'value' => 1
                ],
                'help' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.status.help')
            ],
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.created.label')
                ],
                'attributes' => [
                    'id' => 'permissionCreatedInput',
                    'name' => 'permissionCreatedInput',
                    'value' => date("Y-m-d H:i:s"),
                ]
            ],
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Permissions\PermissionsFormsLanguage.permissionInfoTab.fields.updated.label')
                ],
                'attributes' => [
                    'id' => 'permissionUpdatedInput',
                    'name' => 'permissionUpdatedInput',
                    'value' => date("Y-m-d H:i:s"),
                    'disabled' => true
                ]
            ],
        ];
        $html = '';
        foreach ($formElements as $formElement) {
            $html .= $this->formService->getFormElementHtml($formElement);
        }
        return $html;
    }

    public function getAllRolesOptions(string $currentLanguage): array
    {
        $rolesOptions = [];

        $getAllRoles = $this->roleService->getAllRoles($currentLanguage);
        foreach ($getAllRoles as $role) {
            $rolesOptions[$role->id]["label"] = $role->name;
            $rolesOptions[$role->id]["value"] = $role->id;
            $rolesOptions[$role->id]["selected"] = false;
        }

        return $rolesOptions;
    }
}
