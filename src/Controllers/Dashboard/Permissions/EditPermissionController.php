<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Permissions;

use Devolegkosarev\Dashboard\Controllers\BaseController;

class EditPermissionController extends BaseController
{
    public function edit(int $id = 0)
    {
        $data = [
            'title' => 'Edit Permission',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Permissions',
                    'link' => '/dashboard/permissions'
                ],
                [
                    'label' => 'Edit Permission',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Edit Permission',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/permissions#Edit',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
        ];
        return view(viewOverview('Pages\\Dashboard\\Permissions\\EditPermission'), array_merge($data, $this->data));
    }

    public function update(int $id = 0)
    {
    }

    public function new()
    {
    }
    public function add()
    {
    }
}
