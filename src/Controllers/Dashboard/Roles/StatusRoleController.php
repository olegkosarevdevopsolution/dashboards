<?php

namespace DevOlegKosarev\Dashboard\Controllers\Dashboard\Roles;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * StatusRoleController
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Controllers\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class StatusRoleController extends BaseController
{
    /**
     * RoleService instance
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesService
     */
    private $roleService;

    public function __construct()
    {
        $this->roleService = Services::rolesService();
    }
    public function publish(int $idRole)
    {
        $session = session();
        $role = $this->roleService->getRoleById($idRole);
        if ($role == null) {
            $session->setFlashdata('error', 'Role not found');
            return redirect()->to('/dashboard/roles');
        }
        $this->roleService->update($idRole, ['status' => 1]);
        $session->setFlashdata('success', 'Role published ' . $role->type);
        return redirect()->to('/dashboard/roles');
    }

    public function unpublish(int $idRole)
    {
        $session = session();
        $role = $this->roleService->getRoleById($idRole);
        if ($role == null) {
            $session->setFlashdata('error', 'Role not found');
            return redirect()->to('/dashboard/roles');
        }
        $this->roleService->update($idRole, ['status' => 0]);
        $session->setFlashdata('success', 'Role published ' . $role->type);
        return redirect()->to('/dashboard/roles');
    }
}
