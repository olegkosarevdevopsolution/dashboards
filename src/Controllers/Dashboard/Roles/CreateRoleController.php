<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Roles;

use Config\Services;
use CodeIgniter\HTTP\ResponseInterface;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * CreateRoleController
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CreateRoleController extends BaseController
{
    /**
     * RoleService instance
     * 
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesService
     */
    private $roleService;

    /**
     * RoleTranslationService instance
     * 
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesTranslationsService
     */
    private $rolesTranslationsService;


    /**
     * FormService instance
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesTranslationsService
     */
    private $rolesRenderService;

    public function __construct()
    {
        $this->roleService = Services::rolesService();
        $this->rolesRenderService = Services::rolesRendererService();
        $this->rolesTranslationsService = Services::rolesTranslationsService();
    }
    public function new()
    {
        $languages = $this->data["languages"];
        $data = [
            'breadcrumbs' => [
                [
                    'label' => lang('Dashboard\DashboardLanguage.breadcrumbs.label'),
                    'link' => '/dashboard',
                ],
                [
                    'label' => lang('Dashboard\Roles\RolesLanguage.breadcrumbs.label'),
                    'link' => '/dashboard/roles',
                ],
                [
                    'label' => lang('Dashboard\Roles\CreateRoleLanguage.breadcrumbs.label'),
                    'active' => true,
                ],
            ],
            'pageHeader' => [
                'title' => lang('Dashboard\Roles\CreateRoleLanguage.pageHeader.title'),
                'description' => lang('Dashboard\Roles\CreateRoleLanguage.pageHeader.description'),
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'route' => 'documtations/roles#create',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="' . lang('Dashboard\Roles\CreateRoleLanguage.buttons.help.tooltip-title') . '"',
                ],
            ],
            'meta' => [
                'title' => lang('Dashboard\Roles\CreateRoleLanguage.meta.title'),
                'description' => lang('Dashboard\Roles\CreateRoleLanguage.meta.description'),
                'keywords' => lang('Dashboard\Roles\CreateRoleLanguage.meta.keywords'),
            ],
            'translatedTab' => $this->rolesRenderService->getFormElementsTranslatedTab($this->data["languages"]),
            'content' => $this->rolesRenderService->getFormElements(),


        ];
        return view(viewOverview('Pages\\Dashboard\\Roles\\CreateRole'), array_merge($data, $this->data));
    }

    /**
     * Store the data from the request.
     *
     * @return string JSON-encoded response
     */
    public function store(): ResponseInterface
    {
        $request = Services::request();
        $response = Services::response();
        $configApp = config("App");

        if (!$request->isAJAX()) {
            return $response->setJSON([
                'status' => 'error',
                'message' => 'Invalid request'
            ])->setStatusCode(400);
        }

        $roleData = $request->getPost("role");
        $translated = $request->getPost('translated');

        $roleData["type"] = $roleData["type"] ?: $translated[$configApp->defaultLocale]["name"];
        $roleData["status"] = (int) $request->getPost('status');
        $roleId = $this->roleService->create($roleData);

        if ($roleId <= 0) {
            return $response->setJSON([
                'status' => 'error',
                'message' => lang('Dashboard\Roles\RolesErrorLanguage.roleNotCreated')
            ])->setStatusCode(500);
        }

        foreach ($translated as $locale => $translatedItem) {
            $translatedItem["role_id"] = $roleId;
            $translatedItemId = $this->rolesTranslationsService->create($translatedItem);

            if ($translatedItemId <= 0) {
                return $response->setJSON([
                    'status' => 'error',
                    'message' => lang('Dashboard\Roles\RolesErrorLanguage.roleTranslationNotCreated', ['locale' => $locale])
                ])->setStatusCode(500);
            }
        }

        return $response->setJSON([
            'status' => 'success',
            'message' => lang('Dashboard\Roles\CreateRoleLanguage.roleCreatedSuccessfully')
        ])->setStatusCode(201);
    }
}
