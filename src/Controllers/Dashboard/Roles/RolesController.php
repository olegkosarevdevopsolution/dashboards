<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Roles;

use Devolegkosarev\Dashboard\Controllers\BaseController;
use Config\Services;

/**
 * RolesController
 *
 * This is the RolesController class 
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class RolesController extends BaseController
{
    public function index()
    {
        $rolesServices = Services::rolesService();
        $rolesRenderer = Services::rolesRendererService();
        $data = [
            'title' => 'Roles',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Roles',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Roles',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Create',
                    'class' => 'btn-theme',
                    'icon' => 'fa-plus-circle',
                    'route' => 'release-notes/create',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="Create new role"',
                ],
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/release-notes',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => $rolesRenderer->preRenderRoles($rolesServices->getAllRoles($this->data['currentLanguage'])),
        ];
        return view(viewOverview('Pages\\Dashboard\\Roles\\Roles'), array_merge($data, $this->data));
    }
}
