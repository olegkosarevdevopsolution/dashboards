<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Roles;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

class EditRoleController extends BaseController
{
    /**
     * RoleService instance
     * 
     * @var \Devolegkosarev\Dashboard\Services\RolesService
     */
    private $roleService;

    /**
     * RoleTranslationService instance
     * 
     * @var \Devolegkosarev\Dashboard\Services\RolesTranslationsService
     */
    private $rolesTranslationsService;


    /**
     * FormService instance
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesRenderService
     */
    private $rolesRenderService;

    public function __construct()
    {
        $this->roleService = Services::rolesService();
        $this->rolesRenderService = Services::rolesRendererService();
        $this->rolesTranslationsService = Services::rolesTranslationsService();
    }
    public function edit(int $id = 0)
    {

        $role = $this->roleService->getRoleById($id);
        if ($role === null) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException("Role not found");
        }
        $data = [
            'title' => 'Edit Role',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Roles',
                    'link' => '/dashboard/roles'
                ],
                [
                    'label' => 'Edit Role',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Edit Role',
                'description' => 'Edit Role page description goes here...',
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/roles#edit',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Edit Role',
                'description' => 'Edit Role meta description goes here...',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'translatedTab' => $this->rolesRenderService->getFormElementsTranslatedTab($this->data["languages"], $role->translations),
            'content' => $this->rolesRenderService->getFormElements($role),
            'role' => $role
        ];
        return view(viewOverview('Pages\\Dashboard\\Roles\\EditRole'), array_merge($data, $this->data));
    }

    public function update(int $id = 0)
    {
        $request = request();
        $getPost = $request->getPost();

        $data = [
            'type' => $getPost['roleType']
        ];
        $this->roleService->update($id, $data);

        foreach ($getPost as $key => $value) {
            if (strstr($key, 'dataTranslatedInput',) == true) {
                $lang = strtolower(str_replace('dataTranslatedInput', '', $key));
                $dataTranslatedUpdatedInput = [
                    'language' => $lang,
                    'name' => $getPost[$key],
                ];
                var_dump($dataTranslatedUpdatedInput);
                var_dump($this->rolesTranslationsService->updateByRoleId($id, $dataTranslatedUpdatedInput));
            }
        }
        session()->setFlashdata('success', lang('Dashboard\Roles\EditRoleLanguage.messages.success'));
        return redirect()->to('/dashboard/roles/');
    }
}
