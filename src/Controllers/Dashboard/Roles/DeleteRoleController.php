<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Roles;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * DeleteRoleController
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class DeleteRoleController extends BaseController
{
    /**
     * RoleService instance
     * @var Devolegkosarev\Dashboard\Services\Roles\RolesService
     */
    private $roleService;

    /**
     * FormService instance
     * @var Devolegkosarev\Dashboard\Services\Roles\RolesTranslationsService
     */
    private $rolesTranslationsService;

    public function __construct()
    {
        $this->roleService = Services::rolesService();
        $this->rolesTranslationsService = Services::rolesTranslationsService();
    }

    public function delete(int $id = 0)
    {
        $session = session();

        $role = $this->roleService->getRoleById($id);
        $indexedData = array_column($role->translations, null, 'language');
        $translate = $indexedData[$this->data["currentLanguage"]] ?? [];

        $roleName = (isset($translate["name"])) ? $translate["name"] : '';
        if ($role === null) {
            $session->setFlashdata("error", 'Role not found', 'title', 'Error');
            return redirect()->to('/dashboard/roles');
        }

        $deleteRoleTranslations = $this->rolesTranslationsService->delete($id);
        if ($deleteRoleTranslations === false) {
            $session->setFlashdata("error", 'Role translations not deleted', 'title', 'Error');
            return redirect()->to('/dashboard/roles');
        }

        $deleteRole = $this->roleService->deleteRole($id);
        if ($deleteRole === false) {
            $session->setFlashdata("error", 'Role not deleted');
            return redirect()->to('/dashboard/roles')->with('error', 'Role ' . $roleName . ' not deleted', 'title', 'Error');
        }

        $session->setFlashdata("success", 'Role "' . $roleName . '" deleted successfully', 'title', 'Success');
        return redirect()->to('/dashboard/roles');
    }
}
