<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Notifications;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * CreateNotificationController
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Notifications;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CreateNotificationController extends BaseController
{
    public function new(int $adminId = 0, int $userId = 0)
    {
        $data = [
            'title' => 'Create Notification',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Notifications',
                    'link' => '/dashboard/notifications'
                ],
                [
                    'label' => 'Create Notification',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Create Notification',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/notifications#create',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
        ];
        return view(viewOverview('Pages\\Dashboard\\Notifications\\CreateNotification'), array_merge($data, $this->data));
    }
}
