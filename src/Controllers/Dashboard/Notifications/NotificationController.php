<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Notifications;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * NotificationController
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Notifications;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class NotificationController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Notifications',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Notifications',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Notifications',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Create',
                    'class' => 'btn-theme',
                    'icon' => 'fa-plus-circle',
                    'route' => 'release-notes/create',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="Create new notification"',
                ],
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/release-notes',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
        ];
        return view(viewOverview('Pages\\Dashboard\\Notifications\\Notifications'), array_merge($data, $this->data));
    }
}
