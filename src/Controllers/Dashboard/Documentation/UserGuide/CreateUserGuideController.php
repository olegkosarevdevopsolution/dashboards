<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Documentation\UserGuide;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * CreateUserGuideController
 *
 * This class was generated by the App Starter package.
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Documentation\UserGuide;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CreateUserGuideController extends BaseController
{
}
