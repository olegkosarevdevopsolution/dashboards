<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Documentation\UserGuide;

use Devolegkosarev\Dashboard\Controllers\BaseController;

class UserGuideController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'User Guide',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'User Guide',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'User Guide',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Create',
                    'class' => 'btn-theme',
                    'icon' => 'fa-plus-circle',
                    'route' => 'release-notes/create',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="Create new UserGuide"',
                ],
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/release-notes',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
        ];
        return view(viewOverview('Pages\\Dashboard\\Documentation\\UserGuide\\UserGuide'), array_merge($data, $this->data));
    }
}
