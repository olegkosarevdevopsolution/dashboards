<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Documentation\ReleaseNotes;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * ReleaseNotesController
 *
 * This file contains the ReleaseNotesController class, which is responsible for managing release notes.
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesRendererService
 */
class ReleaseNotesController extends BaseController
{
    /**
     * @var ReleaseNotesService $releaseNotesService The release note service instance.
     */
    private $releaseNotesService;

    private $releaseNotesRendererService;

    /**
     * Constructor.
     *
     * Initializes the release note service instance.
     */
    public function __construct()
    {
        // Instantiate the release note service.
        $this->releaseNotesService = Services::releaseNotesService();
        $this->releaseNotesRendererService = Services::releaseNotesRendererService();
    }

    /**
     * Index method.
     *
     * Retrieves all release notes and renders the index view.
     *
     * @return view The index view with the release notes data.
     */
    public function index(): string
    {
        // Get the release notes.
        $releaseNotes = $this->releaseNotesService->getAll();

        $context = $this->releaseNotesRendererService->preRenderReleaseNotes($releaseNotes);

        $data = [
            'title' => 'Release Notes',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Release Notes',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Release Notes',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Create',
                    'class' => 'btn-theme',
                    'icon' => 'fa-plus-circle',
                    'route' => 'release-notes/create',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="Create new release note"',
                ],
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/release-notes',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => $context
        ];




        return view(viewOverview('Views\\Pages\\Dashboard\\Documentation\\ReleaseNotes\\ReleaseNotes'), array_merge($data, $this->data));
    }
}
