<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Documentation\ReleaseNotes;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * EditReleaseNotesController
 *
 * This file contains the EditReleaseNotesController class, which is responsible for editing release notes.
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService
 */
class EditReleaseNoteController extends BaseController
{
    /**
     * @var ReleaseNotesService $releaseNotesService The release note service instance.
     */
    private $releaseNotesService;

    /**
     * @var ReleaseNotesChangeService $releaseNotesChangeService The release note service instance.
     */
    private $releaseNotesChangeService;

    /**
     * Constructor.
     *
     * Initializes the release note service instances.
     * 
     */
    public function __construct()
    {
        // Instantiate the release note service.
        $this->releaseNotesService = Services::releaseNotesService();
        $this->releaseNotesChangeService = Services::releaseNotesChangeService();
    }

    /**
     * Edit method.
     *
     * Retrieves a specific release note by ID and its changes, and renders the edit view.
     *
     * @param int $id The ID of the release note to edit.
     * @throws \InvalidArgumentException If the provided release note ID is invalid.
     * @throws \CodeIgniter\Exceptions\PageNotFoundException If the release note is not found.
     * @return view The edit view with the release note and its changes data.
     * 
     * @see \Devolegkosarev\Dashboard\Views\dashboard\documentation\release-notes\edit
     */
    public function edit(int $id = 0): string
    {
        if ($id <= 0) {
            // Your code to handle invalid release note ID here
            // For example, you can throw an exception or perform other actions
            throw new \InvalidArgumentException("Invalid release note ID");
        }

        // Get the release note.
        $releaseNote = $this->releaseNotesService->getById($id);
        if (!$releaseNote) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound("Release note not found");
        }

        $releaseNoteChanges = $this->releaseNotesChangeService->getByRelease($id, "*");

        return view(viewOverview('Views\\Pages\\Dashboard\\Documentation\\ReleaseNotes\\EditReleaseNotes'), ['release' => $releaseNote, 'changes' => $releaseNoteChanges]);
    }

    /**
     * Updates a release note with the given ID using the provided data.
     *
     * @param int $id The ID of the release note to update.
     * @param array $data An array containing the updated release note and release note changes.
     * @throws \InvalidArgumentException If the ID is invalid or if the required fields are missing in the data array.
     * @return void
     */
    public function update(int $id, array $data): void
    {
        if ($id <= 0) {
            throw new \InvalidArgumentException("Invalid release note ID");
        }

        if (!isset($data["releaseNote"]) || !isset($data["releaseNoteChanges"])) {
            throw new \InvalidArgumentException("Missing required fields in data array");
        }

        $this->updateReleaseNote($id, $data["releaseNote"]);
        $this->updateReleaseNoteChanges($id, $data["releaseNoteChanges"]);
    }

    /**
     * Updates a release note.
     *
     * @param int $id The ID of the release note.
     * @param array $releaseNoteData An array containing the updated release note data.
     * @throws \Exception If the release note fails to update.
     * @return void
     */
    private function updateReleaseNote(int $id, array $releaseNoteData): void
    {
        $updatedReleaseNote = $this->releaseNotesChangeService->update($id, $releaseNoteData);

        if (!$updatedReleaseNote) {
            throw new \Exception("Failed to update release note");
        }
    }

    /**
     * Updates the release note changes with the given ID and data.
     *
     * @param int $id The ID of the release note changes.
     * @param array $releaseNoteChangesData The data to update the release note changes with.
     * @throws \Exception If the update fails.
     * @return bool
     */
    private function updateReleaseNoteChanges(int $id, array $releaseNoteChangesData): bool
    {
        $updatedReleaseNoteChanges = $this->releaseNotesChangeService->update($id, $releaseNoteChangesData);

        if (!$updatedReleaseNoteChanges) {
            throw new \Exception("Failed to update release note changes");
        }
        return $updatedReleaseNoteChanges;
    }

    /**
     * Toggle publish status method.
     *
     * Toggles the publish status of a specific release note by ID.
     *
     * @param int $id The ID of the release note to toggle.
     * @throws \InvalidArgumentException If the provided release note ID is invalid.
     * @throws \CodeIgniter\Exceptions\PageNotFoundException If the release note is not found.
     */
    public function togglePublishStatus(int $id, bool $status): bool
    {
        return $this->releaseNotesService->update($id, ["status" => $status]);
    }
}
