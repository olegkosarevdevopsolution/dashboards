<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Documentation\ReleaseNotes;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * CreateReleaseNoteController
 * 
 * This file contains the CreateReleaseNoteController class, which is responsible for creating release notes.
 * 
 * @package \Devolegkosarev\Dashboard\Controllers\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService
 */

class CreateReleaseNoteController extends BaseController
{
    /**
     * @var ReleaseNotesService $releaseNotesService The release note service instance.
     */
    private $releaseNotesService;

    /**
     * @var ReleaseNotesChangeService $releaseNotesChangeService The release note service instance.
     */
    private $releaseNotesChangeService;
    private $input;

    public function __construct()
    {
        $this->releaseNotesService = Services::releaseNotesService();
        $this->releaseNotesChangeService = Services::releaseNotesChangeService();
        $this->input = \Config\Services::input();
    }

    /**
     * Show the create release note view.
     *
     * @return \CodeIgniter\HTTP\ResponseInterface
     */
    public function add(): string
    {
        $data = [
            'title' => 'Create Release Notes',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Release Notes',
                    'link' => 'release-notes'
                ],
                [
                    'label' => 'Create Release Notes',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Create Release Notes',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/release-notes#create',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
            'languages' => $this->data["languages"]
        ];

        return view(viewOverview('Views\\Pages\\Dashboard\\Documentation\\ReleaseNotes\\CreateReleaseNotes'), array_merge($this->data, $data));
    }

    /**
     * Store the release note data.
     *
     * @return void
     */
    public function store(): void
    {
        if ($this->input->getMethod() === 'post') {
            $data = [
                'releaseNote' => $this->input->post('releaseNote'),
                'releaseNoteChanges' => $this->input->post('releaseNoteChanges')
            ];
            $this->createReleaseNote($data["releaseNote"]);
            $this->createReleaseNoteChanges($data["releaseNoteChanges"]);
        }
    }

    /**
     * Create a new release note.
     *
     * @param mixed $data
     * @return mixed
     */
    private function createReleaseNote($data): mixed
    {
        return $this->releaseNotesService->create($data);
    }

    /**
     * Create new release note changes.
     *
     * @param mixed $data
     * @return mixed
     */
    private function createReleaseNoteChanges($data): mixed
    {
        return $this->releaseNotesChangeService->create($data);
    }
}
