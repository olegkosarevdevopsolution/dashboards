<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Documentation\ReleaseNotes;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * DeleteReleaseNoteController
 * 
 * This file contains the DeleteReleaseNoteController class, which is responsible for deleting release notes.
 * 
 * @package \Devolegkosarev\Dashboard\Controllers\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService
 */

class DeleteReleaseNoteController extends BaseController
{
    /**
     * @var ReleaseNotesService $releaseNotesService The release note service instance.
     */
    private $releaseNotesService;

    /**
     * @var ReleaseNotesChangeService $releaseNotesChangeService The release note service instance.
     */
    private $releaseNotesChangeService;
    public function __construct()
    {
        $this->releaseNotesService = Services::releaseNotesService();
        $this->releaseNotesChangeService = Services::releaseNotesChangeService();
    }

    /**
     * Delete method.
     *
     * Deletes a specific release note by ID.
     *
     * @param int $id The ID of the release note to delete.
     * @throws \InvalidArgumentException If the provided release note ID is invalid.
     * @throws \CodeIgniter\Exceptions\PageNotFoundException If the release note is not found.
     */
    public function delete(int $id)
    {
        $this->deleteReleaseNote($id);
        $this->deleteReleaseNoteChanges($id);
    }

    /**
     * Delete release note.
     *
     * Deletes a specific release note by ID.
     *
     * @param int $id The ID of the release note to delete.
     */
    private function deleteReleaseNote(int $id = 0)
    {
        $this->releaseNotesService->delete($id);
    }

    /**
     * Delete release note changes.
     *
     * Deletes the changes associated with a specific release note by release note ID.
     *
     * @param int $releaseNoteId The ID of the release note to delete the changes for.
     */
    private function deleteReleaseNoteChanges(int $releaseNoteId = 0)
    {
        $this->releaseNotesChangeService->deleteByReleaseNoteId($releaseNoteId);
    }
}
