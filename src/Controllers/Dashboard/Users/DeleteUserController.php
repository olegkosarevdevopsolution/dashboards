<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Users;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;
use CodeIgniter\HTTP\RedirectResponse;

/**
 * DeleteUserController
 *
 * This controller is responsible for deleting a user
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class DeleteUserController extends BaseController
{
    /**
     * Summary of usersService
     * @var \Devolegkosarev\Dashboard\Services\Users\UsersService
     */
    private $usersService;
    public function __construct()
    {
        $this->usersService = Services::usersService();
    }

    /**
     * Remove a user based on their ID.
     *
     * @param int $id The ID of the user to be removed.
     * @return RedirectResponse
     */
    public function remove(int $id = 0): RedirectResponse
    {
        $session = session();
        $userInfo = $this->usersService->getUser($id);
        if (!$userInfo) {
            $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.userNotFound'));
            return redirect()->to('/dashboard/users');
        }
        $deleteUser = $this->usersService->deleteUser($id);
        if ($deleteUser) {
            $session->setFlashdata('success', lang('Dashboard\Users\UsersLanguage.deleteUser.success'));
            return redirect()->to('/dashboard/users');
        }
        $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.deleteUser.error'));
        return redirect()->to('/dashboard/users');
    }
}
