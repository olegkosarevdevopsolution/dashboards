<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Users;

use Devolegkosarev\Dashboard\Controllers\BaseController;
use Config\Services;
use CodeIgniter\HTTP\RedirectResponse;

/**
 * BlockUserController
 *
 * This controller is responsible for handling requests for block user
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class BlockUserController extends BaseController
{
    /**
     * Summary of usersService
     * @var \Devolegkosarev\Dashboard\Services\Users\UsersService
     */
    private $usersService;
    public function __construct()
    {
        $this->usersService = Services::usersService();
    }

    public function block(int $id = 0): RedirectResponse
    {
        $userInfo = $this->usersService->getUser($id);
        $session = session();
        if (!$userInfo) {
            throw new \Exception(lang('Dashboard\Users\UsersLanguage.userNotFound'));
        }
        if ($userInfo["blocked"] == 1) {
            $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.blockUser.alreadyBlocked'));
            return redirect()->to('/dashboard/users');
        }
        $updateUser = $this->usersService->updateUser($id, [
            "blocked" => 1
        ]);
        if ($updateUser) {
            $session->setFlashdata('success', lang('Dashboard\Users\UsersLanguage.blockUser.success'));
            return redirect()->to('/dashboard/users');
        }
        $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.blockUser.error'));
        return redirect()->to('/dashboard/users');
    }

    public function unblock(int $id = 0): RedirectResponse
    {
        $userInfo = $this->usersService->getUser($id);
        $session = session();
        if (!$userInfo) {
            throw new \Exception(lang('Dashboard\Users\UsersLanguage.userNotFound'));
        }
        if ($userInfo["blocked"] == 0) {
            $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.unBlockUser.alreadyUnBlocked'));
            return redirect()->to('/dashboard/users');
        }
        $updateUser = $this->usersService->updateUser($id, [
            "blocked" => 0
        ]);
        if ($updateUser) {
            $session->setFlashdata('success', lang('Dashboard\Users\UsersLanguage.unBlockUser.success'));
            return redirect()->to('/dashboard/users');
        }
        $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.unBlockUser.error'));
        return redirect()->to('/dashboard/users');
    }
}
