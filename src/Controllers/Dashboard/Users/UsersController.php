<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Users;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * UsersController
 *
 * This class is used to handle the Users 
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class UsersController extends BaseController
{
    /**
     * Summary of usersService
     * @var \Devolegkosarev\Dashboard\Services\Users\UsersService
     */
    private $usersService;

    /**
     * Summary of rolesService
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesService
     */
    private $rolesService;
    public function __construct()
    {
        $this->usersService = Services::usersService();
        $this->rolesService = Services::rolesService();
    }
    public function index()
    {
        $data = [
            'breadcrumbs' => [
                [
                    'label' => lang('Dashboard\DashboardLanguage.breadcrumbs.label'),
                    'link' => '/dashboard',
                ],
                [
                    'label' => lang('Dashboard\Users\UsersLanguage.breadcrumbs.label'),
                    'active' => true,
                ],
            ],
            'pageHeader' => [
                'title' => lang('Dashboard\Users\UsersLanguage.pageHeader.title'),
                'description' => lang('Dashboard\Users\UsersLanguage.pageHeader.description'),
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'route' => 'documtations/roles#create',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="' . lang('Dashboard\Users\UsersLanguage.buttons.help.tooltip-title') . '"',
                ],
                [
                    'label' => 'Create',
                    'route' => 'dashboard/users/create',
                    'icon' => 'fas fa-plus',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="' . lang('Dashboard\Users\UsersLanguage.buttons.create.tooltip-title') . '"',
                ],
            ],
            'meta' => [
                'title' => lang('Dashboard\Users\UsersLanguage.meta.title'),
                'description' => lang('Dashboard\Users\UsersLanguage.meta.description'),
                'keywords' => lang('Dashboard\Users\UsersLanguage.meta.keywords'),
            ],
            'users' => $this->getUsersBySortRole(),
        ];
        return view(viewOverview('Pages\\Dashboard\\Users\\Users'), array_merge($data, $this->data));
    }

    private function getUsersBySortRole(): array
    {
        $users = [];
        foreach ($this->usersService->getUsers() as $user) {
            if (array_key_exists($user['role_id'], $users) == false) {
                $getRoleById = $this->rolesService->getRoleById($user['role_id']);
                $indexedData = array_column($getRoleById->translations, null, 'language');
                $translateRole = $indexedData[$this->data['currentLanguage']] ?? $getRoleById->translations[0];
                $users[$user['role_id']]['name'] = $translateRole['name'];
            }
            $users[$user['role_id']]['users'][] = $user;
        }
        return $users;
    }
}
