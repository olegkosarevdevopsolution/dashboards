<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Users;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * EditUsersController
 *
 * This class is used to handle the Users Edit
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class EditUserController extends BaseController
{
    /**
     * Summary of usersService
     * @var \Devolegkosarev\Dashboard\Services\Users\UsersService
     */
    private $usersService;

    /**
     * Summary of rolesService
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesService
     */
    private $rolesService;
    public function __construct()
    {
        $this->rolesService = Services::rolesService();
        $this->usersService = Services::usersService();
    }
    public function index(int $id = 0)
    {
        $user = $this->usersService->getUser($id);
        $session = session();
        if (!$user) {
            $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.userNotFound'));
            return redirect()->to('/dashboard/users');
        }
        $roles = $this->rolesService->getAllRoles($this->data["currentLanguage"]);
        $data = [
            'title' => 'Edit Profile',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Edit Profile',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Edit Profile',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/profile#edit',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
            'roles' => $roles,
            'user' => $user,

        ];
        return view(viewOverview('Pages\\Dashboard\\Users\\EditUser'), array_merge($data, $this->data));
    }

    public function store(int $id = 0)
    {
        $session = session();
        $user = $this->usersService->getUser($id);
        if (!$user) {
            $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.userNotFound'));
            return redirect()->to('/dashboard/users');
        }
        $data = [
            'name' => $this->request->getPost('name'),
            'email' => $this->request->getPost('email'),
            'role' => $this->request->getGetPost('role'),
        ];
        $updateUser = $this->usersService->updateUser($id, $data);
        if ($updateUser) {
            $session->setFlashdata('success', lang('Dashboard\Users\UsersLanguage.userUpdated'));
        } else {
            $session->setFlashdata('error', lang('Dashboard\Users\UsersLanguage.userNotUpdated'));
        }
        return redirect()->to('/dashboard/users');
    }
}
