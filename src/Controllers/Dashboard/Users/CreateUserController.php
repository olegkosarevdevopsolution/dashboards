<?php

namespace Devolegkosarev\Dashboard\Controllers\Dashboard\Users;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * CreateUserController
 *
 * This class is used for create new user
 *
 * @package \Devolegkosarev\Dashboard\Controllers\Dashboard\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CreateUserController extends BaseController
{


    public function index()
    {
        $rolesService = Services::rolesService();
        $roles = $rolesService->getAllRoles($this->data["currentLanguage"]);
        $data = [
            'title' => 'Edit Profile',
            'breadcrumb' => [
                [
                    'label' => 'Dashboard',
                    'link' => '#'
                ],
                [
                    'label' => 'Edit Profile',
                    'active' => true
                ]
            ],
            'page' => [
                'title' => 'Edit Profile',
                'description' => 'page header description goes here...'
            ],
            'buttons' => [
                [
                    'label' => 'Help',
                    'class' => 'btn-theme',
                    'icon' => 'fa-question-circle',
                    'route' => 'documtations/profile#edit',
                    'target' => '_blank',
                    'attributes' => 'data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="View documentation"',
                ]
            ],
            'meta' => [
                'title' => 'Your SEO meta title',
                'description' => 'Your SEO meta description',
                'keywords' => ['keyword1', 'keyword2', 'keyword3', 'keywordN']
            ],
            'content' => '',
            'roles' => $roles
        ];
        return view(viewOverview('Pages\\Dashboard\\Users\\CreateUser'), array_merge($data, $this->data));
    }
}
