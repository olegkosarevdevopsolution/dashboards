<?php

namespace Devolegkosarev\Dashboard\Controllers;

use CodeIgniter\Controller;
use Config\Services;

/**
 * BaseController
 *
 * The base controller for all other controllers in the application.
 *
 * namespace \Devolegkosarev\Dashboard\Controllers;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class BaseController extends Controller
{
	/**
	 * The data array that holds the initial values for the view.
	 *
	 * @var array
	 */
	public array $data = [];

	/**
	 * The current language of the application.
	 *
	 * @var string
	 */
	protected string $currentLanguage = '';

	/**
	 * The list of active languages in the application.
	 *
	 * @var array
	 */
	protected array $activeLanguages = [];

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = ['form', 'text', 'cookie', 'view'];

	/**
	 * This class handles the cookie manager services.
	 * 
	 * @var \Devolegkosarev\Dashboard\Services\EncryptedCookieManagerService
	 */
	protected object $encryptedCookieManagerService;

	/**
	 * This class handles the language services.
	 * 
	 * @var \Devolegkosarev\Dashboard\Services\LanguageService
	 */
	protected object $languageService;

	/**
	 * This class handles the menu renderer services.
	 * 
	 * @var \Devolegkosarev\Dashboard\Services\Menus\MenusRendererService
	 */
	protected object $menuRendererService;

	/**
	 * Initializes the controller.
	 *
	 * @param \CodeIgniter\HTTP\RequestInterface  $request
	 * @param \CodeIgniter\HTTP\ResponseInterface $response
	 * @param \Psr\Log\LoggerInterface            $logger
	 *
	 * @return void
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		// Get the encryptedCookieManagerService and languageService and menuRendererService instances
		$encryptedCookieManagerService = Services::encryptedCookieManagerService();
		$languageService = Services::languageService();
		$menuRendererService = Services::menuRendererService();
		$validation = \Config\Services::validation();
		// Get the current language
		$this->currentLanguage = $encryptedCookieManagerService->getLanguage();
		// Get the user roles id
		$userRolesId = $encryptedCookieManagerService->getUserRolesId();
		// Get the list of active languages
		$this->activeLanguages = $languageService->getActiveLanguages($this->currentLanguage);

		// Set the locale for the language service
		$language = Services::language();
		$language->setLocale($this->currentLanguage);

		$getUserAvatar = $encryptedCookieManagerService->getUserAvatar();
		$avatar = $encryptedCookieManagerService->getUserAvatar();
		if ($getUserAvatar) {
			if (filter_var($getUserAvatar, FILTER_VALIDATE_URL)) {
				$avatar = $getUserAvatar;
			} else {
				$avatar = base_url($encryptedCookieManagerService->getUserAvatar());
			}
		}

		// Set the data array with initial values
		$this->data = [
			'authenticationConfig' => config("BaseAuthenticationConfig")->getAuthenticationConfig(),
			'dashboardConfig' => config("BaseDashboardConfig")->getDashboardConfig(),
			'currentLanguage' => $this->currentLanguage,
			'languages' => $this->activeLanguages,
			'pageClass' => 'app-footer-fixed',
			'menu' => [
				'getMenu' => $menuRendererService->prepareMenuItemsByRole($userRolesId, $this->currentLanguage)
			],
			'email' => $encryptedCookieManagerService->getUserEmail(),
			'avatar' => $avatar,
			'alertHelper' => Services::alertHelperService()

		];
	}
}
