<?php

namespace Devolegkosarev\Dashboard\Controllers;

/**
 * Home
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class Home extends BaseController
{
    public function index(): string
    {
        return view(viewOverview('welcome_message'), []);
    }
}
