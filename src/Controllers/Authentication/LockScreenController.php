<?php

namespace Devolegkosarev\Dashboard\Controllers\Authentication;

use Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * LockScreenController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class LockScreenController extends BaseController
{
    public function index()
    {
        $session = session();
        $authenticator = Services::getAuthenticator();

        if ($this->data['authenticationConfig']->lockScreen === false) {
            throw new \Exception('Lock screen is disabled', 500);
        }

        if (!$authenticator->checkAuthorization()) {
            throw new \Exception('Unauthorized access');
        }

        $data = [
            'title' => 'Lock Screen',
            'pageClass' => 'auth-content app-with-top-nav app-without-sidebar',
        ];
        return view(viewOverview('Pages\\Authentication\\LockScreen'), array_merge($this->data, $data));
    }

    public function lockScreen()
    {
        return redirect('dashboard');
    }
}
