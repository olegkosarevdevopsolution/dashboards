<?php

namespace Devolegkosarev\Dashboard\Controllers\Authentication;

use Devolegkosarev\Dashboard\Controllers\BaseController;
use CodeIgniter\Config\Services;

/**
 * LogoutController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class LogoutController extends BaseController
{
    private $authenticator;

    public function __construct()
    {

        $this->authenticator = Services::getAuthenticator();
    }

    public function index()
    {
        $this->authenticator->logout();
        return redirect()->to('/authentication/login')->withCookies();
    }
}
