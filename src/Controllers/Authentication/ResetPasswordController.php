<?php

namespace Devolegkosarev\Dashboard\Controllers\Authentication;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * ResetPasswordController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class ResetPasswordController extends BaseController
{
    public function index()
    {
        $session = session();
        $session->setFlashdata("error", 'Reset password cod is expired', 'title', 'Error');
        return redirect("dashboard/authentication/login");

        if ($this->data['authenticationConfig']->allowForgetPassword === false) {
            $session->setFlashdata("error", 'Not allowed', 'title', 'Error');
            return redirect("dashboard/authentication/login");
        }
        $data = [
            'title' => 'Reset Password',
            'pageClass' => 'auth-content app-with-top-nav app-without-sidebar',
        ];
        return view(viewOverview('Pages\\Authentication\\ResetPassword'), array_merge($this->data, $data));
    }
}
