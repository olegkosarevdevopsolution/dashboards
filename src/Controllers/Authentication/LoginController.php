<?php

namespace Devolegkosarev\Dashboard\Controllers\Authentication;

use Devolegkosarev\Dashboard\Config\Services;
use Devolegkosarev\Dashboard\Controllers\BaseController;
use Devolegkosarev\Dashboard\Factory\Authentication\AuthenticationFactory;
use Devolegkosarev\Dashboard\Libraries\Authentication\DatabaseAuthenticationLibraries;

/**
 * LoginController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class LoginController extends BaseController
{
    private $authenticator;
    private $session;
    private $validation;
    public function __construct()
    {
        $this->session = session();

        $this->validation =  Services::validation();
        $this->authenticator = Services::getAuthenticator(); // Assuming getAuthenticator() is used correctly
    }
    public function index()
    {
    // $usersService = Services::usersService();
    // $usersService->q();
        $data = [
            'meta' => [
                'title' => 'Login',
                'description' => 'Login page',
                'keywords' => 'Login page',
            ],
            'pageClass' => 'auth-content app-with-top-nav app-without-sidebar'
        ];
        return view(viewOverview('Pages\\Authentication\\Login'), array_merge($this->data, $data));
    }

    public function login()
    {
   
        $request = Services::request();

        $email = $request->getPost('email');
        $password = $request->getPost('password');
        $rules = [
            "email" => ["label" => lang('Authentication/Login.emailAddress'), "rules" => "required|trim|valid_email"],
            "password" => ["label" => lang('Authentication/Login.password'), "rules" => "required|trim"],
        ];
        if (!$this->validate($rules)) {
            if ($this->validation->getError('email')) {
                $this->session->setFlashdata("email", $this->validation->getError('email'), 'title', 'Fatal Error');
            }
            if ($this->validation->getError('password')) {
                $this->session->setFlashdata("password", $this->validation->getError('password'), 'title', 'Fatal Error');
            }

            return redirect("authentication")->withInput()->withCookies();
        }
        try {
            $user = $this->authenticator->login($email, $password);
            return redirect("dashboard")->withCookies(); // Редирект на главную страницу после успешного входаnj
        } catch (\Exception $e) {
            $this->session->setFlashdata("error", $e->getMessage(), 'title', 'Fatal Error');
            return redirect("authentication")->withCookies();
        }
    }
}
