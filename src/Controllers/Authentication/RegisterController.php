<?php

namespace Devolegkosarev\Dashboard\Controllers\Authentication;

use Devolegkosarev\Dashboard\Controllers\BaseController;
use Config\Services;

/**
 * RegisterController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class RegisterController extends BaseController
{
    private $authenticator;
    private $session;
    private $validation;
    public function __construct()
    {
        $this->session = session();

        $this->authenticator = Services::getAuthenticator();

        $this->validation =  Services::validation();
    }
    public function index()
    {
        if (method_exists($this->authenticator, 'register') == false) {
            throw new \Exception('Authenticator does not have register method', 500);
        }
        $authenticationConfig = config("AuthenticationConfig");
        if ($authenticationConfig->allowRegister === false) {
            $this->session->setFlashdata("error", 'Registration is disabled', 'title', 'Error');
            return redirect("authentication");
        }
        $data = [
            'title' => 'Registeration',
            'pageClass' => 'auth-content app-with-top-nav app-without-sidebar'
        ];
        return view(viewOverview('Pages\\Authentication\\Register'), array_merge($this->data, $data));
    }

    public function register()
    {
        $request = Services::request();

        $name = $request->getPost("name");
        $email = $request->getPost('email');
        $password = $request->getPost('password');
        $confirm_password = $request->getPost('confirm_password');
        $firstname = $request->getPost('firstname');
        $lastname = $request->getPost('lastname');
        $secondname = $request->getPost('secondname');

        $rules = [
            'firstname' => ["label" => "First name", "rules" => "required|trim|min_length[3]|max_length[255]|alpha_numeric_space"],
            'lastname' => ["label" => "Last name", "rules" => "required|trim|min_length[3]|max_length[255]|alpha_numeric_space"],
            'secondname' => ["label" => "Second name", "rules" => "required|trim|min_length[3]|max_length[255]|alpha_numeric_space"],
            "email" => ["label" => "Email", "rules" => "required|trim|valid_email|max_length[255]"],
            "password" => ["label" => "Password", "rules" => "required|trim|min_length[8]|max_length[20]"],
            "confirm_password" => ["label" => "Confirm Password", "rules" => "required|trim|matches[password]"],
        ];

        if (!$this->validate($rules)) {
            if ($this->validation->getError('firstname')) {
                $this->session->setFlashdata("firstname", $this->validation->getError('firstname'), 'title', 'Fatal Error');
            }
            if ($this->validation->getError('lastname')) {
                $this->session->setFlashdata("lastname", $this->validation->getError('lastname'), 'title', 'Fatal Error');
            }
            if ($this->validation->getError('secondname')) {
                $this->session->setFlashdata("secondname", $this->validation->getError('secondname'), 'title', 'Fatal Error');
            }
            if ($this->validation->getError('email')) {
                $this->session->setFlashdata("email", $this->validation->getError('email'), 'title', 'Fatal Error');
            }
            if ($this->validation->getError('password')) {
                $this->session->setFlashdata("password", $this->validation->getError('password'), 'title', 'Fatal Error');
            }
            if ($this->validation->getError('confirm_password')) {
                $this->session->setFlashdata("confirm_password", $this->validation->getError('confirm_password'), 'title', 'Fatal Error');
            }
            return redirect("authentication/register")->withInput()->withCookies();
        }

        $userInfo = [
            'email' => $email,
            'password' => $password,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'secondname' => $secondname,
            'role_id' => 2,
            'activated' => 1
        ];

        $register = $this->authenticator->register($userInfo);
        if ($register) {
            $this->session->setFlashdata("success", 'Registration successful. 🎉');
            return redirect("authentication")->withInput()->withCookies();
        }
        $this->session->setFlashdata("error", 'Registration failed. 😥');
        return redirect("authentication/register")->withInput()->withCookies();
    }
}
