<?php

namespace Devolegkosarev\Dashboard\Controllers\Authentication;

use Devolegkosarev\Dashboard\Controllers\BaseController;

/**
 * PasswordController
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Controllers\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class PasswordController extends BaseController
{
    public function index()
    {
        if ($this->data['authenticationConfig']->allowForgetPassword === false) {
            $session = session();
            $session->setFlashdata("error", 'Forgot password is disabled', 'title', 'Error');
            return redirect("authentication");
        }
        $data = [
            'meta' => [
                'title' => 'Forget Password',
            ],
            'pageClass' => 'auth-content app-with-top-nav app-without-sidebar'
        ];
        return view(viewOverview('Pages\\Authentication\\ForgotPassword'), array_merge($this->data, $data));
    }
}
