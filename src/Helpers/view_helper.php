<?php

/**
 * view_helper
 * 
 * This file contains the view_helper, which is responsible for providing helper methods for views.
 * 
 * @package \Devolegkosarev\Dashboard\Helpers
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */


// Function: Returns the view path based on the provided base path.
if (!function_exists("viewOverview")) {
    /**
     * Returns the view path based on the provided base path.
     *
     * @param string $viewBasePath The base path of the view.
     * @return string The view path.
     */
    function viewOverview(string $viewBasePath): string
    {
        if (file_exists(APPPATH . "Views\\$viewBasePath.php")) {
            return $viewBasePath;
        }
        return 'Devolegkosarev\\Dashboard\\' . $viewBasePath;
    }
}
