<?php

namespace Devolegkosarev\Dashboard\Services\Bootstrap;

class AlertHelperService
{
    protected const COLOR_PRIMARY = 'primary';
    protected const COLOR_SECONDARY = 'secondary';
    protected const COLOR_SUCCESS = 'success';
    protected const COLOR_DANGER = 'danger';
    protected const COLOR_WARNING = 'warning';
    protected const COLOR_INFO = 'info';
    protected const COLOR_LIGHT = 'light';
    protected const COLOR_DARK = 'dark';
    protected static array $cachedViews = [];

    /**
     * Generate HTML code for a alert with the specified heading, message and color scheme.
     *
     * @param string $heading
     * @param string $message
     * @param string $colorScheme
     * @return string
     */
    protected static function generateAlert(string $heading, string $message, string $colorScheme): string
    {
        $viewKey = 'Alert:' . $colorScheme . ':' . $message;
        if (isset(self::$cachedViews[$viewKey])) {
            return self::$cachedViews[$viewKey];
        }

        $view = view(viewOverview('Views\\Partials\\Bootstrap\\Alert'), ['heading' => $heading, 'message' => $message, 'colorScheme' => $colorScheme]);
        self::$cachedViews[$viewKey] = $view;

        return $view;
    }

    /**
     * Generate HTML code for primary alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function primary(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_PRIMARY);
    }

    /**
     * Generate HTML code for secondary alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function secondary(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_SECONDARY);
    }

    /**
     * Generate HTML code for success alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function success(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_SUCCESS);
    }

    /**
     * Generate HTML code for danger alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function danger(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_DANGER);
    }

    /**
     * Generate HTML code for warning alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function warning(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_WARNING);
    }

    /**
     * Generate HTML code for info alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function info(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_INFO);
    }

    /**
     * Generate HTML code for dark alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function dark(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_DARK);
    }

    /**
     * Generate HTML code for light alert.
     *
     * @param string $message
     * @param string|null $heading
     * @return string
     */
    public static function light(string $message, ?string $heading = null): string
    {
        return self::generateAlert($heading, $message, self::COLOR_LIGHT);
    }
}
