<?php

use Devolegkosarev\Dashboard\Services\Bootstrap\BadgetHelperService;

class ListGroupHelperService
{
    protected array $listGroupItems = [];
    protected BadgetHelperService $badgetHelperService;
    protected bool $active = false;
    protected bool $disabled = false;
    protected string $ariaCurrent = '';
    protected string $ariaDisabled = '';
    protected $badge = '';

    public function __construct(BadgetHelperService $badgetHelperService)
    {
        $this->badgetHelperService = $badgetHelperService;
    }

    public function setBadge(string $badge, string $label = ""): self
    {
        if (empty($badge)) {
            throw new InvalidArgumentException("Badge Label Is Empty");
        }

        if (!method_exists($this->badgetHelperService, $badge)) {
            throw new InvalidArgumentException("Badge Type Not Exists");
        }

        $this->badge = $this->badgetHelperService->$badge($label);
        return $this;
    }

    public function setActive(bool $active = false): self
    {
        $this->active = $active;
        $this->ariaCurrent = ($active) ? 'aria-current="true"' : "";
        return $this;
    }

    public function setDisabled(bool $disabled = false): self
    {
        $this->disabled = $disabled;
        $this->ariaDisabled = ($disabled) ? 'aria-disabled="true"' : "";
        return $this;
    }

    public function setLink(string $link, string $textLink, string $target = null): self
    {
        $target = ($target) ? $target : "";
        $this->listGroupItems[] = $this->generateListItem('<a href="' . $link . '" class="list-group-item list-group-item-action" ' . $this->ariaAttributes() . '>' . $textLink . '</a>');
        return $this;
    }

    public function setListGroupItem(string $label, string $type = ''): self
    {
        $type = ($type) ? "list-group-item-$type" : '';
        $this->listGroupItems[] = $this->generateListItem('<li class="list-group-item ' . $type . '" ' . $this->ariaAttributes() . '>' . $label . '</li>');
        return $this;
    }

    public function setButton(string $label): self
    {
        $this->listGroupItems[] = $this->generateListItem('<button type="button" class="list-group-item list-group-item-action" ' . $this->ariaAttributes() . '>' . $label . '</button>');
        return $this;
    }

    public function renderListGroup(): string
    {
        return $this->renderList('list-group');
    }

    public function renderListGroupFlush(): string
    {
        return $this->renderList('list-group list-group-flush');
    }

    public function renderListGroupNumbered(): string
    {
        return $this->renderList('list-group list-group-numbered');
    }

    protected function generateListItem(string $content): string
    {
        return $content;
    }

    protected function ariaAttributes(): string
    {
        return $this->ariaCurrent . ' ' . $this->ariaDisabled;
    }

    protected function renderList(string $listClass): string
    {
        $html = '<div class="' . $listClass . '">';
        $html .= implode('', $this->listGroupItems);
        $html .= '</div>';
        return $html;
    }
}


    //   <li class="list-group-item">An item</li>
    //   <li class="list-group-item active" aria-current="true">An active item</li>
    //   <li class="list-group-item disabled" aria-disabled="true">A disabled item</li>

    // <div class="list-group">
    //   <a href="#" class="list-group-item list-group-item-action active" aria-current="true">
    //     The current link item
    //   </a>
    //   <a href="#" class="list-group-item list-group-item-action">A second link item</a>
    //   <a href="#" class="list-group-item list-group-item-action">A third link item</a>
    //   <a href="#" class="list-group-item list-group-item-action">A fourth link item</a>
    //   <a class="list-group-item list-group-item-action disabled" aria-disabled="true">A disabled link item</a>
    // </div>

    // <div class="list-group">
    //   <button type="button" class="list-group-item list-group-item-action active" aria-current="true">
    //     The current button
    //   </button>
    //   <button type="button" class="list-group-item list-group-item-action">A second button item</button>
    //   <button type="button" class="list-group-item list-group-item-action">A third button item</button>
    //   <button type="button" class="list-group-item list-group-item-action">A fourth button item</button>
    //   <button type="button" class="list-group-item list-group-item-action" disabled>A disabled button item</button>
    // </div>

    // <ul class="list-group">
    //   <li class="list-group-item">A simple default list group item</li>

    //   <li class="list-group-item list-group-item-primary">A simple primary list group item</li>
    //   <li class="list-group-item list-group-item-secondary">A simple secondary list group item</li>
    //   <li class="list-group-item list-group-item-success">A simple success list group item</li>
    //   <li class="list-group-item list-group-item-danger">A simple danger list group item</li>
    //   <li class="list-group-item list-group-item-warning">A simple warning list group item</li>
    //   <li class="list-group-item list-group-item-info">A simple info list group item</li>
    //   <li class="list-group-item list-group-item-light">A simple light list group item</li>
    //   <li class="list-group-item list-group-item-dark">A simple dark list group item</li>
    // </ul>

    // <ul class="list-group list-group-flush">
    //   <li class="list-group-item">An item</li>
    //   <li class="list-group-item">A second item</li>
    //   <li class="list-group-item">A third item</li>
    //   <li class="list-group-item">A fourth item</li>
    //   <li class="list-group-item">And a fifth one</li>
    // </ul>

    // <ol class="list-group list-group-numbered">
    //   <li class="list-group-item">A list item</li>
    //   <li class="list-group-item">A list item</li>
    //   <li class="list-group-item">A list item</li>
    // </ol>

    // <ol class="list-group list-group-numbered">
    // <li class="list-group-item d-flex justify-content-between align-items-start">
    //     <div class="ms-2 me-auto">
    //     <div class="fw-bold">Subheading</div>
    //     Content for list item
    //     </div>
    //     <span class="badge bg-primary rounded-pill">14</span>
    // </li>
    // <li class="list-group-item d-flex justify-content-between align-items-start">
    //     <div class="ms-2 me-auto">
    //     <div class="fw-bold">Subheading</div>
    //     Content for list item
    //     </div>
    //     <span class="badge bg-primary rounded-pill">14</span>
    // </li>
    // <li class="list-group-item d-flex justify-content-between align-items-start">
    //     <div class="ms-2 me-auto">
    //     <div class="fw-bold">Subheading</div>
    //     Content for list item
    //     </div>
    //     <span class="badge bg-primary rounded-pill">14</span>
    // </li>
    // </ol>