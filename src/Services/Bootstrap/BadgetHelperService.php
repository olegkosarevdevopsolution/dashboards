<?php

namespace Devolegkosarev\Dashboard\Services\Bootstrap;

class BadgetHelperService
{
    protected const COLOR_PRIMARY = 'primary';
    protected const COLOR_SECONDARY = 'secondary';
    protected const COLOR_SUCCESS = 'success';
    protected const COLOR_DANGER = 'danger';
    protected const COLOR_WARNING = 'warning';
    protected const COLOR_INFO = 'info';
    protected const COLOR_LIGHT = 'light';
    protected const COLOR_DARK = 'dark';
    protected array $attributes = [];

    protected static array $cachedViews = [];

    public function setAttributes(string $name, string $value): self
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    protected static function generateBadge(string $message, string $colorScheme): string
    {
        $view = view(viewOverview('Views\\Partials\\Bootstrap\\Badget'), ['message' => $message, 'colorScheme' => $colorScheme]);
        return $view;
    }

    public static function primary(string $message): string
    {
        return self::generateBadge($message, self::COLOR_PRIMARY);
    }

    public static function secondary(string $message): string
    {
        return self::generateBadge($message, self::COLOR_SECONDARY);
    }

    public static function success(string $message): string
    {
        return self::generateBadge($message, self::COLOR_SUCCESS);
    }

    public static function danger(string $message): string
    {
        return self::generateBadge($message, self::COLOR_DANGER);
    }

    public static function warning(string $message): string
    {
        return self::generateBadge($message, self::COLOR_WARNING);
    }

    public static function info(string $message): string
    {
        return self::generateBadge($message, self::COLOR_INFO);
    }

    public static function light(string $message): string
    {
        return self::generateBadge($message, self::COLOR_LIGHT);
    }

    /**
     * Generates a dark badge with the given message.
     *
     * @param string $message The message to be displayed on the badge.
     * @throws Some_Exception_Class If there is an error generating the badge.
     * @return string The generated dark badge.
     */
    public static function dark(string $message): string
    {
        return self::generateBadge($message, self::COLOR_DARK);
    }
}
