<?php

namespace Devolegkosarev\Dashboard\Services\Bootstrap;

class ButtonHelperService
{
    public  $attributes = [];
    public  $buttons = [];

    public $size = "";
    protected function colorScheme(string $color, bool $outline): string
    {
        $colorScheme = ' btn-' . $color;
        if ($outline) {
            $colorScheme = 'btn-outline-' . $color;
        }
        return 'btn ' . $colorScheme;
    }

    protected function generateButton(string $colorSchemeGet, bool $outline, array $button = []): string
    {
        $colorScheme = $this->colorScheme($colorSchemeGet, $outline);
        $class = $colorScheme;
        $this->size = '';
        if (array_key_exists('class', $this->attributes)) {
            $class .= ' ' . $this->attributes['class'];
        }

        $this->attributes["class"] = $class;

        $button["buttonAttributes"] = '';
        foreach ($this->attributes as $name => $value) {
            $button["buttonAttributes"]  .= $name . '="' . $value . '" ';
        }

        $view = view(viewOverview('Views\\Partials\\Bootstrap\\Buttons'), $button);

        return $view;
    }

    public function setAttributes(string $name, string $value): self
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    public function setSizeButtons(string $size): self
    {
        $this->size = (in_array($size, ['lg', 'sm'])) ? 'btn-' . $size : '';
        return $this;
    }

    public function renderButtons(): string
    {
        if (count($this->buttons) > 0) {
            return implode("", $this->buttons);
        }
        throw new \Exception('No buttons to render');
    }

    public function theme(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton('theme', $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function default(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton('default', $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }
}
