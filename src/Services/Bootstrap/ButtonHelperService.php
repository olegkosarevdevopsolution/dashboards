<?php

namespace Devolegkosarev\Dashboard\Services\Bootstrap;

class ButtonHelperService
{
    protected array $buttons = [];
    protected string $size = '';

    protected string $colorTheme = 'theme';
    protected string $colorDefault = 'default';
    protected string $colorSecondary = 'secondary';
    protected string $colorLight = 'light';
    protected string $colorDark = 'dark';
    protected string $colorInfo = 'info';
    protected string $colorPrimary = 'primary';
    protected string $colorPurple = 'purple';
    protected string $colorIndigo = 'indigo';
    protected string $colorLink = 'link';
    protected string $colorDanger = 'danger';
    protected string $colorPink = 'pink';
    protected string $colorWarning = 'warning';
    protected string $colorYellow = 'yellow';
    protected string $colorLime = 'lime';
    protected string $colorGreen = 'green';
    protected string $colorSuccess = 'success';
    protected string $colorWhite = 'white';
    protected string $colorBlack = 'black';
    protected array $attributes = [];

    protected function colorScheme(string $color, bool $outline): string
    {
        $colorScheme = ' btn-' . $color;
        if ($outline) {
            $colorScheme = 'btn-outline-' . $color;
        }
        return 'btn ' . $colorScheme;
    }

    public function setAttributes(array $attributes): self
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function setSizeButtons(string $size): self
    {
        $this->size = (in_array($size, ['lg', 'sm'])) ? 'btn-' . $size : '';
        return $this;
    }

    public function renderButtons(): string
    {
        if (count($this->buttons) > 0) {
            $buttons = $this->buttons;
            $this->buttons = [];
            return implode("", $buttons);
        }
        throw new \Exception('No buttons to render');
    }

    public function renderGroup(string $size = ''): string
    {
        return view(
            viewOverview('Views\\Partials\\Bootstrap\\ButtonGroup'),
            [
                'buttons' => $this->renderButtons(),
                'buttonGroupClass' => (in_array($size, ['lg', 'sm'])) ? 'btn-group-' . $size : ''
            ]
        );
    }

    public function renderGroupVertical(string $size = ''): string
    {
        return view(
            viewOverview('Views\\Partials\\Bootstrap\\ButtonGroupVertical'),
            [
                'buttons' => $this->renderButtons(),
                'buttonGroupClass' => (in_array($size, ['lg', 'sm'])) ? 'btn-group-' . $size : ''
            ]
        );
    }

    protected function generateButton(string $colorScheme, bool $outline, array $button = []): string
    {
        $colorScheme = $this->colorScheme($colorScheme, $outline);
        $class = $colorScheme . ' ' . $this->size;

        if (array_key_exists('class', $this->attributes)) {
            $class .= ' ' . $this->attributes['class'];
        }

        $this->attributes["class"] = $class;

        $button["buttonAttributes"] = '';
        foreach ($this->attributes as $name => $value) {
            $button["buttonAttributes"]  .= $name . '="' . $value . '" ';
        }

        $this->attributes = [];
        $this->size = '';
        $view = view(viewOverview('Views\\Partials\\Bootstrap\\Buttons'), $button);
        return $view;
    }

    public function theme(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorTheme, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function default(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorDefault, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function secondary(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorSecondary, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function light(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorLight, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function dark(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorDark, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function info(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorInfo, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function primary(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorPrimary, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function purple(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorPurple, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function indigo(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorIndigo, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function link(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorLink, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function danger(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorDanger, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function pink(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorPink, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function warning(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorWarning, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function yellow(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorYellow, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function lime(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorLime, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function green(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorGreen, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function success(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorSuccess, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function white(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorWhite, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }

    public function black(string $buttonText, string $buttonType = 'button', bool $outline = false): self
    {
        $this->buttons[] = $this->generateButton($this->colorBlack, $outline, ['buttonText' => $buttonText, 'buttonType' => $buttonType]);
        return $this;
    }
}

/*
ты можешь эти 2 варианта кнопок обедетить и сделать единый сервис 

<button type="button" class="btn btn-outline-theme">Theme</button>
<button type="button" class="btn btn-outline-default">Default</button>
<button type="button" class="btn btn-outline-secondary">Secondary</button>
<button type="button" class="btn btn-outline-light">Light</button>
<button type="button" class="btn btn-outline-dark">Dark</button>
<button type="button" class="btn btn-outline-info">Info</button>
<button type="button" class="btn btn-outline-primary">Primary</button>
<button type="button" class="btn btn-outline-purple">Purple</button>
<button type="button" class="btn btn-outline-indigo">Indigo</button>
<button type="button" class="btn btn-outline-link">Link</button>
<button type="button" class="btn btn-outline-danger">Danger</button>
<button type="button" class="btn btn-outline-pink">Pink</button>
<button type="button" class="btn btn-outline-warning">Warning</button>
<button type="button" class="btn btn-outline-yellow">Yellow</button>
<button type="button" class="btn btn-outline-lime">Lime</button>
<button type="button" class="btn btn-outline-green">Green</button>
<button type="button" class="btn btn-outline-success">Success</button>
<button type="button" class="btn btn-outline-white">White</button>
<button type="button" class="btn btn-outline-black">Black</button>



<button type="button" class="btn btn-theme">Theme</button>

<button type="button" class="btn btn-default">Default</button>

<button type="button" class="btn btn-secondary">Secondary</button>

<button type="button" class="btn btn-light">Light</button>

<button type="button" class="btn btn-dark">Dark</button>

<button type="button" class="btn btn-info">Info</button>

<button type="button" class="btn btn-primary">Primary</button>

<button type="button" class="btn btn-purple">Purple</button>

<button type="button" class="btn btn-indigo">Indigo</button>

<button type="button" class="btn btn-link">Link</button>

<button type="button" class="btn btn-danger">Danger</button>

<button type="button" class="btn btn-pink">Pink</button>

<button type="button" class="btn btn-warning">Warning</button>

<button type="button" class="btn btn-yellow">Yellow</button>

<button type="button" class="btn btn-lime">Lime</button>

<button type="button" class="btn btn-green">Green</button>

<button type="button" class="btn btn-success">Success</button>

<button type="button" class="btn btn-white">White</button>

<button type="button" class="btn btn-black">Black</button>



Так же добавь btn-lg и btn-sm 



И так же  добавь что бы кнопки можно было собирать в группы и был размер

<div class="btn-group btn-group-lg">...</div>

<div class="btn-group">...</div>

<div class="btn-group btn-group-sm">...</div>



так же что бы была возможность делать 

<div class="btn-group-vertical">...</div>

*/
