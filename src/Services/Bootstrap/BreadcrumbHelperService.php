<?php

namespace Devolegkosarev\Dashboard\Services\Bootstrap;

class BreadcrumbHelperService
{
    private $breadcrumbs = [];

    public function addBreadcrumbItem($title, $url = '')
    {
        $breadcrumb = [
            'title' => $title,
            'url' => $url
        ];

        $this->breadcrumbs[] = $breadcrumb;
    }

    public function getBreadcrumbsItems()
    {
        return $this->breadcrumbs;
    }

    public function getBreadcrumbs()
    {
        $html = '<ol class="breadcrumb">';

        foreach ($this->breadcrumbs as $breadcrumb) {
            $html .= '<li class="breadcrumb-item';
            if ($breadcrumb['active']) {
                $html .= ' active';
            }
            $html .= '">';
            if (!empty($breadcrumb['url'])) {
                $html .= '<a href="' . $breadcrumb['url'] . '">' . $breadcrumb['title'] . '</a>';
            } else {
                $html .= $breadcrumb['title'];
            }
            $html .= '</li>';
        }

        $html .= '</ol>';

        return $html;
    }
}
