<?php

namespace Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes;

use Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes\ReleaseNotesModel;

/**
 * ReleaseNotesService
 *
 * Service for working with the ReleaseNotesModel.
 * 
 * This class provides methods to interact with the "release_notes" table.
 * It allows retrieving, creating, updating, and deleting records related to release notes.
 *
 * @package \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes\ReleaseNotesModel
 */
class ReleaseNotesService
{
    protected $model;

    public function __construct(ReleaseNotesModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get all release notes.
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->model->getReleaseNotes();
    }

    /**
     * Get a release note by ID.
     *
     * @param int $id
     *
     * @return array|null
     */
    public function getById(int $id): ?array
    {
        return $this->model->getReleaseNoteById($id);
    }

    /**
     * Create a release note.
     *
     * @param array $data
     *
     * @return bool
     */
    public function create(array $data): bool
    {
        return $this->model->createReleaseNote($data);
    }

    /**
     * Update a release note.
     *
     * @param int   $id
     * @param array $data
     *
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        return $this->model->updateReleaseNote($id, $data);
    }

    /**
     * Delete a release note.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->deleteReleaseNote($id);
    }
}
