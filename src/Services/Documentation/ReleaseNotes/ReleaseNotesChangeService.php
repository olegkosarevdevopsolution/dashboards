<?php

namespace Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes;

use Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes\ReleaseNotesChangeModel;

/**
 * ReleaseNotesChangeService
 *
 * Service for working with the ReleaseNoteChangeModel.
 *
 * This class provides methods to interact with the "release_notes_change" table.
 * It allows retrieving, creating, updating, and deleting records related to release notes changes.
 *
 * @package \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService
 */
class ReleaseNotesChangeService
{
    protected $model;

    /**
     * Constructor.
     *
     * @param ReleaseNotesChangeModel $model The ReleaseNotesChangeModel instance.
     */
    public function __construct(ReleaseNotesChangeModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get all records from the release_notes_change table.
     *
     * @return array The array of all records.
     */
    public function getAll()
    {
        return $this->model->getAll();
    }

    /**
     * Get all records from the release_notes_change table by release.
     *
     * @param int    $releaseId The release ID.
     * @param string $language  The language (default: "en").
     * @return array The array of records.
     */
    public function getByRelease(int $releaseId, string $language = "en")
    {
        return $this->model->getByRelease($releaseId, $language);
    }

    /**
     * Get a record by id.
     *
     * @param int $id The record ID.
     * @return mixed|null The record or null if not found.
     */
    public function getById(int $id)
    {
        return $this->model->getById($id);
    }

    /**
     * Create a new record in the release_notes_change table.
     *
     * @param array $data The data for the new record.
     * @return int The ID of the created record.
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update a record in the release_notes_change table by id.
     *
     * @param int   $id   The record ID.
     * @param array $data The updated data for the record.
     * @return bool True if the record was updated successfully, false otherwise.
     */
    public function update(int $id, array $data)
    {
        return $this->model->updateRecord($id, $data);
    }

    /**
     * Delete a record from the release_notes_change table by id.
     *
     * @param int $id The record ID.
     * @return bool True if the record was deleted successfully, false otherwise.
     */
    public function delete(int $id)
    {
        return $this->model->deleteRecord($id);
    }
}
