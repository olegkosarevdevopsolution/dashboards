<?php

namespace Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes;

/**
 * ReleaseNotesRendererService
 *
 * This file contains the ReleaseNotesRendererService class, which is responsible for rendering release notes.
 *
 * @package \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Partials\Views\Partials\Templates\List
 */
class ReleaseNotesRendererService
{
    /**
     * Pre-render the release notes.
     *
     * @param array $releaseNotes The release notes array.
     * @return View The rendered view.
     */
    public function preRenderReleaseNotes($releaseNotes = []): string
    {
        // Define the table header data
        $tHeadData = [
            (object)['name' => '#'],
            (object)['name' => 'Version'],
            (object)['name' => 'Release Date'],
            (object)['name' => 'Version Status'],
            (object)['name' => 'Publish Status'],
            (object)['name' => 'Created'],
            (object)['name' => 'Updated'],
        ];

        // Initialize the table body data
        $tBodyData = '';

        // Iterate through each release note
        foreach ($releaseNotes as $releaseNote) {
            $releaseNoteId = $releaseNote->id;

            // Append the table row with release note data
            $tBodyData .= '
        <tr>
            <th scope="row">' . $releaseNoteId . '</th>
            <td><a href="' . site_url('dashboard/documentation/release-notes/edit/' . $releaseNoteId) . '">' . $releaseNote->version . '</a></td>
            <td>' . $releaseNote->release_date . '</td>
            <td>' . $releaseNote->version_status . '</td>
            <td>';

            // Check the publish status and add the corresponding badge
            if ($releaseNote->publish_status == 1) {
                $tBodyData .= '<span class="badge border border-success text-success px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                        <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                        Published
                    </span>';
            } else {
                $tBodyData .= '<span class="badge border border-danger text-danger px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                        <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                        Unpublished
                    </span>';
            }

            // Append the remaining table columns
            $tBodyData .= '</td>
            <td>' . $releaseNote->created_at . '</td>
            <td>' . $releaseNote->updated_at . '</td>
        </tr>';
        }

        // Return the rendered view with the table data
        return view(viewOverview('Views\\Partials\\Templates\\List'), ["tHeadData" => $tHeadData, "tBodyData" => $tBodyData]);
    }
}
