<?php

namespace Devolegkosarev\Dashboard\Services\Roles;

use Devolegkosarev\Dashboard\Models\Roles\RolesModel;
use Devolegkosarev\Dashboard\Models\Roles\RolesTranslationsModel;
use Exception;

class RolesService
{
    protected $rolesModel;
    protected $translationsModel;

    public function __construct(RolesModel $rolesModel, RolesTranslationsModel $translationsModel)
    {
        $this->rolesModel = $rolesModel;
        $this->translationsModel = $translationsModel;
    }

    public function getAllRoles($language)
    {
        return $this->rolesModel->getAllRoles($language);
    }

    public function getRoleById(int $id): ?object
    {
        $role = $this->rolesModel->getRoleById($id);
        if ($role) {
            $role->translations = $this->translationsModel->getTranslationsByRoleId($id);
        }
        return $role;
    }

    public function create(array $data)
    {
        return $this->rolesModel->create($data);
    }

    public function update(int $id, array $data)
    {
        return $this->rolesModel->update($id, $data);
    }

    public function deleteRole(int $id)
    {
        return $this->rolesModel->deleteRoleById($id);
    }

    /**
     * Checks if a role has a specific permission.
     * @param int $role The role identifier.
     * @param string $permission The permission name.
     * @return bool True if the role has the permission, false otherwise.
     */
    public function hasPermission(string $role, string $permission): bool
    {
        $rolePermissions = $this->rolesModel->getRolePermissions($role);

        return in_array($permission, $rolePermissions);
    }

    public function attachPermission(int $roleId, string $permission)
    {
        $this->rolesModel->attachPermission($roleId, $permission);
    }
}
