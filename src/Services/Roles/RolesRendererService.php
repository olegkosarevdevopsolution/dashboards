<?php

namespace Devolegkosarev\Dashboard\Services\Roles;

use Devolegkosarev\Dashboard\Services\FormService;

/**
 * RolesRendererService
 *
 * This file contains the RolesRendererService class, which is responsible for rendering roles.
 *
 * @package \Devolegkosarev\Dashboard\Services\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class RolesRendererService
{
    /**
     * FormService
     *
     * @var Devolegkosarev\Dashboard\Services\FormService
     */
    private $formService;
    function __construct(FormService $formService)
    {
        $this->formService = $formService;
    }

    function preRenderRoles($roles = [])
    {
        // Define the table header data
        $tHeadData = [
            (object)['name' => 'Id'],
            (object)['name' => 'Type'],
            (object)['name' => 'Name Role'],
            (object)['name' => 'Status'],
            (object)['name' => 'Created'],
            (object)['name' => 'Updated'],
            (object)['name' => 'Actions'],
        ];

        // Initialize the table body data
        $tBodyData = '';

        // Iterate through each roles
        foreach ($roles as $role) {
            $roleId = $role->id;

            // Append the table row with role data
            $tBodyData .= '
            <tr>
                <th scope="row">' . $roleId . '</th>
                <td>' . $role->type . '</td>
                <td>' . $role->name . '</td>
                <td>';

            // Check the publish status and add the corresponding badge
            if ($role->status == 0) {
                $tBodyData .=
                    '<span class="badge border border-danger text-danger px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                            <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                            Unpublished
                        </span>';
                $btnStatus = '<a href="' . site_url('dashboard/roles/publish/' . $roleId) . '" class="btn btn-success btn-sm">Publish</a>';
            } else {
                $tBodyData .=
                    '<span class="badge border border-success text-success px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                            <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                            Published
                        </span>';
                $btnStatus = '<a href="' . site_url('dashboard/roles/unpublish/' . $roleId) . '" class="btn btn-danger btn-sm">Unpublish</a>';
            }

            // Append the remaining table columns
            $tBodyData .= '</td>
                <td>' . $role->created_at . '</td>
                <td>' . $role->updated_at . '</td>
                <td>
                    <div class="btn-group">
                    <a href="' . site_url('dashboard/roles/edit/' . $roleId) . '" class="btn btn-theme btn-sm">Edit</a>
                    <a href="' . site_url('dashboard/roles/delete/' . $roleId) . '" class="btn btn-danger btn-sm">Delete</a>
                    ' . $btnStatus . '
                    </div>
                </td>
            
            </tr>';
        }

        return view(viewOverview('Views\\Partials\\Templates\\List'), ["tHeadData" => $tHeadData, "tBodyData" => $tBodyData]);
    }



    /**
     * Retrieves an array of form elements translated for each language.
     *
     * @param array $languages An array of languages.
     * @return string The generated HTML
     */
    public function getFormElementsTranslatedTab(array $languages = [], array $data = []): string
    {

        foreach ($languages as $language) {
            $languageCode = $language["code"];
            $languageName = $language["name"];
            $translate = [];

            $indexedData = array_column($data, null, 'language');
            $translate = $indexedData[$languageCode] ?? [];

            $name = (isset($translate["name"])) ? $translate["name"] : '';
            $created_at = (isset($translate["created_at"])) ? $translate["created_at"] : date("Y-m-d H:i:s");
            $updated_at = (isset($translate["updated_at"])) ? $translate["updated_at"] : date("Y-m-d H:i:s");


            $formElements[$languageCode] = [
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationLanguageCode.label')
                    ],
                    'attributes' => [
                        'readonly' => true,
                        'value' => $languageCode
                    ],
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationName.label')
                    ],
                    'attributes' => [
                        'placeholder' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationName.placeholder', [], $languageCode),
                        'id' => 'dataTranslatedInput',
                        'name' => 'dataTranslatedInput',
                        'value' => $name,
                    ],
                    'help' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationName.help', ['language' => $languageName]),
                    'invalid_tooltip' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationName.invalid_tooltip', ['language' => $languageName]),
                    'valid_tooltip' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationName.valid_tooltip'),
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationCreated.label')
                    ],
                    'attributes' => [
                        'id' => 'dataTranslatedCreatedInput',
                        'name' => 'dataTranslatedCreatedInput',
                        'value' => $created_at,
                        'disabled' => true
                    ],
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.fields.roleTranslationUpdated.label')
                    ],
                    'attributes' => [
                        'id' => 'dataTranslatedUpdatedInput',
                        'name' => 'dataTranslatedUpdatedInput',
                        'value' => $updated_at,
                    ],
                ]
            ];
        }

        return $this->formService->translatedTab(
            $formElements,
            $languages
        );
    }

    /**
     * Retrieves the form elements.
     *
     * @return string The HTML representation of the form elements
     */
    public function getFormElements(object $data = null): string
    {
        $formElements = [
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.fields.roleType.label')
                ],
                'attributes' => [
                    'id' => 'roleTypeInput',
                    'name' => 'roleType',
                    'value' => (isset($data->type)) ? $data->type : '',
                    'placeholder' => lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.fields.roleType.placeholder'),
                ],
                'help' => lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.fields.roleType.help')
            ],
            [
                'type' => 'switch',
                'class' => 'form-switch',
                'label' => [
                    'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.fields.status.label')
                ],
                'attributes' => [
                    'id' => 'status',
                    'name' => 'status',
                    'checked' => (isset($data->status) and (int) $data->status) ? true : false,
                ],
                'help' => lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.fields.status.help')
            ],
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.fields.created.label')
                ],
                'attributes' => [
                    'id' => 'roleCreatedInput',
                    'name' => 'roleCreatedInput',
                    'value' => (isset($data->created_at)) ? $data->created_at : date("Y-m-d H:i:s"),
                    'disabled' => true
                ]
            ],
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.fields.updated.label')
                ],
                'attributes' => [
                    'id' => 'roleUpdatedInput',
                    'name' => 'roleUpdatedInput',
                    'value' => (isset($data->created_at)) ? $data->updated_at : date("Y-m-d H:i:s"),
                ]
            ],
        ];
        $html = '';

        foreach ($formElements as $formElement) {
            $html .= $this->formService->getFormElementHtml($formElement);
        }
        return $html;
    }
}
