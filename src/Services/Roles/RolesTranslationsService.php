<?php

namespace Devolegkosarev\Dashboard\Services\Roles;

use Devolegkosarev\Dashboard\Models\Roles\RolesTranslationsModel;


/**
 * RolesTranslationService
 *
 * This class is the RolesTranslationService
 *
 * @package \Devolegkosarev\Dashboard\Services\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class RolesTranslationsService
{
    /** 
     * @var \Devolegkosarev\Dashboard\Models\Roles\RolesModel
     */
    public $model;

    public function __construct(RolesTranslationsModel $rolesTranslationsModel)
    {
        $this->model = $rolesTranslationsModel;
    }

    public function create(array $data = [])
    {
        return $this->model->create($data);
    }

    public function updateByRoleId(int $idRole, array $data = [])
    {
        return $this->model->updateByRoleId($idRole, $data);
    }

    public function delete(int $id)
    {
        return $this->model->deleteTranslationsByRoleId($id);
    }
}
