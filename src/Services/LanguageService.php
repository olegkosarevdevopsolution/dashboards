<?php

namespace Devolegkosarev\Dashboard\Services;

class LanguageService
{
    protected $languages;

    public function __construct(array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * Retrieves a list of active languages.
     *
     * @param string $currentLocale The locale of the current language.
     * @return array Returns an array of active languages with their flags, names, and active status.
     */
    public function getActiveLanguages(string $currentLocale): array
    {
        $activeLanguages = [];
        foreach ($this->languages as $key => $language) {
            $activeLanguages[$language] = [
                'code' => $language,
                'flag' => $this->getLanguageFlagByLocale($language),
                'name' => $this->getLanguageNameByLocale($language),
                'active' => true,
                'currentLocale' => ($currentLocale == $language) ? true : false
            ];
        }
        return $activeLanguages;
    }

    /**
     * Retrieves the language name based on the given language locale.
     *
     * @param string $langLocale The language locale to retrieve the name for.
     * @return string|null The name of the language if found, null otherwise.
     */
    protected function getLanguageNameByLocale(string $langLocale = null)
    {
        if ($langLocale == null) {
            return null;
        }
        $languagesName = [
            "lv" => "Latvian",
            "ru" => "Russian",
            "en" => "English",
            "se" => "Swedish",
            "de" => "Deutsch",
            "lt" => "Lithuanian",
            "ch" => "Chinese",
            "hu" => "Hungarian",
            "es" => "Spanish",
        ];
        if (array_key_exists($langLocale, $languagesName)) {
            return $languagesName[$langLocale];
        }
        return null;
    }

    /**
     * Retrieves the language flag based on the given locale.
     *
     * @param string $langLocale The locale for which to retrieve the language flag. Default is null.
     * @return string|null The language flag corresponding to the given locale, or null if the locale is null or not found.
     */
    protected function getLanguageFlagByLocale(string $langLocale = null)
    {
        if ($langLocale == null) {
            return null;
        }

        $flagsCode = [
            "lv" => "lv",
            "ru" => "ru",
            "en" => "gb",
            "se" => "se",
            "de" => "de",
            "lt" => "lt",
            "ch" => "ch",
            "hu" => "hu",
            "es" => "es",
        ];
        if (array_key_exists($langLocale, $flagsCode)) {
            return $flagsCode[$langLocale];
        }
        return null;
    }
}
