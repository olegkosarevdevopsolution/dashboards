<?php

namespace Devolegkosarev\Dashboard\Services\Users;

use Exception;
use Devolegkosarev\Dashboard\Models\Users\UsersModel;
use Devolegkosarev\Dashboard\Services\Users\PasswordService;

/**
 * UserService
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Services\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class UsersService
{
    /**
     * Users Model Instanse
     * 
     * @var \Devolegkosarev\Dashboard\Models\Users\UsersModel
     */
    protected $usersModel;

    /** 
     * Password Service Instanse
     * 
     * @var \Devolegkosarev\Dashboard\Services\Users\PasswordService
     */
    protected $passwordService;

    /**
     * Dashboard Config
     * 
     * @var \Devolegkosarev\Dashboard\Config\BaseDashboardConfig
     */
    protected $dashboardConfig;

    public function __construct(
        UsersModel $usersModel,
        PasswordService $passwordService,
        object $dashboardConfig
    ) {
        $this->usersModel = $usersModel;
        $this->passwordService = $passwordService;
        $this->dashboardConfig = $dashboardConfig;
    }

    public function getUser($id): array
    {
        $getUser = $this->usersModel->getUser($id);
        if ($getUser) {
            return $getUser;
        }
        return [];
    }

    public function getUserByEmail(string $email)
    {
        $getUserByEmail = $this->usersModel->getUserByEmail($email);
        if ($getUserByEmail) {
            return $getUserByEmail;
        }
        return null;
    }

    public function getUsers()
    {
        return $this->usersModel->getUsers();
    }

    public function createUser(array $data): bool
    {
        $data["password"] = $this->passwordService->encryptPassword($data["password"]);
        if (array_key_exists("avator", $data)) {
            $data['avator'] = $this->dashboardConfig->avatarDefault;
        }
        if ($this->usersModel->createUser($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateUser($id, $data)
    {
        if (array_key_exists("password", $data)) {
            $data["password"] = $this->passwordService->encryptPassword($data["password"]);
        }
        return $this->usersModel->updateUser($id, $data);
    }

    public function deleteUser($id)
    {
        return $this->usersModel->deleteUser($id);
    }

    public function verifyPassword(string $password, string $hash): bool
    {
        return $this->passwordService->verifyPassword($password, $hash);
    }
}
