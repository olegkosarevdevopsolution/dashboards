<?php

namespace Devolegkosarev\Dashboard\Services\Users;

/**
 * PasswordService
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Services\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class PasswordService
{

    protected $encryptionKey;
    protected $ivLength;

    public function __construct($encryptionKey)
    {
        $this->encryptionKey = $encryptionKey;
        $this->ivLength = openssl_cipher_iv_length('AES-256-CBC');
    }

    public function encryptPassword($password)
    {
        $iv = openssl_random_pseudo_bytes($this->ivLength);
        $encryptedPassword = openssl_encrypt($password, 'AES-256-CBC', $this->encryptionKey, OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $encryptedPassword, $this->encryptionKey, true);
        $encryptedData = base64_encode($iv . $hmac . $encryptedPassword);
        return $encryptedData;
    }

    public function decryptPassword($encryptedData)
    {
        $decodedData = base64_decode($encryptedData);
        $iv = substr($decodedData, 0, $this->ivLength);
        $hmac = substr($decodedData, $this->ivLength, 32);
        $encryptedPassword = substr($decodedData, $this->ivLength + 32);
        $calculatedHmac = hash_hmac('sha256', $encryptedPassword, $this->encryptionKey, true);
        if (!hash_equals($hmac, $calculatedHmac)) {
            throw new \Exception('HMAC verification failed.');
        }
        $decryptedPassword = openssl_decrypt($encryptedPassword, 'AES-256-CBC', $this->encryptionKey, OPENSSL_RAW_DATA, $iv);
        return $decryptedPassword;
    }

    public function verifyPassword($password, $hash)
    {
        $decryptedPassword = $this->decryptPassword($hash);
        if ($password === $decryptedPassword) {
            return true;
        }
        return false;
    }
}
