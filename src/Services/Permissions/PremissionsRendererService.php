<?php

namespace Devolegkosarev\Dashboard\Services\Permissions;

use Devolegkosarev\Dashboard\Services\FormService;

/**
 * PremissionsRendererService
 *
 * Service for rendering permissions
 *
 * @package \Devolegkosarev\Dashboard\Services\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class PremissionsRendererService
{
    /**
     * FormService
     *
     * @var Devolegkosarev\Dashboard\Services\FormService
     */
    private $formService;
    function __construct(FormService $formService)
    {
        $this->formService = $formService;
    }

    public function preRenderPremissions(array $permissions = []): string
    {
        // Define the table header data
        $tHeadData = [
            (object)['name' => 'Id'],
            (object)['name' => 'Type'],
            (object)['name' => 'Name Premission'],
            (object)['name' => 'Status'],
            (object)['name' => 'Created'],
            (object)['name' => 'Updated'],
        ];

        // Initialize the table body data
        $tBodyData = '';

        // Iterate through each roles
        foreach ($permissions as $permission) {
            $permissionId = $permission->id;


            // Append the table row with role data
            $tBodyData .= '
            <tr>
                <th scope="row">' . $permissionId . '</th>
                <td>' . $permission->type . '</td>
                <td><a href="' . site_url('dashboard/permissions/edit/' . $permissionId) . '">' . $permission->name . '</a></td>
                <td>';

            // Check the publish status and add the corresponding badge
            if ($permission->status == 1) {
                $tBodyData .= '<span class="badge border border-success text-success px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                            <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                            Published
                        </span>';
            } else {
                $tBodyData .= '<span class="badge border border-danger text-danger px-2 pt-5px pb-5px rounded fs-12px d-inline-flex align-items-center">
                            <i class="fa fa-circle fs-9px fa-fw me-5px"></i>
                            Unpublished
                        </span>';
            }

            // Append the remaining table columns
            $tBodyData .= '</td>
                <td>' . $permission->created_at . '</td>
                <td>' . $permission->updated_at . '</td>
            </tr>';
        }

        return view(viewOverview('Views\\Partials\\Templates\\List'), ["tHeadData" => $tHeadData, "tBodyData" => $tBodyData]);
    }
}
