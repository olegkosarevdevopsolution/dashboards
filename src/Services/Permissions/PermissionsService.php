<?php

namespace Devolegkosarev\Dashboard\Services\Permissions;

use Devolegkosarev\Dashboard\Models\Permissions\PermissionsModel;

class PermissionsService
{
    protected $permissionsModel;

    public function __construct(PermissionsModel $permissionsModel)
    {
        $this->permissionsModel = $permissionsModel;
    }

    public function getAllPermissions($language): array
    {
        return $this->permissionsModel->getAllPermissions($language);
    }

    public function getPermissionById($id)
    {
        return $this->permissionsModel->find($id);
    }

    public function createPermission($data)
    {
        return $this->permissionsModel->insert($data);
    }

    public function updatePermission($id, $data): bool
    {
        return $this->permissionsModel->update($id, $data);
    }

    public function deletePermission($id)
    {
        return $this->permissionsModel->delete($id);
    }
}
