<?php

namespace Devolegkosarev\Dashboard\Services\Permissions;

use Devolegkosarev\Dashboard\Models\Permissions\PermissionsTranslationsModel;

/**
 * PermissionsTranslationsService
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Services\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class PermissionsTranslationsService
{
    /**
     * @var PermissionsTranslationsModel $permissionsTranslationsModel
     */
    private $permissionsTranslationsModel;

    /**
     * __construct function
     *
     * @param PermissionsTranslationsModel $permissionsTranslationsModel
     */
    public function __construct(PermissionsTranslationsModel $permissionsTranslationsModel)
    {
        $this->permissionsTranslationsModel = $permissionsTranslationsModel;
    }

    public function insert($data)
    {
        $this->permissionsTranslationsModel->insert($data);
    }
}
