<?php

namespace Devolegkosarev\Dashboard\Services\Menus;

use Devolegkosarev\Dashboard\Models\Menus\MainmenuModel;
use Devolegkosarev\Dashboard\Models\Menus\MainmenuTranslationsModel;

class MainmenuService
{
    protected $menuModel;
    protected $menuTranslationsModel;
    public function __construct(MainmenuModel $menuModel, MainmenuTranslationsModel $menuTranslationsModel)
    {
        $this->menuModel = $menuModel;
        $this->menuTranslationsModel = $menuTranslationsModel;
    }

    public function getMenuItems($language): array
    {
        return $this->menuModel->getMenuItems($language);
    }

    public function getMenuItem($id): ?object
    {
        $getMenuItem = $this->menuModel->getMenuItem($id);
        if (!$getMenuItem) {
            return new \stdClass();
        }
        $getMenuItem->translations = $this->menuTranslationsModel->getTranslationsByMenuItemId($id);
        return $getMenuItem;
    }

    public function getMenuByRole($roleId, $language = 'en'): array
    {
        return $this->menuModel->getMenuByRole($roleId, $language);
    }
}
