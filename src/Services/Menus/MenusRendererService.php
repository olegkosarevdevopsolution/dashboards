<?php

namespace Devolegkosarev\Dashboard\Services\Menus;

use Devolegkosarev\Dashboard\Services\FormService;
use Devolegkosarev\Dashboard\Services\Roles\RolesService;
use Devolegkosarev\Dashboard\Services\Menus\MainmenuService;
use Devolegkosarev\Dashboard\Libraries\Menus\MenuRendererLibrary;

/**
 * MenusRendererService
 *
 * This class provides methods for rendering menus
 *
 * @package \Devolegkosarev\Dashboard\Services\Menus;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class MenusRendererService
{
    /**
     * FormService
     *
     * @var \Devolegkosarev\Dashboard\Services\FormService
     */
    private $formService;

    /**
     * RolesService
     * 
     * @var \Devolegkosarev\Dashboard\Services\Roles\RolesService
     */
    private $roleService;

    /**
     * MenuService
     * 
     * @var \Devolegkosarev\Dashboard\Libraries\Menus\MenuRendererLibrary
     */
    private $menuService;

    /**
     * __construct
     * 
     * @param \Devolegkosarev\Dashboard\Services\FormService $formService
     * @param \Devolegkosarev\Dashboard\Services\Roles\RolesService $roleService
     * @param \Devolegkosarev\Dashboard\Libraries\Menus\MenuRendererLibrary $menuService
     * 
     * @return void
     */
    function __construct(FormService $formService, RolesService $roleService, MainmenuService $menuService)
    {
        $this->formService = $formService;
        $this->roleService = $roleService;
        $this->menuService = $menuService;
    }


    public  function getFormElements(string $currentLanguage, object $data = null): string
    {
        $formElements = [
            [
                'type' => 'select',
                'label' => [
                    'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuType.label')
                ],
                'attributes' => [
                    'id' => 'typeMenuSelect',
                    'name' => 'typeMenuSelect',
                    'required' => true,
                ],
                'options' => [
                    [
                        'value' => 'link',
                        'label' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuType.options.link'),
                        'selected' => (isset($data->type) && $data->type == 'link') ? true : false
                    ],
                    [
                        'value' => 'header',
                        'label' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuType.options.header'),
                        'selected' => (isset($data->type) && $data->type == 'header') ? true : false

                    ],
                    [
                        'value' => 'page',
                        'label' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuType.options.page'),
                        'disabled' => true
                    ],
                    [
                        'value' => 'custom',
                        'label' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuType.options.custom'),
                        'disabled' => true
                    ],
                    [
                        'value' => 'external',
                        'label' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuType.options.external'),
                        'disabled' => true
                    ]
                ],
                'help' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuType.help')
            ],
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuLink.label')
                ],
                'help' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuLink.help'),
                'invalid_tooltip' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuLink.invalid_tooltip'),
                'valid_tooltip' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuLink.valid_tooltip'),
                'attributes' => [
                    'id' => 'menuLinkInput',
                    'name' => 'menuLinkInput',
                    'placeholder' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuLink.placeholder'),
                    'required' => true,
                    'value' => (isset($data->url)) ? $data->url : '',
                ]
            ],
            [
                'type' => 'select',
                'label' => [
                    'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuParrent.label')
                ],
                'attributes' => [
                    'id' => 'menuParrentSelect',
                    'name' => 'menuParrent',
                    'required' => true,
                ],
                'help' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuParrent.help'),

                'options' =>
                array_merge(
                    [[
                        'value' => 0,
                        'label' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuParrent.options.root'),
                        'selected' => (isset($data->parent_id) && $data->parent_id == 0) ? true : false
                    ]],
                    $this->getAllMenusOptions($this->prepareMenuItems($currentLanguage), 0, 0, (isset($data->parent_id) ? $data->parent_id : null))
                )
            ],
            [
                'type' => 'select',
                'label' => [
                    'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.role.label'),
                ],
                'attributes' => [
                    'id' => 'roleSelect',
                    'name' => 'role[]',
                    'multiple' => true,
                    'required' => true
                ],
                'help' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.role.help'),
                'options' => $this->getAllRolesOptions($currentLanguage)
            ],

            [
                'type' => 'switch',
                'class' => 'form-switch',
                'label' => [
                    'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuStatus.label')
                ],
                'attributes' => [
                    'id' => 'menuStatus',
                    'name' => 'menuStatus',
                    'class' => '',
                    'checked' => true,
                    'value' => (isset($data->status) && $data->status == 1) ? true : false
                ],
                'help' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuStatus.help')
            ],
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuCreated.label')
                ],
                'attributes' => [
                    'id' => 'menuCreatedInput',
                    'name' => 'menuCreatedInput',
                    'value' => (isset($data->created_at)) ? $data->created_at : date("Y-m-d H:i:s"),
                    'disabled' => (isset($data->created_at)) ? true : false
                ]
            ],
            [
                'type' => 'input',
                'label' => [
                    'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.menuUpdated.label')
                ],
                'attributes' => [
                    'id' => 'menuUpdatedInput',
                    'name' => 'menuUpdatedInput',
                    'value' => (isset($data->updated_at)) ? $data->updated_at : date("Y-m-d H:i:s"),
                    'disabled' => (isset($data->created_at)) ? true : false
                ]
            ],
        ];

        $html = '';
        foreach ($formElements as $formElement) {
            $html .= $this->formService->getFormElementHtml($formElement);
        }
        return $html;
    }

    private function getAllRolesOptions(string $currentLanguage): array
    {
        $rolesOptions = [];

        $getAllRoles = $this->roleService->getAllRoles($currentLanguage);
        foreach ($getAllRoles as $role) {
            $rolesOptions[$role->id]["label"] = $role->name;
            $rolesOptions[$role->id]["value"] = $role->id;
            $rolesOptions[$role->id]["selected"] = false;
        }

        return $rolesOptions;
    }

    public function getAllMenusOptions(array $array, int $parentId, int $level = 0, int $selectedParentId = null): array
    {
        $html = [];
        $count = count($array);
        $index = 0;

        foreach ($array as $item) {
            $index++;
            if ($item->parent_id == $parentId) {
                $isLastItem = ($index === $count);
                $row = [];
                $row['value'] = $item->id;
                $row['label'] = str_repeat('&nbsp;', $level * 4);
                $row['label'] .= $isLastItem ? '└── ' : '├── ';
                $row['label'] .= $item->name;
                $row['selected'] = (isset($selectedParentId) && $selectedParentId == $item->id) ? true : false;
                $html[] = $row;

                if (isset($item->items) && is_array($item->items) && count($item->items) > 0) {
                    $children = $this->getAllMenusOptions($item->items, $item->id, $level + 1, $selectedParentId);
                    $html = array_merge($html, $children);
                }
            }
        }

        return $html;
    }

    /**
     * Retrieves an array of form elements translated for each language.
     *
     * @param array $languages An array of languages.
     * @return array An array of form elements.
     */
    public function getFormElementsTranslatedTab(array $languages = [], array $data = []): string
    {
        foreach ($languages as $language) {
            $languageCode = $language["code"];
            $languageName = $language["name"];
            $translate = [];

            $indexedData = array_column($data, null, 'language');
            $translate = $indexedData[$languageCode] ?? [];

            $name = (isset($translate["name"])) ? $translate["name"] : '';
            $created_at = (isset($translate["created_at"])) ? $translate["created_at"] : date("Y-m-d H:i:s");
            $updated_at = (isset($translate["updated_at"])) ? $translate["updated_at"] : date("Y-m-d H:i:s");

            $formElements[$languageCode] = [
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationLanguageCode.label')
                    ],
                    'attributes' => [
                        'readonly' => true,
                        'value' => $languageCode
                    ],
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationName.label')
                    ],
                    'attributes' => [
                        'placeholder' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationName.placeholder', [], $languageCode),
                        'id' => 'dataTranslatedInput',
                        'name' => 'dataTranslatedInput',
                        'value' => $name,
                    ],
                    'help' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationName.help', ['language' => $languageName]),
                    'invalid_tooltip' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationName.invalid_tooltip', ['language' => $languageName]),
                    'valid_tooltip' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationName.valid_tooltip'),
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationCreated.label')
                    ],
                    'attributes' => [
                        'id' => 'dataTranslatedCreatedInput',
                        'name' => 'dataTranslatedCreatedInput',
                        'class' => 'form-control',
                        'value' => $created_at,
                        'disabled' => true
                    ],
                ],
                [
                    'type' => 'input',
                    'label' => [
                        'text' => lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.fields.menuTranslationUpdated.label')
                    ],
                    'attributes' => [
                        'id' => 'dataTranslatedUpdatedInput',
                        'name' => 'dataTranslatedUpdatedInput',
                        'class' => 'form-control',
                        'value' => $updated_at,
                        'disabled' => true
                    ],
                ]
            ];
        }
        return $this->formService->translatedTab($formElements, $languages);
    }

    /**
     * Prepare menu items.
     *
     * @param string $language The language for the menu items.
     * @return array The prepared menu items.
     */
    public function prepareMenuItems(string $language): array
    {
        $menuItems = $this->menuService->getMenuItems($language);

        return $this->buildTree($menuItems);
    }

    /**
     * Prepares menu items based on the role and language.
     *
     * @param int $roleId The ID of the role.
     * @param string $language The language for the menu items.
     * @return array The prepared menu items.
     */
    public function prepareMenuItemsByRole(int $roleId, string $language): array
    {
        $menuItems = $this->menuService->getMenuByRole($roleId, $language);

        return $this->buildTree($menuItems);
    }


    /**
     * Build menu tree.
     *
     * @param array $elements The menu items.
     * @param int $parentId The parent ID.
     * @return array The built menu tree.
     */
    function buildTree(array $elements, $parentId = 0)
    {
        $tree = [];

        foreach ($elements as $element) {
            if ($element->parent_id == $parentId) {
                $element->items = $this->buildTree($elements, $element->id);
                $tree[] = $element;
            }
        }

        return $tree;
    }
}
