<?php

namespace Devolegkosarev\Dashboard\Services;

/**
 * FormElementService
 *
 * This class provides methods to generate different types of form elements.
 *
 * @package \Devolegkosarev\Dashboard\Services;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 */
class FormElementService
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Generate an input form element.
     * 
     * $formElement['type'] string - The type of the form element.
     * $formElement['label'] string - The label for the form element.
     * $formElement['value'] string - The value of the form element.
     * $formElement['attributes'] array - The attributes for the form element.
     * $formElement['attributes']['id'] - The ID attribute for the form element.
     * $formElement['attributes']['name'] - The name attribute for the form element.
     * $formElement['attributes']['class'] string - The CSS class for the form element.
     * $formElement['attributes']['placeholder'] string - The placeholder text for the form element.
     * $formElement['attributes']['required'] bool - Indicates if the form element is required.
     * $formElement['attributes']['readonly'] bool - Indicates if the form element is read-only.
     * $formElement['attributes']['disabled'] bool - Indicates if the form element is disabled.
     *
     * @param array<string, mixed> $formElement The form element configuration.
     * @return string The generated HTML for the input form element.
     */
    public function generateInput(array $formElement): string
    {
        return view(
            viewOverview("Views\\Partials\\Templates\\FormElements\\Input"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function generateCheckbox(array $formElement): string
    {
        return view(
            viewOverview("Views\\Partials\\Templates\\FormElements\\FormCheck\\Checkbox"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function generateSwitch(array $formElement): string
    {
        $formElement['type'] = 'checkbox';
        return view(
            viewOverview("Views\\Partials\\Templates\\FormElements\\FormCheck\\Switch"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function generateRadio(array $formElement): string
    {
        return view(
            viewOverview("Views\\Partials\\Templates\\FormElements\\FormCheck\\Radio"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function generateTextarea(array $formElement): string
    {
        return view(
            viewOverview("Views\\Partials\\Templates\\FormElements\\Textarea"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    /**
     * Generate a select form element.
     *
     * @param array $formElement The form element configuration.
     * @return string The generated HTML for the select form element.
     */
    public function generateSelect(array $formElement): string
    {
        return view(
            viewOverview("Views\\Partials\\Templates\\FormElements\\Select"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    /**
     * Generate a password form element.
     *
     * @param array $formElement The form element configuration.
     * @return string The generated HTML for the password form element.
     */
    public function generatePassword(array $formElement): string
    {
        return view(
            viewOverview("Views\\Partials\\Templates\\FormEditCreate\\InputPassword"),
            []
        );
    }

    /**
     * Generate a file upload form element.
     *
     * @param array $formElement The form element.
     * @return string The generated file upload form element.
     */
    public function generateFile(array $formElement): string
    {
        return view(
            viewOverview("Views\\Partials\\Templates\\FormElements\\InputFiles"),
            [
                'formElement' => $formElement,
            ]
        );
    }
}
