<?php

namespace Devolegkosarev\Dashboard\Services;

use Devolegkosarev\Dashboard\Services\Bootstrap\BadgetHelperService;
use Devolegkosarev\Dashboard\Services\Bootstrap\AlertHelperService;
use Devolegkosarev\Dashboard\Services\Bootstrap\ButtonHelperService;
use Devolegkosarev\Dashboard\Services\Bootstrap\BreadcrumbHelperService;
use Devolegkosarev\Dashboard\Services\Bootstrap\CardHelperService;
use Devolegkosarev\Dashboard\Services\Bootstrap\ModalHelperService;
// use Devolegkosarev\Dashboard\Services\Bootstrap\NavbarHelperService;
// use Devolegkosarev\Dashboard\Services\Bootstrap\FormHelperService;

class BootstrapElements
{
    /**
     * Get the alerts helper service.
     *
     * @param AlertHelperService $alertHelperService The alert helper service instance.
     * @return AlertHelperService The alert helper service instance.
     */
    public function getAlerts(AlertHelperService $alertHelperService): AlertHelperService
    {
        return $alertHelperService;
    }

    /**
     * Get the buttons helper service.
     *
     * @param ButtonHelperService $buttonHelperService The button helper service instance.
     * @return ButtonHelperService The button helper service instance.
     */
    public function getButtons(ButtonHelperService $buttonHelperService): ButtonHelperService
    {
        return $buttonHelperService;
    }

    /**
     * Get the badget helper service.
     *
     * @param BadgetHelperService $badgetHelperService The badget helper service instance.
     * @return BadgetHelperService The badget helper service instance.
     */
    public function getBadget(BadgetHelperService $badgetHelperService): BadgetHelperService
    {
        return $badgetHelperService;
    }

    /**
     * Get the breadcrumb helper service.
     *
     * @param BreadcrumbHelperService $breadcrumbHelperService The breadcrumb helper service instance.
     * @return BreadcrumbHelperService The breadcrumb helper service instance.
     */
    public function getBreadcrumb(BreadcrumbHelperService $breadcrumbHelperService): BreadcrumbHelperService
    {
        return $breadcrumbHelperService;
    }

    /**
     * Get the card helper service.
     *
     * @param CardHelperService $cardHelperService The card helper service instance.
     * @return CardHelperService The card helper service instance.
     */
    public function getCard(CardHelperService $cardHelperService): CardHelperService
    {
        return $cardHelperService;
    }

    /**
     * Get the modal helper service.
     *
     * @param ModalHelperService $modalHelperService The modal helper service instance.
     * @return ModalHelperService The modal helper service instance.
     */
    public function getModal(ModalHelperService $modalHelperService): ModalHelperService
    {
        return $modalHelperService;
    }

    // /**
    //  * Get the form helper service.
    //  *
    //  * @param FormHelperService $formHelperService The form helper service instance.
    //  * @return FormHelperService The form helper service instance.
    //  */
    // public function getForm(FormHelperService $formHelperService): FormHelperService
    // {
    //     return $formHelperService;
    // }

    // /**
    //  * Get the navbar helper service.
    //  *
    //  * @param NavbarHelperService $navbarHelperService The navbar helper service instance.
    //  * @return NavbarHelperService The navbar helper service instance.
    //  */
    // public function getNavbar(NavbarHelperService $navbarHelperService): NavbarHelperService
    // {
    //     return $navbarHelperService;
    // }
}
