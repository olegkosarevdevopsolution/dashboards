<?php

namespace Devolegkosarev\Dashboard\Services\Forms;

use Devolegkosarev\Dashboard\Services\Forms\Elements\TextareaService;
use Devolegkosarev\Dashboard\Services\Forms\Elements\CheckboxService;
use Devolegkosarev\Dashboard\Services\Forms\Elements\SelectService;
use Devolegkosarev\Dashboard\Services\Forms\Elements\TextService;
use Devolegkosarev\Dashboard\Services\Forms\Elements\FilesService;

/**
 * FormsServices
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Services\Forms;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class FormsServices
{
    /**
     * Creates a new instance of the FormGenerator class.
     *
     * @return FormGenerator
     */
    public function form(): FormGenerator
    {
        return new FormGenerator();
    }

    /**
     * Creates a new instance of the TextareaService class.
     *
     * @param array $formElement The array of form elements.
     * @return TextareaService The newly created TextareaService instance.
     */
    public function textarea(array $formElement = []): TextareaService
    {
        return new TextareaService($formElement);
    }

    public function checkbox(array $formElement = []): CheckboxService
    {
        return new CheckboxService($formElement);
    }

    public function select(array $formElement = []): SelectService
    {
        return new SelectService($formElement);
    }

    public function text(array $formElement = []): TextService
    {
        return new TextService($formElement);
    }

    public function files(array $formElement = []): FilesService
    {
        return new FilesService($formElement);
    }
}
