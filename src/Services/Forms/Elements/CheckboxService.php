<?php

namespace DevOlegKosarev\Dashboard\Services\Forms\Elements;

use DevOlegKosarev\Dashboard\Abstracts\Forms\AbstractFormElement;

/**
 * CheckboxService
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Services\Forms\Elements;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CheckboxService extends AbstractFormElement
{
    public function __construct(array $formElement = [])
    {
    }
    public function generate(): string
    {
        $formElement = [];
        return view(
            viewOverview("Views\\Partials\\FormsElements\\Checkbox"),
            [
                'formElement' => $formElement,
            ]
        );
    }
}
