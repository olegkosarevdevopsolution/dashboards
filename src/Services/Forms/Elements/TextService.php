<?php

namespace DevOlegKosarev\Dashboard\Services\Forms\Elements;

use DevOlegKosarev\Dashboard\Abstracts\Forms\AbstractFormElement;

/**
 * TextService
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Services\Forms\Elements;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class TextService extends AbstractFormElement
{
    private string $value;
    public function __construct(array $formElement)
    {
        if (array_key_exists('label', $formElement)) {
            if (array_key_exists('text', $formElement['label'])) {
                $this->setLabel($formElement['label']['text']);
            }
            if (array_key_exists('class', $formElement['label'])) {
                $this->setLabelClass($formElement['label']['class']);
            }
        }
        if (array_key_exists('readonly', $formElement)) {
            $this->setReadOnly($formElement['readonly']);
        }
        if (array_key_exists('disabled', $formElement)) {
            $this->setDisabled($formElement['disabled']);
        }
        if (array_key_exists('required', $formElement)) {
            $this->setRequired($formElement['required']);
        }
        if (array_key_exists('attributes', $formElement)) {
            if (array_key_exists('id', $formElement['attributes'])) {
                $this->setId($formElement['attributes']['id']);
            } elseif (array_key_exists('name', $formElement['attributes'])) {
                $this->setName($formElement['attributes']['name']);
            } elseif (array_key_exists('placeholder', $formElement['attributes'])) {
                $this->setPlaceholder($formElement['attributes']['placeholder']);
            } elseif (array_key_exists('class', $formElement['attributes'])) {
                $this->setClass($formElement['attributes']['class']);
            } elseif (array_key_exists('value', $formElement['attributes'])) {
                $this->setValue($formElement['attributes']['value']);
            }
            $this->setAttributes($formElement['attributes']);
        }
        if (array_key_exists('help', $formElement)) {
            $this->setHelp($formElement['help']);
        }
        if (isset($formElement['validationSuccessMessage'])) {
            $this->setValidationSuccessMessage($formElement['validationSuccessMessage']);
        }
        if (isset($formElement['validationErrorMessage'])) {
            $this->setValidationErrorMessage($formElement['validationErrorMessage']);
        }
    }


    public function generate(): string
    {
        $formElement = [
            'label' => [
                'text' => $this->label,
                'class' => $this->labelClass
            ],
            'attributes' => [
                'required' => $this->required,
                'readonly' => $this->readonly,
                'disabled' => $this->disabled,
                'class' => 'form-control' . $this->class,
                'id' => $this->id,
                'placeholder' => $this->placeholder,
                'name' => $this->name,
                'value' => $this->value,
                $this->attributes ?? []
            ],
            'help' => $this->help,
        ];

        return view(
            viewOverview("Views\\Partials\\FormsElements\\Text"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
