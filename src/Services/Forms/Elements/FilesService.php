<?php

namespace DevOlegKosarev\Dashboard\Services\Forms\Elements;

use DevOlegKosarev\Dashboard\Abstracts\Forms\AbstractFormElement;

/**
 * FilesService
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Services\Forms\Elements;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class FilesService extends AbstractFormElement
{
    public int $max = 1;
    public array $allowedExtensions = ["*"];

    public function __construct(array $formElement = [])
    {
    }

    public function generate(): string
    {
        $formElement = [];
        return view(
            viewOverview("Views\\Partials\\FormsElements\\Files"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function setMax(int $max): self
    {
        $this->max = $max;
        return $this;
    }

    public function setAllowedExtensions(array $allowedExtensions): self
    {
        $this->allowedExtensions = $allowedExtensions;
        return $this;
    }
}
