<?php

namespace DevOlegKosarev\Dashboard\Services\Forms\Elements;

use DevOlegKosarev\Dashboard\Abstracts\Forms\AbstractFormElement;

/**
 * SelectService
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Services\Forms\Elements;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class SelectService extends AbstractFormElement
{
    private array $options = [];
    public function __construct(array $formElement = [])
    {
    }
    public function generate(): string
    {
        $formElement = [
            'label' => [
                'text' => $this->label,
                'class' => $this->labelClass
            ],
            'attributes' => [
                'required' => $this->required,
                'readonly' => $this->readonly,
                'disabled' => $this->disabled,
                'class' => 'form-control' . $this->class,
                'id' => $this->id,
                'placeholder' => $this->placeholder,
                'name' => $this->name,
                $this->attributes ?? []
            ],
            'help' => $this->help,
            'options' => $this->options,
        ];
        return view(
            viewOverview("Views\\Partials\\FormsElements\\Select"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function setDefaultOptions(string $value, string $label): self
    {
        $option = [
            'value' => $value,
            'label' => $label
        ];
        array_unshift($this->options, $option);
        return $this;
    }

    public function setOptions(array $options): self
    {
        array_map(function ($option) {
            if (array_key_exists('value', $option) and array_key_exists('label', $option)) {
                $this->options[] = $option;
            }
        }, $options);
        return $this;
    }
}
