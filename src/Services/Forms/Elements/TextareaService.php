<?php

namespace Devolegkosarev\Dashboard\Services\Forms\Elements;

use Devolegkosarev\Dashboard\Abstracts\Forms\AbstractFormElement;

/**
 * TextareaService
 *
 * This class represents a textarea form element.
 *
 * @package DevOlegKosarev\Dashboard\Services\Forms\Elements;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class TextareaService extends AbstractFormElement
{

    private int $rows = 3;
    private int $cols = 0;
    /**
     * TextareaService constructor.
     *
     * @param array $formElement The form element configuration.
     */
    public function __construct(array $formElement = [])
    {
        if ($formElement) {
            if (isset($formElement['label']['text'])) {
                $this->setLabel($formElement['label']['text']);
            }
            if (isset($formElement['label']["class"])) {
                $this->setlabelClass($formElement['label']["class"]);
            }
            if (isset($formElement['readonly'])) {
                $this->setReadOnly($formElement['readonly']);
            }
            if (isset($formElement['disabled'])) {
                $this->setDisabled($formElement['disabled']);
            }
            if (isset($formElement['required'])) {
                $this->setRequired($formElement['required']);
            }
            if (isset($formElement['attributes'])) {
                if (isset($formElement['attributes']['id'])) {
                    $this->setId($formElement['attributes']['id']);
                } elseif (isset($formElement['attributes']['name'])) {
                    $this->setName($formElement['attributes']['name']);
                } elseif (isset($formElement['attributes']['placeholder'])) {
                    $this->setPlaceholder($formElement['attributes']['placeholder']);
                } elseif (isset($formElement['attributes']['class'])) {
                    $this->setClass($formElement['attributes']['class']);
                }
                $this->setAttributes($formElement['attributes']);
            }
            if (isset($formElement['help'])) {
                $this->setHelp($formElement['help']);
            }
            if (isset($formElement['validationSuccessMessage'])) {
                $this->setValidationSuccessMessage($formElement['validationSuccessMessage']);
            }
            if (isset($formElement['validationErrorMessage'])) {
                $this->setValidationErrorMessage($formElement['validationErrorMessage']);
            }
        }
    }

    /**
     * Generate the HTML code for the textarea form element.
     *
     * @return string The generated HTML code.
     */
    public function generate(): string
    {
        $this->name ?? $this->name = uniqid();
        $this->id ?? $this->id = $this->name;
        // $this->help ?? $this->attributes[] = 
        $formElement = [
            'label' => [
                'text' => $this->getLabel(),
                'class' => 'form-label ' . $this->labelClass
            ],
            'id' => $this->id,
            'required' => $this->required,
            'attributes' => $this->getAttributes(array_merge([
                'required' => $this->required,
                'readonly' => $this->readonly,
                'disabled' => $this->disabled,
                'class' => 'form-control ' . $this->class,
                'id' => $this->id,
                'placeholder' => $this->placeholder,
                'name' => $this->name ?? $this->id,
                'rows' => $this->rows,
                'cols' => $this->cols

            ], $this->attributes)),
            'validationSuccessMessage' => $this->validationSuccessMessage,
            'validationErrorMessage' => $this->validationErrorMessage,
            'value' => $this->value,
            'help' => $this->help,
        ];
        return view(
            viewOverview("Views\\Partials\\FormsElements\\Textarea"),
            [
                'formElement' => $formElement,
            ]
        );
    }

    public function setRows(int $rows): self
    {
        $this->rows = $rows;
        return $this;
    }

    public function setCols(int $cols): self
    {
        $this->cols = $cols;
        return $this;
    }
}
