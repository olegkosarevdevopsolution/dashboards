<?php

namespace DevOlegKosarev\Dashboard\Services\Forms;

/**
 * FormGenerator
 *
 * This class is responsible for generating a form.
 *
 * @package DevOlegKosarev\Dashboard\Services\Forms;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 */
class FormGenerator
{
    /**
     * @var array $formElements The elements of the form.
     */
    private array $formElements;

    /**
     * @var string $formAction The action attribute of the form.
     */
    private string $formAction;

    /**
     * @var string $formMethod The method attribute of the form.
     */
    private string $formMethod;

    /**
     * @var string $formName The name attribute of the form.
     */
    private string $formName;

    /**
     * Set the elements of the form.
     *
     * @param array $elements The elements of the form.
     * @return FormGenerator
     */
    public function setElements($elements): self
    {
        $this->formElements = $elements;
        return $this;
    }

    /**
     * Set the name attribute of the form.
     *
     * @param string $name The name attribute of the form.
     * @return FormGenerator
     */
    public function setName($name): self
    {
        $this->formName = $name;
        return $this;
    }

    /**
     * Set the method attribute of the form.
     *
     * @param string $method The method attribute of the form.
     * @return FormGenerator
     */
    public function setMethod($method): self
    {
        $this->formMethod = $method;
        return $this;
    }

    /**
     * Set the action attribute of the form.
     *
     * @param string $action The action attribute of the form.
     * @return FormGenerator
     */
    public function setAction($action): self
    {
        $this->formAction = $action;
        return $this;
    }

    /**
     * Generate the form HTML.
     *
     * @return string The generated form HTML.
     */
    public function generate(): string
    {
        return view(
            viewOverview("Views\\Partials\\FormsElements\\Form"),
            [
                'elements' => implode("", $this->formElements),
                'name' => $this->formName,
                'action' => $this->formAction,
                'method' => $this->formMethod,
            ]
        );
    }
}
