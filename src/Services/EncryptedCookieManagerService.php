<?php

namespace Devolegkosarev\Dashboard\Services;

use Devolegkosarev\Dashboard\Interfaces\CookieManagerInterface;

/**
 * Class CookieManager
 *
 * @package \Devolegkosarev\Dashboard\Services
 */
class EncryptedCookieManagerService implements CookieManagerInterface
{
    private const IV_LENGTH = 16;

    /**
     * @var array The cookie configuration.
     */
    protected $cookieConfig;
    protected $defaultCookieConfig;
    protected $cookieEncryptionKey;


    /**
     * Constructor for the class.
     *
     * @param string $baseURL The base URL.
     * @param string $configCookiekey The cookie key for configuration.
     */
    public function __construct(string $baseURL, object $cookie)
    {
        if (empty($cookie->cookieEncryptionKey)) {
            throw new \InvalidArgumentException("Encryption key is empty. Please set it app\Config\Cookie. example: 'cookieEncryptionKey' => '" . bin2hex(random_bytes(16)) . "'", 500);
        }

        // Set the configuration cookie key.
        $this->cookieEncryptionKey  = $cookie->cookieEncryptionKey;

        // Parse the base URL.
        $parsedUrl = parse_url($baseURL);

        // Load the cookie helper.
        helper('cookie');

        // Set the default cookie configuration.
        $this->defaultCookieConfig = [
            'expires' => ((int) $cookie->expires <= 0) ? 3600 : $cookie->expires,
            'domain' => ($cookie->domain === '') ? $parsedUrl['host'] : $cookie->prefix,
            'path' => ($cookie->path === '') ? '/' : $cookie->path,
            'prefix' => ($cookie->prefix === '') ? 'auth_' : $cookie->prefix,
            'secure' => $cookie->secure,
            'httponly' => $cookie->httponly,
            'samesite' => ($cookie->samesite === '') ? 'Lax' : $cookie->samesite,
        ];

        // Set the cookie configuration.
        $this->cookieConfig = $this->defaultCookieConfig;
    }

    /**
     * Set the configuration for the cookie.
     *
     * @param array $cookieConfig The configuration for the cookie. Defaults to an empty array.
     * @return void
     */
    public function setCookieConfig(array $cookieConfig = []): void
    {
        $this->cookieConfig = array_merge($this->defaultCookieConfig, $cookieConfig);
    }

    /**
     * Signs the provided data using AES-256-CBC encryption and returns the encrypted data with a signature.
     *
     * @param string $data The data to be signed.
     * @return string The encrypted data with signature.
     */
    protected function signData(string $data): string
    {
        // Generate a random initialization vector (IV)
        $iv = $this->generateIV(self::IV_LENGTH);

        // Encrypt the data using AES-256-CBC encryption
        $encryptedData = openssl_encrypt($data, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);

        // Generate the signature using HMAC-SHA256 algorithm
        $signature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Combine the IV, encrypted data, and signature into a single string
        $encryptedDataWithSignature = base64_encode($iv . $encryptedData . $signature);

        // Return the encrypted data with signature
        return $encryptedDataWithSignature;
    }

    /**
     * Generate a random initialization vector (IV).
     *
     * @param int $length The length of the IV.
     * @return string The generated IV.
     */
    protected function generateIV(int $length): string
    {
        return openssl_random_pseudo_bytes($length);
    }

    /**
     * Generate a random initialization vector (IV).
     *
     * @param int $length The length of the IV.
     * @param string $data The data to be signed.
     * @return string The generated IV.
     */
    protected function generateIVFromData(string $data, int $length, int $offset = 0): string
    {
        return substr($data, $offset, $length);
    }

    protected function extractDataFromEncryptedDataWithSignature(string $encryptedDataWithSignatureDecoded): array
    {
        $iv = $this->generateIVFromData($encryptedDataWithSignatureDecoded, self::IV_LENGTH);
        $encryptedData = substr($encryptedDataWithSignatureDecoded, self::IV_LENGTH, -64);
        $signature = substr($encryptedDataWithSignatureDecoded, -64);

        return array($iv, $encryptedData, $signature);
    }

    /**
     * Set a cookie.
     *
     * @param string $name The name of the cookie.
     * @param string $value The value of the cookie.
     *
     * @return void
     */
    public function setCookie(string $name, string $value): void
    {
        set_cookie(
            $name,
            $this->signData($value),
            $this->cookieConfig['expires'],
            $this->cookieConfig['domain'],
            $this->cookieConfig['path'],
            $this->cookieConfig['prefix'],
            $this->cookieConfig['secure'],
            $this->cookieConfig['httponly'],
            $this->cookieConfig['samesite']
        );
    }

    /**
     * Prepare a cookie. 
     * @param string $name The name of the cookie.
     * @param string $value The value of the cookie.
     * @return string
     */
    public function prepareCookie(string $name, string $value): string
    {
        $cookieConfig = $this->cookieConfig;
        $cookieConfig['prefix'] = '';
        $cookie = sprintf(
            '%s=%s; expires=%s; domain=%s; path=%s;',
            $name,
            $this->signData($value),
            $cookieConfig['expires'],
            $cookieConfig['domain'],
            $cookieConfig['path'],
        );
        return $cookie;
    }

    /**
     * Set the user ID cookie.
     *
     * @param string $userId The user ID.
     * @return void
     */
    public function setUserId(string $userId): void
    {
        $this->setCookie('userId', $userId);
    }

    /**
     * Set the user name cookie.
     *
     * @param string $userName The user name.
     * @return void
     */
    public function setUserName(string $userName): void
    {
        $this->setCookie('userName', $userName);
    }

    /**
     * Set the user last name cookie.
     *
     * @param string $userLastName The user last name.
     * @return void
     */
    public function setUserLastName(string $userLastName): void
    {
        $this->setCookie('userLastName', $userLastName);
    }

    /**
     * Set the user second name cookie.
     *
     * @param string $userSecondName The user second name.
     * @return void
     */
    public function setUserSecondName(string $userSecondName): void
    {
        $this->setCookie('userSecondName', $userSecondName);
    }

    /**
     * Set the user email cookie.
     *
     * @param string $userEmail The user email.
     * @return void
     */
    public function setUserEmail(string $userEmail): void
    {
        $this->setCookie('userEmail', $userEmail);
    }

    /**
     * Set the user photo cookie.
     *
     * @param string $userPhoto The user photo.
     * @return void
     */
    public function setUserAvatar(string $userPhoto): void
    {
        $this->setCookie('userPhoto', $userPhoto);
    }

    public function setUserRolesId(string $role): void
    {
        $this->setCookie('userRole', $role);
    }

    /**
     * Set the language.
     * @param string $language The language default is en
     * @return void
     */
    public function setLanguage(string $language): void
    {
        $this->setCookie('language', $language);
    }


    /**
     * Get the user ID from the cookie.
     *
     * @return int The user ID or 0 if the cookie is not set.
     */
    public function getUserId(): int
    {
        // Get the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("userId", false, $this->defaultCookieConfig["prefix"]);

        // If the encrypted data is not set, return 0
        if ($encryptedDataWithSignature == null) {
            return 0;
        }

        // Decode the encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);


        $iv = $this->generateIVFromData($encryptedDataWithSignatureDecoded, self::IV_LENGTH);
        $encryptedData = substr($encryptedDataWithSignatureDecoded, self::IV_LENGTH, -64);
        $signature = substr($encryptedDataWithSignatureDecoded, -64);

        // Calculate the signature using the encrypted data and the key
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the calculated signature matches the provided signature
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // If the signature is valid, decrypt the encrypted data and return the user ID
        if ($isValidSignature == true) {
            return (int) openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // If the signature is not valid, return null
        return 0;
    }

    /**
     * Get the user name from the cookie.
     *
     * @return string|null The user name or null if not set.
     */
    public function getUserName(): ?string
    {
        // Get the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("userName", false, $this->defaultCookieConfig["prefix"]);

        // Decode the encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);

        // Calculate the signature using HMAC-SHA256
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the signature is valid
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // Decrypt the encrypted data if the signature is valid
        if ($isValidSignature) {
            return openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // Return null if the signature is not valid
        return null;
    }

    /**
     * Get the user last name from the cookie.
     *
     * @return string|null The user last name or null if not set.
     */
    public function getUserLastName(): ?string
    {
        // Get the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("userLastName", false, $this->defaultCookieConfig["prefix"]);

        // Decode the base64 encoded encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);

        // Calculate the signature using HMAC-SHA256
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the calculated signature matches the provided signature
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // If the signature is valid, decrypt the encrypted data using AES-256-CBC
        if ($isValidSignature) {
            return openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // If the signature is not valid, return null
        return null;
    }

    /**
     * Get the user second name from the cookie.
     *
     * @return string|null The user second name or null if not set.
     */
    public function getUserSecondName(): ?string
    {
        // Get the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("userSecondName", false, $this->defaultCookieConfig["prefix"]);

        // Decode the encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);

        // Calculate the signature using HMAC-SHA256
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the calculated signature matches the provided signature
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // If the signature is valid, decrypt the encrypted data using AES-256-CBC
        if ($isValidSignature == true) {
            return openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // If the signature is not valid, return null
        return null;
    }


    /**
     * Get the user email from the encrypted cookie.
     *
     * @return string|null|false The user email if valid, otherwise null.
     */
    public function getUserEmail(): ?string
    {
        // Get the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("userEmail", false, $this->defaultCookieConfig["prefix"]);

        // Decode the encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);

        // Calculate the signature for the decrypted data
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the calculated signature matches the extracted signature
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // If the signature is valid, decrypt and return the user email
        if ($isValidSignature == true) {
            return openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // Return null if the signature is not valid
        return null;
    }


    /**
     * Retrieves the user's photo from the encrypted cookie.
     *
     * @return string|null The user's photo if it exists, null otherwise.
     */
    public function getUserAvatar(): ?string
    {
        // Retrieve the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("userPhoto", false, $this->defaultCookieConfig["prefix"]);

        if ($encryptedDataWithSignature == null) {
            return null;
        }

        // Decode the encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);

        // Calculate the signature based on the encrypted data
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the calculated signature matches the extracted signature
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // If the signature is valid, decrypt the encrypted data using the key and IV
        if ($isValidSignature == true) {
            return openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // Return null if the signature is not valid
        return null;
    }

    /**
     * Retrieves the user role ID from the encrypted userRole cookie.
     *
     * @return int The user role ID.
     */
    public function getUserRolesId(): int
    {
        // Get the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("userRole", false, $this->defaultCookieConfig["prefix"]);

        // If the encrypted data with signature is not found, return default role ID
        if ($encryptedDataWithSignature == null) {
            return 1;
        }

        // Decode the encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);

        // Calculate the signature using the encrypted data and the secret key
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the calculated signature matches the extracted signature
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // If the signature is valid, decrypt and return the user role ID
        if ($isValidSignature) {
            return (int) openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // Return default role ID if the signature is invalid
        return 1;
    }

    /**
     * Get the language from the cookie.
     * If the language is not set in the cookie, it sets the default language to 'en'.
     *
     * @return string The language.
     */
    public function getLanguage(): string
    {
        // Get the encrypted data with signature from the cookie
        $encryptedDataWithSignature = get_cookie("language", false, '');

        // If the encrypted data with signature is not set
        if (!$encryptedDataWithSignature) {
            // Set the language to 'en'
            $language = 'en';
            // Update the cookie configuration
            $this->cookieConfig['prefix'] = '';
            $this->cookieConfig['expires'] = strtotime('+1 year');
            // Set the language cookie
            $this->setCookie('language', $language);
            // Return the language
            return $language;
        }

        // Decode the encrypted data with signature
        $encryptedDataWithSignatureDecoded = base64_decode($encryptedDataWithSignature);

        // Extract the initialization vector (IV), encrypted data, and signature from the decoded data
        list($iv, $encryptedData, $signature) = $this->extractDataFromEncryptedDataWithSignature($encryptedDataWithSignatureDecoded);

        // Calculate the signature using HMAC-SHA256 and the encryption key
        $calculatedSignature = hash_hmac('sha256', $encryptedData, $this->cookieEncryptionKey);

        // Check if the calculated signature matches the stored signature
        $isValidSignature = hash_equals($signature, $calculatedSignature);

        // If the signature is valid
        if ($isValidSignature) {
            // Decrypt the encrypted data using AES-256-CBC and the encryption key
            return openssl_decrypt($encryptedData, 'AES-256-CBC', $this->cookieEncryptionKey, 0, $iv);
        }

        // If the signature is not valid, return the default language 'en'
        return 'en';
    }

    /**
     * Deletes a cookie by name.
     *
     * @param string $name The name of the cookie to delete.
     * @param string $domain The domain of the cookie (default: 'partners.datamarket.lv').
     * @param string $prefix The prefix of the cookie (default: 'auth_b24_').
     * @return bool Returns true if the cookie was successfully deleted, false otherwise.
     */
    function deleteCookieByName($name, $domain = '', $prefix = '')
    {
        // Call the delete_cookie function with the specified parameters
        return delete_cookie($name, $domain, '/', $prefix);
    }

    public function deleteAuthCookie()
    {
        $this->deleteCookieByName('userId', $this->defaultCookieConfig["domain"], $this->defaultCookieConfig["prefix"]);
        $this->deleteCookieByName('userRole', $this->defaultCookieConfig["domain"], $this->defaultCookieConfig["prefix"]);
        $this->deleteCookieByName('userEmail', $this->defaultCookieConfig["domain"], $this->defaultCookieConfig["prefix"]);
        return true;
    }


    /**
     * Delete all cookies.
     *
     * @return void
     */
    public function deleteAllCookies(): void
    {
        throw new \Exception('WIP delete all cookies');
    }
}
