<?php

namespace Devolegkosarev\Dashboard\Services;

use Devolegkosarev\Dashboard\Services\FormElementService;

/**
 * FormService
 *
 * This class provides methods for retrieving the HTML representation of form elements.
 *
 * @package \Devolegkosarev\Dashboard\Services;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class FormService
{
    private $formElementService;
    /**
     * Constructs a new instance of the class.
     *
     * @param FormElementService $formElementService The form element service.
     */
    public function __construct(FormElementService $formElementService)
    {
        $this->formElementService = $formElementService;
    }

    /**
     * Generates a function comment for the given function body.
     *
     * @param array $formData the form data
     * @return string the generated HTML
     */
    public function preRenderFrom(array $formData = [], array $languages = []): string
    {
        $html = '';
        $languageTab = '';
        foreach ($formData["generalTab"]["formElements"] as $elementData) {
            $html .= $this->getFormElementHtml($elementData);
        }
        foreach ($formData["generalTab"]["languageTab"] as $elementData) {
            $languageTab .= $this->getFormElementHtml($elementData);
        }

        return view(
            viewOverview(
                "Views\\Partials\\Templates\\FormEditCreate"
            ),
            [
                'content' => [
                    'generalTab' => [
                        'formElements' => $html,
                    ],
                    'languageTab' => [
                        'formElements' => $html,
                    ]
                ],
                'languages' => $languages
            ]
        );
    }


    /**
     * Generate the function comment for the translatedTab function.
     *
     * @param array $formElements The array of form elements.
     * @param array $languages The array of languages.
     * @return string The generated HTML.
     */
    public function translatedTab(array $formElements = [], array $languages = []): string
    {
        $html = '';
        foreach ($languages as $language) {
            $languageCode = $language["code"];
            $html .= '<div class="tab-pane fade translated-tab" data-language="' . $languageCode . '" id="language' . ucfirst($languageCode) . 'Tab" role="tabpanel">';
            $formElementsLanguage = $formElements[$languageCode];
            foreach ($formElementsLanguage as $formElement) {
                $formElement['attributes']['id'] = isset($formElement['attributes']['id']) ? $formElement['attributes']['id'] . strtoupper($language["code"]) : '';
                $formElement['attributes']['name'] = isset($formElement['attributes']['name']) ? $formElement['attributes']['name'] . strtoupper($language["code"]) : '';

                $html .= $this->getFormElementHtml($formElement);
            }
            $html .= '</div>';
        }
        return $html;
    }

    /**
     * Retrieves the HTML representation of a form element.
     *
     * @param array $elementData The data of the form element.
     * @return string The HTML representation of the form element.
     */
    public function getFormElementHtml(array $elementData = []): string
    {
        $type = $elementData['type'];
        $generateMethod = 'generate' . ucfirst($type);
        if (method_exists($this->formElementService, $generateMethod)) {
            return $this->formElementService->$generateMethod($elementData);
        } else {
            throw new \Exception("Invalid form element type: $type");
        }

        return '';
    }
}
