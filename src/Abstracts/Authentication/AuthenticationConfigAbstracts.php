<?php

namespace Devolegkosarev\Dashboard\Abstracts\Authentication;

use CodeIgniter\Config\BaseConfig;
use Devolegkosarev\Dashboard\Interfaces\Authentication\AuthenticationConfigInterface;

/**
 * AuthenticationConfigAbstarcts
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Abstracts\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

abstract class AuthenticationConfigAbstracts extends BaseConfig implements AuthenticationConfigInterface
{
    public function getAuthenticationConfig()
    {
        $authenticationConfig = config("AuthenticationConfig");
        if ($authenticationConfig == null) {
            $authenticationConfig = config("BaseAuthenticationConfig");
        }

        // var_dump($authenticationConfig);
        // exit();
        return $authenticationConfig;
    }
}
