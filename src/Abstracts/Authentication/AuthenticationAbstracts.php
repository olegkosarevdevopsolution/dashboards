<?php

namespace Devolegkosarev\Dashboard\Abstracts\Authentication;

use Config\Services;
use Devolegkosarev\Dashboard\Interfaces\Authentication\AuthenticationInterface;

/**
 * AuthenticationAbstracts
 *
 * description
 *
 * namespace \Devolegkosarev\Dashboard\Abstracts\Authentication;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
abstract class AuthenticationAbstracts implements AuthenticationInterface
{
    /**
     * Cookie manager service
     * 
     * @var \Devolegkosarev\Dashboard\Services\EncryptedCookieManagerService
     */
    protected $encryptedCookieManagerService;

    /**
     * Roles service
     * 
     * @var \Devolegkosarev\Dashboard\Services\RolesServices
     */
    protected $rolesService;
    function __construct()
    {
        $this->encryptedCookieManagerService = Services::encryptedCookieManagerService();

        $this->rolesService = Services::rolesService();
    }
    protected $loggedIn = false;
    protected $currentUser = null;

    /**
     * Set the necessary cookies for the user.
     *
     * @param array $user The user data.
     */
    protected function setCookies($user)
    {
        // Set the user email cookie
        $this->encryptedCookieManagerService->setUserEmail($user['email']);

        // Set the user roles ID cookie
        $this->encryptedCookieManagerService->setUserRolesId($user['role_id']);

        // Set the user ID cookie
        $this->encryptedCookieManagerService->setUserId($user['user_id']);

        // Set the user Avatar cookie
        $this->encryptedCookieManagerService->setUserAvatar($user['avatar']);
    }

    public function checkAccess($roleId, $permission): bool
    {
        if ($this->isLoggedIn() == false) {
            $roleId = 1;
        }
        return $this->rolesService->hasPermission($roleId, $permission);
    }

    /**
     * Check if the user is logged in.
     *
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        // Get user ID, role ID, and email from encrypted cookie manager service
        $getUserId = (int) $this->encryptedCookieManagerService->getUserId();
        $getUserRole = (int) $this->encryptedCookieManagerService->getUserRolesId();
        $getUserEmail = $this->encryptedCookieManagerService->getUserEmail();
        $getUserAvator = $this->encryptedCookieManagerService->getUserAvatar();
        $session = session();

        if (
            (is_integer($getUserId) && $getUserId >= 0 &&
                is_integer($getUserRole) && $getUserRole >= 0 &&
                is_string($getUserEmail)) == null &&
            is_string($getUserAvator) == null
        ) {
            $this->encryptedCookieManagerService->deleteAuthCookie();
            $session->setFlashdata("error", '🍪 Sorry, we couldn\'t find or validate your cookies. Cookies may have been modified or damaged. Please try logging in again.', 'title', 'Fatal Error');
            return false;
        }
        // Get user information
        $this->getUser($getUserId);
        // var_dump($this->currentUser);
        // exit();
        // Check if any of the user information matches the current user
        if (
            ($this->currentUser['id'] == $getUserId &&
                $this->currentUser['role_id'] == $getUserRole &&
                $this->currentUser['email'] == $getUserEmail &&
                $this->currentUser['avatar'] == $getUserAvator
            ) == true
        ) {
            // User is logged in
            return true;
        }

        // Delete authentication cookie if user is not logged in OR if cookie is not valid
        $this->encryptedCookieManagerService->deleteAuthCookie();
        $session->setFlashdata("error", '🍪 Sorry, we were able to find and validate your cookies, but they do not match the expected cookie data. Please try clearing your browser\'s cookies and logging in again.', 'title', 'Fatal Error');

        // User is not logged in
        return false;
    }
}
