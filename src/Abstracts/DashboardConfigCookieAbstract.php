<?php

namespace Devolegkosarev\Dashboard\Abstracts;

use CodeIgniter\Config\BaseConfig;
use Devolegkosarev\Dashboard\Interfaces\DashboardConfigCookieInterface;

/**
 * BaseDashboardConfigCookie
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Abstracts;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

abstract class DashboardConfigCookieAbstract extends BaseConfig implements DashboardConfigCookieInterface
{
    public function getDashboardConfigCookie()
    {
        $dashboardConfigCookie = config("DasboardCookie");
        if ($dashboardConfigCookie == null) {
            $dashboardConfigCookie = config("BaseDasboardCookie");
        }

        return $dashboardConfigCookie;
    }
}
