<?php

namespace Devolegkosarev\Dashboard\Abstracts\Forms;

use Devolegkosarev\Dashboard\Interfaces\Forms\FormElementInterface;

/**
 * AbstractFormElement
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Abstracts\Forms;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

abstract class AbstractFormElement implements FormElementInterface
{
    protected ?string $help = null;
    protected bool $required = false;
    protected ?string $label = null;
    protected ?string $labelClass = null;
    protected ?string $placeholder = null;
    protected array $attributes = [];
    protected bool $disabled = false;
    protected bool $readonly = false;
    protected ?string $validationSuccessMessage = null;
    protected ?string $validationErrorMessage = null;
    protected ?string $id = null;
    protected ?string $class = null;
    protected ?string $name = null;
    protected ?string $value = null;

    /**
     * Set the help message for the form element.
     *
     * @param string $message The help message.
     * @return self
     */
    public function setHelp(string $message): self
    {
        $this->help = $message;
        return $this;
    }

    /**
     * Set the label for the form element.
     *
     * @param string $label The label text.
     * @return self
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;
        return $this;
    }

    public function getLabel(): string
    {
        if ($this->required == true) {
            $this->label .= ' <span class="text-danger">*</span>';
        }
        return $this->label;
    }

    /**
     * Set the CSS class for the form element's label.
     *
     * @param string $label The label class.
     * @return self
     */
    public function setLabelClass(string $label): self
    {
        $this->labelClass = $label;
        return $this;
    }

    /**
     * Set whether the form element is required.
     *
     * @param bool $required Whether the form element is required.
     * @return self
     */
    public function setRequired(bool $required): self
    {
        $this->required = $required;
        return $this;
    }

    /**
     * Set the placeholder text for the form element.
     *
     * @param string $placeholder The placeholder text.
     * @return self
     */
    public function setPlaceholder(string $placeholder): self
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * Set the attributes for the form element, excluding certain reserved attributes.
     *
     * @param array $attributes The attributes.
     * @return self
     *
     * The following attributes are excluded: 'class', 'name', 'id', 'readonly', 'disabled', 'required', 'placeholder'.
     */
    public function setAttributes(array $attributes): self
    {
        $this->attributes = array_diff_key($attributes, array_flip(['class', 'name', 'id', 'readonly', 'disabled', 'required', 'placeholder', 'value']));
        return $this;
    }

    /**
     * Set whether the form element is disabled.
     *
     * @param bool $disabled Whether the form element is disabled.
     * @return self
     */
    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;
        return $this;
    }

    /**
     * Set whether the form element is readonly.
     *
     * @param bool $readonly Whether the form element is readonly.
     * @return self
     */
    public function setReadonly(bool $readonly): self
    {
        $this->readonly = $readonly;
        return $this;
    }

    /**
     * Set the validation success message for the form element.
     *
     * @param string $message The validation success message.
     * @return self
     */
    public function setValidationSuccessMessage(string $message): self
    {
        $this->validationSuccessMessage = $message;
        return $this;
    }

    /**
     * Set the validation error message for the form element.
     *
     * @param string $message The validation error message.
     * @return self
     */
    public function setValidationErrorMessage(string $message): self
    {
        $this->validationErrorMessage = $message;
        return $this;
    }

    /**
     * Set the ID attribute for the form element.
     *
     * @param string $id The ID attribute.
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set the CSS class for the form element.
     *
     * @param string $class The CSS class.
     * @return self
     */
    public function setClass(string $class): self
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Set the name attribute for the form element.
     *
     * @param string $name The name attribute.
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }

    public function getAttributes(array $formElement): string
    {
        return implode(
            " ",
            array_map(function ($value, $key) {
                if ($key === "required") {
                    return 'required';
                } elseif ($key === "readonly") {
                    return 'readonly';
                } elseif ($key === "disabled") {
                    return 'disabled';
                } elseif ($key == 'help') {
                    return 'aria-describedby="' . $this->id . "HelpBlock" . '"';
                } else {
                    if (
                        (is_array($key) or is_array($value)) or
                        (is_object($key) or is_object($value))
                    ) {
                        var_dump($key, $value);
                        exit();
                    }

                    return $key . '="' . $value . '"';
                }
            }, $formElement, array_keys($formElement))
        );
    }
}
