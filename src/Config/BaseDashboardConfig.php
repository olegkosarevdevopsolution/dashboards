<?php

namespace Devolegkosarev\Dashboard\Config;

use Devolegkosarev\Dashboard\Abstracts\DashboardConfigAbstract;

/**
 * BaseDashboardConfig
 *
 * Dashboard Base configuration 
 *
 * @package \Devolegkosarev\Dashboard\Config;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 */
class BaseDashboardConfig extends DashboardConfigAbstract
{
    /**
     * The project name.
     *
     * @var string
     */
    public string $project = 'Appstarter-Dashboard';

    /**
     * The company name.
     *
     * @var string
     */
    public string $company = 'OlegKosarevDevOpsolution';

    /**
     * The base version.
     *
     * @var string
     */
    public string $versionBase = '1.1.0-dev';

    /**
     * The project version.
     *
     * @var string
     */
    public string $versionProject = '';

    /**
     * The author name.
     *
     * @var string
     */
    public string $author = 'Oleg Kosarev';

    /**
     * The theme name.
     *
     * @var string
     */
    public string $theme = 'theme-cyan';

    /**
     * The default avatar path.
     *
     * @var string
     */
    public string $avatarDefault = '_admin/img/user/profile.jpg';

    /**
     * The background cover path.
     *
     * @var string
     */
    public string $bgCover  = '';

    /**
     * The dark logo path.
     *
     * @var string
     */
    public string $logoDark = '';

    /**
     * The light logo path.
     *
     * @var string
     */
    public string $logoLight = '';

    /**
     * The favicon path.
     *
     * @var string
     */
    public string $favicon = '_admin/img/favicon.ico';

    /**
     * An array of stylesheet paths.
     *
     * @var array
     */
    public array $stylesheets = [];

    /**
     * An array of JavaScript paths.
     *
     * @var array
     */
    public array $javascript = [];
}
