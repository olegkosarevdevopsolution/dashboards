<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Documentation\ReleaseNotes;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService;
use Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes\ReleaseNotesModel;

/**
 * ReleaseNotesServiceProvaider
 *
 * This class is a service provider for the ReleaseNoteService.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Documentation\ReleaseNotes;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService
 * @see Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes\ReleaseNotesModel
 * 
 */
class ReleaseNotesServiceProvider extends BaseService
{
    /**
     * Get the ReleaseNoteService instance.
     *
     * @return ReleaseNotesService - An instance of the ReleaseNoteService.
     */
    public static function releaseNotesService(): ReleaseNotesService
    {
        // Create a new instance of ReleaseNoteService with a ReleaseNoteModel instance.
        return new ReleaseNotesService(new ReleaseNotesModel());
    }
}
