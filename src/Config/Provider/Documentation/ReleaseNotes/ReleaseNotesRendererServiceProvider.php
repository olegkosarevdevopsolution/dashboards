<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Documentation\ReleaseNotes;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesRendererService;

/**
 * ReleaseNoteServiceProvaider
 *
 * This class is a service provider for the ReleaseNoteService.
 * It extends the BaseService class.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Documentation\ReleaseNotes
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesRendererService
 */
class ReleaseNotesRendererServiceProvider extends BaseService
{
    /**
     * Get the ReleaseNotesRendererService instance.
     *
     * @return ReleaseNotesRendererService - An instance of the ReleaseNotesRendererService.
     */
    public static function releaseNotesRendererService(): ReleaseNotesRendererService
    {
        // Create a new instance of ReleaseNoteService with a ReleaseNoteModel instance.
        return new ReleaseNotesRendererService();
    }
}
