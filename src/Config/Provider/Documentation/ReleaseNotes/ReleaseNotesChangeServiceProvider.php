<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Documentation\ReleaseNotes;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesChangeService;
use Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes\ReleaseNotesChangeModel;

/**
 * ReleaseNotesChangeServiceProvaider
 *
 * This class is a service provider for the ReleaseNoteChangeService.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Documentation\ReleaseNotes;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @see Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesChangeService
 * @see Devolegkosarev\Dashboard\Models\Documentation\ReleaseNotes\ReleaseNotesChangeModel
 */
class ReleaseNotesChangeServiceProvider extends BaseService
{
    /**
     * releaseNoteChangeService
     *
     * Creates a new instance of ReleaseNoteChangeService.
     *
     * @return ReleaseNotesChangeService The created instance of ReleaseNoteChangeService.
     */
    public static function releaseNotesChangeService(): ReleaseNotesChangeService
    {
        return new ReleaseNotesChangeService(new ReleaseNotesChangeModel());
    }
}
