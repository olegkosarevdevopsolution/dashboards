<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Bootstrap;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\Bootstrap\AlertHelperService;


/**
 * AlertHelperService
 *
 * Provides the AlertHelperService instance.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Bootstrap;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class AlertHelperServiceProvider extends BaseService
{
    public static function alertHelperService(): AlertHelperService
    {
        // Create a new AlertHelperService instance and return it as an instance.
        return new AlertHelperService();
    }
}
