<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Menus;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\Menus\MainmenuService;
use Devolegkosarev\Dashboard\Models\Menus\MainmenuModel;
use Devolegkosarev\Dashboard\Models\Menus\MainmenuTranslationsModel;

/**
 * Class MainmenuServiceProvider
 * Provides the MainmenuService instance.
 */
class MainmenuServiceProvider extends BaseService
{
    /**
     * Returns an instance of MainmenuService.
     *
     * @return MainmenuService
     */
    public static function mainmenuService(): MainmenuService
    {
        // Create a new MainmenuService instance with a MainmenuModel instance.
        return new MainmenuService(new MainmenuModel(), new MainmenuTranslationsModel);
    }
}
