<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Menus;

use Config\Services;
use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\Menus\MenusRendererService;

class MenuRendererServiceProvider extends BaseService
{


    public static function menuRendererService(): MenusRendererService
    {
        $formService = Services::formService();
        $roleService = Services::rolesService();
        $mainmenuService = Services::mainmenuService();

        return new MenusRendererService($formService, $roleService, $mainmenuService);;
    }
}
