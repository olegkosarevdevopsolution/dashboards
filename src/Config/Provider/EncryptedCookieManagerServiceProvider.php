<?php

namespace Devolegkosarev\Dashboard\Config\Provider;

use CodeIgniter\Config\BaseService;
use Config\Services;
use
    Devolegkosarev\Dashboard\Services\EncryptedCookieManagerService;

/**
 * CookieManagerServiceProvider
 *
 * Provides the CookieManagerService instance.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class EncryptedCookieManagerServiceProvider extends BaseService
{
    /**
     * Get the CookieManagerService instance.
     *
     * @return EncryptedCookieManagerService 
     */
    public static function encryptedCookieManagerService(): EncryptedCookieManagerService
    {
        return new EncryptedCookieManagerService(config('App')->baseURL, config('Cookie'));
    }
}
