<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Users;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Models\Users\UsersModel;
use Devolegkosarev\Dashboard\Services\Users\PasswordService;
use Devolegkosarev\Dashboard\Services\Users\UsersService;

/**
 * UsersServiceProvider
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class UsersServiceProvider extends BaseService
{
    public static function usersService(): UsersService
    {
        $getAuthenticationConfig = config("AuthenticationConfig")->getAuthenticationConfig();
        $getDashboardConfig = config("BaseDashboardConfig")->getDashboardConfig();
        return new UsersService(new UsersModel(), new PasswordService($getAuthenticationConfig->passwordEncryptionKey), $getDashboardConfig);
    }
}
