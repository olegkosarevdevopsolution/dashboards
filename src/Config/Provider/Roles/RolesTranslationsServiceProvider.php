<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Roles;

use Devolegkosarev\Dashboard\Models\Roles\RolesTranslationsModel;
use Devolegkosarev\Dashboard\Services\Roles\RolesTranslationsService;
use CodeIgniter\Config\BaseService;

/**
 * RoleTranslationServiceProvider
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class RolesTranslationsServiceProvider extends BaseService
{
    public static function rolesTranslationService(): RolesTranslationsService
    {
        return new RolesTranslationsService(new RolesTranslationsModel());
    }
}
