<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Roles;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Models\Roles\RolesModel;
use Devolegkosarev\Dashboard\Models\Roles\RolesTranslationsModel;
use Devolegkosarev\Dashboard\Services\Roles\RolesService;

/**
 * RolesServiceProvider
 *
 * Provides the RolesService instance.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class RolesServiceProvider extends BaseService
{
    /**
     * Returns an instance of the RolesService class.
     *
     * @return RolesService
     */
    public static function rolesService(): RolesService
    {
        return new RolesService(new RolesModel(), new RolesTranslationsModel());
    }
}
