<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Roles;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\FormService;
use Devolegkosarev\Dashboard\Services\Roles\RolesRendererService;
use Config\Services;

/**
 * RolesRendererServiceProvider
 *
 * Provides the RolesRendererService instance.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class RolesRendererServiceProvider extends BaseService
{
    public static function rolesRendererService(): RolesRendererService
    {
        // Create a new instance of RolesRendererService
        return new RolesRendererService(Services::FormService());
    }
}
