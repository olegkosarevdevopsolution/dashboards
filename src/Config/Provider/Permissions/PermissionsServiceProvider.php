<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Permissions;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Models\Permissions\PermissionsModel;
use Devolegkosarev\Dashboard\Services\Permissions\PermissionsService;

/**
 * Class PermissionsServiceProvider
 *
 * This class is responsible for providing the PermissionsService instance.
 */
class PermissionsServiceProvider extends BaseService
{
    /**
     * Get the PermissionsService instance.
     *
     * @return PermissionsService The PermissionsService instance.
     */
    public static function permissionsService(): PermissionsService
    {
        // Create a new PermissionsService instance with a PermissionsModel instance.
        return new PermissionsService(new PermissionsModel());
    }
}
