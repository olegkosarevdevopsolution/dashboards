<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Permissions;

use Config\Services;
use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\Permissions\PremissionsRendererService;

/**
 * PremissionsRendererServiceProvider
 *
 * This file contains the PremissionsRendererServiceProvider class, which is responsible for rendering permissions.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class PremissionsRendererServiceProvider extends BaseService
{
    public static function premissionsRendererService(): PremissionsRendererService
    {

        // Create a new instance of PermissionsRendererService
        return new PremissionsRendererService(Services::formService());
    }
}
