<?php

namespace Devolegkosarev\Dashboard\Config\Provider\Permissions;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Models\Permissions\PermissionsTranslationsModel;
use Devolegkosarev\Dashboard\Services\Permissions\PermissionsTranslationsService;

/**
 * PermissionsTranslationsServiceProvider
 *
 * description
 *
 * @package DevOlegKosarev\Dashboard\Config\Provider\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class PermissionsTranslationsServiceProvider extends BaseService
{

    /**
     * Creates a new instance of the PermissionsTranslationsService class.
     *
     * @return PermissionsTranslationsService A new instance of the PermissionsTranslationsService class.
     */
    public static function permissionsTranslationsService(): PermissionsTranslationsService
    {
        return new PermissionsTranslationsService(new PermissionsTranslationsModel());
    }
}
