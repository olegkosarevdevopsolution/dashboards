<?php

namespace Devolegkosarev\Dashboard\Config\Provider;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Services\LanguageService;

/**
 * LanguageServiceProvider
 *
 * Provides the LanguageService instance.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class LanguageServiceProvider extends BaseService
{
    /**
     * languageService method
     * 
     * Creates and returns a new LanguageService instance.
     * 
     * @return LanguageService The LanguageService instance.
     */
    public static function languageService(): LanguageService
    {
        // Create a new LanguageService instance with the supported locales from the App config.
        return new LanguageService(config('App')->supportedLocales);
    }
}
