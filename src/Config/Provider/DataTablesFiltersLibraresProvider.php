<?php

namespace Devolegkosarev\Dashboard\Config\Provider;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares;


/**
 * FormServiceProvider
 *
 * Provides the FormService instance.
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class DataTablesFiltersLibraresProvider extends BaseService
{
    public static function dataTablesFiltersLibrares(): DataTablesFiltersLibrares
    {
        // Create a new FormService instance with the FormElementService instance.
        return new DataTablesFiltersLibrares();
    }
}
