<?php

namespace Devolegkosarev\Dashboard\Config\Provider;

use CodeIgniter\Config\BaseService;
use Devolegkosarev\Dashboard\Factory\Authentication\AuthenticationFactory;
use Devolegkosarev\Dashboard\Interfaces\Authentication\AuthenticationInterface;

/**
 * AuthenticationFactoryProvider
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Config\Provider;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class AuthenticationFactoryProvider extends BaseService
{
    public static function getAuthenticator(string $authenticatorName = null): AuthenticationInterface
    {
        if (is_null($authenticatorName) || empty($authenticatorName) || !is_string($authenticatorName)) {
            $authenticationConfig = config("AuthenticationConfig")->getAuthenticationConfig();
            $authenticatorName = $authenticationConfig->authenticator;
        }

        $authenticationFactory  = new AuthenticationFactory($authenticatorName);
        return $authenticationFactory->getAuthenticator();
    }
}
