<?php

namespace Devolegkosarev\Dashboard\Config;

use CodeIgniter\Router\RouteCollection;
use Config\Services;

/**
 * @var RouteCollection $routes
 */
$routes = Services::routes();
$routes->get('/', '\Devolegkosarev\Dashboard\Controllers\Home::index');

// Language Switcher
$routes->get('language/(:segment)', '\Devolegkosarev\Dashboard\Controllers\LanguageController::index/$1');

// Authentication , ['filter' => 'test']
$routes->group('authentication', ['namespace' => '\Devolegkosarev\Dashboard\Controllers'], static function ($authenticationRoutes): void {
    // Login
    $authenticationRoutes->get('/', 'Authentication\LoginController::index');
    $authenticationRoutes->get('login', 'Authentication\LoginController::index');
    $authenticationRoutes->post('login', 'Authentication\LoginController::login');
    // Lock Screen
    $authenticationRoutes->get('lock-screen', 'Authentication\LockScreenController::index');
    $authenticationRoutes->post('lock-screen', 'Authentication\LockScreenController::lockScreen');
    // Logout
    $authenticationRoutes->get('logout', 'Authentication\LogoutController::index');
    $authenticationRoutes->post('logout', 'Authentication\LogoutController::logout');
    // Forgot Password
    $authenticationRoutes->get('forgot-password', 'Authentication\PasswordController::index');
    $authenticationRoutes->get('reset-password', 'Authentication\ResetPasswordController::index');
    // Reset Password
    $authenticationRoutes->post('reset-password', 'Authentication\ResetPasswordController::reset');
    $authenticationRoutes->get('register', 'Authentication\RegisterController::index');
    $authenticationRoutes->post('register', 'Authentication\RegisterController::register');
});

// Documentation
$routes->group('documentation', ['namespace' => '\Devolegkosarev\Dashboard\Controllers\Documentation'], static function ($documentationRoutes): void {
    // User Guide
    $documentationRoutes->get('/', 'UserGuideController::index');
    $documentationRoutes->group('user-guide', ['namespace' => 'UserGuide'], static function ($userGuide): void {
        $userGuide->get('/', 'UserGuideController::index');
        $userGuide->get('(:segment)', 'UserGuideController::show/$1');
    });

    // Release Notes
    $documentationRoutes->get('release-notes', 'Documentation\ReleaseNotes\ReleaseNotesController::index');
});

// Support
$routes->group('support', ['namespace' => '\Devolegkosarev\Dashboard\Controllers\Support'], static function ($supportRoutes): void {
    $supportRoutes->get('/', 'FeedbackFormController::index');
    $supportRoutes->get('feedback', 'FeedbackFormController::index');
    $supportRoutes->post('feedback', 'FeedbackFormController::sendFeedback');
});

$routes->group('dashboard', ['namespace' => '\Devolegkosarev\Dashboard\Controllers\Dashboard', 'filter' => 'auth:dashboard,view'], static function ($dashboardRoutes) {



    // Dashboard
    $dashboardRoutes->get('/', 'DashboardController::index');

    // Menus
    $dashboardRoutes->group('menus', function ($menuRoutes): void {
        // Menus Items
        $menuRoutes->get('/', 'Menus\MenusItemsController::index');
        // Edit Menus Items
        $menuRoutes->get('edit/(:num)', 'Menus\EditMenusItemsController::edit/$1');
        $menuRoutes->post('edit/(:num)', 'Menus\EditMenusItemsController::update/$1');
        // Add Menus Items
        $menuRoutes->get('add', 'Menus\CreateMenusItemsController::new');
        $menuRoutes->post('add', 'Menus\CreateMenusItemsController::add');
        // Delete Menus Items
        $menuRoutes->get('delete/(:num)', 'Menus\DeleteMenusItemsController::delete/$1');
    });

    // Notifications
    $dashboardRoutes->group("notifications", function ($notificationRoutes): void {
        // Notifications
        $notificationRoutes->get('/', 'Notifications\NotificationController::index');
        // Add Notifications
        $notificationRoutes->get('add/(:num)/(:num)', 'Notifications\CreateNotificationController::new/$1/$2');
        $notificationRoutes->post('add/(:num)/(:num)', 'Notifications\CreateNotificationController::add/$1/$2');
        // Delete Notifications
        $notificationRoutes->get('delete/(:num)', 'Notifications\DeleteNotificationController::delete/$1');
        $notificationRoutes->post('delete/(:num)', 'Notifications\DeleteNotificationController::delete/$1');
    });

    // Permissions
    $dashboardRoutes->group('permissions', function ($permissionsRoutes): void {
        // Permissions
        $permissionsRoutes->get('/', 'Permissions\PermissionsController::index');
        // Edit Permissions
        $permissionsRoutes->get('edit/(:num)', 'Permissions\EditPermissionController::edit/$1');
        $permissionsRoutes->post('edit/(:num)', 'Permissions\EditPermissionController::update/$1');
        // Add Permissions
        $permissionsRoutes->get('add', 'Permissions\CreatePermissionController::new');
        $permissionsRoutes->post('add', 'Permissions\CreatePermissionController::store');
        // Delete Permissions
        $permissionsRoutes->get('delete/(:num)', 'Permissions\DeletePermissionController::delete/$1');
    });

    // Profile
    $dashboardRoutes->group('profile', function ($profileRoutes): void {
        // Profile
        $profileRoutes->get('/', 'Profile\EditProfileController::edit');
        // Edit Profile
        $profileRoutes->get('edit', 'Profile\EditProfileController::edit');
        $profileRoutes->post('edit', 'Profile\EditProfileController::update');
        // Change Password
        $profileRoutes->get('change-password', 'Profile\ChangePasswordController::index');
        $profileRoutes->post('change-password', 'Profile\ChangePasswordController::update');
    });

    // Roles
    $dashboardRoutes->group('roles', function ($rolesRoutes): void {
        // Roles
        $rolesRoutes->get('/', 'Roles\RolesController::index');
        // Edit Roles
        $rolesRoutes->get('edit/(:num)', 'Roles\EditRoleController::edit/$1');
        $rolesRoutes->post('edit/(:num)', 'Roles\EditRoleController::update/$1');

        // publish Roles and unpublish
        $rolesRoutes->get('publish/(:num)', 'Roles\StatusRoleController::publish/$1');
        $rolesRoutes->get('unpublish/(:num)', 'Roles\StatusRoleController::unpublish/$1');

        // $rolesRoutes->get('publish/(:num)', 'Roles\StatusRoleController:publish/$1');
        // $rolesRoutes->get('unpublish/(:num)', 'Roles\StatusRoleController:unpublish/$1');

        // Add Roles
        $rolesRoutes->get('add', 'Roles\CreateRoleController::new');
        $rolesRoutes->post('add', 'Roles\CreateRoleController::store');
        // Delete Roles
        $rolesRoutes->get('delete/(:num)', 'Roles\DeleteRoleController::delete/$1');
    });

    // Users
    $dashboardRoutes->group('users', function ($userRoutes): void {
        // Users
        $userRoutes->get('/', 'Users\UsersController::index');
        // Edit Users
        $userRoutes->get('edit/(:num)', 'Users\EditUserController::index/$1');
        $userRoutes->post('edit/(:num)', 'Users\EditUserController::update/$1');
        // Add Users
        $userRoutes->get('add', 'Users\CreateUserController::index');
        $userRoutes->post('add', 'Users\CreateUserController::store');
        // Delete Users
        $userRoutes->get('delete/(:num)', 'Users\DeleteUserController::delete/$1');
        // Block Users
        $userRoutes->get('block/(:num)', 'Users\BlockUserController::block/$1');
        $userRoutes->get('unblock/(:num)', 'Users\BlockUserController::unblock/$1');
    });

    // Settings
    $dashboardRoutes->group('settings', function ($settingsRoutes): void {
        $settingsRoutes->get('/', 'Settings\EditSettingsController::edit');
        $settingsRoutes->post('/', 'Settings\EditSettingsController::update');
    });

    // Documentation
    $dashboardRoutes->group('documentation', function ($documentationRoutes): void {

        // Release Notes
        $documentationRoutes->group('release-notes', function ($releaseNotes): void {
            // Release Notes Index
            $releaseNotes->get('/', 'Documentation\ReleaseNotes\ReleaseNotesController::index');
            // Release Notes Create
            $releaseNotes->get('add', 'Documentation\ReleaseNotes\CreateReleaseNoteController::add');
            $releaseNotes->post('add', 'Documentation\ReleaseNotes\CreateReleaseNoteController::store');
            // Release Notes Edit
            $releaseNotes->get('edit/(:num)', 'Documentation\ReleaseNotes\EditReleaseNoteController::edit/$1');
            $releaseNotes->post('edit/(:num)', 'Documentation\ReleaseNotes\EditReleaseNoteController::update/$1');
            // Release Notes Toggle Publish Status
            $releaseNotes->get('edit/(:num)/toggle-publish-status/(:num)', 'Documentation\ReleaseNotes\EditReleaseNotesController::togglePublishStatus/$1/$2');
            // Release Notes Delete
            $releaseNotes->get('delete/(:num)', 'Documentation\ReleaseNotes\DeleteReleaseNotesController::delete/$1');
        });

        // User Guide
        $documentationRoutes->group('user-guide', function ($userGuide): void {
            // User Guide Index
            $userGuide->get('/', 'Documentation\UserGuide\UserGuideController::index');
            // User Guide Create
            $userGuide->get('add', 'Documentation\UserGuide\CreateUserGuideController::create');
            $userGuide->post('add', 'Documentation\UserGuide\CreateUserGuideController::store');
            // User Guide Edit
            $userGuide->get('edit/(:num)', 'Documentation\UserGuide\EditUserGuideController::edit/$1');
            $userGuide->post('edit/(:num)', 'Documentation\UserGuide\EditUserGuideController::update/$1');
            // User Guide Delete
            $userGuide->get('delete/(:num)', 'Documentation\UserGuide\DeleteUserGuideController::delete/$1');
        });
    });
});
