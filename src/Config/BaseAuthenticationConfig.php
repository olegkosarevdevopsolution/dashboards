<?php

namespace Devolegkosarev\Dashboard\Config;

use Devolegkosarev\Dashboard\Abstracts\Authentication\AuthenticationConfigAbstracts;

class BaseAuthenticationConfig extends AuthenticationConfigAbstracts
{

    /**
     * The default authenticator for the application.
     * 
     * Possible values: "Bitrix24" or "Database"
     * 
     * @var string
     */
    public $authenticator = 'Database';

    /**
     * Allow Register
     * 
     * You can turn on or off the registration page.
     * 
     * The default setting is true, where the registration page will appear.
     * When this is set to false, a notice will appear instead of the
     * registration page.
     * 
     * You can also override this from .env file with auth.allowRegister
     * 
     * @var array
     */
    public $allowRegister = false;

    /**
     * Allow Forget Password
     *
     * You can turn on or off the Forget Password page.
     * 
     * The default setting is true, where the Forget Password page will appear.
     * When this is set to false, a notice will appear instead of the
     * Forget Password page.
     * 
     * You can also override this from .env file with auth.allowForgetPassword
     * 
     * @var array
     */
    public $allowForgetPassword = false;

    /**
     * Send Activation Email On Registration
     * 
     * Should we send the user an email to activate their account?
     * default is true to minimise fake email registrations
     * 
     * true / false
     *
     * @var bool
     */
    public $sendActivationEmail = false;

    /**
     * Reset Password Token Expiry
     * 
     * The time in hours the password reset token expires
     * default is 1 hour
     * 
     * @var int
     */
    public $resetTokenExpire = 1;

    /**
     * User Activation Token Expiry
     * 
     * The time in hours the user has to activate their account
     * Only used if User Activation is set to true.
     *
     * @var int
     */
    public $activateTokenExpire = 24;

    /**
     * Lock Screen
     * 
     * Option to enable / disable a lock screen
     * Good for Admin dashboards where a user may lock the session
     * only requiring their password to get back in as apposed to logging 
     * out and back in again
     *
     * @var bool
     */
    public $lockScreen = false;

    /**
     * Remember Me (Persistent Login)
     * 
     * If you want to enable / disable the remember me function set to
     * true / false
     * 
     * Uses a timing-attack safe remember me token in the DB and cookie
     * Implemented using the proposed strategy
     * 
     * @see https://paragonie.com/blog/2015/04/secure-authentication-php-with-long-term-persistence
     *
     * @var bool
     */
    public $rememberMe = false;

    /**
     * Remember Me Expiry
     * 
     * The amount of days the login lasts for
     * default is 30 days
     *
     * @var int
     */
    public $rememberMeExpire = 30;

    /**
     * Remember Me Renew
     * 
     * if set to true anytime the user visits the site and a cookie is found
     * a new expiry date is set using the $rememberMeExpire setting above. Technically
     * creating an infinate login session if the user is active on the site
     * 
     * cookie will stille expire after set days if user does not visit the site forcing a login
     * 
     * if set to false the user login cookie will expire and force a login within the expiry
     * time set above regardless of how active they are on the site.
     *
     * @var bool
     */
    public $rememberMeRenew = false;

    /**
     * Encryption Key Password
     * 
     * The password for the encryption key
     * 
     * @var string
     */
    public string $passwordEncryptionKey = 'Ycn5dmCV-Ggo7-qjqE-3s1kbfu0zIIH';

    /**
     * Activate Email Subject
     * 
     * The subject line for the email that is sent when a user registers
     * if the user activation setting is set to true.
     *
     * @var string
     */
    public $activateEmailSubject = 'Activate Your Account';

    /**
     * Reset Email Subject
     * 
     * The subject line for the email that is sent when a user resets their password
     * from the forgot password form
     *
     * @var string
     */
    public $resetEmailSubject = 'Reset Your Password';
}
