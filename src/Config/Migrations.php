<?php

namespace Devolegkosarev\Dashboard\Config;

use CodeIgniter\Config\BaseConfig;

class Migrations extends BaseConfig
{

    public array $aliases = [
        'DashboardGroup' => 'vendor\Devolegkosarev\dashboard\Database\Migrations',
    ];
    public array $groups = [
        'default' => [
            'DashboardGroup',
            'App\Migrations',
        ],
    ];
}
