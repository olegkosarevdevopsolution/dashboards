<?php

namespace Devolegkosarev\Dashboard\Config;

use CodeIgniter\Config\Services as BaseServices;
use Devolegkosarev\Dashboard\Config\Provider\{
    AuthenticationFactoryProvider,
    FormServiceProvider,
    LanguageServiceProvider,
    Roles\RolesServiceProvider,
    EncryptedCookieManagerServiceProvider,
    DataTablesFiltersLibraresProvider,
    Menus\MainmenuServiceProvider,
    Menus\MenuRendererServiceProvider,
    Roles\RolesRendererServiceProvider,
    Permissions\PermissionsServiceProvider,
    Roles\RolesTranslationsServiceProvider,
    Permissions\PremissionsRendererServiceProvider,
    Permissions\PermissionsTranslationsServiceProvider,
    Documentation\ReleaseNotes\ReleaseNotesServiceProvider,
    Documentation\ReleaseNotes\ReleaseNotesChangeServiceProvider,
    Documentation\ReleaseNotes\ReleaseNotesRendererServiceProvider,
    Users\UsersServiceProvider,
    Bootstrap\AlertHelperServiceProvider
};

/**
 * Services
 *
 * This file contains the service providers for the Devolegkosarev\Dashboard\Config namespace.
 *
 * @package \Devolegkosarev\Dashboard\Config
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
class Services extends BaseServices
{

    /**
     * Get the authenticator instance
     *
     * @param string|null $authenticatorName The name of the authenticator
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Interfaces\Authentication\AuthenticationInterface The authenticator instance
     */
    public static function getAuthenticator(?string $authenticatorName = null, $getShared = true): \Devolegkosarev\Dashboard\Interfaces\Authentication\AuthenticationInterface
    {
        if ($getShared) {
            return static::getSharedInstance('getAuthenticator', $authenticatorName);
        }
        return AuthenticationFactoryProvider::getAuthenticator($authenticatorName);
    }

    /**
     * Get the language service instance
     *
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\LanguageService The language service instance
     */
    public static function languageService($getShared = true): \Devolegkosarev\Dashboard\Services\LanguageService
    {
        if ($getShared) {
            return static::getSharedInstance('languageService');
        }

        return LanguageServiceProvider::languageService();
    }

    /**
     * Get the cookie manager service instance
     *
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\EncryptedCookieManagerService The cookie manager service instance
     */
    public static function encryptedCookieManagerService($getShared = true): \Devolegkosarev\Dashboard\Services\EncryptedCookieManagerService
    {
        if ($getShared) {
            return static::getSharedInstance('encryptedCookieManagerService');
        }

        return EncryptedCookieManagerServiceProvider::encryptedCookieManagerService();
    }

    /**
     * Get the release notes service instance
     *
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService The release notes service instance
     */
    public static function releaseNotesService($getShared = true): \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesService
    {
        if ($getShared) {
            return static::getSharedInstance('releaseNotesService');
        }

        return ReleaseNotesServiceProvider::releaseNotesService();
    }

    /**
     * Get the release notes change service instance
     *
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesChangeService The release notes change service instance
     */
    public static function releaseNotesChangeService($getShared = true): \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesChangeService
    {
        if ($getShared) {
            return static::getSharedInstance('releaseNotesChangeService');
        }

        return ReleaseNotesChangeServiceProvider::releaseNotesChangeService();
    }

    /**
     * Get the release notes renderer service instance
     *
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesRendererService The release notes renderer service instance
     */
    public static function releaseNotesRendererService($getShared = true): \Devolegkosarev\Dashboard\Services\Documentation\ReleaseNotes\ReleaseNotesRendererService
    {
        if ($getShared) {
            return static::getSharedInstance('releaseNotesRendererService');
        }

        return ReleaseNotesRendererServiceProvider::releaseNotesRendererService();
    }

    /**
     * Returns an instance of the MainmenuService class.
     * 
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\Menus\MainmenuService An instance of MainmenuService
     */
    public static function mainmenuService($getShared = true): \Devolegkosarev\Dashboard\Services\Menus\MainmenuService
    {
        if ($getShared) {
            return static::getSharedInstance('mainmenuService');
        }

        return MainmenuServiceProvider::mainmenuService();
    }

    /**
     * Returns an instance of the MenusRendererService class.
     * 
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\Menus\MenusRendererService An instance of MenusRendererService
     */
    public static function menuRendererService($getShared = true): \Devolegkosarev\Dashboard\Services\Menus\MenusRendererService
    {
        if ($getShared) {
            return static::getSharedInstance('menuRendererService');
        }
        return MenuRendererServiceProvider::menuRendererService();
    }

    /**
     * Returns an instance of the RolesService class.
     *
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\Roles\RolesService An instance of the RolesService class.
     */
    public static function rolesService($getShared = true): \Devolegkosarev\Dashboard\Services\Roles\RolesService
    {
        // Check if a shared instance is requested
        if ($getShared) {
            return static::getSharedInstance('rolesService');
        }

        // Return a new instance of RolesService
        return RolesServiceProvider::rolesService();
    }

    /**
     * Returns an instance of the RolesRendererService class.
     *
     * @param bool $getShared Whether to get a shared instance
     * @return \Devolegkosarev\Dashboard\Services\Roles\RolesRendererService An instance of the RolesRendererService class.
     */
    public static function rolesRendererService($getShared = true): \Devolegkosarev\Dashboard\Services\Roles\RolesRendererService
    {
        if ($getShared) {
            // Return a shared instance of the RolesRendererService class
            return static::getSharedInstance('rolesRendererService');
        }

        // Return a new instance of the RolesRendererService class
        return RolesRendererServiceProvider::rolesRendererService();
    }

    /**
     * Returns an instance of the RolesTranslationsService class.
     *
     * @param bool $getShared Whether to return a shared instance.
     * @return \Devolegkosarev\Dashboard\Services\Roles\RolesTranslationsService An instance of the RolesTranslationsService class.
     */
    public function rolesTranslationsService($getShared = true): \Devolegkosarev\Dashboard\Services\Roles\RolesTranslationsService
    {
        if ($getShared) {
            return static::getSharedInstance('rolesTranslationsService');
        }

        return RolesTranslationsServiceProvider::rolesTranslationService();
    }

    /**
     * Returns an instance of the PermissionsService class.
     *
     * @param bool $getShared Whether to return a shared instance.
     * @return \Devolegkosarev\Dashboard\Services\Permissions\PermissionsService An instance of the PermissionsService class.
     */
    public static function permissionsService($getShared = true): \Devolegkosarev\Dashboard\Services\Permissions\PermissionsService
    {
        if ($getShared) {
            return static::getSharedInstance('permissionsService');
        }

        return PermissionsServiceProvider::permissionsService();
    }

    /**
     * Returns an instance of the PremissionsRendererService class.
     *
     * @param bool $getShared Whether to return a shared instance.
     * @return \Devolegkosarev\Dashboard\Services\Permissions\PremissionsRendererService An instance of the PremissionsRendererService class.
     */
    public static function premissionsRendererService($getShared = true): \Devolegkosarev\Dashboard\Services\Permissions\PremissionsRendererService
    {
        if ($getShared) {
            return static::getSharedInstance('premissionsRendererService');
        }

        return PremissionsRendererServiceProvider::premissionsRendererService();
    }

    public static function permissionsTranslationsService($getShared = true): \Devolegkosarev\Dashboard\Services\Permissions\PermissionsTranslationsService
    {
        if ($getShared) {
            return static::getSharedInstance('permissionsTranslationsService');
        }

        return PermissionsTranslationsServiceProvider::permissionsTranslationsService();
    }

    /**
     * Returns an instance of the FormService class.
     *
     * @param bool $getShared Whether to return a shared instance.
     * @return \Devolegkosarev\Dashboard\Services\FormService An instance of the FormService class.
     */
    public static function formService($getShared = true): \Devolegkosarev\Dashboard\Services\FormService
    {
        if ($getShared) {
            return static::getSharedInstance('formService');
        }

        return FormServiceProvider::formService();
    }

    public static function usersService($getShared = true): \Devolegkosarev\Dashboard\Services\Users\UsersService
    {
        if ($getShared) {
            return static::getSharedInstance('usersService');
        }
        return UsersServiceProvider::usersService();
    }

    public static function dataTablesFiltersLibrares($getShared = true): \Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares
    {
        if ($getShared) {
            return static::getSharedInstance('dataTablesFiltersLibrares');
        }
        return DataTablesFiltersLibraresProvider::dataTablesFiltersLibrares();
    }

    public static function alertHelperService($getShared = true): \Devolegkosarev\Dashboard\Services\Bootstrap\AlertHelperService
    {
        if ($getShared) {
            return static::getSharedInstance('alertHelperService');
        }
        return AlertHelperServiceProvider::alertHelperService();
    }
}
