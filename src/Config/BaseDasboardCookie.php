<?php

namespace Devolegkosarev\Dashboard\Config;

use Config\Cookie;

class BaseDasboardCookie extends Cookie
{
    /**
     * --------------------------------------------------------------------------
     * Cookie Key Encryption
     * --------------------------------------------------------------------------
     *
     * Set a cookie key to use for encryption.
     */
    public string $cookieEncryptionKey  = '5f8f0f6a-8c5e-4d2c-9e1d-1a2b3c4d5e6f';
}
