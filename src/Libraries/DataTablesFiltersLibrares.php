<?php

namespace Devolegkosarev\Dashboard\Libraries;

/**
 * DataTablesFiltersLibrares class provides methods to generate filters for DataTables.
 */
class DataTablesFiltersLibrares
{
    protected $filters = [];

    /**
     * Generate a text filter for the DataTable.
     *
     * @param string $id The ID of the text filter.
     * @param string $placeholder The placeholder text for the text filter.
     * @param int $column The column to filter on.
     * @return void
     */
    public function text(string $id, string $placeholder, int $column)
    {
        if (!is_string($id) or empty($id)) {
            throw new \InvalidArgumentException('Invalid argument id or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:text');
        }
        if (!is_string($placeholder) or empty($placeholder)) {
            throw new \InvalidArgumentException('Invalid argument placeholder or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:text');
        }
        if (!is_int($column) or (int)$column <= 0) {
            throw new \InvalidArgumentException('Invalid argument column or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:text');
        }
        // vendor\Devolegkosarev\dashboard\src\Views\Partials\Templates\DataTblesFilters\text.php
        $this->filters[] = view(
            viewOverview("Views\\Partials\\Templates\\DataTablesFilters\\Text"),
            [
                'id' => $id, 'placeholder' => $placeholder, 'column' => $column, 'type' => 'text'
            ]
        );
    }

    /**
     * Generate the function comment for the given function body.
     *
     * @param string $id The ID of the dropdown.
     * @param array $options The dropdown options.
     * @param int $column The column name.
     * @param string $defaultOptions The default options.
     */
    public function dropdown(string $id, array $options, int $column, string $defaultOptions, string $placeholder)
    {
        if (!is_string($id) or empty($id)) {
            throw new \InvalidArgumentException('Invalid argument id or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:dropdown');
        }
        if (!is_int($column) or (int)$column <= 0) {
            throw new \InvalidArgumentException('Invalid argument column or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:dropdown');
        }
        if (!is_string($defaultOptions) or empty($defaultOptions)) {
            throw new \InvalidArgumentException('Invalid argument defaultOptions or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:dropdown');
        }
        if (!is_string($placeholder) or empty($placeholder)) {
            throw new \InvalidArgumentException('Invalid argument placeholder or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:dropdown');
        }
        if (!is_array($options) or count($options) <= 0) {
            throw new \InvalidArgumentException('Invalid argument options or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:dropdown');
        }

        $this->filters[] = view(
            viewOverview("Views\\Partials\\Templates\\DataTablesFilters\\Dropdown"),
            [
                'id' => $id, 'options' => $options, 'defaultOptions' => $defaultOptions, 'column' => $column, 'placeholder' => $placeholder, 'type' => 'dropdown'
            ]
        );
    }

    public function numberSlider(string $id, string $label = null, int $column, int $min = 0, int $max = 1000, int $step = 1, int $value = 50)
    {

        if (!is_string($id) or empty($id)) {
            throw new \InvalidArgumentException('Invalid argument id or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }

        if (!is_int($column) or (int)$column <= 0) {
            throw new \InvalidArgumentException('Invalid argument column or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }

        if (!is_int($min)) {
            throw new \InvalidArgumentException('Invalid argument min. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }

        if (!is_int($max) or (int)$max <= 0) {
            throw new \InvalidArgumentException('Invalid argument max. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }

        if ($max <= $min) {
            throw new \InvalidArgumentException('Invalid argument max. Cannot less or equal to min. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }

        if (!is_int($step) or (int)$step <= 0) {
            throw new \InvalidArgumentException('Invalid argument step. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }

        if (!is_int($value) or (int) $value <= 0) {
            throw new \InvalidArgumentException('Invalid argument value. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }

        if ($value < $min or $value > $max) {
            throw new \InvalidArgumentException('Invalid argument value. Value only range ' . $min . ' and ' . $max . '. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:numberSlider');
        }


        $this->filters[] = view(
            viewOverview("DataTablesFilters\\Views\\Partials\\Templates\\date_range"),
            [
                'id' => $id,
                'column' => $column,
                'type' => 'number_slider',
                'label' => $label,
                'min' => $min,
                'max' => $max,
                'step' => $step,
                'value' => $value,
            ]
        );
    }

    /**
     * Generates a date range filter for the data table.
     *
     * @param string $id The ID of the filter.
     * @param string $label The label of the filter.
     * @param int $column The column to filter.
     * @param string $placeholder_date_from The placeholder for the "from" date input.
     * @param string $placeholder_date_to The placeholder for the "to" date input.
     * @return void
     */
    public function dateRange(string $id, string $label, int $column, string $placeholder_date_from, string $placeholder_date_to)
    {
        if (!is_string($id) or empty($id)) {
            throw new \InvalidArgumentException('Invalid argument id or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:date_range');
        }
        if (!is_string($label) or empty($label)) {
            throw new \InvalidArgumentException('Invalid argument label or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:date_range');
        }
        if (!is_int($column) or (int)$column <= 0) {
            throw new \InvalidArgumentException('Invalid argument column or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:date_range');
        }
        if (!is_string($placeholder_date_from) or empty($placeholder_date_from)) {
            throw new \InvalidArgumentException('Invalid argument placeholder_date_from or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:date_range');
        }
        if (!is_string($placeholder_date_to) or empty($placeholder_date_to)) {
            throw new \InvalidArgumentException('Invalid argument placeholder_date_to or empty. In Function: Devolegkosarev\Dashboard\Libraries\DataTablesFiltersLibrares.php:date_range');
        }
        $this->filters[] = view(
            viewOverview("DataTablesFilters\\Views\\Partials\\Templates\\date_range"),
            [
                'id' => $id, 'label' => $label, 'column' => $column, 'placeholder_date_from' => $placeholder_date_from, 'placeholder_date_to' => $placeholder_date_to, 'type' => 'date_range'
            ]
        );
    }

    /**
     * Returns the filters as a string.
     *
     * @return string The filters as a string.
     * @throws \RuntimeException if there are no filters to return.
     */
    public function getFilters()
    {
        if (empty($this->filters)) {
            throw new \RuntimeException('No filters found.');
        }

        try {
            return view(
                viewOverview("Views\\Partials\\Templates\\Filters"),
                [
                    'filters' => implode("\n", $this->filters)
                ]
            );
        } catch (\Throwable $e) {
            throw new \RuntimeException('Failed to concatenate filters.', 0, $e);
        }
    }
}
