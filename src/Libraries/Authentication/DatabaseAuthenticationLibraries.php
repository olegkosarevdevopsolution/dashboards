<?php

namespace Devolegkosarev\Dashboard\Libraries\Authentication;

use stdClass;
use Exception;
use Devolegkosarev\Dashboard\Models\Users\UsersModel;
use Devolegkosarev\Dashboard\Services\Users\UsersService;
use Devolegkosarev\Dashboard\Abstracts\Authentication\AuthenticationAbstracts;

class DatabaseAuthenticationLibraries extends AuthenticationAbstracts
{
    /**
     * @var usersService
     */
    protected $usersService;

    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
        parent::__construct();
    }

    public function login($email, $password): array
    {
        $user = $this->usersService->getUserByEmail($email);
        if (!$user) {
            throw new Exception('User Not Found', 500);
        }

        $verifyPassword = $this->usersService->verifyPassword($password, $user->password);
        if (!$verifyPassword) {
            throw new Exception('Invalid Password', 500);
        }

        if ($user->activated === 0) {
            throw new Exception('User Not Activated', 500);
        }

        if ($user->blocked === 1) {
            throw new Exception('User Blocked', 500);
        }

        // throw new Exception('Invalid username or password');
        // Логика аутентификации пользователя через БД
        $user = [
            'email' => $user->email,
            'user_id' => $user->id,
            'role_id' => $user->role_id,
            'avatar' => $user->avatar
        ];


        // Пользователь найден, установить флаг loggedIn в true и сохранить данные пользователя
        $this->loggedIn = true;
        $this->currentUser = $user;
        $this->setCookies($user);
        return $user;
    }
    public function getUser(int $userId): array
    {
        $user = $this->usersService->getUser($userId);


        if ($user) {
            $this->currentUser = $user;
            return $user;
        } else {
            throw new Exception('User not found');
        }
    }

    public function logout(): bool
    {
        // Логика выхода пользователя из системы
        $this->loggedIn = false;
        $this->currentUser = null;
        $deleteAuthCookie = $this->encryptedCookieManagerService->deleteAuthCookie();
        $session = session();
        if ($deleteAuthCookie == true) {
            $session->setFlashdata("info", 'Logout successful. 🎉');
            return true;
        }
        $session->setFlashdata("error", 'An error occurred while deleting cookies. The exit failed.');
        return false;
    }
    /**
     * Check if the user is authorized.
     *
     * @return bool
     */
    public function checkAuthorization(): bool
    {
        return $this->isLoggedIn();
    }

    public function register(array $users)
    {
        return $this->usersService->createUser($users);
    }
}
