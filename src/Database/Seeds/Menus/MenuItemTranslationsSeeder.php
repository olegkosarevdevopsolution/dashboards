<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Menus;

use CodeIgniter\Database\Seeder;

class MenuItemTranslationsSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'mainmenu_id' => '1',
                'language' => 'en',
                'name' => 'Navigation Menu',
                'created_at' => '2023-09-13 00:05:37',
                'updated_at' => '2023-09-13 00:05:37'
            ],
            [
                'id' => '2',
                'mainmenu_id' => '2',
                'language' => 'en',
                'name' => 'Dashboard',
                'created_at' => '2023-09-14 00:05:37',
                'updated_at' => '2023-09-14 00:05:37'
            ],
            [
                'id' => '3',
                'mainmenu_id' => '3',
                'language' => 'en',
                'name' => 'Notifications',
                'created_at' => '2023-09-15 00:05:37',
                'updated_at' => '2023-09-15 00:05:37'
            ],
            [
                'id' => '4',
                'mainmenu_id' => '4',
                'language' => 'en',
                'name' => 'Notifications List',
                'created_at' => '2023-09-16 00:05:37',
                'updated_at' => '2023-09-16 00:05:37'
            ],
            [
                'id' => '5',
                'mainmenu_id' => '5',
                'language' => 'en',
                'name' => 'Create Notification',
                'created_at' => '2023-09-17 00:05:37',
                'updated_at' => '2023-09-17 00:05:37'
            ],
            [
                'id' => '6',
                'mainmenu_id' => '6',
                'language' => 'en',
                'name' => 'Users',
                'created_at' => '2023-09-18 00:05:37',
                'updated_at' => '2023-09-18 00:05:37'
            ],
            [
                'id' => '7',
                'mainmenu_id' => '7',
                'language' => 'en',
                'name' => 'Users List',
                'created_at' => '2023-09-19 00:05:37',
                'updated_at' => '2023-09-19 00:05:37'
            ],
            [
                'id' => '8',
                'mainmenu_id' => '8',
                'language' => 'en',
                'name' => 'Create User',
                'created_at' => '2023-09-20 00:05:37',
                'updated_at' => '2023-09-20 00:05:37'
            ],
            [
                'id' => '9',
                'mainmenu_id' => '9',
                'language' => 'en',
                'name' => 'Role',
                'created_at' => '2023-09-21 00:05:37',
                'updated_at' => '2023-09-21 00:05:37'
            ],
            [
                'id' => '10',
                'mainmenu_id' => '10',
                'language' => 'en',
                'name' => 'Role List',
                'created_at' => '2023-09-22 00:05:37',
                'updated_at' => '2023-09-22 00:05:37'
            ],
            [
                'id' => '11',
                'mainmenu_id' => '11',
                'language' => 'en',
                'name' => 'Create Role',
                'created_at' => '2023-09-23 00:05:37',
                'updated_at' => '2023-09-23 00:05:37'
            ],
            [
                'id' => '12',
                'mainmenu_id' => '12',
                'language' => 'en',
                'name' => 'Permissions',
                'created_at' => '2023-09-24 00:05:37',
                'updated_at' => '2023-09-24 00:05:37'
            ],
            [
                'id' => '13',
                'mainmenu_id' => '13',
                'language' => 'en',
                'name' => 'Permissions List',
                'created_at' => '2023-09-25 00:05:37',
                'updated_at' => '2023-09-25 00:05:37'
            ],
            [
                'id' => '14',
                'mainmenu_id' => '14',
                'language' => 'en',
                'name' => 'Create Permissions',
                'created_at' => '2023-09-26 00:05:37',
                'updated_at' => '2023-09-26 00:05:37'
            ],
            [
                'id' => '15',
                'mainmenu_id' => '15',
                'language' => 'en',
                'name' => 'Menu',
                'created_at' => '2023-09-27 00:05:37',
                'updated_at' => '2023-09-27 00:05:37'
            ],
            [
                'id' => '16',
                'mainmenu_id' => '16',
                'language' => 'en',
                'name' => 'Menu List',
                'created_at' => '2023-09-28 00:05:37',
                'updated_at' => '2023-09-28 00:05:37'
            ],
            [
                'id' => '17',
                'mainmenu_id' => '17',
                'language' => 'en',
                'name' => 'Create Menu',
                'created_at' => '2023-09-29 00:05:37',
                'updated_at' => '2023-09-29 00:05:37'
            ],
            [
                'id' => '18',
                'mainmenu_id' => '18',
                'language' => 'en',
                'name' => 'Help',
                'created_at' => '2023-09-30 00:05:37',
                'updated_at' => '2023-09-30 00:05:37'
            ],
            [
                'id' => '19',
                'mainmenu_id' => '19',
                'language' => 'en',
                'name' => 'Edit Documentations',
                'created_at' => '2023-10-01 00:05:37',
                'updated_at' => '2023-10-01 00:05:37'
            ],
            [
                'id' => '20',
                'mainmenu_id' => '20',
                'language' => 'en',
                'name' => 'Documentations List',
                'created_at' => '2023-10-02 00:05:37',
                'updated_at' => '2023-10-02 00:05:37'
            ],
            [
                'id' => '21',
                'mainmenu_id' => '21',
                'language' => 'en',
                'name' => 'Create Documentations',
                'created_at' => '2023-10-03 00:05:37',
                'updated_at' => '2023-10-03 00:05:37'
            ],
            [
                'id' => '22',
                'mainmenu_id' => '22',
                'language' => 'en',
                'name' => 'Edit Release Notes',
                'created_at' => '2023-10-04 00:05:37',
                'updated_at' => '2023-10-04 00:05:37'
            ],
            [
                'id' => '23',
                'mainmenu_id' => '23',
                'language' => 'en',
                'name' => 'Release Notes List',
                'created_at' => '2023-10-05 00:05:37',
                'updated_at' => '2023-10-05 00:05:37'
            ],
            [
                'id' => '24',
                'mainmenu_id' => '24',
                'language' => 'en',
                'name' => 'Create Release Notes',
                'created_at' => '2023-10-06 00:05:37',
                'updated_at' => '2023-10-06 00:05:37'
            ],
            [
                'id' => '25',
                'mainmenu_id' => '25',
                'language' => 'en',
                'name' => 'Documentations',
                'created_at' => '2023-10-07 00:05:37',
                'updated_at' => '2023-10-07 00:05:37'
            ],
            [
                'id' => '26',
                'mainmenu_id' => '26',
                'language' => 'en',
                'name' => 'Release Notes',
                'created_at' => '2023-10-08 00:05:37',
                'updated_at' => '2023-10-08 00:05:37'
            ],
            [
                'id' => '27',
                'mainmenu_id' => '27',
                'language' => 'en',
                'name' => 'Feedback ',
                'created_at' => '2023-10-09 00:05:37',
                'updated_at' => '2023-10-09 00:05:37'
            ],
            [
                'id' => '28',
                'mainmenu_id' => '28',
                'language' => 'en',
                'name' => 'Settings',
                'created_at' => '2023-12-06 00:05:37',
                'updated_at' => '2023-12-06 00:05:37'
            ],
            [
                'id' => '29',
                'mainmenu_id' => '29',
                'language' => 'en',
                'name' => 'System Setings',
                'created_at' => '2023-10-10 00:05:37',
                'updated_at' => '2023-10-10 00:05:37'
            ],
            [
                'id' => '30',
                'mainmenu_id' => '1',
                'language' => 'lv',
                'name' => 'Navigācijas izvēlne',
                'created_at' => '2023-10-11 00:05:37',
                'updated_at' => '2023-10-11 00:05:37'
            ],
            [
                'id' => '31',
                'mainmenu_id' => '2',
                'language' => 'lv',
                'name' => 'Informācijas panelis',
                'created_at' => '2023-10-12 00:05:37',
                'updated_at' => '2023-10-12 00:05:37'
            ],
            [
                'id' => '32',
                'mainmenu_id' => '3',
                'language' => 'lv',
                'name' => 'Paziņojumi',
                'created_at' => '2023-10-13 00:05:37',
                'updated_at' => '2023-10-13 00:05:37'
            ],
            [
                'id' => '33',
                'mainmenu_id' => '4',
                'language' => 'lv',
                'name' => 'Paziņojumu saraksts',
                'created_at' => '2023-10-14 00:05:37',
                'updated_at' => '2023-10-14 00:05:37'
            ],
            [
                'id' => '34',
                'mainmenu_id' => '5',
                'language' => 'lv',
                'name' => 'Izveidot paziņojumu',
                'created_at' => '2023-10-15 00:05:37',
                'updated_at' => '2023-10-15 00:05:37'
            ],
            [
                'id' => '35',
                'mainmenu_id' => '6',
                'language' => 'lv',
                'name' => 'Lietotāji',
                'created_at' => '2023-10-16 00:05:37',
                'updated_at' => '2023-10-16 00:05:37'
            ],
            [
                'id' => '36',
                'mainmenu_id' => '7',
                'language' => 'lv',
                'name' => 'Lietotāju saraksts',
                'created_at' => '2023-10-17 00:05:37',
                'updated_at' => '2023-10-17 00:05:37'
            ],
            [
                'id' => '37',
                'mainmenu_id' => '8',
                'language' => 'lv',
                'name' => 'Izveidot lietotāju',
                'created_at' => '2023-10-18 00:05:37',
                'updated_at' => '2023-10-18 00:05:37'
            ],
            [
                'id' => '38',
                'mainmenu_id' => '9',
                'language' => 'lv',
                'name' => 'Loma',
                'created_at' => '2023-10-19 00:05:37',
                'updated_at' => '2023-10-19 00:05:37'
            ],
            [
                'id' => '39',
                'mainmenu_id' => '10',
                'language' => 'lv',
                'name' => 'Lomu saraksts',
                'created_at' => '2023-10-20 00:05:37',
                'updated_at' => '2023-10-20 00:05:37'
            ],
            [
                'id' => '40',
                'mainmenu_id' => '11',
                'language' => 'lv',
                'name' => 'Izveidot lomu',
                'created_at' => '2023-10-21 00:05:37',
                'updated_at' => '2023-10-21 00:05:37'
            ],
            [
                'id' => '41',
                'mainmenu_id' => '12',
                'language' => 'lv',
                'name' => 'Atļaujas',
                'created_at' => '2023-10-22 00:05:37',
                'updated_at' => '2023-10-22 00:05:37'
            ],
            [
                'id' => '42',
                'mainmenu_id' => '13',
                'language' => 'lv',
                'name' => 'Atļauju saraksts',
                'created_at' => '2023-10-23 00:05:37',
                'updated_at' => '2023-10-23 00:05:37'
            ],
            [
                'id' => '43',
                'mainmenu_id' => '14',
                'language' => 'lv',
                'name' => 'Izveidot atļaujas',
                'created_at' => '2023-10-24 00:05:37',
                'updated_at' => '2023-10-24 00:05:37'
            ],
            [
                'id' => '44',
                'mainmenu_id' => '15',
                'language' => 'lv',
                'name' => 'Izvēlne',
                'created_at' => '2023-10-25 00:05:37',
                'updated_at' => '2023-10-25 00:05:37'
            ],
            [
                'id' => '45',
                'mainmenu_id' => '16',
                'language' => 'lv',
                'name' => 'Izvēlņu saraksts',
                'created_at' => '2023-10-26 00:05:37',
                'updated_at' => '2023-10-26 00:05:37'
            ],
            [
                'id' => '46',
                'mainmenu_id' => '17',
                'language' => 'lv',
                'name' => 'Izveidot izvēlni',
                'created_at' => '2023-10-27 00:05:37',
                'updated_at' => '2023-10-27 00:05:37'
            ],
            [
                'id' => '47',
                'mainmenu_id' => '18',
                'language' => 'lv',
                'name' => 'Palīdzība',
                'created_at' => '2023-10-28 00:05:37',
                'updated_at' => '2023-10-28 00:05:37'
            ],
            [
                'id' => '48',
                'mainmenu_id' => '19',
                'language' => 'lv',
                'name' => 'Rediģēt dokumentus',
                'created_at' => '2023-10-29 00:05:37',
                'updated_at' => '2023-10-29 00:05:37'
            ],
            [
                'id' => '49',
                'mainmenu_id' => '20',
                'language' => 'lv',
                'name' => 'Dokumentu saraksts',
                'created_at' => '2023-10-30 00:05:37',
                'updated_at' => '2023-10-30 00:05:37'
            ],
            [
                'id' => '50',
                'mainmenu_id' => '21',
                'language' => 'lv',
                'name' => 'Izveidojiet dokumentus',
                'created_at' => '2023-10-31 00:05:37',
                'updated_at' => '2023-10-31 00:05:37'
            ],
            [
                'id' => '51',
                'mainmenu_id' => '22',
                'language' => 'lv',
                'name' => 'Rediģēt izlaiduma piezīmes',
                'created_at' => '2023-11-01 00:05:37',
                'updated_at' => '2023-11-01 00:05:37'
            ],
            [
                'id' => '52',
                'mainmenu_id' => '23',
                'language' => 'lv',
                'name' => 'Izlaiduma piezīmju saraksts',
                'created_at' => '2023-11-02 00:05:37',
                'updated_at' => '2023-11-02 00:05:37'
            ],
            [
                'id' => '53',
                'mainmenu_id' => '24',
                'language' => 'lv',
                'name' => 'Izveidojiet izlaiduma piezīmes',
                'created_at' => '2023-11-03 00:05:37',
                'updated_at' => '2023-11-03 00:05:37'
            ],
            [
                'id' => '54',
                'mainmenu_id' => '25',
                'language' => 'lv',
                'name' => 'Dokumentācijas',
                'created_at' => '2023-11-04 00:05:37',
                'updated_at' => '2023-11-04 00:05:37'
            ],
            [
                'id' => '55',
                'mainmenu_id' => '26',
                'language' => 'lv',
                'name' => 'Izlaiduma piezīmes',
                'created_at' => '2023-11-05 00:05:37',
                'updated_at' => '2023-11-05 00:05:37'
            ],
            [
                'id' => '56',
                'mainmenu_id' => '27',
                'language' => 'lv',
                'name' => 'Atsauksmes',
                'created_at' => '2023-11-06 00:05:37',
                'updated_at' => '2023-11-06 00:05:37'
            ],
            [
                'id' => '57',
                'mainmenu_id' => '28',
                'language' => 'lv',
                'name' => 'Iestatījumi',
                'created_at' => '2023-11-07 00:05:37',
                'updated_at' => '2023-11-07 00:05:37'
            ],
            [
                'id' => '58',
                'mainmenu_id' => '29',
                'language' => 'lv',
                'name' => 'Sistēmas iestatījumi',
                'created_at' => '2023-12-06 00:05:37',
                'updated_at' => '2023-12-06 00:05:37'
            ],
            [
                'id' => '59',
                'mainmenu_id' => '1',
                'language' => 'ru',
                'name' => 'Меню навигации',
                'created_at' => '2023-11-08 00:05:37',
                'updated_at' => '2023-11-08 00:05:37'
            ],
            [
                'id' => '60',
                'mainmenu_id' => '2',
                'language' => 'ru',
                'name' => 'Панель управление',
                'created_at' => '2023-11-09 00:05:37',
                'updated_at' => '2023-11-09 00:05:37'
            ],
            [
                'id' => '61',
                'mainmenu_id' => '3',
                'language' => 'ru',
                'name' => 'Уведомления',
                'created_at' => '2023-11-10 00:05:37',
                'updated_at' => '2023-11-10 00:05:37'
            ],
            [
                'id' => '62',
                'mainmenu_id' => '4',
                'language' => 'ru',
                'name' => 'Список уведомлений',
                'created_at' => '2023-11-11 00:05:37',
                'updated_at' => '2023-11-11 00:05:37'
            ],
            [
                'id' => '63',
                'mainmenu_id' => '5',
                'language' => 'ru',
                'name' => 'Создать уведомление',
                'created_at' => '2023-11-12 00:05:37',
                'updated_at' => '2023-11-12 00:05:37'
            ],
            [
                'id' => '64',
                'mainmenu_id' => '6',
                'language' => 'ru',
                'name' => 'Пользователи',
                'created_at' => '2023-11-13 00:05:37',
                'updated_at' => '2023-11-13 00:05:37'
            ],
            [
                'id' => '65',
                'mainmenu_id' => '7',
                'language' => 'ru',
                'name' => 'Список пользователей',
                'created_at' => '2023-11-14 00:05:37',
                'updated_at' => '2023-11-14 00:05:37'
            ],
            [
                'id' => '66',
                'mainmenu_id' => '8',
                'language' => 'ru',
                'name' => 'Создать пользователя',
                'created_at' => '2023-11-15 00:05:37',
                'updated_at' => '2023-11-15 00:05:37'
            ],
            [
                'id' => '67',
                'mainmenu_id' => '9',
                'language' => 'ru',
                'name' => 'Роль',
                'created_at' => '2023-11-16 00:05:37',
                'updated_at' => '2023-11-16 00:05:37'
            ],
            [
                'id' => '68',
                'mainmenu_id' => '10',
                'language' => 'ru',
                'name' => 'Список ролей',
                'created_at' => '2023-11-17 00:05:37',
                'updated_at' => '2023-11-17 00:05:37'
            ],
            [
                'id' => '69',
                'mainmenu_id' => '11',
                'language' => 'ru',
                'name' => 'Создать роль',
                'created_at' => '2023-11-18 00:05:37',
                'updated_at' => '2023-11-18 00:05:37'
            ],
            [
                'id' => '70',
                'mainmenu_id' => '12',
                'language' => 'ru',
                'name' => 'Разрешения',
                'created_at' => '2023-11-19 00:05:37',
                'updated_at' => '2023-11-19 00:05:37'
            ],
            [
                'id' => '71',
                'mainmenu_id' => '13',
                'language' => 'ru',
                'name' => 'Список разрешений',
                'created_at' => '2023-11-20 00:05:37',
                'updated_at' => '2023-11-20 00:05:37'
            ],
            [
                'id' => '72',
                'mainmenu_id' => '14',
                'language' => 'ru',
                'name' => 'Создать разрешения',
                'created_at' => '2023-11-21 00:05:37',
                'updated_at' => '2023-11-21 00:05:37'
            ],
            [
                'id' => '73',
                'mainmenu_id' => '15',
                'language' => 'ru',
                'name' => 'Меню',
                'created_at' => '2023-11-22 00:05:37',
                'updated_at' => '2023-11-22 00:05:37'
            ],
            [
                'id' => '74',
                'mainmenu_id' => '16',
                'language' => 'ru',
                'name' => 'Список меню',
                'created_at' => '2023-11-23 00:05:37',
                'updated_at' => '2023-11-23 00:05:37'
            ],
            [
                'id' => '75',
                'mainmenu_id' => '17',
                'language' => 'ru',
                'name' => 'Создать меню',
                'created_at' => '2023-11-24 00:05:37',
                'updated_at' => '2023-11-24 00:05:37'
            ],
            [
                'id' => '76',
                'mainmenu_id' => '18',
                'language' => 'ru',
                'name' => 'Помощь',
                'created_at' => '2023-11-25 00:05:37',
                'updated_at' => '2023-11-25 00:05:37'
            ],
            [
                'id' => '77',
                'mainmenu_id' => '19',
                'language' => 'ru',
                'name' => 'Редактировать документацию',
                'created_at' => '2023-11-26 00:05:37',
                'updated_at' => '2023-11-26 00:05:37'
            ],
            [
                'id' => '78',
                'mainmenu_id' => '20',
                'language' => 'ru',
                'name' => 'Список документации',
                'created_at' => '2023-11-27 00:05:37',
                'updated_at' => '2023-11-27 00:05:37'
            ],
            [
                'id' => '79',
                'mainmenu_id' => '21',
                'language' => 'ru',
                'name' => 'Создать документацию',
                'created_at' => '2023-11-28 00:05:37',
                'updated_at' => '2023-11-28 00:05:37'
            ],
            [
                'id' => '80',
                'mainmenu_id' => '22',
                'language' => 'ru',
                'name' => 'Редактировать примечания к выпуску',
                'created_at' => '2023-11-29 00:05:37',
                'updated_at' => '2023-11-29 00:05:37'
            ],
            [
                'id' => '81',
                'mainmenu_id' => '23',
                'language' => 'ru',
                'name' => 'Список примечаний к выпуску',
                'created_at' => '2023-11-30 00:05:37',
                'updated_at' => '2023-11-30 00:05:37'
            ],
            [
                'id' => '82',
                'mainmenu_id' => '24',
                'language' => 'ru',
                'name' => 'Создание примечаний к выпуску',
                'created_at' => '2023-12-01 00:05:37',
                'updated_at' => '2023-12-01 00:05:37'
            ],
            [
                'id' => '83',
                'mainmenu_id' => '25',
                'language' => 'ru',
                'name' => 'Документация',
                'created_at' => '2023-12-02 00:05:37',
                'updated_at' => '2023-12-02 00:05:37'
            ],
            [
                'id' => '84',
                'mainmenu_id' => '26',
                'language' => 'ru',
                'name' => 'Примечания к выпуску',
                'created_at' => '2023-12-03 00:05:37',
                'updated_at' => '2023-12-03 00:05:37'
            ],
            [
                'id' => '85',
                'mainmenu_id' => '27',
                'language' => 'ru',
                'name' => 'Обратная связь',
                'created_at' => '2023-12-04 00:05:37',
                'updated_at' => '2023-12-04 00:05:37'
            ],
            [
                'id' => '86',
                'mainmenu_id' => '28',
                'language' => 'ru',
                'name' => 'Настройки',
                'created_at' => '2023-12-05 00:05:37',
                'updated_at' => '2023-12-05 00:05:37'
            ],
            [
                'id' => '87',
                'mainmenu_id' => '29',
                'language' => 'ru',
                'name' => 'Системные настройки',
                'created_at' => '2023-12-06 00:05:37',
                'updated_at' => '2023-12-06 00:05:37'
            ]
        ];
        $this->db->table('menu_item_translations')->insertBatch($data);
    }
}
