<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Menus;

use CodeIgniter\Database\Seeder;

class MainMenuSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-09-12 00:05:37',
                'updated_at' => '2023-09-12 00:05:37'
            ],
            [
                'id' => '2',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '1',
                'url' => 'dashboard',
                'type' => 'link',
                'created_at' => '2023-09-13 00:05:37',
                'updated_at' => '2023-09-13 00:05:37'
            ],
            [
                'id' => '3',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-09-14 00:05:37',
                'updated_at' => '2023-09-14 00:05:37'
            ],
            [
                'id' => '4',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '3',
                'url' => 'dashboard/notifications',
                'type' => 'link',
                'created_at' => '2023-09-15 00:05:37',
                'updated_at' => '2023-09-15 00:05:37'
            ],
            [
                'id' => '5',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '3',
                'url' => 'dashboard/notifications/add',
                'type' => 'link',
                'created_at' => '2023-09-16 00:05:37',
                'updated_at' => '2023-09-16 00:05:37'
            ],
            [
                'id' => '6',
                'status' => '1',
                'order' => '3',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-09-17 00:05:37',
                'updated_at' => '2023-09-17 00:05:37'
            ],
            [
                'id' => '7',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '6',
                'url' => 'dashboard/users',
                'type' => 'link',
                'created_at' => '2023-09-18 00:05:37',
                'updated_at' => '2023-09-18 00:05:37'
            ],
            [
                'id' => '8',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '6',
                'url' => 'dashboard/users/add',
                'type' => 'link',
                'created_at' => '2023-09-19 00:05:37',
                'updated_at' => '2023-09-19 00:05:37'
            ],
            [
                'id' => '9',
                'status' => '1',
                'order' => '4',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-09-20 00:05:37',
                'updated_at' => '2023-09-20 00:05:37'
            ],
            [
                'id' => '10',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '9',
                'url' => 'dashboard/roles',
                'type' => 'link',
                'created_at' => '2023-09-21 00:05:37',
                'updated_at' => '2023-09-21 00:05:37'
            ],
            [
                'id' => '11',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '9',
                'url' => 'dashboard/roles/add',
                'type' => 'link',
                'created_at' => '2023-09-22 00:05:37',
                'updated_at' => '2023-09-22 00:05:37'
            ],
            [
                'id' => '12',
                'status' => '1',
                'order' => '5',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-09-23 00:05:37',
                'updated_at' => '2023-09-23 00:05:37'
            ],
            [
                'id' => '13',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '12',
                'url' => 'dashboard/permissions',
                'type' => 'link',
                'created_at' => '2023-09-24 00:05:37',
                'updated_at' => '2023-09-24 00:05:37'
            ],
            [
                'id' => '14',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '12',
                'url' => 'dashboard/permissions/add',
                'type' => 'link',
                'created_at' => '2023-09-25 00:05:37',
                'updated_at' => '2023-09-25 00:05:37'
            ],
            [
                'id' => '15',
                'status' => '1',
                'order' => '6',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-09-26 00:05:37',
                'updated_at' => '2023-09-26 00:05:37'
            ],
            [
                'id' => '16',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '15',
                'url' => 'dashboard/menus ',
                'type' => 'link',
                'created_at' => '2023-09-27 00:05:37',
                'updated_at' => '2023-09-27 00:05:37'
            ],
            [
                'id' => '17',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '15',
                'url' => 'dashboard/menus/add',
                'type' => 'link',
                'created_at' => '2023-09-28 00:05:37',
                'updated_at' => '2023-09-28 00:05:37'
            ],
            [
                'id' => '18',
                'status' => '1',
                'order' => '7',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-09-29 00:05:37',
                'updated_at' => '2023-09-29 00:05:37'
            ],
            [
                'id' => '19',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '18',
                'url' => 'null',
                'type' => 'dropdown',
                'created_at' => '2023-09-30 00:05:37',
                'updated_at' => '2023-09-30 00:05:37'
            ],
            [
                'id' => '20',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '19',
                'url' => 'dashboard/documentation/user-guide',
                'type' => 'link',
                'created_at' => '2023-10-01 00:05:37',
                'updated_at' => '2023-10-01 00:05:37'
            ],
            [
                'id' => '21',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '19',
                'url' => 'dashboard/documentation/user-guide/add',
                'type' => 'link',
                'created_at' => '2023-10-02 00:05:37',
                'updated_at' => '2023-10-02 00:05:37'
            ],
            [
                'id' => '22',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '18',
                'url' => 'null',
                'type' => 'dropdown',
                'created_at' => '2023-10-03 00:05:37',
                'updated_at' => '2023-09-21 22:38:45'
            ],
            [
                'id' => '23',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '22',
                'url' => 'dashboard/documentation/release-notes',
                'type' => 'link',
                'created_at' => '2023-10-04 00:05:37',
                'updated_at' => '2023-09-21 22:38:50'
            ],
            [
                'id' => '24',
                'status' => '1',
                'order' => '2',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '22',
                'url' => 'dashboard/documentation/release-notes/add',
                'type' => 'link',
                'created_at' => '2023-10-05 00:05:37',
                'updated_at' => '2023-09-21 22:38:54'
            ],
            [
                'id' => '25',
                'status' => '1',
                'order' => '3',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '18',
                'url' => 'documentation/user-guide',
                'type' => 'link',
                'created_at' => '2023-10-06 00:05:37',
                'updated_at' => '2023-10-06 00:05:37'
            ],
            [
                'id' => '26',
                'status' => '1',
                'order' => '4',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '18',
                'url' => 'documentation/release-notes',
                'type' => 'link',
                'created_at' => '2023-10-07 00:05:37',
                'updated_at' => '2023-10-07 00:05:37'
            ],
            [
                'id' => '27',
                'status' => '1',
                'order' => '5',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '18',
                'url' => 'support/feedback',
                'type' => 'link',
                'created_at' => '2023-10-08 00:05:37',
                'updated_at' => '2023-10-08 00:05:37'
            ],
            [
                'id' => '28',
                'status' => '1',
                'order' => '8',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '0',
                'url' => 'null',
                'type' => 'header',
                'created_at' => '2023-10-08 00:05:37',
                'updated_at' => '2023-10-08 00:05:37'
            ],
            [
                'id' => '29',
                'status' => '1',
                'order' => '1',
                'options' => '{"menu-icon": "card-list"}',
                'parent_id' => '28',
                'url' => 'dashboard/settings',
                'type' => 'link',
                'created_at' => '2023-10-09 00:05:37',
                'updated_at' => '2023-10-09 00:05:37'
            ]
        ];

        $this->db->table('mainmenu')->insertBatch($data);
    }
}
