<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\ReleaseNotes;

use CodeIgniter\Database\Seeder;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\ReleaseNotes\Versions;

/**
 * ReleaseNoteseSeeder
 *
 * Generates documentation sections and content for each supported locale language.
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\ReleaseNotes;
 */
class ReleaseNoteseSeeder extends Seeder
{
    /**
     * Runs the function.
     *
     * This function generates documentation sections and content for each supported locale language.
     *
     * @throws Exception If an error occurs during the generation of documentation sections and content.
     */
    public function run()
    {
        // Get the supported languages from the configuration
        $languages = config('App')->supportedLocales;

        // Create an array of versions objects
        $versions = [
            new Versions\Version001Dev(),
            new Versions\Version011Dev(),

        ];

        // Create start id for the documentation sections
        $versions_id = 1;

        // Get the current date
        $currentDate = date('Y-m-d H:i:s');

        // Loop through the supported languages
        foreach ($languages as $language) {
            // Loop through the sections
            foreach ($versions as $keyVersions  => $version) {
                // Get the sections for the current language
                $item = $version->getVersion($language);

                // Skip if there are no sections for the current language
                if ($item == null) {
                    continue;
                }


                // Create the base row for the documentation section
                $rowBase = [
                    'id' => $versions_id,
                    'language' => $language,
                    'parent_id' => 0,
                    'status' => 1,
                    'order' => $keyVersions + 1,
                    'created_at' => $currentDate,
                    'updated_at' => $currentDate
                ];

                // Merge the base row with the sections for the current language
                $row = array_merge($rowBase, $item);

                // Remove the contents from row for the current language
                unset($row["changes"]);

                // Insert the documentation section into the database
                $this->db->table('documentation_sections')->insert($row);

                // Check if there are contents for the section
                if (is_array($item["changes"]) and array_key_exists("changes", $item) and count($item["changes"]) > 0) {
                    // Loop through the contents for the current language
                    foreach ($item["changes"] as $keyVersion => $valueVersion) {
                        // Create the base row content for the documentation section
                        $rowBaseContent = [
                            'section_id' => $versions_id,
                            'language' => $language,
                            'status' => 1,
                            'order' => $keyVersion + 1,
                            'created_at' => $currentDate,
                            'updated_at' => $currentDate
                        ];

                        // Merge the base row content with the content for the current language
                        $rowContent = array_merge($rowBaseContent, $valueVersion);

                        // Insert the documentation content into the database
                        $this->db->table('documentation_content')->insert($rowContent);
                    }
                }
                // Increase the section id for the next section
                $versions_id++;
            }
        }
    }
}
