<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\ReleaseNotes\Versions;

use CodeIgniter\Database\Seeder;

/**
 * Version001Dev
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\ReleaseNotes\Versions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Version001Dev
{
    public function getVersion($language): array
    {
        return [
            'version' => '0.0.1-dev',
            'slug' => 'v0-0-1-dev',
            'release_date' => '2023-09-20 08:53:28',
            'project' => 'AppStarter',
            'changes' => $this->getChanges($language),
        ];
    }

    private function getChanges($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    'content' => 'Initial release',
                ];
            case 'ru':
                return [
                    'content' => 'Первая релиз',
                ];
            case 'lv':
                return [
                    'content' => 'Pirma pārējā versija',
                ];
            default:
                return [];
        }
    }
}
