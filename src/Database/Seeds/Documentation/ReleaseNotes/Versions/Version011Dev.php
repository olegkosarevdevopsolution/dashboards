<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\ReleaseNotes\Versions;

use CodeIgniter\Database\Seeder;

/**
 * Version001Dev
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\ReleaseNotes\Versions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Version011Dev
{
    public function getVersion($language): array
    {
        return [
            'version' => '0.1.1-dev',
            'slug' => 'v0-1-1-dev',
            'release_date' => '2023-10-01 08:53:28',
            'project' => 'AppStarter',
            'change' => $this->getChanges($language),
        ];
    }

    private function getChanges($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    'content' => '',
                ];
            case 'ru':
                return [
                    'content' => '',
                ];
            case 'lv':
                return [
                    'content' => '',
                ];
            default:
                return [];
        }
    }
}
