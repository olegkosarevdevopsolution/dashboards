<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Profile
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Profile',
                    'slug' => 'profile',
                    'description' => 'Profile description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Профиль',
                    'slug' => 'profile',
                    'description' => 'Профиль описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Profils',
                    'slug' => 'profile',
                    'description' => 'Profils apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Profile editing',
                        'slug' => 'profile-editing',
                        'description' => 'Profile editing description',
                    ],
                    [
                        'title' => 'Change password',
                        'slug' => 'change-password',
                        'description' => 'Change password description',
                    ],
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Редактирование профиля',
                        'slug' => 'profile-editing',
                        'description' => 'Редактирование профиля описание',
                    ],
                    [
                        'title' => 'Смена пароля',
                        'slug' => 'change-password',
                        'description' => 'Смена пароля описание',
                    ],
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Profils redigēšana',
                        'slug' => 'profile-editing',
                        'description' => 'Profils redigēšana apraksts',
                    ],
                    [
                        'title' => 'Paroles izmaini',
                        'slug' => 'change-password',
                        'description' => 'Paroles izmaini apraksts',
                    ],
                ];
            default:
                return [];
        }
    }
}
