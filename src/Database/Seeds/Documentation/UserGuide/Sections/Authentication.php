<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * AuthenticationSection
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Authentication
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Authentication',
                    'slug' => 'authentication',
                    'description' => 'Authentication description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Авторизация',
                    'slug' => 'authentication',
                    'description' => 'Описание авторизации',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Autentifikācija',
                    'slug' => 'authentication',
                    'description' => 'Autentifikācija apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }

    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return
                    [
                        [
                            'title' => 'Login',
                            'slug' => 'login',
                            'description' => 'Login description'
                        ],
                        [
                            'title' => 'Screen lock',
                            'slug' => 'sceen-lock',
                            'description' => 'Screen lock description'

                        ],
                        [
                            'title' => 'Logout',
                            'slug' => 'logout',
                            'description' => 'Logout description'
                        ],
                        [
                            'title' => 'Password recovery',
                            'slug' => 'password-recovery',
                            'description' => 'Password recovery description',

                        ],
                        [
                            'title' => 'Password reset',
                            'slug' => 'password-reset',
                            'description' => 'Password reset description',

                        ],
                        [
                            'title' => 'Registration',
                            'slug' => 'registration',
                            'description' => 'Registration description',
                        ],
                    ];
            case 'ru':
                return
                    [
                        [
                            'title' => 'Вход',
                            'slug' => 'login',
                            'description' => 'Вход описание'
                        ],
                        [
                            'title' => 'Блокировка экрана',
                            'slug' => 'sceen-lock',
                            'description' => 'Блокировка экрана описание'

                        ],
                        [
                            'title' => 'Выход',
                            'slug' => 'logout',
                            'description' => 'Выход описание'
                        ],
                        [
                            'title' => 'Восстановление пароля',
                            'slug' => 'password-recovery',
                            'description' => 'Восстановление пароля описание',

                        ],
                        [
                            'title' => 'Сброс пароля',
                            'slug' => 'password-reset',
                            'description' => 'Сброс пароля описание',

                        ],
                        [
                            'title' => 'Регистрация',
                            'slug' => 'registration',
                            'description' => 'Регистрация описание',
                        ],
                    ];
            case 'lv':
                return
                    [
                        [
                            'title' => 'Ieeja',
                            'slug' => 'login',
                            'description' => 'Ieeja apraksts'
                        ],
                        [
                            'title' => 'Ekrāna bloķēšana',
                            'slug' => 'sceen-lock',
                            'description' => 'Ekrāna bloķēšana apraksts'

                        ],
                        [
                            'title' => 'Izlogoties',
                            'slug' => 'logout',
                            'description' => 'Izlogoties apraksts'
                        ],
                        [
                            'title' => 'Paroles atjaunošana',
                            'slug' => 'password-recovery',
                            'description' => 'Paroles atjaunošana apraksts',

                        ],
                        [
                            'title' => 'Paroles atiestatīšana',
                            'slug' => 'password-reset',
                            'description' => 'Paroles atiestatīšana apraksts',

                        ],
                        [
                            'title' => 'Reģistrācija',
                            'slug' => 'registration',
                            'description' => 'Reģistrācija apraksts',
                        ],
                    ];
            default:
                return [];
        }
    }
}
