<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Menu
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Menu',
                    'slug' => 'menu',
                    'description' => 'Menu description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Меню',
                    'slug' => 'menu',
                    'description' => 'Меню описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Izvēlne',
                    'slug' => 'menu',
                    'description' => 'Izvēlne apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Menu items',
                        'slug' => 'menu-items',
                        'description' => 'Menu items description',
                    ],
                    [
                        'title' => 'Editing menu item',
                        'slug' => 'editing-menu-item',
                        'description' => 'Editing menu item description',
                    ],
                    [
                        'title' => 'Adding menu item',
                        'slug' => 'adding-menu-item',
                        'description' => 'Adding menu item description',
                    ],
                    [
                        'title' => 'Removing menu item',
                        'slug' => 'removing-menu-item',
                        'description' => 'Removing menu item description',
                    ],
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Список меню сущностей',
                        'slug' => 'menu-items',
                        'description' => 'Список меню сущностей описание',
                    ],
                    [
                        'title' => 'Редактирование пункта меню',
                        'slug' => 'editing-menu-item',
                        'description' => 'Редактирование пункта меню описание',
                    ],
                    [
                        'title' => 'Добавление пункта меню',
                        'slug' => 'adding-menu-item',
                        'description' => 'Добавление пункта меню описание',
                    ],
                    [
                        'title' => 'Удаление пункта меню',
                        'slug' => 'removing-menu-item',
                        'description' => 'Удаление пункта меню описание',
                    ]
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Izvēlnes vienumi',
                        'slug' => 'menu-items',
                        'description' => 'Izvēlnes vienumi apraksts',
                    ],
                    [
                        'title' => 'Rediģēt izvēlnes vienumu',
                        'slug' => 'editing-menu-item',
                        'description' => 'Rediģēt izvēlnes vienumu apraksts',
                    ],
                    [
                        'title' => 'Pievienots izvēlnes vienumu',
                        'slug' => 'adding-menu-item',
                        'description' => 'Pievienots izvēlnes vienumu apraksts',
                    ],
                    [
                        'title' => 'Noņemt izvēlnes vienums',
                        'slug' => 'removing-menu-item',
                        'description' => 'Noņemt izvēlnes vienums apraksts',
                    ],
                ];
            default:
                return [];
        }
    }
}
