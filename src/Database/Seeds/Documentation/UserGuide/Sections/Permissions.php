<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Permissions
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Permissions',
                    'slug' => 'permissions',
                    'description' => 'Permissions description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Разрешения',
                    'slug' => 'permissions',
                    'description' => 'Разрешения описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Atļaujas',
                    'slug' => 'permissions',
                    'description' => 'Atļaujas apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Permissions list',
                        'slug' => 'permissions-list',
                        'description' => 'Permissions list description'
                    ],
                    [
                        'title' => 'Editing permission',
                        'slug' => 'editing-permission',
                        'description' => 'Editing permission description'
                    ],
                    [
                        'title' => 'Adding permission',
                        'slug' => 'adding-permission',
                        'description' => 'Adding permission description'
                    ],
                    [
                        'title' => 'Removing permission',
                        'slug' => 'removing-permission',
                        'description' => 'Removing permission description'
                    ],
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Список разрешений',
                        'slug' => 'permissions-list',
                        'description' => 'Permissions list description'
                    ],
                    [
                        'title' => 'Редактирование разрешения',
                        'slug' => 'editing-permission',
                        'description' => 'Редактирование разрешения описание'
                    ],
                    [
                        'title' => 'Добавление разрешения',
                        'slug' => 'adding-permission',
                        'description' => 'Добавление разрешения описание'
                    ],
                    [
                        'title' => 'Удаление разрешения',
                        'slug' => 'removing-permission',
                        'description' => 'Удаление разрешения описание'
                    ],
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Atļauju saraksts',
                        'slug' => 'permissions-list',
                        'description' => 'Atļauju saraksts apraksts'
                    ],
                    [
                        'title' => 'Rediģēšanas atļauja',
                        'slug' => 'editing-permission',
                        'description' => 'Rediģēšanas atļauja apraksts'
                    ],
                    [
                        'title' => 'Atļaujas pievienošana',
                        'slug' => 'adding-permission',
                        'description' => 'Atļaujas pievienošana apraksts'
                    ],
                    [
                        'title' => 'Atļauju noņemšana',
                        'slug' => 'removing-permission',
                        'description' => 'Atļauju noņemšana apraksts'
                    ],
                ];
            default:
                return [];
        }
    }
}
