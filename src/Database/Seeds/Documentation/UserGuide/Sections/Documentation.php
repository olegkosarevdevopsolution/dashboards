<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Documentation
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Documentation',
                    'slug' => 'documentation',
                    'description' => 'Documentation description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Документация',
                    'slug' => 'documentation',
                    'description' => 'Документация описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Dokumentācija',
                    'slug' => 'documentation',
                    'description' => 'Dokumentācija apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'User guide',
                        'slug' => 'user-guide',
                        'description' => 'User guide description',
                    ],
                    [
                        'title' => 'Release Notes',
                        'slug' => 'release-notes',
                        'description' => 'Release Notes description',
                    ]
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Пользовательское руководство',
                        'slug' => 'user-guide',
                        'description' => '<p>Добро пожаловать в наше пользовательское руководство. Здесь вы найдете детальную информацию о том, как использовать наше приложение или сервис, чтобы получить максимальную отдачу от него.</p>
<p>Чтобы начать, рекомендуется ознакомиться с оглавлением руководства. В оглавлении вы найдете список разделов, которые охватывают различные аспекты нашего продукта. Вы можете выбрать интересующий вас раздел и перейти к его изучению.</p>
<p>В каждом разделе вы найдете подробное описание функций и возможностей нашего приложения или сервиса. Мы предоставляем пошаговые инструкции о том, как использовать конкретные функции, а также даем полезные советы и рекомендации.</p>
<p>Если у вас возникнут какие-либо вопросы или проблемы в процессе использования нашего продукта, не стесняйтесь обратиться в нашу службу поддержки. Наша команда готова помочь вам решить любые трудности и ответить на все ваши вопросы.</p>
<p>Мы благодарим вас за выбор нашего приложения или сервиса и надеемся, что наше пользовательское руководство поможет вам использовать наш продукт максимально эффективно и комфортно.</p>',
                    ],
                    [
                        'title' => 'Заметки о выпуске',
                        'slug' => 'release-notes',
                        'description' => '<p>Заметки о выпуске - это информация о всех изменениях, которые были внесены в наше приложение или сервис в каждой новой версии. В этом разделе вы найдете подробную информацию о каждом выпуске, включая новые функции, улучшения, исправления ошибок и другие изменения.</p>
<p>Мы рекомендуем вам ознакомиться с заметками о выпускеес<p>Если у вас возникнут вопросы или проблемы в процессе использования новой версии, обратитесь к нашей службе поддержки, и мы с радостью поможем вам. Мы ценим ваше мнение и всегда открыты к обратной связи, поэтому не стесняйтесь сообщать нам о любых проблемах или предложениях по улучшению нашего продукта.</p>
<p>Спасибо, что выбрали наше приложение или сервис! Мы надеемся, что наши обновления будут полезны для вас и помогут сделать использование нашего продукта еще более комфортным и эффективным.</p>',
                    ]
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Lietotāja rokasgrāmata',
                        'slug' => 'user-guide',
                        'description' => 'Lietotāja rokasgrāmata apraksts',
                    ],
                    [
                        'title' => 'Piezīmes par izlaidumu',
                        'slug' => 'release-notes',
                        'description' => 'Piezīmes par izlaidumu apraksts',
                    ]
                ];
            default:
                return [];
        }
    }
}
