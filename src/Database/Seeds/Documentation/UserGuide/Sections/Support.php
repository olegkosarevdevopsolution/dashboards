<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Support
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Support',
                    'slug' => 'support',
                    'description' => 'Support description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Поддержка',
                    'slug' => 'support',
                    'description' => 'Поддержка описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Atbalsts',
                    'slug' => 'support',
                    'description' => 'Atbalsts apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Feedback form',
                        'slug' => 'feedback-form',
                        'description' => 'Feedback form description',
                    ]
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Форма обратной связи',
                        'slug' => 'feedback-form',
                        'description' => 'Форма обратной связи описание',
                    ]
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Atsauksmju veidlapa',
                        'slug' => 'feedback-form',
                        'description' => 'Atsauksmju veidlapa apraksts',
                    ]
                ];
            default:
                return [];
        }
    }
}
