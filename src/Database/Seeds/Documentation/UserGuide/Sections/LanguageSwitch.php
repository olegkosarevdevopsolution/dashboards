<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class LanguageSwitch
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Language Switch',
                    'slug' => 'language-switch',
                    'description' => 'Language Switch description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Языковой переключатель',
                    'slug' => 'language-switch',
                    'description' => 'Языковой переключатель описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Valodas slēdzis',
                    'slug' => 'language-switch',
                    'description' => 'Valodas slēdzis apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Language Switch',
                        'slug' => 'language-switch',
                        'description' => 'Language Switch description',
                    ]
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Языковой переключатель',
                        'slug' => 'language-switch',
                        'description' => 'Языковой переключатель описание',
                    ]
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Valodas slēdzis',
                        'slug' => 'language-switch',
                        'description' => 'Valodas slēdzis apraksts',
                    ]
                ];
            default:
                return [];
        }
    }
}
