<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Settings
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Settings',
                    'slug' => 'settings',
                    'description' => 'Settings description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Настройки',
                    'slug' => 'settings',
                    'description' => 'Настройки описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Iestatījumi',
                    'slug' => 'settings',
                    'description' => 'Iestatījumi apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Editing settings',
                        'slug' => 'editing-settings',
                        'description' => 'Editing settings description',
                    ]
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Редактирование настроек',
                        'slug' => 'editing-settings',
                        'description' => 'Редактирование настроек описание',
                    ]
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Iestatījumu rediģēšana',
                        'slug' => 'editing-settings',
                        'description' => 'Iestatījumu rediģēšana apraksts',
                    ]
                ];
            default:
                return [];
        }
    }
}
