<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Users
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Users',
                    'slug' => 'users',
                    'description' => 'Users description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Пользователи',
                    'slug' => 'users',
                    'description' => 'Пользователи описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Lietotāji',
                    'slug' => 'users',
                    'description' => 'Lietotāji apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Users list',
                        'slug' => 'users-list',
                        'description' => 'Users list description'
                    ],
                    [
                        'title' => 'Editing user',
                        'slug' => 'editing-user',
                        'description' => 'Editing user description'
                    ],
                    [
                        'title' => 'Adding user',
                        'slug' => 'adding-user',
                        'description' => 'Adding user description'
                    ],
                    [
                        'title' => 'Removing user',
                        'slug' => 'removing-user',
                        'description' => 'Removing user description'
                    ],
                    [
                        'title' => 'Blocking user',
                        'slug' => 'blocking-user',
                        'description' => 'Blocking user description'
                    ],
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Список пользователей',
                        'slug' => 'users-list',
                        'description' => 'Список пользователей описание'
                    ],
                    [
                        'title' => 'Редактирование пользователя',
                        'slug' => 'editing-user',
                        'description' => 'Редактирование пользователя описание'
                    ],
                    [
                        'title' => 'Добавление пользователя',
                        'slug' => 'adding-user',
                        'description' => 'Добавление пользователя описание'
                    ],
                    [
                        'title' => 'Удаление пользователя',
                        'slug' => 'removing-user',
                        'description' => 'Удаление пользователя описание'
                    ],
                    [
                        'title' => 'Блокировка пользователя',
                        'slug' => 'blocking-user',
                        'description' => 'Блокировка пользователя описание'
                    ],
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Lietotāju saraksts',
                        'slug' => 'users-list',
                        'description' => 'Lietotāju saraksts apraksts'
                    ],
                    [
                        'title' => 'Lietotāja rediģēšana',
                        'slug' => 'editing-user',
                        'description' => 'Lietotāja rediģēšana apraksts'
                    ],
                    [
                        'title' => 'Lietotāja pievienošana',
                        'slug' => 'adding-user',
                        'description' => 'Lietotāja pievienošana apraksts'
                    ],
                    [
                        'title' => 'Lietotāja noņemšana',
                        'slug' => 'removing-user',
                        'description' => 'Lietotāja noņemšana apraksts'
                    ],
                    [
                        'title' => 'Lietotāja bloķēšana',
                        'slug' => 'blocking-user',
                        'description' => 'Lietotāja bloķēšana apraksts'
                    ],
                ];
            default:
                return [];
        }
    }
}
