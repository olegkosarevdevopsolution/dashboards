<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Notifications
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Notifications',
                    'slug' => 'notifications',
                    'description' => 'Notifications description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Уведомления',
                    'slug' => 'notifications',
                    'description' => 'Уведомления описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Paziņojumi',
                    'slug' => 'notifications',
                    'description' => 'Paziņojumi apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }

    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Notifications list',
                        'slug' => 'notifications-list',
                        'description' => 'Notifications list description'
                    ],
                    [
                        'title' => 'Adding notifications',
                        'slug' => 'adding-notification',
                        'description' => 'Adding notifications description'
                    ],
                    [
                        'title' => 'Removing notification',
                        'slug' => 'removing-notification',
                        'description' => 'Removing notification description'
                    ],
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Список уведомлений',
                        'slug' => 'notifications-list',
                        'description' => 'Список уведомлений описание'
                    ],
                    [
                        'title' => 'Добавление уведомления',
                        'slug' => 'adding-notifications',
                        'description' => 'Добавление уведомления описание'
                    ],
                    [
                        'title' => 'Удаление уведомления',
                        'slug' => 'removing-notifications',
                        'description' => 'Удаление уведомления описание'
                    ],
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Paziņojumu saraksts',
                        'slug' => 'notifications-list',
                        'description' => 'Paziņojumu saraksts apraksts'
                    ],
                    [
                        'title' => 'Paziņojuma pievienošana',
                        'slug' => 'adding-notification',
                        'description' => 'Paziņojuma pievienošana apraksts'
                    ],
                    [
                        'title' => 'Paziņojuma noņemšana',
                        'slug' => 'removing-notification',
                        'description' => 'Paziņojuma noņemšana apraksts'
                    ],
                ];
            default:
                return [];
        }
    }
}
