<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;

/**
 * LanguageSwitch
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Roles
{
    /**
     * Retrieves the sections for a given language.
     *
     * @param string $language The language code.
     * @return array|null An array containing the sections for the given language, or null if the language is not supported.
     */
    public function getSections($language): ?array
    {
        switch ($language) {
            case 'en':
                return [
                    'title' => 'Roles',
                    'slug' => 'roles',
                    'description' => 'Roles description',
                    'contents' => $this->getContent($language),
                ];
            case 'ru':
                return [
                    'title' => 'Роли',
                    'slug' => 'roles',
                    'description' => 'Роли описание',
                    'contents' => $this->getContent($language),
                ];
            case 'lv':
                return [
                    'title' => 'Lomas',
                    'slug' => 'roles',
                    'description' => 'Lomas apraksts',
                    'contents' => $this->getContent($language),
                ];
            default:
                return null;
        }
    }
    /**
     * Retrieves the content based on the specified language.
     *
     * @param string $language The language code to retrieve the content for.
     * @return array The content array for the specified language.
     */
    private function getContent($language): array
    {
        switch ($language) {
            case 'en':
                return [
                    [
                        'title' => 'Roles list',
                        'slug' => 'roles-list',
                        'description' => 'Roles list description',
                    ],
                    [
                        'title' => 'Editing role',
                        'slug' => 'editing-role',
                        'description' => 'Editing role description',
                    ],
                    [
                        'title' => 'Adding role',
                        'slug' => 'adding-role',
                        'description' => 'Adding role description',
                    ],
                    [
                        'title' => 'Removing role',
                        'slug' => 'removing-role',
                        'description' => 'Removing role description',
                    ],
                ];
            case 'ru':
                return [
                    [
                        'title' => 'Список ролей',
                        'slug' => 'roles-list',
                        'description' => 'Список ролей описание',
                    ],
                    [
                        'title' => 'Редактирование роли',
                        'slug' => 'editing-role',
                        'description' => 'Редактирование роли описание',
                    ],
                    [
                        'title' => 'Добавление роли',
                        'slug' => 'adding-role',
                        'description' => 'Добавление роли описание',
                    ],
                    [
                        'title' => 'Удаление роли',
                        'slug' => 'removing-role',
                        'description' => 'Удаление роли описание',
                    ],
                ];
            case 'lv':
                return [
                    [
                        'title' => 'Lomu saraksts',
                        'slug' => 'roles-list',
                        'description' => 'Lomu saraksts apraksts',
                    ],
                    [
                        'title' => 'Rediģēšanas loma',
                        'slug' => 'editing-role',
                        'description' => 'Rediģēšanas loma apraksts',
                    ],
                    [
                        'title' => 'Lomas pievienošana',
                        'slug' => 'adding-role',
                        'description' => 'Lomas pievienošana apraksts',
                    ],
                    [
                        'title' => 'Noņem lomu',
                        'slug' => 'removing-role',
                        'description' => 'Noņem lomu apraksts',
                    ],
                ];
            default:
                return [];
        }
    }
}
