<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide;

use DateTime;
use CodeIgniter\Database\Seeder;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Authentication;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\LanguageSwitch;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Documentation;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Support;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Notifications;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Permissions;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Roles;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Profile;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Users;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Settings;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\Menu;
use Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide\Sections\DocumentationDashboardSection;

/**
 * UserGuideSeeder
 *
 * Generates documentation sections and content for each supported locale language.
 *
 * @package \Devolegkosarev\Dashboard\Database\Seeds\Documentation\UserGuide;
 */
class UserGuideSeeder extends Seeder
{
    /**
     * Runs the function.
     *
     * This function generates documentation sections and content for each supported locale language.
     *
     * @throws Exception If an error occurs during the generation of documentation sections and content.
     */
    public function run()
    {
        // Get the supported languages from the configuration
        $languages = config('App')->supportedLocales;

        // Create an array of section objects
        $sections = [
            new LanguageSwitch(),
            new Authentication(),
            new Menu(),
            new Profile(),
            new Notifications(),
            new Roles(),
            new Permissions(),
            new Users(),
            new Settings(),
            // new DocumentationDashboardSection(),
            new Documentation(),
            new Support(),
        ];

        // Create start id for the documentation sections
        $section_id = 1;

        // Get the current date
        $currentDate = date('Y-m-d H:i:s');

        // Loop through the supported languages
        foreach ($languages as $language) {
            // Loop through the sections
            foreach ($sections as $keySections  => $section) {
                // Get the sections for the current language
                $item = $section->getSections($language);

                // Skip if there are no sections for the current language
                if ($item == null) {
                    continue;
                }


                // Create the base row for the documentation section
                $rowBase = [
                    'id' => $section_id,
                    'language' => $language,
                    'parent_id' => 0,
                    'status' => 1,
                    'order' => $keySections + 1,
                    'created_at' => $currentDate,
                    'updated_at' => $currentDate
                ];

                // Merge the base row with the sections for the current language
                $row = array_merge($rowBase, $item);

                // Remove the contents from row for the current language
                unset($row["contents"]);

                // Insert the documentation section into the database
                $this->db->table('documentation_sections')->insert($row);

                // Check if there are contents for the section
                if (is_array($item["contents"]) and array_key_exists("contents", $item) and count($item["contents"]) > 0) {
                    // Loop through the contents for the current language
                    foreach ($item["contents"] as $keyContent => $valueContent) {
                        // Create the base row content for the documentation section
                        $rowBaseContent = [
                            'section_id' => $section_id,
                            'language' => $language,
                            'status' => 1,
                            'order' => $keyContent + 1,
                            'created_at' => $currentDate,
                            'updated_at' => $currentDate
                        ];

                        // Merge the base row content with the content for the current language
                        $rowContent = array_merge($rowBaseContent, $valueContent);

                        // Insert the documentation content into the database
                        $this->db->table('documentation_content')->insert($rowContent);
                    }
                }
                // Increase the section id for the next section
                $section_id++;
            }
        }
    }
}
