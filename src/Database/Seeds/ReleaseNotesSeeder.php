<?php

namespace Devolegkosarev\Dashboard\Database\Seeds;

use CodeIgniter\Database\Seeder;

class ReleaseNotesSeeder extends Seeder
{
    public function run()
    {
        // Создаем массив с демо данными
        $data = [
            [
                'version' => '1.0.0',
                'slug' => 'pervyj-vypusk',
                'release_date' => '2023-01-01',
                'version_status' => 'pre-alpha',
                'publish_status' => '1',
                'changes' => [
                    [
                        'language' => 'ru',
                        'content' => 'В этом выпуске мы представляем наше новое программное обеспечение, которое ...'
                    ],
                    [
                        'language' => 'lv',
                        'content' => 'Šajā izlaidumā mēs iepazīstinām ar mūsu jauno programmatūru, kas ...'
                    ],
                    [
                        'language' => 'en',
                        'content' => 'In this release we introduce our new software, which ...'
                    ]
                ]
            ],
            [
                'version' => '1.1.0',
                'slug' => 'novye-funkcii-i-ispravleniya-oshibok',
                'release_date' => '2023-02-15',
                'version_status' => 'beta',
                'publish_status' => '1',
                'changes' => [
                    [
                        'language' => 'ru',
                        'content' => 'В этой версии мы добавили новые функции, такие как ... и исправили некоторые ошибки, связанные с ...'
                    ],
                    [
                        'language' => 'lv',
                        'content' => 'Šajā versijā mēs pievienojām jaunas funkcijas, piemēram, ... un novērsām dažas kļūdas, kas saistītas ar ...'
                    ],
                    [
                        'language' => 'en',
                        'content' => 'In this version we added new features, such as ... and fixed some bugs related to ...'
                    ]
                ]
            ],
            [
                'version' => '1.2.0',
                'slug' => 'uluchshenie-proizvoditelnosti-i-bezopasnosti',
                'release_date' => '2023-03-31',
                'version_status' => 'alpha',
                'publish_status' => '1',
                'changes' => [
                    [
                        'language' => 'ru',
                        'content' => 'В этой версии мы улучшили производительность и безопасность нашего программного обеспечения, используя ...'
                    ],
                    [
                        'language' => 'lv',
                        'content' => 'Šajā versijā mēs uzlabojām mūsu programmatūras veiktspēju un drošību, izmantojot ...'
                    ],
                    [
                        'language' => 'en',
                        'content' => 'In this version we improved the performance and security of our software, using ...'
                    ]
                ]
            ]
        ];

        // Вставляем данные в таблицу releaseNotes
        foreach ($data as $row) {
            $this->db->table('release_notes')->insert([
                'version' => $row['version'],
                'slug' => $row['slug'],
                'release_date' => $row['release_date'],
                'version_status' => $row['version_status'],
                'publish_status' => $row['publish_status']
            ]);
            // Получаем id последней вставленной записи
            $release_notes_id = $this->db->insertID();
            // Вставляем данные в таблицу releaseNotesChange
            foreach ($row['changes'] as $change) {
                $this->db->table('release_notes_change')->insert([
                    'release_notes_id' => $release_notes_id,
                    'language' => $change['language'],
                    'content' => $change['content']
                ]);
            }
        }
    }
}
