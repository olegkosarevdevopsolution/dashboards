<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Roles;

use CodeIgniter\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'type' => 'guest',
                'status' => '1',
                'created_at' => '2023-09-12 00:04:37',
                'updated_at' => '2023-09-13 03:41:04'
            ],
            [
                'id' => '2',
                'type' => 'admin',
                'status' => '1',
                'created_at' => '2023-09-12 00:05:37',
                'updated_at' => '2023-09-13 03:41:51'
            ],
            [
                'id' => '3',
                'type' => 'developer',
                'status' => '1',
                'created_at' => '2023-09-12 00:06:04',
                'updated_at' => '2023-09-13 03:41:59'
            ],
        ];

        $this->db->table('roles')->insertBatch($data);
    }
}
