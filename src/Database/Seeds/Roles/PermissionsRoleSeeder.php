<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Roles;

use CodeIgniter\Database\Seeder;

class PermissionsRoleSeeder extends Seeder
{
    public function run()
    {

        $data = [
            ['id' => '1', 'permission_id' => '1', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '2', 'permission_id' => '2', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '3', 'permission_id' => '3', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '4', 'permission_id' => '4', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '5', 'permission_id' => '5', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '6', 'permission_id' => '6', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '7', 'permission_id' => '7', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '8', 'permission_id' => '8', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '9', 'permission_id' => '9', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '10', 'permission_id' => '10', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '11', 'permission_id' => '11', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '12', 'permission_id' => '12', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '13', 'permission_id' => '13', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '14', 'permission_id' => '14', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '15', 'permission_id' => '15', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '16', 'permission_id' => '16', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '17', 'permission_id' => '17', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '18', 'permission_id' => '18', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '19', 'permission_id' => '19', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '20', 'permission_id' => '20', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '21', 'permission_id' => '21', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '22', 'permission_id' => '22', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '23', 'permission_id' => '23', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '24', 'permission_id' => '24', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '25', 'permission_id' => '25', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '26', 'permission_id' => '26', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '27', 'permission_id' => '27', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '28', 'permission_id' => '28', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '29', 'permission_id' => '29', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '30', 'permission_id' => '30', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '31', 'permission_id' => '1', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '32', 'permission_id' => '2', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '33', 'permission_id' => '3', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '34', 'permission_id' => '4', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '35', 'permission_id' => '7', 'role_id' => '3', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '36', 'permission_id' => '8', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '37', 'permission_id' => '11', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '38', 'permission_id' => '12', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '39', 'permission_id' => '16', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '40', 'permission_id' => '17', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '41', 'permission_id' => '18', 'role_id' => '2', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '42', 'permission_id' => '7', 'role_id' => '1', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '43', 'permission_id' => '11', 'role_id' => '1', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '44', 'permission_id' => '12', 'role_id' => '1', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37']
        ];

        $this->db->table('role_permissions')->insertBatch($data);
    }
}
