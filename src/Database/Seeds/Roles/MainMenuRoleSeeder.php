<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Roles;

use CodeIgniter\Database\Seeder;

class MainMenuRoleSeeder extends Seeder
{
    public function run()
    {
        $data = [
            ['id' => '1', 'role_id' => '3', 'mainmenu_id' => '1', 'created_at' => '2023-10-03 23:10:37', 'updated_at' => '2023-10-03 23:10:37'],
            ['id' => '2', 'role_id' => '3', 'mainmenu_id' => '2', 'created_at' => '2023-10-04 23:10:37', 'updated_at' => '2023-10-04 23:10:37'],
            ['id' => '3', 'role_id' => '3', 'mainmenu_id' => '3', 'created_at' => '2023-10-05 23:10:37', 'updated_at' => '2023-10-05 23:10:37'],
            ['id' => '4', 'role_id' => '3', 'mainmenu_id' => '4', 'created_at' => '2023-10-06 23:10:37', 'updated_at' => '2023-10-06 23:10:37'],
            ['id' => '5', 'role_id' => '3', 'mainmenu_id' => '5', 'created_at' => '2023-10-07 23:10:37', 'updated_at' => '2023-10-07 23:10:37'],
            ['id' => '6', 'role_id' => '3', 'mainmenu_id' => '6', 'created_at' => '2023-10-08 23:10:37', 'updated_at' => '2023-10-08 23:10:37'],
            ['id' => '7', 'role_id' => '3', 'mainmenu_id' => '7', 'created_at' => '2023-10-09 23:10:37', 'updated_at' => '2023-10-09 23:10:37'],
            ['id' => '8', 'role_id' => '3', 'mainmenu_id' => '8', 'created_at' => '2023-10-10 23:10:37', 'updated_at' => '2023-10-10 23:10:37'],
            ['id' => '9', 'role_id' => '3', 'mainmenu_id' => '9', 'created_at' => '2023-10-11 23:10:37', 'updated_at' => '2023-10-11 23:10:37'],
            ['id' => '10', 'role_id' => '3', 'mainmenu_id' => '10', 'created_at' => '2023-10-12 23:10:37', 'updated_at' => '2023-10-12 23:10:37'],
            ['id' => '11', 'role_id' => '3', 'mainmenu_id' => '11', 'created_at' => '2023-10-13 23:10:37', 'updated_at' => '2023-10-13 23:10:37'],
            ['id' => '12', 'role_id' => '3', 'mainmenu_id' => '12', 'created_at' => '2023-10-14 23:10:37', 'updated_at' => '2023-10-14 23:10:37'],
            ['id' => '13', 'role_id' => '3', 'mainmenu_id' => '13', 'created_at' => '2023-10-15 23:10:37', 'updated_at' => '2023-10-15 23:10:37'],
            ['id' => '14', 'role_id' => '3', 'mainmenu_id' => '14', 'created_at' => '2023-10-16 23:10:37', 'updated_at' => '2023-10-16 23:10:37'],
            ['id' => '15', 'role_id' => '3', 'mainmenu_id' => '15', 'created_at' => '2023-10-17 23:10:37', 'updated_at' => '2023-10-17 23:10:37'],
            ['id' => '16', 'role_id' => '3', 'mainmenu_id' => '16', 'created_at' => '2023-10-18 23:10:37', 'updated_at' => '2023-10-18 23:10:37'],
            ['id' => '17', 'role_id' => '3', 'mainmenu_id' => '17', 'created_at' => '2023-10-19 23:10:37', 'updated_at' => '2023-10-19 23:10:37'],
            ['id' => '18', 'role_id' => '3', 'mainmenu_id' => '18', 'created_at' => '2023-10-20 23:10:37', 'updated_at' => '2023-10-20 23:10:37'],
            ['id' => '19', 'role_id' => '3', 'mainmenu_id' => '19', 'created_at' => '2023-10-21 23:10:37', 'updated_at' => '2023-10-21 23:10:37'],
            ['id' => '20', 'role_id' => '3', 'mainmenu_id' => '20', 'created_at' => '2023-10-22 23:10:37', 'updated_at' => '2023-10-22 23:10:37'],
            ['id' => '21', 'role_id' => '3', 'mainmenu_id' => '21', 'created_at' => '2023-10-23 23:10:37', 'updated_at' => '2023-10-23 23:10:37'],
            ['id' => '22', 'role_id' => '3', 'mainmenu_id' => '22', 'created_at' => '2023-10-24 23:10:37', 'updated_at' => '2023-10-24 23:10:37'],
            ['id' => '23', 'role_id' => '3', 'mainmenu_id' => '23', 'created_at' => '2023-10-25 23:10:37', 'updated_at' => '2023-10-25 23:10:37'],
            ['id' => '24', 'role_id' => '3', 'mainmenu_id' => '24', 'created_at' => '2023-10-26 23:10:37', 'updated_at' => '2023-10-26 23:10:37'],
            ['id' => '25', 'role_id' => '3', 'mainmenu_id' => '25', 'created_at' => '2023-10-27 23:10:37', 'updated_at' => '2023-10-27 23:10:37'],
            ['id' => '26', 'role_id' => '3', 'mainmenu_id' => '26', 'created_at' => '2023-10-28 23:10:37', 'updated_at' => '2023-10-28 23:10:37'],
            ['id' => '27', 'role_id' => '3', 'mainmenu_id' => '27', 'created_at' => '2023-10-29 23:10:37', 'updated_at' => '2023-10-29 23:10:37'],
            ['id' => '28', 'role_id' => '3', 'mainmenu_id' => '28', 'created_at' => '2023-10-30 23:10:37', 'updated_at' => '2023-10-30 23:10:37'],
            ['id' => '29', 'role_id' => '3', 'mainmenu_id' => '29', 'created_at' => '2023-10-31 23:10:37', 'updated_at' => '2023-10-31 23:10:37'],
            ['id' => '30', 'role_id' => '2', 'mainmenu_id' => '1', 'created_at' => '2023-11-01 23:10:37', 'updated_at' => '2023-11-01 23:10:37'],
            ['id' => '31', 'role_id' => '2', 'mainmenu_id' => '2', 'created_at' => '2023-11-02 23:10:37', 'updated_at' => '2023-11-02 23:10:37'],
            ['id' => '32', 'role_id' => '2', 'mainmenu_id' => '3', 'created_at' => '2023-11-03 23:10:37', 'updated_at' => '2023-11-03 23:10:37'],
            ['id' => '33', 'role_id' => '2', 'mainmenu_id' => '4', 'created_at' => '2023-11-04 23:10:37', 'updated_at' => '2023-11-04 23:10:37'],
            ['id' => '34', 'role_id' => '2', 'mainmenu_id' => '5', 'created_at' => '2023-11-05 23:10:37', 'updated_at' => '2023-11-05 23:10:37'],
            ['id' => '35', 'role_id' => '2', 'mainmenu_id' => '6', 'created_at' => '2023-11-06 23:10:37', 'updated_at' => '2023-11-06 23:10:37'],
            ['id' => '36', 'role_id' => '3', 'mainmenu_id' => '7', 'created_at' => '2023-11-07 23:10:37', 'updated_at' => '2023-11-07 23:10:37'],
            ['id' => '37', 'role_id' => '2', 'mainmenu_id' => '8', 'created_at' => '2023-11-08 23:10:37', 'updated_at' => '2023-11-08 23:10:37'],
            ['id' => '38', 'role_id' => '2', 'mainmenu_id' => '25', 'created_at' => '2023-11-09 23:10:37', 'updated_at' => '2023-11-09 23:10:37'],
            ['id' => '39', 'role_id' => '2', 'mainmenu_id' => '26', 'created_at' => '2023-11-10 23:10:37', 'updated_at' => '2023-11-10 23:10:37'],
            ['id' => '40', 'role_id' => '2', 'mainmenu_id' => '27', 'created_at' => '2023-11-11 23:10:37', 'updated_at' => '2023-11-11 23:10:37'],
            ['id' => '41', 'role_id' => '1', 'mainmenu_id' => '25', 'created_at' => '2023-11-12 23:10:37', 'updated_at' => '2023-11-12 23:10:37'],
            ['id' => '42', 'role_id' => '1', 'mainmenu_id' => '26', 'created_at' => '2023-11-13 23:10:37', 'updated_at' => '2023-11-13 23:10:37'],
            ['id' => '43', 'role_id' => '1', 'mainmenu_id' => '27', 'created_at' => '2023-11-14 23:10:37', 'updated_at' => '2023-11-14 23:10:37'],
            ['id' => '44', 'role_id' => '1', 'mainmenu_id' => '18', 'created_at' => '2023-11-14 23:10:37', 'updated_at' => '2023-11-14 23:10:37']
        ];

        $this->db->table('role_menu_items')->insertBatch($data);
    }
}
