<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Roles;

use CodeIgniter\Database\Seeder;

class RolesTranslationsSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'role_id' => '1',
                'language' => 'en',
                'name' => 'Guest',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '2',
                'role_id' => '2',
                'language' => 'en',
                'name' => 'Admin',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '3',
                'role_id' => '3',
                'language' => 'en',
                'name' => 'Developer',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '4',
                'role_id' => '1',
                'language' => 'ru',
                'name' => 'Гость',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '5',
                'role_id' => '2',
                'language' => 'ru',
                'name' => 'Администратор',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '6',
                'role_id' => '3',
                'language' => 'ru',
                'name' => 'Разработчик',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '7',
                'role_id' => '1',
                'language' => 'lv',
                'name' => 'Viesis',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '8',
                'role_id' => '2',
                'language' => 'lv',
                'name' => 'Administrators',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
            [
                'id' => '9',
                'role_id' => '3',
                'language' => 'lv',
                'name' => 'Izstrādātājs',
                'created_at' => '2023-09-12 03:25:22',
                'updated_at' => '2023-09-12 03:25:22'
            ],
        ];

        $this->db->table('roles_translations')->insertBatch($data);
    }
}
