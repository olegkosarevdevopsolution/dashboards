<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Permissions;

use CodeIgniter\Database\Seeder;

class PermissionsTranslationsSeeder extends Seeder
{
    public function run()
    {
        $data = [
            // Latvian
            [
                'id' => '1',
                'permission_id' => '1',
                'language' => 'lv',
                'name' => 'Rediģēt citu kontu',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '2',
                'permission_id' => '2',
                'language' => 'lv',
                'name' => 'Rediģēt paša kontu',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '3',
                'permission_id' => '3',
                'language' => 'lv',
                'name' => 'Pievienot kontus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '4',
                'permission_id' => '4',
                'language' => 'lv',
                'name' => 'Skatīt kontus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '5',
                'permission_id' => '5',
                'language' => 'lv',
                'name' => 'Pievienot izmaiņas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '6',
                'permission_id' => '6',
                'language' => 'lv',
                'name' => 'Rediģēt izmaiņas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '7',
                'permission_id' => '7',
                'language' => 'lv',
                'name' => 'Skatīt izmaiņas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '8',
                'permission_id' => '8',
                'language' => 'lv',
                'name' => 'Skatīt vadības paneli',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '9',
                'permission_id' => '9',
                'language' => 'lv',
                'name' => 'Pievienot dokumentāciju',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '10',
                'permission_id' => '10',
                'language' => 'lv',
                'name' => 'Rediģēt dokumentāciju',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '11',
                'permission_id' => '11',
                'language' => 'lv',
                'name' => 'Skatīt dokumentāciju',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '12',
                'permission_id' => '12',
                'language' => 'lv',
                'name' => 'Pievienot izvēlnes vienumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '13',
                'permission_id' => '13',
                'language' => 'lv',
                'name' => 'Rediģēt izvēlnes vienumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '14',
                'permission_id' => '14',
                'language' => 'lv',
                'name' => 'Noņemt izvēlnes vienumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '15',
                'permission_id' => '15',
                'language' => 'lv',
                'name' => 'Izveidot paziņojumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '16',
                'permission_id' => '16',
                'language' => 'lv',
                'name' => 'Noņemt paziņojumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '17',
                'permission_id' => '17',
                'language' => 'lv',
                'name' => 'Skatīt paziņojumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '18',
                'permission_id' => '18',
                'language' => 'lv',
                'name' => 'Skatīt atļaujas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '19',
                'permission_id' => '19',
                'language' => 'lv',
                'name' => 'Izveidot atļaujas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '20',
                'permission_id' => '20',
                'language' => 'lv',
                'name' => 'Dzēst atļaujas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '21',
                'permission_id' => '21',
                'language' => 'lv',
                'name' => 'Rediģēt atļaujas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '22',
                'permission_id' => '22',
                'language' => 'lv',
                'name' => 'Skatīt lomas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '23',
                'permission_id' => '23',
                'language' => 'lv',
                'name' => 'Izveidot lomas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '24',
                'permission_id' => '24',
                'language' => 'lv',
                'name' => 'Dzēst lomas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '25',
                'permission_id' => '25',
                'language' => 'lv',
                'name' => 'Rediģēt lomas',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '26',
                'permission_id' => '26',
                'language' => 'lv',
                'name' => 'Skatīt iestatījumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '27',
                'permission_id' => '27',
                'language' => 'lv',
                'name' => 'Rediģēt iestatījumus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '28',
                'permission_id' => '28',
                'language' => 'lv',
                'name' => 'Skatīt žurnālus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '29',
                'permission_id' => '29',
                'language' => 'lv',
                'name' => 'Dzēst žurnālus',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],

            // Russian
            [
                'id' => '30',
                'permission_id' => '1',
                'language' => 'ru',
                'name' => 'Редактировать чужой аккаунт',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '31',
                'permission_id' => '2',
                'language' => 'ru',
                'name' => 'Редактировать свой аккаунт',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '32',
                'permission_id' => '3',
                'language' => 'ru',
                'name' => 'Добавить аккаунт',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '33',
                'permission_id' => '4',
                'language' => 'ru',
                'name' => 'Смотреть аккаунт',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '34',
                'permission_id' => '5',
                'language' => 'ru',
                'name' => 'Добавить изменения',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '35',
                'permission_id' => '6',
                'language' => 'ru',
                'name' => 'Редактировать изменения',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '36',
                'permission_id' => '7',
                'language' => 'ru',
                'name' => 'Смотреть изменения',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '37',
                'permission_id' => '8',
                'language' => 'ru',
                'name' => 'Смотреть панель управления',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '38',
                'permission_id' => '9',
                'language' => 'ru',
                'name' => 'Добавить документацию',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '39',
                'permission_id' => '10',
                'language' => 'ru',
                'name' => 'Редавить документацию',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '40',
                'permission_id' => '11',
                'language' => 'ru',
                'name' => 'Смотреть документацию',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '41',
                'permission_id' => '12',
                'language' => 'ru',
                'name' => 'Добавить пункт меню',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '42',
                'permission_id' => '13',
                'language' => 'ru',
                'name' => 'Редавить пункт меню',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '43',
                'permission_id' => '14',
                'language' => 'ru',
                'name' => 'Удалить пунткт меню',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '44',
                'permission_id' => '15',
                'language' => 'ru',
                'name' => 'Создать уведомления',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '45',
                'permission_id' => '16',
                'language' => 'ru',
                'name' => 'Удалить уведомления',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '46',
                'permission_id' => '17',
                'language' => 'ru',
                'name' => 'Смотреть уведомления',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '47',
                'permission_id' => '18',
                'language' => 'ru',
                'name' => 'Смотреть разрешения',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '48',
                'permission_id' => '19',
                'language' => 'ru',
                'name' => 'Создать разрешения',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '49',
                'permission_id' => '20',
                'language' => 'ru',
                'name' => 'Удалить разрешения',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '50',
                'permission_id' => '21',
                'language' => 'ru',
                'name' => 'Редактировать разрешения',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '51',
                'permission_id' => '22',
                'language' => 'ru',
                'name' => 'Смотреть роли',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '52',
                'permission_id' => '23',
                'language' => 'ru',
                'name' => 'Создать роли',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '53',
                'permission_id' => '24',
                'language' => 'ru',
                'name' => 'Удалить роли',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '54',
                'permission_id' => '25',
                'language' => 'ru',
                'name' => 'Редактировать роли',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '55',
                'permission_id' => '26',
                'language' => 'ru',
                'name' => 'Смотреть настройки',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '56',
                'permission_id' => '27',
                'language' => 'ru',
                'name' => 'Редактировать настройки',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '57',
                'permission_id' => '28',
                'language' => 'ru',
                'name' => 'Смотреть журналы',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '58',
                'permission_id' => '29',
                'language' => 'ru',
                'name' => 'Удалить журналы',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],

            // English
            [
                'id' => '59',
                'permission_id' => '1',
                'language' => 'en',
                'name' => 'Edit other account',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '60',
                'permission_id' => '2',
                'language' => 'en',
                'name' => 'Edit own account',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '61',
                'permission_id' => '3',
                'language' => 'en',
                'name' => 'Add account',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '62',
                'permission_id' => '4',
                'language' => 'en',
                'name' => 'See accounts',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '63',
                'permission_id' => '5',
                'language' => 'en',
                'name' => 'Add changes',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '64',
                'permission_id' => '6',
                'language' => 'en',
                'name' => 'Edit changes',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '65',
                'permission_id' => '7',
                'language' => 'en',
                'name' => 'See changes',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '66',
                'permission_id' => '8',
                'language' => 'en',
                'name' => 'See dashboard',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '67',
                'permission_id' => '9',
                'language' => 'en',
                'name' => 'Add documentation',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '68',
                'permission_id' => '10',
                'language' => 'en',
                'name' => 'Edit documentation',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '69',
                'permission_id' => '11',
                'language' => 'en',
                'name' => 'See documentation',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '70',
                'permission_id' => '12',
                'language' => 'en',
                'name' => 'Add menu item',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '71',
                'permission_id' => '13',
                'language' => 'en',
                'name' => 'Edit menu item',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '72',
                'permission_id' => '14',
                'language' => 'en',
                'name' => 'Remove menu item',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '73',
                'permission_id' => '15',
                'language' => 'en',
                'name' => 'Create notifications',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '74',
                'permission_id' => '16',
                'language' => 'en',
                'name' => 'Remove notifications',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '75',
                'permission_id' => '17',
                'language' => 'en',
                'name' => 'See notifications',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '76',
                'permission_id' => '18',
                'language' => 'en',
                'name' => 'See permissions',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '77',
                'permission_id' => '19',
                'language' => 'en',
                'name' => 'Add permissions',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '78',
                'permission_id' => '20',
                'language' => 'en',
                'name' => 'Remove permissions',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '79',
                'permission_id' => '21',
                'language' => 'en',
                'name' => 'Edit permissions',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '80',
                'permission_id' => '22',
                'language' => 'en',
                'name' => 'See roles',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '81',
                'permission_id' => '23',
                'language' => 'en',
                'name' => 'Add roles',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '82',
                'permission_id' => '24',
                'language' => 'en',
                'name' => 'Remove roles',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '83',
                'permission_id' => '25',
                'language' => 'en',
                'name' => 'Edit roles',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '84',
                'permission_id' => '26',
                'language' => 'en',
                'name' => 'See settings',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '85',
                'permission_id' => '27',
                'language' => 'en',
                'name' => 'Edit settings',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '86',
                'permission_id' => '28',
                'language' => 'en',
                'name' => 'See logs',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
            [
                'id' => '87',
                'permission_id' => '29',
                'language' => 'en',
                'name' => 'Remove logs',
                'created_at' => '2023-09-12 03:43:08',
                'updated_at' => '2023-09-12 03:43:08'
            ],
        ];

        $this->db->table('permission_translations')->insertBatch($data);
    }
}
