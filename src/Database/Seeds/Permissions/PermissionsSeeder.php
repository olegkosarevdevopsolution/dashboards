<?php

namespace Devolegkosarev\Dashboard\Database\Seeds\Permissions;

use CodeIgniter\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    public function run()
    {

        $data = [
            [
                'id' => '1',
                'type' => 'account:edit-others',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '2',
                'type' => 'account:edit-self',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '3',
                'type' => 'accounts:add',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '4',
                'type' => 'accounts:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '5',
                'type' => 'changes:add',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '6',
                'type' => 'changes:edit',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '7',
                'type' => 'changes:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '8',
                'type' => 'dashboard:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '9',
                'type' => 'documentation:add',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '10',
                'type' => 'documentation:edit',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '11',
                'type' => 'feedback:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],

            [
                'id' => '12',
                'type' => 'documentation:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '13',
                'type' => 'menu_items:add',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '14',
                'type' => 'menu_items:edit',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '15',
                'type' => 'menu_items:remove',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '16',
                'type' => 'notification:create',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '17',
                'type' => 'notification:remove',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '18',
                'type' => 'notification:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '19',
                'type' => 'permissions:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '20',
                'type' => 'permissions:create',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '21',
                'type' => 'permissions:delete',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '22',
                'type' => 'permissions:edit',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '23',
                'type' => 'roles:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '24',
                'type' => 'roles:create',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '25',
                'type' => 'roles:delete',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '26',
                'type' => 'roles:edit',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '27',
                'type' => 'settings:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '28',
                'type' => 'settings:edit',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '29',
                'type' => 'logs:view',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
            [
                'id' => '30',
                'type' => 'logs:delete',
                'status' => '1',
                'created_at' => '2023-09-12 00:34:00',
                'updated_at' => '2023-09-12 00:34:46'
            ],
        ];

        $this->db->table('permissions')->insertBatch($data);
    }
}
