<?php

namespace Devolegkosarev\Dashboard\Database\Migrations\Users;

use CodeIgniter\Database\Migration;
use \CodeIgniter\Database\RawSql;

class CreateUsersTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'firstname' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
            ],
            'lastname' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
            ],
            'secondname' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '50',
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => '512',
            ],
            'avatar' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'reset_token' => [
                'type' => 'VARCHAR',
                'constraint' => '250',
            ],
            'reset_expire' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'activated' => [
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 1,
            ],
            'blocked' => [
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
            ],
            'activate_token' => [
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true,
            ],
            'activate_expire' => [
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true,
            ],
            'role_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'constraint' => 11,
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('role_id', 'roles', 'id');
        $this->forge->createTable('users');
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}
