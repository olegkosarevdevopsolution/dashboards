<?php

namespace Devolegkosarev\Dashboard\Database\Migrations\Documentation\ReleaseNotes;

use CodeIgniter\Database\Migration;
use \CodeIgniter\Database\RawSql;

class CreateReleaseNotesTablesMigration extends Migration
{
    public function up()
    {
        // Создаем таблицу releaseNotes
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'version' => [
                'type' => 'VARCHAR',
                'constraint' => 10,
            ],
            'slug' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'charset' => 'utf8',
                'collation' => 'utf8_general_ci',
            ],
            'release_date' => [
                'type' => 'DATE',
            ],
            'project' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => 'AppStarter',
            ],
            'publish_status' => [
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => '1',
            ],
            'version_status' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('release_notes');

        // Создаем таблицу releaseNotesChange
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'release_notes_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
            'language' => [
                'type' => 'CHAR',
                'constraint' => 2,
                'default' => '*',

            ],
            'content' => [
                'type' => 'TEXT',
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('release_notes_id', 'release_notes', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('release_notes_change');
    }

    public function down()
    {
        // Удаляем таблицы
        $this->forge->dropTable('release_notes_change');
        $this->forge->dropTable('release_notes');
    }
}
