<?php

namespace Devolegkosarev\Dashboard\Database\Migrations\Menus;

use CodeIgniter\Database\Migration;
use \CodeIgniter\Database\RawSql;

class CreateMenuTableMigration extends Migration
{
    public function up()
    {
        // Создание таблицы mainmenu
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'status' => [
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => '1',
            ],
            'order' => [
                'type' => 'INT',
                'constraint' => 5,
                'default' => 0,
            ],
            'options' => [
                'type' => 'JSON',
                'null' => true,
            ],
            'parent_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'null' => true,
            ],
            'url' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true,
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => 'link',
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('mainmenu', true);

        // Создание таблицы menu_item_translations
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'mainmenu_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'language' => [
                'type' => 'CHAR',
                'constraint' => 2,
                'default' => '*',
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('mainmenu_id', 'mainmenu', 'id');
        $this->forge->createTable('menu_item_translations', true);
    }

    public function down()
    {
        $this->forge->dropTable('menu_item_translations');
        $this->forge->dropTable('mainmenu');
    }
}
