<script src="<?= base_url('_admin/js/vendor.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('_admin/js/app.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('_admin/plugins/bs5-toast/bs5-toast.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('_admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('_admin/plugins/select-picker/dist/picker.min.js') ?>" type="text/javascript"></script>

<?php $javascripts = array_merge($DashboardConfig->javascripts ?? [], $javascripts ?? []); ?>

<?php if (isset($javascripts) and is_array($javascripts) and count($javascripts) > 0) : ?>
    <?php foreach ($javascripts as $javascript) : ?>
        <script src="<?= $javascript ?>" type="text/javascript"></script>
    <?php endforeach ?>
<?php endif; ?>
<?php echo $this->renderSection('javascript') ?>
<script>
    $(document).ready(function() {
        var colorScheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
        document.documentElement.setAttribute('data-bs-theme', colorScheme);
    });
</script>