<div class="invalid-feedback">
    <?php if (!empty($validationSuccessMessage)) : ?>
        <?php echo $validationSuccessMessage ?>
    <?php else : ?>
        Field is required
    <?php endif; ?>
</div>

<div class="valid-feedback">
    <?php if (!empty($validationErrorMessage)) : ?>
        <?php echo $validationErrorMessage ?>
    <?php else : ?>
        Looks good!
    <?php endif; ?>
</div>