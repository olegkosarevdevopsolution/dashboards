<div id="footer" class="app-footer">
    © 2023 <?php echo $company ?> All Rights Reserved. (Version: <?php echo $versionBase ?><?php echo ($versionProject) ? ', Project Version: ' . $versionProject : '' ?>)
</div>