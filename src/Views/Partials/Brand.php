<div class="brand">
    <a href="<?php echo base_url() ?>" class="brand-logo">
        <?php if (!empty($logoDark)) : ?>
            <img class="brand-img" src="<?= base_url($logoDark) ?>" alt="<?php echo $project ?>">
        <?php else : ?>
            <span class="brand-img">
                <span class="brand-img-text text-theme">
                    <?php echo substr($project, 0, 1)[0] ?>
                </span>
            </span>
        <?php endif; ?>
        <span class="brand-text"><?php echo $project ?></span>
    </a>
</div>