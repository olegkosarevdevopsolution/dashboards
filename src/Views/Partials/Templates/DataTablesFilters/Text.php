<div class="flex-fill">
    <div id="<?php echo $id; ?>_search" class="form-group position-relative">
        <?php if (isset($label) && !empty($label)) : ?>
            <label class="form-label"><?php echo $label; ?></label>
        <?php endif ?>
        <input type="text" data-column="<?php echo $column; ?>" data-type="<?php echo $type; ?>_search" placeholder="<?php echo $placeholder; ?>" class="custom-search-input form-control" id="<?php echo $id; ?>">
        <span class="position-absolute top-50 clear-button translate-middle-y" style="display:none; right: 10px">
            <i id="clear_<?php echo $id; ?>" class="clear fas fa-times"></i>
        </span>
    </div>
</div>

<?php echo $this->section('DataTable') ?>

<script>
    // $(document).ready(function() {

    //     const id = '<?php echo $id; ?>_search';
    //     const searchContainer = $(`#${id}`);
    //     const clearButton = searchContainer.find('.clear-button');
    //     const searchInput = searchContainer.find('input');

    //     clearButton.on('click', function(e) {
    //         e.preventDefault();
    //         searchInput.val('');
    //         clearButton.hide();
    //         console.log(window.table.column(0).search("", true, false).draw());
    //     });

    //     searchInput.on('keyup', function(e) {
    //         const value = searchInput.val();
    //         console.log(`keyup event triggered ${id} value: ${value}`);

    //         if (value.length > 0) {
    //             console.log(window.table.column(0).search(value, true, false).draw());
    //             clearButton.show();
    //         } else {
    //             console.log(window.table.column(0).search("", true, false).draw());
    //             clearButton.hide();
    //         }
    //     });
    // });
</script>


<?php echo $this->endSection() ?>