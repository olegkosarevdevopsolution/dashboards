<!-- <link href="/assets/plugins/bootstrap-slider/dist/css/bootstrap-slider.min.css" rel="stylesheet">
<script src="/assets/plugins/bootstrap-slider/dist/bootstrap-slider.min.js"></script> -->

<div class="flex-fill">
    <?php if (isset($label) && !empty($label)) : ?>
        <label class="form-label"><?php echo $label; ?></label>
    <?php endif ?>
    <div id="<?php echo $id; ?>_search" class="form-group">
        <input type="text" data-column="<?php echo $column; ?>" data-type="<?php echo $type; ?>_search" class="custom-search-range form-control" id="<?php echo $id; ?>" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="14">

    </div>
</div>