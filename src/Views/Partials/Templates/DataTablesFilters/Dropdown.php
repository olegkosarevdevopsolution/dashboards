<div class="flex-fill">

    <div class="dropdown form-group">
        <button class="btn btn-theme dropdown-toggle dropdown-toggle-nocaret" type="button" id="<?php echo $id; ?>" data-bs-toggle="dropdown" aria-expanded="false">
            <?php echo $placeholder; ?>
        </button>
        <ul class="dropdown-menu" aria-labelledby="<?php echo $id; ?>">
            <li>
                <a data-column="<?php echo $column; ?>" data-value="" data-type="<?php echo $type; ?>" class="dropdown-item active" href="#">
                    <?php echo $defaultOptions; ?>
                </a>
            </li>
            <?php foreach ($options as $option) : ?>
                <li>
                    <a data-column="<?php echo $column; ?>" data-value="<?php echo $option; ?>" data-type="<?php echo $type; ?>" class="dropdown-item" href="#">
                        <?php echo $option; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

</div>