<div class="form-group mb-3">
    <?php
    $classLabel = 'form-label';
    if (isset($formElement['label']['attributes']['class'])) {
        $classLabel .= ' ' . $formElement['label']['attributes']['class'];
    } ?>
    <label for="<?php echo $formElement['attributes']['id'] ?>" class="<?php echo $classLabel ?>">
        <?php echo $formElement['label']['text'] ?> <?php echo (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) ? '<span class="text-danger">*</span>' : '' ?>
    </label>
    <?php
    $inputAttributes = '';
    $classInput = 'form-control';
    if (isset($formElement['attributes']['class'])) {
        $classInput .= ' ' . $formElement['attributes']['class'];
    }
    foreach ($formElement['attributes'] as $key => $value) {
        if ($key != 'required' && $key != 'readonly' && $key != 'disabled' && $key != 'сlass') {
            if ($value === true) {
                $inputAttributes .= $key . ' ';
            } elseif ($value !== false) {
                $inputAttributes .= $key . "='" . $value . "' ";
            }
        }
    }
    if (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) {
        $inputAttributes .= 'required ';
    }
    if (isset($formElement['attributes']['readonly']) && $formElement['attributes']['readonly']) {
        $inputAttributes .= 'readonly ';
    }
    if (isset($formElement['attributes']['disabled']) && $formElement['attributes']['disabled']) {
        $inputAttributes .= 'disabled ';
    }
    if (isset($formElement['attributes']['help']) && $formElement['attributes']['help']) {
        $inputAttributes .= "aria-describedby='" . $formElement['attributes']['id'] . "HelpBlock' ";
    }
    ?>
    <textarea class="<?php echo $classInput ?>" class="<?php echo $classInput ?>" <?php echo $inputAttributes ?>></textarea>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\HelpBlock"), ["formElement" => $formElement]) ?>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\ValidationMessages"), ["formElement" => $formElement]) ?>
</div>