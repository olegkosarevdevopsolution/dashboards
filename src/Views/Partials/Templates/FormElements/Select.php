<div class="form-group mb-3">
    <?php
    $classLabel = 'small text-inverse text-opacity-50 mb-2';
    if (isset($formElement['label']['attributes']['class'])) {
        $classLabel .= ' ' . $formElement['label']['attributes']['class'];
    }
    ?>
    <div class="<?php echo $classLabel ?>">
        <b class="fw-bold"><?php echo $formElement['label']['text'] ?> <?php echo (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) ? '<span class="text-danger">*</span>' : '' ?></b>
    </div>
    <?php
    $selectAttributes = '';
    $classInput = 'form-select';
    if (isset($formElement['attributes']['class'])) {
        $classInput .= ' ' . $formElement['attributes']['class'];
    }
    foreach ($formElement['attributes'] as $key => $value) {
        if ($key != 'required' && $key != 'readonly' && $key != 'disabled' && $key != 'сlass' && $key != 'multiple') {
            if ($value === true) {
                $selectAttributes .= $key . ' ';
            } elseif ($value !== false) {
                $selectAttributes .= $key . "='" . $value . "' ";
            }
        }
    }
    if (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) {
        $selectAttributes .= 'required ';
    }
    if (isset($formElement['attributes']['readonly']) && $formElement['attributes']['readonly']) {
        $selectAttributes .= 'readonly ';
    }
    if (isset($formElement['attributes']['disabled']) && $formElement['attributes']['disabled']) {
        $selectAttributes .= 'disabled ';
    }
    if (isset($formElement['attributes']['help']) && $formElement['attributes']['help']) {
        $selectAttributes .= "aria-describedby='" . $formElement['attributes']['id'] . "HelpBlock' ";
    }
    if (isset($formElement["attributes"]["multiple"]) && $formElement['attributes']['multiple']) {
        $selectAttributes .= 'multiple ';
    }
    ?>

    <select class="<?php echo $classInput ?>" <?php echo $selectAttributes ?>>


        <?php foreach ($formElement['options'] as $option) : ?>
            <option value="<?php echo $option['value'] ?>" <?php echo (isset($option['selected']) && $option['selected']) ? 'selected' : '' ?> <?php echo (isset($option['disabled']) && $option['disabled']) ? 'disabled' : '' ?>><?php echo $option['label'] ?></option>
        <?php endforeach; ?>
    </select>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\HelpBlock"), ["formElement" => $formElement]) ?>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\ValidationMessages"), ["formElement" => $formElement]) ?>

</div>