<?php
$inputAttributes = '';
$class = '';
$classLabel = 'form-check-label';
$classInput = 'form-check-input';

if (isset($formElement['attributes']['class'])) {
    $classInput .= ' ' . $formElement['attributes']['class'];
}

if (isset($formElement['label']['attributes']['class'])) {
    $classLabel .= ' ' . $formElement['label']['attributes']['class'];
}

if (isset($formElement['class'])) {
    $class .= ' ' . $formElement['class'];
}

foreach ($formElement['attributes'] as $key => $value) {
    if ($key != 'required' && $key != 'readonly' && $key != 'disabled' && $key != 'сlass') {
        if ($value === true) {
            $inputAttributes .= $key . ' ';
        } elseif ($value !== false) {
            $inputAttributes .= $key . "='" . $value . "' ";
        }
    }
}

if (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) {
    $inputAttributes .= 'required ';
}
if (isset($formElement['attributes']['readonly']) && $formElement['attributes']['readonly']) {
    $inputAttributes .= 'readonly ';
}
if (isset($formElement['attributes']['disabled']) && $formElement['attributes']['disabled']) {
    $inputAttributes .= 'disabled ';
}
if (isset($formElement['attributes']['checked']) && $formElement['attributes']['checked']) {
    $inputAttributes .= 'checked ';
}
if (isset($formElement['attributes']['help']) && $formElement['attributes']['help']) {
    $inputAttributes .= "aria-describedby='" . $formElement['attributes']['id'] . "HelpBlock' ";
}
?>
<div class="form-group mb-3">
    <div class="form-check <?php echo $class ?>">
        <input class="<?php echo $classInput ?>" type="<?php echo $formElement['type'] ?>" <?php echo $inputAttributes ?>>
        <label class="<?php echo $classLabel ?>" for="<?php echo $formElement['attributes']['id'] ?>">
            <?php echo $formElement['label']['text']; ?> <?php echo (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) ? '<span class="text-danger">*</span>' : '' ?>
        </label>
    </div>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\HelpBlock"), ["formElement" => $formElement]) ?>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\ValidationMessages"), ["formElement" => $formElement]) ?>

</div>