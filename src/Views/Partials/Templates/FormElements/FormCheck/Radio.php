<?php if (isset($formElement['title'])) : ?>
    <div class="small text-inverse text-opacity-50 mb-1">
        <b class="fw-bold">
            <?php echo $formElement['title'] ?>
        </b>
    </div>
<?php endif; ?>
<?php if (array_key_exists("group", $formElement) and count($formElement['group'])) : ?>
    <?php foreach ($formElement['group'] as $element) : ?>
        <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\FormCheck\\FormCheck"), ["formElement" => $element]) ?>
    <?php endforeach; ?>
<?php else : ?>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\FormCheck\\FormCheck"), ["formElement" => $formElement]) ?>
<?php endif; ?>