<div class="invalid-feedback">
    <?php if (isset($formElement["invalid_tooltip"])) : ?>
        <?php echo $formElement["invalid_tooltip"] ?>
    <?php else : ?>
        Field is required
    <?php endif; ?>
</div>

<div class="valid-feedback">
    <?php if (isset($formElement["valid_tooltip"])) : ?>
        <?php echo $formElement["valid_tooltip"] ?>
    <?php else : ?>
        Looks good!
    <?php endif; ?>
</div>