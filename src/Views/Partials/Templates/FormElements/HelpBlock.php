<?php if (isset($formElement['help']) && $formElement['help']) : ?>
    <small id="<?php echo $formElement['attributes']['id'] ?>HelpBlock" class="form-text text-muted">
        <?php echo $formElement['help'] ?>
    </small>
<?php endif; ?>