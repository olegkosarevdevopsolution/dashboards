<div class="form-group mb-3">
    <?php
    $classLabel = 'form-label';
    if (isset($formElement['label']['attributes']['class'])) {
        $classLabel .= ' ' . $formElement['label']['attributes']['class'];
    }
    ?>
    <label class="<?php echo $classLabel ?>" for="<?php echo $formElement['attributes']['id'] ?>">
        <?php echo $formElement['label']['text'] ?> <?php echo (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) ? '<span class="text-danger">*</span>' : '' ?>
    </label>
    <?php
    $inputAttributes = '';
    $classInput = 'form-control';
    if (isset($formElement['attributes']['class'])) {
        $classInput .= ' ' . $formElement['attributes']['class'];
    }
    foreach ($formElement['attributes'] as $key => $value) {
        if ($key != 'required' && $key != 'readonly' && $key != 'disabled' && $key != 'сlass') {
            if ($value === true) {
                $inputAttributes .= $key . ' ';
            } elseif ($value !== false) {
                $inputAttributes .= $key . "='" . $value . "' ";
            }
        }
    }
    if (isset($formElement['attributes']['required']) && $formElement['attributes']['required']) {
        $inputAttributes .= 'required ';
    }
    if (isset($formElement['attributes']['readonly']) && $formElement['attributes']['readonly']) {
        $inputAttributes .= 'readonly ';
    }
    if (isset($formElement['attributes']['disabled']) && $formElement['attributes']['disabled']) {
        $inputAttributes .= 'disabled ';
    }
    if (isset($formElement['attributes']['help']) && $formElement['attributes']['help']) {
        $inputAttributes .= "aria-describedby='" . $formElement['attributes']['id'] . "HelpBlock' ";
    }
    ?>
    <input type="<?php echo $formElement['type'] ?>" class="<?php echo $classInput ?>" <?php echo $inputAttributes ?>>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\HelpBlock"), ["formElement" => $formElement]) ?>
    <?php echo view(viewOverview("Views\\Partials\\Templates\\FormElements\\ValidationMessages"), ["formElement" => $formElement]) ?>
</div>