    <div class="card">
        <ul class="nav nav-tabs nav-tabs-v2 px-4" role="tablist">
            <li class="nav-item me-3" role="presentation">
                <a href="#generalTab" class="nav-link px-2 active" data-bs-toggle="tab" aria-selected="true" role="tab">
                    GeneralKeyFormLanguageFile
                </a>
            </li>
            <?php foreach ($languages as $language) : ?>
                <li class="nav-item me-3" role="presentation">
                    <a href="#language<?php echo ucfirst($language["code"]); ?>Tab" class="nav-link px-2" data-bs-toggle="tab" aria-selected="true" role="tab">
                        <?php echo $language["name"]; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content p-4">
            <div class="tab-pane fade active show" id="generalTab" role="tabpanel">
                <?php echo $content['generalTab']['formElements']; ?>
            </div>

            <?php foreach ($languages as $language) : ?>
                <div class="tab-pane fade" id="language<?php echo ucfirst($language["code"]); ?>Tab" role="tabpanel">
                    language<?php echo ucfirst($language["code"]); ?>Tab
                </div>
            <?php endforeach; ?>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn m-2 btn-theme">
                btnSubmit
            </button>
            <a href="cancelUrl" class="btn m-2 btn-theme">
                btnCancel
            </a>
        </div>

        <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
        </div>
    </div>