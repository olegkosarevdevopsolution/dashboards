<div class="card">
    <div class="tab-content p-4">
        <div class="table-responsive">
            <!-- Start of table -->
            <table class="table table-hover">
                <thead>
                    <tr>
                        <?php foreach ($tHeadData as $tHeadDataItem) : ?>
                            <th class="pt-0 pb-2">
                                <?php echo $tHeadDataItem->name; ?>
                            </th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?= $tBodyData; ?>
                </tbody>
            </table>
            <!-- End of table -->
        </div>
    </div>
    <div class="card-arrow">
        <!-- Start of arrow -->
        <div class="card-arrow-top-left"></div>
        <div class="card-arrow-top-right"></div>
        <div class="card-arrow-bottom-left"></div>
        <div class="card-arrow-bottom-right"></div>
        <!-- End of arrow -->
    </div>
</div>