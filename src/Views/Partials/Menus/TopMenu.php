<div id="top-nav" class="app-top-nav">
    <div class="menu">
        <div class="menu-item menu-control menu-control-start">
            <a href="javascript:;" class="menu-link" data-toggle="app-top-menu-prev"><i class="bi bi-caret-left"></i></a>
        </div>

        <?php foreach ($menu['getMenu'] as $item) : ?>
            <?php if ($item->type == 'header') : ?>
                <?php if (!empty($item->items)) : ?>
                    <?php foreach ($item->items as $subItem) : ?>
                        <?php renderMenuItem($subItem); ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else : ?>
                <?php renderMenuItem($item); ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <div class="menu-item menu-control menu-control-end">
            <a href="javascript:;" class="menu-link" data-toggle="app-top-menu-next"><i class="bi bi-caret-right"></i></a>
        </div>
    </div>
</div>

<?php
function renderMenuItem($menuItem)
{
    $currentUrl = current_url();
    $menuItemUrl = base_url($menuItem->url);
    $isActive = ($currentUrl == $menuItemUrl) ? ' active' : '';
    $hasSub = (!empty($menuItem->items)) ? ' has-sub' : '';
    $expand = ($hasSub && strpos($currentUrl, $menuItemUrl) === 0) ? ' expand' : '';


?>
    <div class="menu-item<?php echo $hasSub; ?><?php echo $expand; ?><?php echo $isActive; ?>">
        <a href="<?php echo $menuItemUrl; ?>" class="menu-link">
            <?php if (!empty($menuItem->options)) : ?>
                <?php $options = json_decode($menuItem->options, true); ?>
                <span class="menu-icon">
                    <?php if (!empty($options['menu-icon'])) : ?>
                        <i class="bi bi-<?php echo $options['menu-icon']; ?>"></i>
                        <?php if ($isActive) : ?>
                            <span class="w-5px h-5px rounded-3 bg-theme position-absolute top-0 end-0 mt-3px me-3px"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </span>
            <?php endif; ?>
            <span class="menu-text">
                <?php echo $menuItem->name; ?>
            </span>
            <?php if (!empty($menuItem->items)) : ?>
                <span class="menu-caret">
                    <b class="caret"></b>
                </span>
            <?php endif; ?>
        </a>
        <?php if (!empty($menuItem->items)) : ?>
            <div class="menu-submenu">
                <?php foreach ($menuItem->items as $subItem) : ?>
                    <?php renderMenuItem($subItem); ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
<?php
}
?>