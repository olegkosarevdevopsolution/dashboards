<div class="menu-item dropdown dropdown-mobile-full">
    <a href="#" data-bs-toggle="dropdown" data-bs-display="static" class="menu-link">
        <div class="menu-img online">
            <img src="<?php echo $avatar ?>" alt="Profile" height="60">
        </div>
        <div class="menu-text d-sm-block d-none"><?php echo $email; ?></div>
    </a>
    <div class="dropdown-menu dropdown-menu-end me-lg-3 fs-11px mt-1">
        <a class="dropdown-item d-flex align-items-center" href="<?php echo base_url("dashboard/profile"); ?>">
            PROFILE
            <i class="bi bi-person-circle ms-auto text-theme fs-16px my-n1"></i>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item d-flex align-items-center" href="<?php echo base_url("authentication/logout"); ?>">
            LOGOUT
            <i class="bi bi-toggle-off ms-auto text-theme fs-16px my-n1"></i>
        </a>
    </div>
</div>