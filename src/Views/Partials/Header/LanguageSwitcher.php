<div class="menu-item dropdown dropdown-mobile-full">
    <?php $currentLanguage = array_filter($languages, function ($language) {
        return $language["currentLocale"] == true;
    }); ?>
    <?php if (!empty($currentLanguage)) : ?>
        <?php $language = reset($currentLanguage); ?>
        <a href="#" data-bs-toggle="dropdown" data-bs-display="static" class="menu-link" aria-expanded="false">
            <div class="menu-icon">
                <i class="fi fi-<?php echo htmlspecialchars($language['flag']); ?> nav-icon"></i>
            </div>
        </a>
    <?php endif; ?>

    <div class="dropdown-menu fade dropdown-menu-end w-300px text-center p-0 mt-1">
        <?php $chunks = array_chunk($languages, 3); ?>
        <?php foreach ($chunks as $chunk) : ?>
            <div class="row row-grid gx-0">
                <?php foreach ($chunk as $language) : ?>
                    <div class="col-4">
                        <?php $opacity = ($language["currentLocale"] == true) ? null : 'opacity-5'; ?>
                        <a href="<?= site_url('language/' . $language["code"]) ?>" class="dropdown-item text-decoration-none p-3">
                            <div class="position-relative">
                                <?php if ($language["currentLocale"] == true) : ?>
                                    <i class="bi bi-circle-fill position-absolute text-theme top-0 mt-n2 me-n2 fs-6px d-block text-center w-100"></i>
                                <?php endif; ?>
                                <i class="flag fi fi-<?php echo htmlspecialchars($language["flag"]); ?> h2 <?php echo $opacity; ?> d-block my-1"></i>
                            </div>
                            <div class="fw-500 fs-10px text-inverse">
                                <?php echo htmlspecialchars($language["name"]); ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>