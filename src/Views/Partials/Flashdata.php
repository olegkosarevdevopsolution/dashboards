<?php
$alerts = [
    'primary' => ['class' => 'alert-primary', 'title' => 'primary'],
    'secondary' => ['class' => 'alert-secondary', 'title' => 'secondary'],
    'success' => ['class' => 'alert-success', 'title' => 'success'],
    'danger' => ['class' => 'alert-danger', 'title' => 'danger'],
    'error' => ['class' => 'alert-danger', 'title' => 'danger'],
    'warning' => ['class' => 'alert-warning', 'title' => 'warning'],
    'info' => ['class' => 'alert-info', 'title' => 'info'],
    'dark' => ['class' => 'alert-dark', 'title' => 'dark'],
    'light' => ['class' => 'alert-light mb-0', 'title' => 'light'],
];

foreach ($alerts as $key => $alert) {
    if (session()->getFlashdata($key)) {
        $additional_class = isset($alert['additional_class']) ? ' ' . $alert['additional_class'] : '';
        echo '<div class="alert ' . $alert['class'] . $additional_class . '">',
        '<strong>', (session()->getFlashdata('title')) ? session()->getFlashdata('title') : lang('Alert/Common.title.' . $alert['title']),
        ': </strong>',
        session()->getFlashdata($key),
        '</div>';
    }
}
