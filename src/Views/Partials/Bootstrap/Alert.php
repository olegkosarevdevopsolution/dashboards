<div class="alert alert-<?php echo $colorScheme; ?> fade show p-3">
    <?php if ($heading) : ?>
        <h4 class="alert-heading"><?php echo $heading; ?></h4>
    <?php endif; ?>
    <?php echo $message; ?>
</div>