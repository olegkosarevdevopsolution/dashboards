<meta charset="utf-8">
<title>
    <?php if (isset($meta['title']) and $meta['title']) : ?>
        <?php echo $meta['title'] . ' | ' . $project ?>
    <?php else : ?>
        <?php echo $project ?>
    <?php endif; ?>
</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<?php if (isset($meta['description']) and $meta['description']) : ?>
    <meta name="description" content="<?php echo $meta['description'] ?>">
<?php endif; ?>
<?php if (isset($meta['keywords']) and $meta['keywords']) : ?>
    <meta name="keywords" content="<?php echo (is_array($meta['keywords'])) ? implode(',', $meta['keywords']) : $meta['keywords'] ?>">
<?php endif; ?>
<?php if (isset($meta['author']) and $meta['author']) : ?>
    <meta name="author" content="<?php echo $meta['author'] ?>">
<?php else : ?>
    <meta name="author" content="<?php echo $dashboardConfig->author ?>">
<?php endif; ?>