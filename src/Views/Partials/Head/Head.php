    <?php
    $stylesheets = array_merge($dashboardConfig->stylesheets ?? [], $stylesheets ?? []);
    echo view(
        'Devolegkosarev\\Dashboard\\Views\\Partials\\Head\Meta',
        [
            'meta' => (isset($meta)) ? $meta : [],
            'project' => $project,
        ]
    ); ?>

    <link href="<?= base_url('_admin/css/vendor.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('_admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('_admin/plugins/select-picker/dist/picker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('_admin/plugins/flag-icon-css/css/flag-icons.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('_admin/css/app.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('_admin/css/custom.css') ?>" rel="stylesheet">


    <?php if (isset($stylesheets) and is_array($stylesheets) and count($stylesheets) > 0) : ?>
        <?php foreach ($stylesheets as $css) : ?>
            <link href="<?= base_url($css); ?>" rel="stylesheet">
        <?php endforeach ?>
    <?php endif; ?>
    <?php echo $this->renderSection('stylesheet') ?>

    <style type="text/css">

    </style>