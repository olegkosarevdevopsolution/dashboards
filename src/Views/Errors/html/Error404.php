<!DOCTYPE html>
<html lang="en" data-bs-theme="dark">

<head>
    <meta charset="utf-8">
    <title>
        <?= lang('Errors.pageNotFound') ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content>
    <meta name="author" content>

    <link href="<?= base_url('public/_admin/css/vendor.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/_admin/css/app.min.css') ?>" rel="stylesheet">
</head>

<body class="pace-top">
    <div id="app" class="app app-full-height app-without-header">

        <div class="error-page">

            <div class="error-page-content" style="width: 320px;">
                <div class="card mb-5 mx-auto" style="width: 320px;">
                    <div class="card-body">
                        <div class="card">
                            <div class="error-code">404</div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-arrow">
                        <div class="card-arrow-top-left"></div>
                        <div class="card-arrow-top-right"></div>
                        <div class="card-arrow-bottom-left"></div>
                        <div class="card-arrow-bottom-right"></div>
                    </div>
                </div>
                <h1>Oops!</h1>
                <h3>
                    <?php
                    if ($exception->getMessage()) {
                        echo nl2br(esc($exception->getMessage()));
                    } else {
                        echo lang('Errors.sorryCannotFind');
                    }
                    ?>
                </h3>
                <hr>

                <a href="javascript:window.history.back();" class="btn btn-outline-theme px-3 rounded-pill"><i class="fa fa-arrow-left me-1 ms-n1"></i> Go Back</a>
            </div>

        </div>

        <a href="#" data-toggle="scroll-to-top" class="btn-scroll-top fade"><i class="fa fa-arrow-up"></i></a>

    </div>


    <script src="<?= base_url('public/_admin/js/vendor.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('public/_admin/js/app.min.js') ?>" type="text/javascript"></script>
</body>

</html>