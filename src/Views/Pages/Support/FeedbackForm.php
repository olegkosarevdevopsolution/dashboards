<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Dashboard') ?>

<?php echo $this->section('content') ?>
<form action="/feedback" method="post">
    <div class="mb-3">
        <label for="name" class="form-label">
            <?php echo lang("Support\FeedbackFormLanguage.name") ?>
        </label>
        <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo lang("Support\FeedbackFormLanguage.name_placeholder") ?>" required>
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">
            <?php echo lang("Support\FeedbackFormLanguage.name") ?>
        </label>
        <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo lang("Support\FeedbackFormLanguage.email_placeholder") ?>" required>
    </div>
    <div class="mb-3">
        <label for="subject" class="form-label">
            <?php echo lang("Support\FeedbackFormLanguage.subject") ?>
        </label>
        <select class="form-select" id="subject" name="subject" required>
            <option value="" selected disabled>
                <?php echo lang("Support\FeedbackFormLanguage.select_subject") ?>
            </option>
            <option value="Question">
                <?php echo lang("Support\FeedbackFormLanguage.subject_options.question") ?>
            </option>
            <option value="Proposal">
                <?php echo lang("Support\FeedbackFormLanguage.subject_options.proposal") ?>
            </option>
            <option value="Issue">
                <?php echo lang("Support\FeedbackFormLanguage.subject_options.issue") ?>
            </option>
        </select>
    </div>
    <div class="mb-3">
        <label for="message" class="form-label">
            <?php echo lang("Support\FeedbackFormLanguage.message") ?>
        </label>
        <textarea class="form-control" id="message" name="message" rows="5" required placeholder="<?php echo lang("Support\FeedbackFormLanguage.message_placeholder") ?>"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">
        <?php echo lang("Support\FeedbackFormLanguage.submit") ?>
    </button>
</form>
<?php echo $this->endSection() ?>