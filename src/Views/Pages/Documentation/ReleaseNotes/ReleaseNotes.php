<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Documentations') ?>

<?php echo $this->section('content') ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-12">
            <div class="row">

                <div class="col-xl-9">
                    <?php if (!empty($breadcrumb)) : ?>
                        <ul class="breadcrumb">
                            <?php foreach ($breadcrumb as $item) : ?>
                                <?php if (isset($item['link'])) : ?>
                                    <li class="breadcrumb-item"><a href="<?= $item['link'] ?>">
                                            <?= $item['label'] ?>
                                        </a></li>
                                <?php else : ?>
                                    <li class="breadcrumb-item active">
                                        <?= $item['label'] ?>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <?php if (is_array($page)) : ?>
                        <h1 class="page-header">
                            <?php if (isset($page['title'])) : ?>
                                <?= $page['title'] ?>
                            <?php endif; ?>
                            <?php if (isset($page['description'])) : ?>
                                <small>
                                    <?= $page['description'] ?>
                                </small>
                            <?php endif; ?>
                        </h1>
                    <?php endif; ?>
                    <p>
                        Формат основан на <a href="https://keepachangelog.com" target="_blank">Keep a Changelog</a> —
                        рекомендуемом стандарте для ведения изменений в проектах. Этот проект тесно следует <a href="http://semver.org" target="_blank">Семантическому Версионированию</a> — системе
                        присвоения версий, которая обеспечивает понятность и согласованность изменений в проекте. Мы
                        стремимся документировать все значимые изменения на этой странице, чтобы вы всегда могли быть в
                        курсе новостей и улучшений.
                    </p>
                    <hr class="mb-4">
                    <?php foreach ($content as $row) : ?>
                        <?php
                        $changes = $row["changes"];
                        $release = $row["release"];
                        ?>
                        <div id="<?php $release["slug"]; ?>" class="mb-5">
                            <h5>
                                <?php echo '[' . $release["version"] . '-' . $release["version_status"] . '] - ' . $release["release_date"]; ?>
                            </h5>
                            <?php echo $changes; ?>
                        </div>
                    <?php endforeach; ?>

                </div>

                <div class="col-xl-3">
                    <nav id="sidebar-bootstrap" class="navbar navbar-sticky d-none d-xl-block">
                        <nav class="nav">
                            <?php foreach ($content as $row) : ?>
                                <a class="nav-link active" href="#<?php echo $row["release"]['slug'] ?>" data-toggle="scroll-to">
                                    Version
                                    <?php echo $row["release"]['version'] ?>
                                </a>
                            <?php endforeach; ?>
                        </nav>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?>