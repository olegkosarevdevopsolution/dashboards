<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Documentations') ?>

<?php echo $this->section('content') ?>
<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Documentations') ?>

<?php echo $this->section('content') ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-12">
            <div class="row">

                <div class="col-xl-9">
                    <?php if (!empty($breadcrumb)) : ?>
                        <ul class="breadcrumb">
                            <?php foreach ($breadcrumb as $item) : ?>
                                <?php if (isset($item['link'])) : ?>
                                    <li class="breadcrumb-item"><a href="<?= $item['link'] ?>">
                                            <?= $item['label'] ?>
                                        </a></li>
                                <?php else : ?>
                                    <li class="breadcrumb-item active">
                                        <?= $item['label'] ?>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <?php if (is_array($page)) : ?>
                        <h1 class="page-header">
                            <?php if (isset($page['title'])) : ?>
                                <?= $page['title'] ?>
                            <?php endif; ?>
                            <?php if (isset($page['description'])) : ?>
                                <small>
                                    <?= $page['description'] ?>
                                </small>
                            <?php endif; ?>
                        </h1>
                    <?php endif; ?>
                    <hr class="mb-4">

                    <div id="version-1-0-0" class="mb-5">
                        <h4>Authentication</h4>
                        <p>Authentication Content Documentation Is Here</p>
                    </div>
                </div>

                <div class="col-xl-3">
                    <nav id="sidebar-bootstrap" class="navbar navbar-sticky d-none d-xl-block">
                        <nav class="nav">
                            <a class="nav-link" href="#version-1-0-0" data-toggle="scroll-to">Authentication</a>

                        </nav>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->endSection() ?>
<?php echo $this->endSection() ?>