<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Authentication') ?>

<?php echo $this->section('content') ?>
<div class="login">

    <div class="login-content">
        <form action="index.html" method="POST" name="login_form">
            <h1 class="text-center">Forget Password</h1>
            <p class="text-inverse text-opacity-50 text-center">
                Forgotten your password? We can help you recover it.
            </p>
            <div class="mb-3">
                <label class="form-label">Email Address <span class="text-danger">*</span></label>
                <input type="text" class="form-control form-control-lg bg-inverse bg-opacity-5" placeholder="username@address.com" value="" spellcheck="false" data-ms-editor="true">
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-outline-theme btn-lg d-block w-100">Forget Password</button>
            </div>
            <div class="text-inverse text-opacity-50 text-center">
                Already have an Admin ID? <a href="<?php echo base_url('authentication/login') ?>">Sign In</a>
            </div>
        </form>
    </div>

</div>
<?php echo $this->endSection() ?>