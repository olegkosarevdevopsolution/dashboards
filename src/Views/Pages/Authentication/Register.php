<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Authentication') ?>

<?php echo $this->section('content') ?>
<div class="register">

    <div class="register-content">
        <form action="<?php echo site_url('authentication/register') ?>" method="POST" name="register_form">
            <h1 class="text-center">Sign Up</h1>
            <p class="text-inverse text-opacity-50 text-center">One Admin ID is all you need to access all the Admin
                services.</p>
            <div class="mb-3">
                <label class="form-label">First name <span class="text-danger">*</span></label>
                <input type="text" name="firstname" class="form-control form-control-lg bg-inverse bg-opacity-5" placeholder="e.g John" value="<?php echo set_value('firstname') ?>">
                <?php if (session()->getFlashdata('firstname')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('firstname') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="mb-3">
                <label class="form-label">Last name <span class="text-danger">*</span></label>
                <input type="text" name="lastname" class="form-control form-control-lg bg-inverse bg-opacity-5" placeholder="e.g Smith" value="<?php echo set_value('lastname') ?>">
                <?php if (session()->getFlashdata('lastname')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('lastname') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="mb-3">
                <label class="form-label">Second name <span class="text-danger">*</span></label>
                <input type="text" name="secondname" class="form-control form-control-lg bg-inverse bg-opacity-5" placeholder="e.g Smither" value="<?php echo set_value('secondname') ?>">
                <?php if (session()->getFlashdata('secondname')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('secondname') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="mb-3">
                <label class="form-label">Email Address <span class="text-danger">*</span></label>
                <input type="email" name="email" class="form-control form-control-lg bg-inverse bg-opacity-5" placeholder="username@address.com" value="<?php echo set_value('email') ?>">
                <?php if (session()->getFlashdata('email')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('email') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="mb-3">
                <label class="form-label">Password <span class="text-danger">*</span></label>
                <input type="password" name="password" class="form-control form-control-lg bg-inverse bg-opacity-5" value="">
                <?php if (session()->getFlashdata('password')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('password') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="mb-3">
                <label class="form-label">Confirm Password <span class="text-danger">*</span></label>
                <input type="password" name="confirm_password" class="form-control form-control-lg bg-inverse bg-opacity-5" value="">
                <?php if (session()->getFlashdata('confirm_password')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('confirm_password') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="mb-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="customCheck1">
                    <label class="form-check-label" for="customCheck1">I have read and agree to the <a href="#">Terms of
                            Use</a> and <a href="#">Privacy Policy</a>.</label>
                </div>
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-outline-theme btn-lg d-block w-100">Sign Up</button>
            </div>
            <div class="text-inverse text-opacity-50 text-center">
                Already have an Admin ID? <a href="<?php echo base_url('authentication/login') ?>">Sign In</a>
            </div>
        </form>
    </div>

</div>
<?php echo $this->endSection() ?>