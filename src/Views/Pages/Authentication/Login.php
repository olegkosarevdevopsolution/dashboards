<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Authentication') ?>

<?php echo $this->section('content') ?>
<div class="login pt-0">
    <div class="login-content">
        <form action="<?php echo base_url("authentication/login"); ?>" method="POST" name="login_form">
            <h1 class="text-center">
                <?php echo lang('Authentication/Login.signIn') ?>
            </h1>
            <div class="text-inverse text-opacity-50 text-center mb-4">
                <?php echo lang('Authentication/Login.verifyIdentity') ?>
            </div>
            <div class="mb-3">
                <label class="form-label">
                    <?php echo lang('Authentication/Login.emailAddress') ?>
                    <span class="text-danger">*</span>
                </label>
                <input type="text" class="form-control form-control-lg bg-inverse bg-opacity-5" name="email" value="<?php echo set_value('email') ?>" placeholder="" spellcheck="false" data-ms-editor="true">
                <?php if (session()->getFlashdata('email')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('email') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="mb-3">
                <div class="d-flex">
                    <label class="form-label">
                        <?php echo lang('Authentication/Login.password') ?>
                        <span class="text-danger">*</span>
                    </label>
                    <?php if ($authenticationConfig->allowForgetPassword) : ?>
                        <a href="<?php echo base_url("authentication/forgot-password"); ?>" class="ms-auto text-inverse text-decoration-none text-opacity-50">
                            <?php echo lang('Authentication/Login.forgotPassword') ?>
                        </a>
                    <?php endif; ?>
                </div>
                <input type="password" class="form-control form-control-lg bg-inverse bg-opacity-5" name="password" value="" placeholder="">
                <?php if (session()->getFlashdata('password')) : ?>
                    <p class="text-danger text-opacity-75 text-center mt-1">
                        <?php echo session()->getFlashdata('password') ?>
                    </p>
                <?php endif; ?>
            </div>
            <?php if ($authenticationConfig->rememberMe) : ?>
                <div class="mb-3">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="customCheck1">
                        <label class="form-check-label" for="customCheck1">
                            <?php echo lang('Authentication/Login.rememberMe') ?>
                        </label>
                    </div>
                </div>
            <?php endif; ?>
            <button type="submit" class="btn btn-outline-theme btn-lg d-block w-100 fw-500 mb-3">
                <?php echo lang('Authentication/Login.signInButton') ?>
            </button>
            <?php if ($authenticationConfig->allowRegister) : ?>
                <div class="text-center text-inverse text-opacity-50">
                    <?php echo lang('Authentication/Login.noAccount') ?>
                    <a href="<?php echo base_url('authentication/register') ?>" class="text-theme">
                        <?php echo lang('Authentication/Login.signUp') ?>
                    </a>
                </div>
            <?php endif; ?>
        </form>
    </div>

</div>
<?php echo $this->endSection() ?>