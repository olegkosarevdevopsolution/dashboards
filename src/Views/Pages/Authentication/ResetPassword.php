<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Authentication') ?>

<?php echo $this->section('content') ?>
<div class="login">

    <div class="login-content">
        <form action="index.html" method="POST" name="login_form">
            <h1 class="text-center">Reset Password</h1>
            <p class="text-inverse text-opacity-50 text-center">
                Reset Password. Description.
            </p>
            <div class="mb-3">
                <label class="form-label">Password <span class="text-danger">*</span></label>
                <input type="password" class="form-control form-control-lg bg-inverse bg-opacity-5" value="">
            </div>
            <div class="mb-3">
                <label class="form-label">Confirm Password <span class="text-danger">*</span></label>
                <input type="password" class="form-control form-control-lg bg-inverse bg-opacity-5" value="">
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-outline-theme btn-lg d-block w-100">Reset Password</button>
            </div>
            <div class="text-inverse text-opacity-50 text-center">
                Already have an Admin ID? <a href="<?php echo base_url('authentication/login') ?>">Sign In</a>
            </div>
        </form>
    </div>

</div>
<?php echo $this->endSection() ?>