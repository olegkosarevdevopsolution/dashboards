<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Authentication') ?>

<?php echo $this->section('content') ?>
<div class="login">
    <div class="login-content">
        <form action="<?php echo base_url('authentication/lock-screen') ?>" method="POST" name="login_form">
            <h1 class="text-center">
                Lock Screen
            </h1>
            <div class="text-inverse text-opacity-50 text-center mb-4">
                For your protection, please verify your identity.
            </div>
            <div class="mb-3">
                <div class="d-flex">
                    <label class="form-label">
                        Password <span class="text-danger">*</span>
                    </label>
                </div>
                <input type="password" class="form-control form-control-lg bg-inverse bg-opacity-5" value="" placeholder="">
            </div>
            <button type="submit" class="btn btn-outline-theme btn-lg d-block w-100 fw-500 mb-3">
                Sign In </button>
        </form>
    </div>
</div>
<?php echo $this->endSection() ?>