<?php

/**
 * MenuItems
 *
 * description
 *
 * @package \Devolegkosarev\Dashboard\Views\Pages\Dashboard\Menus;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
?>

<?php

// Extending the 'Dashboard' layout
echo $this->extend('Devolegkosarev\Dashboard\Views\Layouts\Dashboard');

// Starting the 'content' section
echo $this->section('content');

echo $content;

$html = buildMenuHTML($rows, 0);

function buildMenuHTML($array, $parentId, $level = 0)
{
    $html = '';
    $count = count($array);
    $index = 0;

    foreach ($array as $item) {
        $index++;
        if ($item->parent_id == $parentId) {
            $isLastItem = ($index === $count);
            $html .= '<tr>';
            $html .= '<td>' . $item->id . '</td>';

            $html .= '<td class="shadow hover-primary">';
            if ($isLastItem) {
                $html .= '<span style="padding-left: ' . ($level * 20) . 'px;"> └── </span>';
            } else {
                $html .= '<span style="padding-left: ' . ($level * 20) . 'px;"> ├── </span>';
            }

            $html .= $item->name . '</td>';
            $html .= '</tr>';

            if (isset($item->items) && is_array($item->items) && count($item->items) > 0) {
                $children = buildMenuHTML($item->items, $item->id, $level + 1);
                $html .= $children;
            }
        }
    }

    return $html;
}

// Usage example
$tableHTML = '
<table class="table table-striped table-bordered">
    <tbody>
        ' . buildMenuHTML($rows, 0) . '
    </tbody>
</table>
';
echo '<div>' . $tableHTML . '</div>';
// Ending the 'content' section
echo $this->endSection();
?>