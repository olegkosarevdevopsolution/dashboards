<?php

/**
 * CreateRole
 *
 * This is the CreateRole view file 
 *
 * @package \Devolegkosarev\Dashboard\Views\Pages\Dashboard\Menus;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
?>

<?php

// Extending the 'Dashboard' layout
echo $this->extend('Devolegkosarev\Dashboard\Views\Layouts\Dashboard');

// Starting the 'content' section
echo $this->section('content');

// echo $content;
?>
<div class="card">
    <ul class="nav nav-tabs nav-tabs-v2 px-4" role="tablist">
        <li class="nav-item me-3" role="presentation">
            <a href="#generalTab" class="nav-link px-2 active" data-bs-toggle="tab" aria-selected="true" role="tab">
                <?php echo lang('Dashboard\Menus\MenusFormsLanguage.menuInfoTab.label') ?>
            </a>
        </li>
        <?php foreach ($languages as $language) : ?>
            <li class="nav-item me-3" role="presentation">
                <a href="#language<?php echo ucfirst($language["code"]); ?>Tab" class="nav-link px-2" data-bs-toggle="tab" aria-selected="true" role="tab">
                    <?php echo lang('Dashboard\Menus\MenusFormsLanguage.menuTranslationTab.label', ['language' => $language['name']]); ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <form action="<?php echo base_url('dashboard/menus/add'); ?>" method="POST" name="create_form" id="create_form">
        <div class="tab-content p-4">
            <div class="tab-pane fade active show" id="generalTab" role="tabpanel">
                <?php

                echo $content;

                ?>
            </div>

            <?php echo $translatedTab; ?>
        </div>

        <div class="card-footer">
            <button type="submit" id="btn-submit" class="btn m-2 btn-theme">
                <?php echo lang('Dashboard\Menus\CreateMenuLanguage.buttons.submit') ?>
            </button>
            <a href="cancelUrl" class="btn m-2 btn-theme">
                <?php echo lang('Dashboard\Menus\CreateMenuLanguage.buttons.cancel') ?>
            </a>
        </div>
    </form>

    <div class="card-arrow">
        <div class="card-arrow-top-left"></div>
        <div class="card-arrow-top-right"></div>
        <div class="card-arrow-bottom-left"></div>
        <div class="card-arrow-bottom-right"></div>
    </div>
</div>
<?php
// Ending the 'content' section
echo $this->endSection();
?>

<?php echo $this->section('javascript'); ?>
<script type="text/javascript">
    const textsRoleSelect = {
        trigger: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.role.texts.trigger") ?>",
        noResult: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.role.texts.noResult") ?>",
        search: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.role.texts.search") ?>",
    }

    const textsTypeMenuSelect = {
        trigger: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.typeMenu.texts.trigger") ?>",
        noResult: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.typeMenu.texts.noResult") ?>",
        search: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.typeMenu.texts.search") ?>",
    }

    const textsParrentSelect = {
        trigger: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.parrent.texts.trigger") ?>",
        noResult: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.parrent.texts.noResult") ?>",
        search: "<?php echo lang("Dashboard\Menus\MenusFormsLanguage.menuInfoTab.fields.parrent.texts.search") ?>"
    }
</script>
<!-- required js / css Select Picker -->
<script src="<?php echo base_url('public/_admin/js/dashboard/Menus/CreateMenus.js'); ?>" type="module"></script>

<script src="<?php echo base_url('public/_admin/plugins/select-picker/src/picker.js'); ?>"></script>

<!-- required js / css Bootstrap datepicker  -->
<script src="<?php echo base_url('public/_admin/plugins/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

<?php $this->endSection(); ?>

<?php echo $this->section('stylesheet'); ?>
<!-- required js / css Bootstrap datepicker  -->
<link href="<?php echo base_url('public/_admin/plugins/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel=" stylesheet">

<!-- required js / css Select Picker -->
<link href="<?php echo base_url('public/_admin/plugins/select-picker/dist/picker.min.css'); ?>" rel="stylesheet">
<?php $this->endSection(); ?>