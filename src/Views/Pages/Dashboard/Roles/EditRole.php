<?php

/**
 * CreateRole
 *
 * This is the CreateRole view file 
 *
 * @package \Devolegkosarev\Dashboard\Views\Pages\Dashboard\Roles;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
?>

<?php

// Extending the 'Dashboard' layout
echo $this->extend('Devolegkosarev\Dashboard\Views\Layouts\Dashboard');

// Starting the 'content' section
echo $this->section('content');

?>
<div class="card">
    <ul class="nav nav-tabs nav-tabs-v2 px-4" role="tablist">
        <li class="nav-item me-3" role="presentation">
            <a href="#generalTab" class="nav-link px-2 active" data-bs-toggle="tab" aria-selected="true" role="tab">
                <?php echo lang('Dashboard\Roles\RolesFormsLanguage.roleInfoTab.label') ?>
            </a>
        </li>
        <?php foreach ($languages as $language) : ?>
            <li class="nav-item me-3" role="presentation">
                <a href="#language<?php echo ucfirst($language["code"]); ?>Tab" class="nav-link px-2" data-bs-toggle="tab" aria-selected="true" role="tab">
                    <?php echo lang('Dashboard\Roles\RolesFormsLanguage.roleTranslationTab.label', ['language' => $language['name']]); ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <form action="<?php echo base_url('dashboard/roles/edit/' . $role->id); ?>" method="POST" name="edit_role_form" id="edit_role_form">
        <div class="tab-content p-4">
            <div class="tab-pane fade active show" id="generalTab" role="tabpanel">
                <?php

                echo $content;

                ?>
            </div>

            <?php echo $translatedTab; ?>

        </div>

        <div class="card-footer">
            <button type="submit" id="btn-submit" class="btn m-2 btn-theme">
                <?php echo lang('Dashboard\Roles\EditRoleLanguage.buttons.submit') ?>
            </button>
            <a href="<?php echo base_url('dashboard/roles'); ?>" class="btn m-2 btn-theme">
                <?php echo lang('Dashboard\Roles\CreateRoleLanguage.buttons.cancel') ?>
            </a>
        </div>
    </form>

    <div class="card-arrow">
        <div class="card-arrow-top-left"></div>
        <div class="card-arrow-top-right"></div>
        <div class="card-arrow-bottom-left"></div>
        <div class="card-arrow-bottom-right"></div>
    </div>
</div>

<?php
// Ending the 'content' section
echo $this->endSection();
?>

<?php echo $this->section('javascript'); ?>
<script src="<?php echo base_url('/public/_admin/js/dashboard/Roles/EditRoles.js'); ?>" type="module"></script>
<?php $this->endSection(); ?>