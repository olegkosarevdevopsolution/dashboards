<?php

/**
 * Users
 *
 * This class is used to handle the Users
 *
 * @package \Devolegkosarev\Dashboard\Views\Pages\Dashboard\Users;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
?>

<?php

// Extending the 'Dashboard' layout
echo $this->extend('Devolegkosarev\Dashboard\Views\Layouts\Dashboard');

// Starting the 'content' section
echo $this->section('content'); ?>

<div class="d-flex justify-content-left align-items-center">
    <?php foreach ($users as $user) : ?>

        <div class="card m-1">
            <div class="card-header fw-bold small">
                <?php echo $user['name']; ?>
            </div>
            <div class="card-group card-body">
                <?php foreach ($user['users'] as $user) : ?>
                    <div class="card m-1" style="width: 18rem;">
                        <img src="<?php echo base_url($user['avatar']); ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo $user['firstname'] . ' ' . $user['lastname'] . ' ' . $user['secondname']; ?>
                            </h5>
                            <a href="<?php echo base_url('dashboard/users/edit/' . $user['id']); ?>" class="btn w-100 btn-primary" style="border-radius: 4px 4px 0px 0px;"><?php echo lang('Dashboard\Users\UsersLanguage.buttons.edit.value'); ?></a>
                            <div class="btn-group w-100" role="group" aria-label="Basic example">
                                <?php if ($user['blocked'] == 1) : ?>
                                    <a href="<?php echo base_url('dashboard/users/unblock/' . $user['id']); ?>" class="btn btn-success" style="border-radius: 0 0 0 4px;"><?php echo lang('Dashboard\Users\UsersLanguage.buttons.unblock.value'); ?></a>
                                <?php else : ?>
                                    <a href="<?php echo base_url('dashboard/users/block/' . $user['id']); ?>" class="btn btn-danger" style="border-radius: 0 0 0 4px;"><?php echo lang('Dashboard\Users\UsersLanguage.buttons.block.value'); ?></a>
                                <?php endif; ?>
                                <a href="<?php echo base_url('dashboard/users/delete/' . $user['id']); ?>" class="btn btn-danger">Delete</a>

                                <?php if ($user['activated'] == 1) : ?>
                                    <a href="<?php echo base_url('dashboard/users/deactivate/' . $user['id']); ?>" class="btn btn-warning" style="border-radius: 0 0 4px 0;"><?php echo lang('Dashboard\Users\UsersLanguage.buttons.deactivate.value'); ?></a>
                                <?php else : ?>
                                    <a href="<?php echo base_url('dashboard/users/activate/' . $user['id']); ?>" class="btn btn-success" style="border-radius: 0 0 4px 0;"><?php echo lang('Dashboard\Users\UsersLanguage.buttons.activate.value'); ?></a>
                                <?php endif; ?>

                            </div>
                        </div>
                        <div class="card-arrow">
                            <div class="card-arrow-top-left"></div>
                            <div class="card-arrow-top-right"></div>
                            <div class="card-arrow-bottom-left"></div>
                            <div class="card-arrow-bottom-right"></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="card-arrow">
                <div class="card-arrow-top-left"></div>
                <div class="card-arrow-top-right"></div>
                <div class="card-arrow-bottom-left"></div>
                <div class="card-arrow-bottom-right"></div>
            </div>
        </div>

    <?php endforeach; ?>
</div>
<?php
// Ending the 'content' section
echo $this->endSection();
