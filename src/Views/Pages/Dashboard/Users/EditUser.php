<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Dashboard') ?>

<?php echo $this->section('content') ?>
<div class="card">
    <div class="card-body p-0">
        <div class="profile">
            <div class="profile-container">
                <div class="profile-sidebar">
                    <div class="desktop-sticky-top">
                        <div class="profile-img" style="height: auto;">
                            <img src="<?php echo base_url($user['avatar']) ?>" alt="">
                            <input type="file" value="<?php echo base_url($user['avatar']) ?>" class="form-control" placeholder="Avatar" style="border-top-left-radius: 0px;border-top-right-radius: 0px;">
                        </div>
                        <h4></h4>
                        <div class="form-group mb-3">
                            <label class="form-label">Role name:</label>
                            <select class="form-select">
                                <option disabled>Select role</option>
                                <?php foreach ($roles as $role) : ?>
                                    <option <?php echo ($role->id == $user["role_id"]) ? 'selected' : ''; ?> value="<?php echo $role->id; ?>"><?php echo $role->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="text-inverse text-opacity-50 fw-bold">
                            Created at: <span><?php echo $user["created_at"]; ?></span>
                        </div>
                        <div class="text-inverse text-opacity-50 fw-bold">
                            Updated at: <span><?php echo $user["updated_at"]; ?></span>
                        </div>
                    </div>
                </div>
                <div class="profile-content">
                    <ul class="profile-tab nav nav-tabs nav-tabs-v2" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a href="#profile-post" class="nav-link active" data-bs-toggle="tab" aria-selected="true" role="tab">
                                <div class="nav-field">User</div>
                                <div class="nav-value">Info</div>
                            </a>
                        </li>
                    </ul>
                    <div class="profile-content-container">
                        <div class="row gx-4">
                            <div class="col-xl-8">
                                <div class="tab-content p-0">
                                    <div class="tab-pane fade show active" id="profile-post" role="tabpanel">
                                        <div class="form-group mb-3">
                                            <label class="form-label">Email</label>
                                            <input type="email" class="form-control" placeholder="Email" value="<?php echo $user["email"]; ?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label class="form-label">First Name</label>
                                            <input type="text" class="form-control" placeholder="First Name" value="<?php echo $user["firstname"]; ?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label class="form-label">Last Name</label>
                                            <input type="text" class="form-control" placeholder="Last Name" value="<?php echo $user["lastname"]; ?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label class="form-label">Second Name</label>
                                            <input type="text" class="form-control" placeholder="Second Name" value="<?php echo $user["secondname"]; ?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <div class="form-check-inline form-check form-switch">
                                                <input type="checkbox" <?php echo ((int) $user["activated"] == 1) ? 'checked' : ''; ?> class="form-check-input" id="customSwitch1">
                                                <label class="form-check-label" for="customSwitch1">Activated</label>
                                                <div id="passwordHelpBlock" class="form-text text-muted">
                                                    User is activated or not
                                                </div>
                                            </div>
                                            <div class="form-check-inline form-check form-switch">
                                                <input type="checkbox" <?php echo ((int) $user["blocked"] == 1) ? 'checked' : ''; ?> class="form-check-input" id="customSwitch1">
                                                <label class="form-check-label" for="customSwitch1">Blocked</label>
                                                <div id="passwordHelpBlock" class="form-text text-muted">
                                                    User is blocked or not
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group mb-3">
                                            <label class="form-label">Avatar</label>
                                            <input type="file" class="form-control" placeholder="Avatar">
                                        </div> -->

                                    </div>

                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="desktop-sticky-top d-none d-lg-block">
                                    <div class="card mb-3">
                                        <div class="list-group list-group-flush">
                                            <div class="list-group-item fw-bold px-3 d-flex">
                                                <span class="flex-fill">Password</span>
                                                <a href="#" class="text-inverse text-opacity-50"><i class="fa fa-lock"></i></a>
                                            </div>
                                            <div class="list-group-item px-3">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">Password</label>
                                                    <input type="password" class="form-control" placeholder="New Password">
                                                    <small id="passwordHelpBlock" class="form-text text-muted">
                                                        Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
                                                    </small>
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label class="form-label">Confirm Password</label>
                                                    <input type="password" class="form-control" placeholder="Confirm New Password">
                                                    <small id="passwordHelpBlock" class="form-text text-muted">
                                                        Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
                                                    </small>
                                                </div>
                                            </div>

                                            <a href="#" class="list-group-item list-group-action text-center">
                                                <i class="fa fa-save"></i> Save
                                            </a>
                                        </div>
                                        <div class="card-arrow">
                                            <div class="card-arrow-top-left"></div>
                                            <div class="card-arrow-top-right"></div>
                                            <div class="card-arrow-bottom-left"></div>
                                            <div class="card-arrow-bottom-right"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-arrow">
        <div class="card-arrow-top-left"></div>
        <div class="card-arrow-top-right"></div>
        <div class="card-arrow-bottom-left"></div>
        <div class="card-arrow-bottom-right"></div>
    </div>
</div>
<?php echo $this->endSection() ?>