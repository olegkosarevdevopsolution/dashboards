<?php

/**
 * Premissions
 *
 * This file contains an array of permissions
 *
 * @package \Devolegkosarev\Dashboard\Views\Pages\Dashboard\Permissions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/ComposerProjects/_git/devopsolution.appstarter
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

// Extending the 'Dashboard' layout
echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Dashboard');

// Starting the 'content' section
echo $this->section('content');

// Outputting the content from the 'content' variable
echo $content;

// Ending the 'content' section
echo $this->endSection();
