<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Dashboard') ?>

<?php echo $this->section('content') ?>
<div class="card">
    <div class="card-body p-0">
        <div class="profile">
            <div class="profile-container">
                <div class="profile-sidebar">
                    <div class="desktop-sticky-top">
                        <div class="profile-img" style="height: auto;">
                            <img src="<?php echo base_url($user['avatar']); ?>" alt="<?php echo $user['firstname']; ?> <?php echo $user['lastname']; ?>">
                            <input type="file" class="form-control" placeholder="Avatar" style="border-top-left-radius: 0px;border-top-right-radius: 0px;">
                        </div>
                        <h4><?php echo $user['firstname']; ?> <?php echo $user['lastname']; ?></h4>
                        <div class="text-inverse text-opacity-50 fw-bold">Role name: Admin</div>
                        <div class="text-inverse text-opacity-50 fw-bold">Created at: <?php echo $user['created_at']; ?></div>
                        <div class="text-inverse text-opacity-50 fw-bold">Updated at: <?php echo $user['updated_at']; ?></div>
                    </div>
                </div>
                <div class="profile-content">
                    <ul class="profile-tab nav nav-tabs nav-tabs-v2" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a href="#profile-post" class="nav-link active" data-bs-toggle="tab" aria-selected="true" role="tab">
                                <div class="nav-field">Profile</div>
                                <div class="nav-value">Edit</div>
                            </a>
                        </li>
                    </ul>
                    <div class="profile-content-container">
                        <div class="row gx-4">
                            <div class="col-xl-8">
                                <div class="tab-content p-0">
                                    <div class="tab-pane fade show active" id="profile-post" role="tabpanel">
                                        <div class="form-group mb-3">
                                            <label class="form-label">Email</label>
                                            <input type="email" class="form-control" placeholder="Email" value="<?php echo $user['email']; ?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label class="form-label">First Name</label>
                                            <input type="text" class="form-control" placeholder="First Name" value="<?php echo $user['firstname']; ?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label class="form-label">Last Name</label>
                                            <input type="text" class="form-control" placeholder="Last Name" value="<?php echo $user['lastname']; ?>">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label class="form-label">Second Name</label>
                                            <input type="text" class="form-control" placeholder="Second Name" value="<?php echo $user['secondname']; ?>">
                                        </div>
                                        <!-- <div class="form-group mb-3">
                                            <label class="form-label">Avatar</label>
                                            <input type="file" class="form-control" placeholder="Avatar">
                                        </div> -->
                                        <div class="form-group mb-3">
                                            <button type="submit" class="btn btn-block btn-theme">Save</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="desktop-sticky-top d-none d-lg-block">
                                    <div class="card mb-3">
                                        <div class="list-group list-group-flush">
                                            <div class="list-group-item fw-bold px-3 d-flex">
                                                <span class="flex-fill">Change Password</span>
                                                <a href="#" class="text-inverse text-opacity-50"><i class="fa fa-lock"></i></a>
                                            </div>
                                            <div class="list-group-item px-3">
                                                <div class="form-group mb-3">
                                                    <label class="form-label">Current Password</label>
                                                    <input type="password" class="form-control" placeholder="Current Password">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label class="form-label">New Password</label>
                                                    <input type="password" class="form-control" placeholder="New Password">
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label class="form-label">Confirm New Password</label>
                                                    <input type="password" class="form-control" placeholder="Confirm New Password">
                                                </div>
                                            </div>

                                            <a href="#" class="list-group-item list-group-action text-center">
                                                <i class="fa fa-lock"></i> Change Password
                                            </a>
                                        </div>
                                        <div class="card-arrow">
                                            <div class="card-arrow-top-left"></div>
                                            <div class="card-arrow-top-right"></div>
                                            <div class="card-arrow-bottom-left"></div>
                                            <div class="card-arrow-bottom-right"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-arrow">
        <div class="card-arrow-top-left"></div>
        <div class="card-arrow-top-right"></div>
        <div class="card-arrow-bottom-left"></div>
        <div class="card-arrow-bottom-right"></div>
    </div>
</div>
<?php echo $this->endSection() ?>