<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Dashboard') ?>

<?php echo $this->section('content') ?>
<form>
    <?php
    $releaseInfo = $release['release'];
    $changes = $release['changes'];
    ?>
    <div class="card">
        <ul class="nav nav-tabs nav-tabs-v2 px-4" role="tablist">
            <li class="nav-item me-3" role="presentation">
                <a href="#releaseNotesTab" class="nav-link px-2 active" data-bs-toggle="tab" aria-selected="true" role="tab">
                    Release Notes
                </a>
            </li>
            <?php foreach ($languages as $key => $language) : ?>
                <li class="nav-item me-3" role="presentation">
                    <a href="#changeLogTab<?php echo $language['name'] ?>" class="nav-link px-2" data-bs-toggle="tab" aria-selected="true" role="tab">
                        <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.change_log", ["languageName" => $language["name"]]) ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content p-4">
            <div class="tab-pane fade active show" id="releaseNotesTab" role="tabpanel">
                <div class="mb-3">
                    <label for="version" class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.version") ?>
                    </label>
                    <input type="text" id="version" name="version" class="form-control" value="<?php echo $releaseInfo['version'] ?>">
                </div>
                <div class="mb-3">
                    <label for="slug" class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.slug") ?>
                    </label>
                    <input type="text" id="slug" name="slug" class="form-control" value="<?php echo $releaseInfo['slug'] ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.release_date") ?>
                        <span class="text-danger">*</span>
                    </label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="release_date" value="<?php echo $releaseInfo['release_date'] ?>">
                        <label class="input-group-text" for="release_date">
                            <i class="fa fa-calendar"></i>
                        </label>
                    </div>
                </div>
                <div class="mb-3">
                    <div class="form-check form-switch">
                        <input type="checkbox" class="form-check-input" id="publish_status" <?php if ($releaseInfo["publish_status"] === "1")
                                                                                                echo "checked" ?>>
                        <label class="form-check-label" for="publish_status">
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.publish_status") ?>
                        </label>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="version_status" class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.version_status") ?>
                    </label>
                    <select class="form-select" id="version_status" name="version_status">
                        <option>
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.version_status") ?>
                        </option>
                        <option value="pre-alpha" <?php if ($releaseInfo["version_status"] == "pre-alpha")
                                                        echo "selected" ?>>
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.pre_alpha") ?>
                        </option>

                        <option value="alpha" <?php if ($releaseInfo["version_status"] == "alpha")
                                                    echo "selected" ?>>
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.alpha") ?>
                        </option>

                        <option value="beta" <?php if ($releaseInfo["version_status"] == "beta")
                                                    echo "selected" ?>>
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.beta") ?>
                        </option>

                        <option value="release_candidate" <?php if ($releaseInfo["version_status"] == "release_candidate")
                                                                echo "selected" ?>>
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.release_candidate") ?>
                        </option>

                        <option value="release" <?php if ($releaseInfo["version_status"] == "release")
                                                    echo "selected" ?>>
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.release") ?>
                        </option>
                    </select>
                </div>
            </div>

            <?php $tabNamePrefix = 'changeLogTab'; ?>
            <?php foreach ($languages as $language) : ?>
                <?php $found = false; ?>
                <?php $tabId = $tabNamePrefix . $language['name']; ?>
                <?php $languageCode = $language['code']; ?>
                <?php $changeId = 0; ?>
                <div class="tab-pane fade" id="<?php echo $tabId ?>" role="tabpanel">
                    <div class="mb-3">
                        <label for="language" class="form-label">Language</label>
                        <input type="text" id="language" name="language" value="<?php echo $languageCode ?>" class="form-control" readonly>
                    </div>
                    <div class="mb-3">
                        <label for="id" class="form-label">Id</label>
                        <?php foreach ($changes as $change) : ?>
                            <?php if ($change["language"] == $languageCode) : ?>
                                <input type="text" id="id" name="id" value="<?php echo $change["id"] ?>" class="form-control" readonly>
                                <?php $changeId = $change["id"]; ?>
                                <?php $found = true; ?>
                                <?php break; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php if (!$found) : ?>
                            <input type="text" id="id" name="id" value="<?php echo $changeId ?>" class="form-control" readonly>
                        <?php endif; ?>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">
                            <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.change_log_contect") ?>
                            <span class="text-danger">*</span>
                        </label>
                        <?php if ($found) : ?>
                            <textarea class="summernote" rows="12"><?php echo $change['content'] ?></textarea>
                        <?php else : ?>
                            <textarea class="summernote" rows="12"></textarea>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>



        </div>

        <div class="card-footer">
            <button type="submit" class="btn m-2 btn-theme">
                <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.btn_save") ?>
            </button>
            <a href="<?php echo base_url('documentation/release-notes/list') ?>" class="btn m-2 btn-theme">
                <?php echo lang("Documentation\ReleaseNotes\EditReleaseNotesLanguage.btn_cancel") ?>
            </a>
        </div>
        <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
        </div>
    </div>


</form>

<?php echo $this->endSection() ?>

<?php echo $this->section('javascript') ?>
<script>
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 400
        });
        $('#release_date').datepicker({
            language: '<?php echo $currentLanguage ?>',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    });
</script>
<?php echo $this->endSection() ?>