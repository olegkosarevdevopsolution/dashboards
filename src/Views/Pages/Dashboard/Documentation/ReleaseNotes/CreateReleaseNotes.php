<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Dashboard') ?>

<?php echo $this->section('content') ?>
<form>
    <div class="card">
        <ul class="nav nav-tabs nav-tabs-v2 px-4" role="tablist">
            <li class="nav-item me-3" role="presentation">
                <a href="#releaseNotesTab" class="nav-link px-2 active" data-bs-toggle="tab" aria-selected="true" role="tab">
                    Release Notes
                </a>
            </li>
            <?php foreach ($languages as $key => $language) : ?>
                <li class="nav-item me-3" role="presentation">
                    <a href="#changeLogTab<?php echo $language['name'] ?>" class="nav-link px-2" data-bs-toggle="tab" aria-selected="true" role="tab">
                        <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.change_log", ["languageName" => $language["name"]]) ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content p-4">
            <div class="tab-pane fade active show" id="releaseNotesTab" role="tabpanel">
                <div class="mb-3">
                    <label for="version" class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.version") ?>
                    </label>
                    <input type="text" id="version" name="version" class="form-control" placeholder="1.0.0">
                </div>
                <div class="mb-3">
                    <label for="slug" class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.slug") ?>
                    </label>
                    <input type="text" id="slug" name="slug" class="form-control" placeholder="release-1.0.0">
                </div>
                <div class="mb-3">
                    <label class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.release_date") ?>
                        <span class="text-danger">*</span>
                    </label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="release_date" placeholder="2023-02-15">
                        <label class="input-group-text" for="release_date">
                            <i class="fa fa-calendar"></i>
                        </label>
                    </div>
                </div>
                <div class="mb-3">
                    <div class="form-check form-switch">
                        <input type="checkbox" value="1" class="form-check-input" id="publish_status">
                        <label class="form-check-label" for="publish_status">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.publish_status") ?>
                        </label>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="version_status" class="form-label">
                        <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.version_status") ?>
                    </label>
                    <select class="form-select" id="version_status" name="version_status">
                        <option selected>
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.version_status") ?>
                        </option>
                        <option value="pre_alpha">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.pre_alpha") ?>
                        </option>

                        <option value="alpha">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.alpha") ?>
                        </option>

                        <option value="beta">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.beta") ?>
                        </option>

                        <option value="release_candidate">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.release_candidate") ?>
                        </option>

                        <option value="release">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.release") ?>
                        </option>
                    </select>
                </div>
            </div>

            <?php foreach ($languages as $key => $language) : ?>
                <div class="tab-pane fade" id="changeLogTab<?php echo $language['name'] ?>" role="tabpanel">
                    <div class="mb-3">
                        <label for="language" class="form-label">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.language") ?>
                        </label>
                        <input type="text" id="language" name="language" value="<?php echo $language['code'] ?>" class="form-control" readonly>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">
                            <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.change_log_contect") ?>
                            <span class="text-danger">*</span>
                        </label>
                        <textarea class="summernote" rows="12"></textarea>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn m-2 btn-theme">
                <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.btn_save") ?>
            </button>
            <a href="<?php echo base_url('documentation/release-notes/list') ?>" class="btn m-2 btn-theme">
                <?php echo lang("Documentation\ReleaseNotes\CreateReleaseNotesLanguage.btn_cancel") ?>
            </a>
        </div>

        <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
        </div>
    </div>
</form>

<?php echo $this->endSection() ?>

<?php echo $this->section('javascript') ?>
<script>
    $(document).ready(function() {
        var today = new Date().toISOString().slice(0, 10);
        $('#release_date').val(today);
        $('.summernote').summernote({
            height: 400
        });
        $('#release_date').datepicker({
            language: '<?php echo $currentLanguage ?>',
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    });
</script>
<?php echo $this->endSection() ?>