<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Dashboard') ?>

<?php echo $this->section('content') ?>
<div class="col-xl-6">

    <div class="card mb-3">

        <div class="card-body">

            <div class="d-flex fw-bold small mb-3">
                <span class="flex-grow-1">ACTIVITY LOG</span>
            </div>


            <div class="table-responsive">
                <table class="table table-striped table-borderless mb-2px small text-nowrap">
                    <tbody>
                        <?php for ($i = 0; $i < 14; $i++) : ?>
                            <tr>
                                <td>
                                    <span class="d-flex align-items-center">
                                        <i class="bi bi-circle-fill fs-6px text-inverse text-opacity-25 me-2"></i>
                                        <?php echo rand(1, 4) ?> pull request from Azure DevOps Repos
                                    </span>
                                </td>
                                <td><small><?php echo rand(1, 59) ?> mins ago</small></td>
                                <td>
                                    <span class="badge d-block text-inverse bg-secondary bg-opacity-25 rounded-0 pt-5px w-70px">Azure</span>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>
            </div>

        </div>


        <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
        </div>

    </div>

</div>
<?php echo $this->endSection() ?>