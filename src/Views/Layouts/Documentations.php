<?php
$versionBase = $dashboardConfig->versionBase;
$versionProject = $dashboardConfig->versionProject;
$project = $dashboardConfig->project;
$company = $dashboardConfig->company;
$logoDark = $dashboardConfig->logoDark;
?>

<!DOCTYPE html>
<html lang="en" class="">

<head>

    <?php include_once FCPATH . 'vendor/Devolegkosarev/dashboard/src/Views/Partials/Head/Head.php';  ?>

</head>

<body class="pace-top <?php echo $dashboardConfig->theme ?>">
    <div id="app" class="app <?php echo $pageClass ?>">
        <div id="header" class="app-header">
            <div class="mobile-toggler">
                <button type="button" class="menu-toggler" data-toggle="app-top-nav-mobile">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </button>
            </div>

            <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Brand', []); ?>

            <div class="menu">
                <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Header\LanguageSwitcher', ['currentLocale' => 'en']); ?>
                <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Header\Profile', []); ?>
            </div>
        </div>

        <div id="sidebar" class="app-sidebar">
            <div class="app-sidebar-content ps ps--active-y" data-scrollbar="true" data-height="100%" data-init="true">
                <div class="menu">
                    <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Menus\\SidebarMenu', []); ?>
                </div>
            </div>
        </div>

        <div id="content" class="app-content">
            <?php echo $this->renderSection('content') ?>
        </div>

        <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Footer', ["project" => $project, "company" => $company, 'versionBase' => $versionBase, 'versionProject' => $versionProject]) ?>
    </div>
    <?php include('./vendor/Devolegkosarev/dashboard/src/Views/Partials/Scripts/Javascript.php'); ?>

</body>

</html>