<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Base'); ?>
<?php echo $this->section('content'); ?>
<div id="sidebar" class="app-sidebar">
    <div class="app-sidebar-content ps ps--active-y" data-scrollbar="true" data-height="100%" data-init="true">
        <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Menus\\SidebarMenu', ['menu' => $menu]); ?>
    </div>
</div>
<div id="content" class="app-content">
    <!-- <div class="container"> -->
    <?php if (!empty($breadcrumbs) and !empty($pageHeader) and is_array($pageHeader)) : ?>
        <div class="d-flex align-items-center mb-3">
            <div>
                <?php if (!empty($breadcrumbs)) : ?>
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $item) : ?>
                            <?php if (isset($item['link'])) : ?>
                                <li class="breadcrumb-item"><a href="<?= $item['link'] ?>">
                                        <?= $item['label'] ?>
                                    </a></li>
                            <?php else : ?>
                                <li class="breadcrumb-item active">
                                    <?= $item['label'] ?>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <?php if (!empty($pageHeader) and is_array($pageHeader)) : ?>
                    <h1 class="page-header">
                        <?php if (isset($pageHeader['title'])) : ?>
                            <?= $pageHeader['title'] ?>
                        <?php endif; ?>
                        <?php if (isset($pageHeader['description'])) : ?>
                            <small>
                                <?= $pageHeader['description'] ?>
                            </small>
                        <?php endif; ?>
                    </h1>
                <?php endif; ?>
            </div>
            <?php if (!empty($buttons) and is_array($buttons)) : ?>
                <div class="ms-auto">
                    <div class="btn-group">
                        <?php foreach ($buttons as $button) : ?>

                            <?php
                            $target = '_self';
                            if (isset($button['target'])) {
                                $target = $button['target'];
                            }

                            $class = 'btn-theme';
                            if (isset($button['class'])) {
                                $class = $button['class'];
                            }

                            $attributes = '';
                            if (isset($button['attributes'])) {
                                $attributes = $button['attributes'];
                            }

                            $icon = 'fa-question-circle';
                            if (isset($button['icon'])) {
                                $icon = $button['icon'];
                            }
                            ?>

                            <a href="<?= base_url($button['route']) ?>" class="btn <?= $class ?>" target="<?= $target ?>" <?= $attributes ?>>
                                <i class="<?= $icon ?> fa fa-fw me-1"></i> <?= $button['label'] ?>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <hr class="mb-4">
    <?php endif; ?>

    <?php echo view(viewOverview('Partials\\Flashdata')); ?>
    <?php echo $this->renderSection('content') ?>
    <!-- </div> -->
</div>
<?php echo $this->endSection(); ?>