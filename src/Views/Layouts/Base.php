<?php
$versionBase = $dashboardConfig->versionBase;
$versionProject = $dashboardConfig->versionProject;
$project = $dashboardConfig->project;
$company = $dashboardConfig->company;
$logoDark = $dashboardConfig->logoDark;
?>
<!DOCTYPE html>
<html lang="<?php echo  $currentLanguage; ?>" class="<?php echo $dashboardConfig->bgCover ?>">

<head>
    <?php include_once FCPATH .'vendor/devolegkosarev/dashboard/src/Views/Partials/Head/Head.php';  ?>
</head>

<body class="pace-top <?php echo $dashboardConfig->theme ?>">
    <div id="app" class="app <?php echo $pageClass ?>">
        <div id="header" class="app-header">

            <div class="desktop-toggler">
                <button type="button" class="menu-toggler" data-toggle-class="app-sidebar-collapsed" data-dismiss-class="app-sidebar-toggled" data-toggle-target=".app">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </button>
            </div>

            <div class="mobile-toggler">
                <button type="button" class="menu-toggler" data-toggle="app-top-nav-mobile">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </button>
            </div>

            <div class="mobile-toggler">
                <button type="button" class="menu-toggler" data-toggle-class="app-sidebar-mobile-toggled" data-toggle-target=".app">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </button>
            </div>

            <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Brand', ["project" => $project, "logoDark" => $logoDark]); ?>

            <div class="menu">
                <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Header\LanguageSwitcher', []); ?>
                <?php if ($email) : ?>
                    <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Header\Profile', ["email" => $email, "avatar" => $avatar]); ?>
                <?php endif; ?>
            </div>


        </div>

        <?php echo $this->renderSection('content'); ?>

        <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Footer', ["project" => $project, "company" => $company, 'versionBase' => $versionBase, 'versionProject' => $versionProject]) ?>
    </div>
    <?php include(FCPATH .'vendor/devolegkosarev/dashboard/src/Views/Partials/Scripts/Javascript.php'); ?>
</body>

</html>