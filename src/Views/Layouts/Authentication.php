<?php echo $this->extend('Devolegkosarev\\Dashboard\\Views\\Layouts\\Base'); ?>
<?php echo $this->section('content'); ?>
<div id="content" class="app-content">
    <div class="container">
        <?php echo view('Devolegkosarev\\Dashboard\\Views\\Partials\\Menus\\TopMenu') ?>
        <div class="login p-0 m-0" style="min-height: auto;">
            <div class=" login-content">
                <?php echo view(viewOverview('Partials\\Flashdata')); ?>
            </div>
        </div>
        <?php echo $this->renderSection('content') ?>
    </div>
</div>
<?php echo $this->endSection(); ?>